@extends('layouts.email')

@section('content')

<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#c0bac4;text-align:center;">Course Access</p>
<hr class="line-footer">


<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Hello {{$name}}</p>


<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Your request to access the {{$course}} course on your company academy has been approved. You may now enroll to the course</p>

    <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#1824a9;font-size:16px;font-weight: bold;">
      <a href="{{$url}}" class="btn-drk-left" style=" background: #1824a9;
      padding: 10px 15px;
      color: #fff;
      font-size: 18px;
      text-decoration: none;"> Access Course
      </a>
    </p>

<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#7C7C7C;text-align:center;">
<h href="https://academy.platcorpgroup.com/user/login">The Platcorp Group Academy</h>
</p>



@endsection
