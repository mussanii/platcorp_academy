@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#1824a9;text-align:center;">New Course Notification</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">

    Kindly note the following course(s) have been launched for your job role on {{ config('app.name', 'The Platcorp Group Academy') }}.</p>

    <ul style="font-family: poppins, sans-serif;font-size: 18px;">
      
        @foreach ($course_name as $name )

         <li> {{ $name }}</li>
            
        @endforeach

    </ul>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
      In order to access the course(s) please login to the platform.</p>

      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#1824a9;font-size:16px;font-weight: bold;">
        <a href="{{$url}}" class="btn-drk-left" style=" background: #1824a9;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Access Course
        </a>
      </p>
  
    
      <p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#1824a9; text-align:center;">
<h href="https://academy.platcorpgroup.com/user/login">The Platcorp Group Academy</h>
</p>



@endsection
