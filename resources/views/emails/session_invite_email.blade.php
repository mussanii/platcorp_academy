@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#1C3761;text-align:center;"> New Live Session Alert</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($data['name'])) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
    A new Live session titled "{{$data['title']}}" has been created to {{ config('app.name') }} platform by {{$data['creator']}}.</p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
      In order to view the details of the session, please login to the platform then click on "Login" button below .</p>
  
      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#23245F;font-size:16px;font-weight: bold;">
        <a href="{{$data['url']}}" class="btn-drk-left" style=" background:#1C3761;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Login
        </a>
      </p>
 

      

      <p style="font-family: poppins, sans-serif;margin: 30px 0 30px; text-align:center;color:#fff;font-size:16px;font-weight: bold;">
        <a href="{{$data['decline_url']}}" class="btn-drk-left" style=" background: #EC6669;
        padding: 10px 15px;
        color: #fff;
        font-size: 18px;
        text-decoration: none;"> Live session Link
        </a>
      </p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>


<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#1C3761;text-align:center;">
<h href="https://academy.platcorpgroup.com/user/login">The Platcorp Group Academy</h>
</p>


@endsection
