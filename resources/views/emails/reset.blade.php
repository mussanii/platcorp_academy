@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#404040;text-align:center;">User Verification</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
  We have received a request to send for your account credentials on the {{ config('app.name', '4G Capital') }} mobile App.</p>

  <p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Your username on the App is: <span color="#ff6c0d">{{$username}}</span></p>

  <p style="font-family: Helvetica Neue, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">Your Password on the App is: <span color="#ff6c0d">{{$password}}</span></p>


  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
    Kindly note that these are credentials for the mobile app ONLY. The website still uses Google single sign on.
   </p>


  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#404040;text-align:center;">
 Kindly ignore this email incase you did not make this request.
</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#404040;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#404040;text-align:center;">
  <h href="https://academy.platcorpgroup.com/user/login">The Platcorp Group Academy</h>
</p>


@endsection
