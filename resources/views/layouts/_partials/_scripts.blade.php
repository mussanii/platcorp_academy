
<script src="{{ asset('emojis/lib/js/nanoscroller.min.js') }}"></script>
<script src="{{ asset('emojis/lib/js/tether.min.js') }}"></script>
<script src="{{ asset('emojis/lib/js/config.js') }}"></script>
<script src="{{ asset('emojis/lib/js/util.js') }}"></script>
<script src="{{ asset('emojis/lib/js/jquery.emojiarea.js') }}"></script>
<script src="{{ asset('emojis/lib/js/emoji-picker.js') }}"></script>
<script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "kyz0QDqj5Z");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script><noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
@yield('js')
{{-- @stack('scripts') --}}