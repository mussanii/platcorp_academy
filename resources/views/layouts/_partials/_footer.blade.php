<footer>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                <div class="row">
                   
                        <div class="col-md-11 ">
                            <div class="footer-text copyright">
                                <ul>
                                    <li><small>Copyright &copy; <?php echo date('Y'); ?> {{ config('app.name', 'Laravel') }} | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                                </ul>
                            </div>
                        </div>
                  
                   
                    
                </div>


            </div>


        </div>
    </div>

</footer>