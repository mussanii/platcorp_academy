<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm sticky-top">
    <div class="container-fluid ps-5 pe-0">
        <a class="navbar-brand" href="{{ url('/') }}">
            <i class="logo-container logo"></i>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" id="navbarSupportedHumberger"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->
                @guest
                    @if (count(notAuthenticatedMenu()) > 0)
                        @foreach (notAuthenticatedMenu() as $menu)
                            <li class="nav-item non-auth">
                                @if (Request::segment(1) == $menu['key'] || strpos(\Route::current()->getName(), $menu['key']) !== false)
                                    <a class="nav-link active" href="{{ route('noneAuthPages', ['key' => $menu['key']]) }}">
                                        {{ $menu['title'] }}</a>
                                @else
                                    <a class="nav-link" href="{{ route('noneAuthPages', ['key' => $menu['key']]) }}">
                                        {{ $menu['title'] }}</a>
                                @endif
                            </li>
                        @endforeach
                    @endif
                    @if (Route::has('login'))
                        @if (Route::current()->getName() === 'login')
                            <button type="button" class="btn btn-overall btn_border_primary me-4"
                                onclick="location.href='{{ route('user.login') }}'">Sign in </button>
                        @else
                            <button type="button" class="btn btn-overall btn_solid_primary me-4"
                                onclick="location.href='{{ route('user.login') }}'">Sign in </button>
                        @endif
                    @endif
                @else
                    @if (count(mainMenu()) > 0)
                        @foreach (mainMenu() as $menu)
                            <li class="nav-item main-menu">
                                @if (Request::segment(1) == $menu['key'] || strpos(\Route::current()->getName(), $menu['key']) !== false)
                                    <a class="nav-link active" href="{{ route('pages', ['key' => $menu['key']]) }}">
                                        {{ $menu['title'] }}</a>
                                @else
                                    <a class="nav-link" href="{{ route('pages', ['key' => $menu['key']]) }}">
                                        {{ $menu['title'] }}</a>
                                @endif
                            </li>
                        @endforeach
                    @endif


                    <!-- notification bell -->

                    @php $notify= array(); @endphp
                    @foreach (auth()->user()->unreadNotifications as $notification)
                        @if ($notification->data['type'] == 4 || $notification->data['type'] == 5 || $notification->data['type'] == 6 || $notification->data['type'] == 7)
                            @php $notify[] = $notification; @endphp
                        @endif
                    @endforeach
                    <li class="nav-item dropdown no-arrow  notification">
                        @if ($notify)
                            <a style="padding-top: 8px;" class="nav-link " href="#" id="alertsDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                title="Your notifications will show here">

                                <span class="header-icon icon-notifications"></span>


                                <!-- Counter - Alerts -->

                                <span class="badge badge-danger badge-counter">{{ count($notify) }}</span>

                            </a>
                        @else
                            <a style="padding-top: 8px;" class="nav-link " href="#" id="alertsDropdown">

                                <span class="header-icon icon-notifications"></span>


                                <!-- Counter - Alerts -->

                                <span class="badge badge-danger badge-counter"></span>
                            </a>
                        @endif
                        @if ($notify)
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in notifications-menu"
                                aria-labelledby="alertsDropdown" id="noti-drop">
                                <span class="dropdown-menu-arrow"></span>
                                <h6 class="dropdown-header">
                                    Notifications
                                </h6>
                                @foreach ($notify as $notification)
                                    @if ($notification->data['type'] == 1)
                                        <a class="dropdown-item wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                            </div>
                                            <div class="inner-notification mt-1">
                                                
                                                {!! $notification->data['data'] !!}
                                            </div>
                                        </a>
                                    @elseif($notification->data['type'] == 4)
                                        <a class="dropdown-item wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                            </div>
                                            <div class="inner-notification mt-1">
                                                
                                                {!! $notification->data['data'] !!}
                                            </div>
                                        </a>
                                    @elseif($notification->data['type'] == 5)
                                        <a class="dropdown-item wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                            </div>
                                            <div class="inner-notification mt-1">
                                              
                                                {!! $notification->data['data'] !!}
                                                   
                                            </div>
                                        </a>

                                    @elseif($notification->data['type'] == 6)
                                        <a class="dropdown-item wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                            </div>
                                            <div class="inner-notification mt-1">
                                              
                                                {!! $notification->data['data'] !!}
                                                   
                                            </div>
                                        </a>

                                        @elseif($notification->data['type'] == 7)
                                        <a class="dropdown-item wrap"
                                            href="{{ route('single.notification', [$notification->id]) }}"
                                            title="Click to View">
                                            <div class="mr-3">
                                                <div class="small text-gray-500">
                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                </div>
                                            </div>
                                            <div class="inner-notification mt-1">
                                              
                                                {!! $notification->data['data'] !!}
                                                   
                                            </div>
                                        </a>
                                    @endif
                                @endforeach

                                {{-- <a class="dropdown-item text-center small text-gray-500"
                                    href="{{ route('markAsRead') }}">Mark all as read</a> --}}
                            </div>
                        @else
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown" id="noti-drop">

                                <span class="dropdown-menu-arrow"></span>
                                <h6 class="dropdown-header">
                                    No new notification
                                </h6>
                            </div>
                        @endif

                    </li>

                    <!-- end notification bell -->

                    <!-- Profile Section -->

                    <li class="nav-item dropdown profile-dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (Auth::user()->profile_pic != null)
                                @php $ppic = Auth::user()->profile_pic; @endphp
                            @else
                                @php $ppic = 'images.jpg'; @endphp
                            @endif

                            <img src="{{ asset('uploads/' . $ppic) }}" alt=""> &nbsp;
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end profile-menu" aria-labelledby="navbarDropdown" id="profile-menu-link">
                            <span class="dropdown-menu-arrow"></span>
                            @if (count(sideMenu()) > 0)
                                @foreach (sideMenu() as $menu)
                                    <a class="dropdown-item" href="{{ route($menu['key']) }}"> {{ $menu['title'] }}</a>
                                @endforeach
                            @endif
                            
                            <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>

                            @if ((Auth::user()->is_admin && Auth::user()->role === 'ADMIN') ||
                                (Auth::user()->is_admin && Auth::user()->role == 'SUPERADMIN'))
                                <a class="dropdown-item" href="/admin" >Admin Section</a>
                            @endif

                            @if (Auth::user()->is_admin && Auth::user()->role === 'GROUPHR')
                                <a class="dropdown-item " href="/admin" > Group HR Dashboard</a>
                            @endif

                            @if (Auth::user()->is_company_admin && Auth::user()->role === 'COMPANYHR')
                            <a class="dropdown-item " href="/admin" > Company HR Dashboard</a>
                            @endif

                            @if (Auth::user()->is_company_admin && Auth::user()->role === 'BRANCHMANAGER')
                                <a class="dropdown-item" href="{{ route('admin.branchReport.completion') }}" >Branch
                                    Reports</a>
                            @endif

                            @if (Auth::user()->is_company_admin && Auth::user()->role === 'HOD')
                                <a class="dropdown-item "
                                    href="{{ route('admin.departmentReport.completion') }}" >Department Reports</a>
                            @endif

                           

                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
