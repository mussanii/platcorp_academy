@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.survey.surverys.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.survey.surverys.index') }}" class="">
                {{ __('Surveys') }}
            </a>
        </li>

        <li class="nav__item {{$activeRoute == 'admin.userSurvey.responses' ? 's--on' : ''}}">
            <a href="{{ route('admin.userSurvey.responses') }}">
                {{ __('User Responses') }}
            </a>
        </li>

    </ul>
	</div>
</nav>