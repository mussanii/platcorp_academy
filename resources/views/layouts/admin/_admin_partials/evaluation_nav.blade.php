@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.evaluation.userEvaluations.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.evaluation.userEvaluations.index') }}">
                {{ __('User Evaluations') }}
            </a>
        </li>

		<li class="nav__item {{$activeRoute == 'admin.evaluation.evaluations.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.evaluation.evaluations.index') }}">
                {{ __('Evaluation Questions') }}
            </a>
        </li>
        
        <li class="nav__item {{$activeRoute == 'admin.evaluation.evaluationOptions.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.evaluation.evaluationOptions.index') }}">
                {{ __('Evaluation Options') }}
            </a>
        </li>

    </ul>
	</div>
</nav>
