@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.users.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.users.index') }}" class="">
                {{ __('Users') }}
            </a>
        </li>

       

    </ul>
	</div>
</nav>