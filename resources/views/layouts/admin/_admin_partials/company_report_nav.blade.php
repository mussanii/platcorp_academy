@php
	$activeRoute = Route::currentRouteName();
@endphp
<nav class="nav">
	<div class="container">
    <ul class="nav__list">
		<li class="nav__item {{$activeRoute == 'admin.companygraph.dashboard' ? 's--on' : ''}}">
            <a href="{{ route('admin.companygraph.dashboard') }}" class="">
                {{ __('Dashboard') }}
            </a>
        </li>
		<li class="nav__item {{$activeRoute == 'admin.companyReport.completion' ? 's--on' : ''}}">
            <a href="{{ route('admin.companyReport.completion') }}">
                {{ __('Course Completion') }}
            </a>
        </li>

        {{-- <li class="nav__item {{$activeRoute == 'admin.branch.completion' ? 's--on' : ''}}">
            <a href="{{ route('admin.branch.completion') }}">
                {{ __('Branch Completion') }}
            </a>
        </li>

		<li class="nav__item {{$activeRoute == 'admin.reports.userLicenses.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.reports.userLicenses.index') }}">
                {{ __('Course Enrollments') }}
            </a>
        </li>
        
        <li class="nav__item {{$activeRoute == 'admin.reports.courseReflections.index' ? 's--on' : ''}}">
            <a href="{{ route('admin.reports.courseReflections.index') }}">
                {{ __('Course Reflections') }}
            </a>
        </li> --}}

    </ul>
	</div>
</nav>