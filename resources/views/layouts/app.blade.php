
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
    

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
   
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>

    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- company specific branding css -->
    <link href="{{ companyBranding() }}" rel="stylesheet">

</head>

<body oncontextmenu="return false;">
    <div id="app">
        @include('layouts._partials._header')



        <main>
            @yield('content')
        </main>


        @include('site.includes.modals.courseerrors')
        @include('site.includes.modals.coursemessages')
        @include('site.includes.modals.notification')
        @include('site.includes.modals.ticket')
        @include('site.includes.modals.tour')
        @if (Auth::user())
            @include('site.includes.modals.jobRoleModal')
            @include('site.includes.modals.divisionModal')
        @endif

    </div>
    @include('layouts._partials._footer')
    <!-- script -->

    @include('layouts._partials._scripts')

    @if(Auth::check())
      @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
     @endif
     @endif
    
    <a class="btn btn-overall btn_solid_secondary btn-support" title="Support" href="https://farwell-consultants.com/farwellticket/farwellticket/index.php?category=18&a=add"  target="_blank"><i class="support-icon" ></i>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Support </a>

    @if (Session::has('course_success'))
    <script>
        jQuery(document).ready(function($) {
             
            $('#CourseSuccess').modal('show');
            
        });
    </script>
    @elseif(Session::has('course_errors'))
    <script>
        jQuery(document).ready(function($) {
            $('#CourseErrors').modal('show');
           
        });
    </script>

   @elseif(Session::has('notification_success'))
   <script>
    jQuery(document).ready(function($) {
        $('#notificationModal').modal('show');  
    });
    </script>



    @endif


    <script>
        $(document).ready(function() {
            // Close modal on button click
            $(".notification-dismiss").click(function() {
                $("#notificationModal").modal('hide');
            });

        });
    </script>


@if(!Auth::check())
<script>
  jQuery(document).ready(function($) {
 function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

was_reloaded = getCookie('was_reloaded')
console.log(was_reloaded)
if (was_reloaded != 'yes') {
        document.cookie = "was_reloaded=yes; path=/; max-age=3600;"
    location.reload();
}
});
</script>

@endif

<script>
    PreventIllegalKeyPress = function (e) {
  if (e.target) t = e.target; //Firefox and others
  else if (e.srcElement) t = e.srcElement; //IE
  
  if (e.keyCode == 116) { //prevent F5 for refresh
      e.preventDefault();
  }
  if (e.keyCode == 123) { //prevent F12 for inspect
      e.preventDefault();
  }
  if (e.keyCode == 122) { //F11 leave fullscreen
      e.preventDefault();
  } else if (e.altKey && e.keyCode == 115) { //Alt + F4
      e.preventDefault();
  } else if (e.altKey && e.keyCode == 37) { //Alt + left
      e.preventDefault();
  } else if (e.altKey && e.keyCode == 39) { //Alt + right
      e.preventDefault();
  } else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
      e.preventDefault();
  }
  };
  
  
  $(document).keydown(function (e) {
  
  PreventIllegalKeyPress(e);
  
  });
  </script>

    <!-- end of scripts -->
</body>

</html>
