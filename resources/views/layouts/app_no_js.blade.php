
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

      <!-- Scripts -->

      <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
      <!-- Fonts -->
  
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
  
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>

    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">
     
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- company specific branding css -->
    <link href="{{ companyBranding() }}" rel="stylesheet">
   
      
    @yield('css')
</head>

<body oncontextmenu="return false;">
    <div id="app">

           @include('layouts._partials._header')
  
    @if (Session::has('course_success'))
    <script>
        jQuery(document).ready(function($) {
             
            $('#CourseSuccess').modal('show');
            
        });
    </script>
    @elseif(Session::has('course_errors'))
    <script>
        jQuery(document).ready(function($) {
            $('#CourseErrors').modal('show');
           
        });
    </script>

   @elseif(Session::has('notification_success'))
   <script>
    jQuery(document).ready(function($) {
        $('#notificationModal').modal('show');  
    });
    </script>
   
     @endif

    
        
         <main>
        @yield('content')
         </main>
        <!-- Back to top button -->
   @include('site.includes.modals.courseerrors')
    @include('site.includes.modals.coursemessages')
    @include('site.includes.modals.notification')
    @include('site.includes.modals.tour')

      @include('layouts._partials._footer')

    </div>
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    
    @include('layouts._partials._scripts')

    @if(Auth::check())
      @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <a id="tour_videos" data-toggle="modal" data-target="#tourModal"><span>Learn to navigate this site</span></a>
     @endif
     @endif

    <a class="btn btn-overall btn_solid_secondary btn-support" title="Support" href="https://farwell-consultants.com/farwellticket/farwellticket/index.php?category=18&a=add"  target="_blank"><i class="support-icon" ></i>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Support </a>

    <script>
        function myFunction() {
            var elmnt = document.getElementById("home-scroll");
            elmnt.scrollIntoView();
        }


        $(function() {

            $('#navbarSupportedContent').on('shown.bs.collapse', function() {
                    $('.navbar-toggler-icon').addClass('hidden');
                    $('#navbar-close').removeClass('hidden');
                })
                .on('hidden.bs.collapse', function() {
                    $('.navbar-hamburger').removeClass('hidden');
                    $('#navbar-close').addClass('hidden');
                });

        });
    </script>


<script>
    jQuery(document).ready(function($) {
    $("#navbarSupportedHumberger").on('click', function(e){
      e.preventDefault();

       if( $('#navbarSupportedContent').hasClass('show')){

        $("#navbarSupportedContent").removeClass('show');
       }else{
        $("#navbarSupportedContent").addClass('show');
       }
     
    //  $("#profile-menu-link").addClass('show');
     
    })
  
    });
      
  </script>


  <script>
   jQuery(document).ready(function($) {
  $("#navbarDropdown").on('click', function(e){

    e.preventDefault();
    if( $('#profile-menu-link').hasClass('show')){

        $("#profile-menu-link").removeClass('show');
    }else{
        $("#profile-menu-link").addClass('show');
    }
  
   
  })

  });

</script>

<script>
    jQuery(document).ready(function($) {
  $("#alertsDropdown").on('click', function(e){
    e.preventDefault();

    console.log('yes');

    if( $('#noti-drop').hasClass('show')){

    $('#noti-drop').removeClass('show');
    }else{
        $('#noti-drop').addClass('show');
    }

  });
});

</script>

<script>
    $(document).ready(function() {
        // Close modal on button click
        $(".notification-dismiss").click(function() {
            $("#notificationModal").modal('hide');
        });

    });
</script>

@if(!Auth::check())
<script>
  jQuery(document).ready(function($) {
 function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

was_reloaded = getCookie('was_reloaded')
console.log(was_reloaded)
if (was_reloaded != 'yes') {
        document.cookie = "was_reloaded=yes; path=/; max-age=3600;"
    location.reload();
} 
});
</script> 
@endif

<script>
  PreventIllegalKeyPress = function (e) {
if (e.target) t = e.target; //Firefox and others
else if (e.srcElement) t = e.srcElement; //IE

if (e.keyCode == 116) { //prevent F5 for refresh
    e.preventDefault();
}
if (e.keyCode == 123) { //prevent F12 for inspect
    e.preventDefault();
}
if (e.keyCode == 122) { //F11 leave fullscreen
    e.preventDefault();
} else if (e.altKey && e.keyCode == 115) { //Alt + F4
    e.preventDefault();
} else if (e.altKey && e.keyCode == 37) { //Alt + left
    e.preventDefault();
} else if (e.altKey && e.keyCode == 39) { //Alt + right
    e.preventDefault();
} else if (e.ctrlKey && e.keyCode == 82) { //Ctrl + R (reload)
    e.preventDefault();
}
};


$(document).keydown(function (e) {

PreventIllegalKeyPress(e);

});
</script>


<script src="{{asset('maps/highmaps.js')}}"></script>
<script src="{{asset('maps/africa.js')}}"></script>

<script>
var width = $(window).width();
var height = $(window).height();

        var data = [

            @foreach($data as  $key=>$value)
            ['{{$value['code']}}','{{json_encode($value, TRUE)}}'],
            @endforeach
        ];
       var data2 = [...data];
       
     
        // tooltips create...generate
        $(document).ready(function () {

            console.log('yes');
          var generate_tooltips = function () {

            console.log(this.point);

            var bodies = JSON.parse(this.point.value.replace(/&quot;/g,'"'));
          
            var common_name = bodies.common_name;
            var users = bodies.users;
            var certified = bodies.certified;
            var country_flag = bodies.country_flag;
           // alert(this.point.trans);
    
              var mintype=" ";    
              
              if(common_name === 'Côte d&#039;Ivoire'){
                common_name = "COTE D IVOIRE";
              }

                var flag ="<img src='{{ url('/images/flags') }}"+"/" + country_flag + "' width='20px'/>" ;

                return '<div>'+ flag +' <span>' + common_name  +'</span>' + '<br/> Number of Staff Registered: '+ 
                users + ' <br/> Number of Staff Certified: '+ certified + '</div>'; 
            

            console.log(flag);
             
            
          };

            var maps = Highcharts.mapChart('map', {
                chart: {
                    map: 'custom/africa',
                    backgroundColor: "rgba(0,0,0,0)",
                    events: {
                        load: function() {
                            console.log('loaded')
                        }
                    }
                },

                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                mapNavigation: {
                    enabled: false,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },

                colorAxis: {
                    min: 0
                },
                tooltip: {
                  formatter: generate_tooltips,
                  backgroundColor: '#333333',
                  borderColor:'#333333',
                  useHTML: true,
                  style: {
                    color: '#ffffff',
                  }
                },


                series: [{
                    data: data.map(elem => {
                        elem.push('#FF6C0D');
                        //console.log(elem);
                        return elem;
                    }),
                    keys: ['hc-key', 'value', 'color'],
                    borderWidth: '1px',
                    borderColor: '#000000',
                    name: 'Random data',
                    states: {
                        hover: {
                            color: '#015488'
                        }
                    },
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}'
                    },
                    
                }],
                // plotOptions: {
                //     series: {
                //         point: {
                //             events: {
                             
                //                 click: function () {
                                
                //                    location.href = '{{ url("/our-progress") }}'+'/' + this.name;
                //                 }
                //             }
                //         }
                //     }
                //  },
                
                
            });
         
            $('.stats').on('mouseout', function(e) {

            $.each(data2, function(key, value) {
                value[2] = 'white';
            });
            maps.series[0].setData(data2);
            });


            $('#countries').on('mouseenter', function(e) {
                $.ajax({
                    url: '{{ url('/our-progress/countries') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#1924A8";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });

       


        $('#countries').on('mouseleave', function(e) {
                $.ajax({
                    url: '{{ url('/our-progress/countries') }}',
                    type: 'GET',
                    success: function(response) {
                        let list = response;
                        let localData = JSON.parse(JSON.stringify(data));

                        $.each(localData, function(key, value) {
                            if (list.includes(value[0])) {
                                value[2] = "#1924A8";
                            }
                        });
                       
                         maps.series[0].setData(localData);
                    }
                });
            });

        });
    </script>



</body>

</html>
