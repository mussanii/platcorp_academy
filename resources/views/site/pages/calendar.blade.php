
@php
use Carbon\Carbon;
use app\Models\JobRole;
$configLms = config()->get("settings.lms.live");
$jobRoles = JobRole::all(); 
$user = Auth::user()->id;
$userRole =Auth::user()->job_role_id;
$search = request()->query('search');
$category = request()->query('category');

if(Auth::user()->company_id == 3 || Auth::user()->company_id == 2){

  $weekly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','courses.company_id','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
            
              
              $query->whereBetween('courses.due_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })->where('courses.company_id', Auth::user()->company_id)
              ->distinct()
              ->paginate(5);

           
            


$monthly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','courses.company_id','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
            
              $query->whereBetween('due_date', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
              
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })->where('courses.company_id', Auth::user()->company_id)
              ->distinct()
              ->paginate(5);



$quarterly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','courses.company_id','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
              $query->whereBetween('due_date', ['due_date', Carbon::now()->addMonths(3)]);

              
           
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })->where('courses.company_id', Auth::user()->company_id)
              ->distinct()
              ->paginate(5);





}else{
  $weekly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
            
              
              $query->whereBetween('courses.due_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })
              ->distinct()
              ->paginate(5);

           
            


$monthly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
            
              $query->whereBetween('due_date', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()]);
              
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })
              ->distinct()
              ->paginate(5);



$quarterly = DB::table('courses')
            ->leftJoin('course_completions', function($join) use ($user) {
                $join->on('courses.id', '=', 'course_completions.course_id')
                     ->where('course_completions.user_id', $user);
                  
            })
            ->join('user_licenses', function($join) use ($user) {
                $join->on('courses.id', '=', 'user_licenses.course_id')
                     ->where('user_licenses.user_id', $user);
            })
            ->select('courses.id','courses.course_id','courses.short_name','courses.name','courses.due_date','course_completions.completion_date','course_completions.score as score','user_licenses.course_id as licensed_course','course_completions.user_id as licensed_user','user_licenses.user_id as luser')
             
            ->where(function($query) use ($search, $category) {
              $query->whereBetween('due_date', ['due_date', Carbon::now()->addMonths(3)]);

              
           
                if ($search) {
                  $query->where('courses.name', 'LIKE', "%{$search}%");
                }
                if($category) {
                    $query->where('course_completions.job_role_id', $category);
                  }
              })
              ->distinct()
              ->paginate(5);



}

     
@endphp
@extends('layouts.app')

@section('content')
@include('site.includes.components.parallax', [
  'banner' => 'enrolled_banner',
  'text' => $pageItem->header_title,
  'link_1' => 'home',
  'link_3' => $pageItem->header_title,
])





@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-5 mb-5 col-md-9">
@include('site.includes.components.calendar_filter',['jobRoles' =>$jobRoles, 'key'=>$pageItem->key,  ['search' => $search]])
<div class="accordion accordion__item faq-item "  id="accordionExample" >
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
       <h5 class="calendar-title"><strong>Courses due this week</strong></h5>
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse accordion-item__desc collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <table class="table table-borderless">
        <thead>
        <tr class="table-title ps-2">
        <th scope="col">Courses</th>
      <th scope="col">Due date </th>
      <th scope="col">Status</th>
      <th scope="col">Awards</th>
       </tr>
        </thead>
       
        <tbody>
        @if(count($weekly) > 0)
        @foreach($weekly as $key=> $course)  
       <tr>   
     <th scope="row"  class="session-calendar_title"> <a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></th>
     <td class="session-calendar-content">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}}</td>
     <td>
     @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In progress </a>
    @endif
</td>


    
        <td>
        @if(!empty($course->completion_date) && ($course->score >= 0.80))
        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_solid_secondary disabled"> Download badge</a>
        @endif
        </td>
        </tr>
        @endforeach
        
        <tr>
          @else
          <tr>
          <td colspan="4"><p class="text-center no-courses" > There are no courses due this week</p></td>
          </tr>
        @endif
          
      </tr>
        </tbody>
       
        

    </table>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button accordion__item-button white-bg" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <h5 class="calendar-title"><strong>Courses due this month</strong></h5>
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
      <table class="table table-borderless">
        <thead>
        <tr class="table-title ps-2">
      <th scope="col">Courses</th>
      <th scope="col">Due date </th>
      <th scope="col">Status</th>
      <th scope="col">Awards</th>
       </tr>
        </thead>
       
        <tbody>
    
        @if(count($monthly) > 0)
        @foreach($monthly as $key=> $course)  
        <tr>   
     <th scope="row"  class="session-calendar_title"> <a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></th>
     <td class="session-calendar-content">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}} </td>
   
     <td>
     @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In Progress </a>
    @endif
</td>

        <td>
        @if(!empty($course->completion_date) && $course->score >= 0.80)
        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_border_secondary disabled"> Download badge</a>
        @endif
        </td>
        </tr>
        @endforeach
        <tr>
          @else
          <tr>
          <td colspan="4"><p class="text-center no-courses" > There are no courses due this month</p></td>
          </tr>
        @endif
          <td>
        <div class="d-flex justify-content-center ">
        {!! $monthly->links() !!}
        </td>
      </tr>
        </tbody>
        
    </table>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button accordion__item-button white-bg collapsed calendar-title" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
 <h5 class="calendar-title"><strong>Courses due this quarter</strong></h5>
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
      <table class="table table-borderless table-responsive">
        <thead>
        <tr class="table-title">
        <th scope="col">Courses</th>
      <th scope="col">Due date </th>
      <th scope="col">Status</th>
      <th scope="col">Awards</th>
       </tr>
        </thead>
        
        <tbody>
          @if(count($quarterly) > 0)
        @foreach($quarterly as $key=> $course)  
        <tr>   
     <th scope="row"  class="session-calendar_title"> <a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></th>
      <td class="session-calendar-content">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}}</td>
   
      <td>
      @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In progress </a>
    @endif
</td>
        <td>
        @if(!empty($course->completion_date) && $course->score >= 0.80)
        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_border_secondary disabled"> Download badge</a>
        @endif
        </td>
        </tr>
        @endforeach
        <tr>
          @else
          <tr>
          <td colspan="4"><p class="text-center no-courses" > There are no courses  due this quarter</p></td>
          </tr>
        @endif
          <td>
        <div class="d-flex justify-content-center ">
        {!! $quarterly->links() !!}
        </td>
      </tr>
        </tbody>
   
        

    </table>
      </div>
    </div>
  
</div>

</div>
@else
<div class="container-fluid  mb-5 col-md-11">
@include('site.includes.components.calendar_filter',['jobRoles' =>$jobRoles, 'key'=>$pageItem->key,  ['search' => $search]])

<div class="accordion accordion__item faq-item"  id="accordionExample" >
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
       <strong class="calendar-title">Courses due this week</strong>
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse accordion-item__desc collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      @if(count($weekly) > 0)
        @foreach($weekly as $key=> $course)
        <div class="card">
  <div class="card-body">

        <dl class="row">
    <dt class="col-sm-3">Courses</dt>
    <dd class="col-sm-9"><a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></dd>
    <dt class="col-sm-3 text-truncate">Due date</dt>
    <dd class="col-sm-9">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}}</dd>
    <dt class="col-sm-3">Status</dt>
    <dd class="col-sm-9">
  
    
    @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In progress </a>
    @endif

    </dd>

    <dt class="col-sm-3">Awards</dt>
    <dd class="col-sm-9">
    @if(!empty($course->completion_date) && ($course->score >= 0.80))        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_border_secondary disabled"> Download badge</a>
        @endif
    </dd>
   
    </dl>
  </div>
        </div>
        @endforeach
        @else
        {!! $weekly->links() !!}
        @endif

      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button accordion__item-button white-bg" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <strong class="calendar-title">Courses due this month</strong>
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">

      @if(count($monthly) > 0)
        @foreach($monthly as $key=> $course) 
        <div class="card">
  <div class="card-body">
        <dl class="row">
    <dt class="col-sm-3">Courses</dt>
    <dd class="col-sm-9 session-calendar_title"><a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></dd>
    <dt class="col-sm-3 text-truncate">Due date</dt>
    <dd class="col-sm-9">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}}</dd>
    <dt class="col-sm-3">Status</dt>
    <dd class="col-sm-9">
    @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In progress </a>
    @endif

    </dd>

    <dt class="col-sm-3">Awards</dt>
    <dd class="col-sm-9">
    @if(!empty($course->completion_date) && ($course->score >= 0.80))
        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_border_secondary disabled"> Download badge</a>
        @endif
    </dd>
   
    </dl>
  </div>
        </div>
        @endforeach
        @else
        {!! $monthly->links() !!}
        @endif
      
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
 <strong class="calendar-title">Courses due this quarter</strong>
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">

      @if(count($quarterly) > 0)
        @foreach($quarterly as $key=> $course) 
        <div class="card">
  <div class="card-body">
        <dl class="row">
    <dt class="col-sm-3">Courses</dt>
    <dd class="col-sm-9 session-calendar_title"><a href="{{ route('course.detail', ['key' => $course->short_name]) }}" target="_blank" style="text-decoration:none;">{{$course->name}}</a></dd>
    <dt class="col-sm-3 text-truncate">Due date</dt>
    <dd class="col-sm-9">{{ isset($course->due_date) ? Carbon::parse($course->due_date)->isoFormat(' MMMM Do YYYY '):'Not Available'}}</dd>
    <dt class="col-sm-3">Status</dt>
    <dd class="col-sm-9">
   
    @if(!empty($course->completion_date))
        @if($course->score >= 0.80)
            <a class="btn btn-overall btn_solid_primary disabled" href="#" >
               Completed
            </a>
        @else
            <a class="btn btn-overall btn_solid_primary " href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
            > Retake Course </a>
        @endif
    @elseif(!empty($course->enrollment_date))
        <a class="btn btn-overall btn_solid_secondary" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
        > Continue with course </a>
    @else
        <a class="btn btn-overall btn_solid_secondary" href="{{ route('course.enroll', $course->course_id) }}/" target="_blank"
        >In progress </a>
    @endif

    </dd>

    <dt class="col-sm-3">Awards</dt>
    <dd class="col-sm-9">
    @if(!empty($course->completion_date) && ($course->score >= 0.80))
        
        <a href="{{ route('course.badge', $course->id)}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_secondary">Download badge</button></a>
        @else
        <a href="#" target="_blank" class="btn btn-overall btn_border_secondary disabled"> Download badge</a>
        @endif
    </dd>
   
    </dl>
  </div>
        </div>
        @endforeach
        @else
        {!! $quarterly->links() !!}
        @endif
      
      </div>
    </div>
  
</div>

</div>

@endif

@endsection



