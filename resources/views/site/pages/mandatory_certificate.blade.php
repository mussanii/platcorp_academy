<html>

<head>
    <style>

@font-face {
    font-family: 'Nunito';
    src: url('{{ storage_path("fonts/Nunito-Regular.ttf") }}') format("ttf");
    font-weight: normal;
    font-style: normal;
    font-variant: normal;
}

    @page {
        size: a4 landscape;
        margin: 0;
        padding: 0; // you can set margin and padding 0
    }

    body {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        margin: 0 !important;
        width: 100% !important;
        -webkit-text-size-adjust: 100% !important;
        -ms-text-size-adjust: 100% !important;
        -webkit-font-smoothing: antialiased !important;
        font-family: 'Nunito';

    }

    table {
        page-break-inside: auto;
        border: 0;
        font-family: 'Nunito';
    }

    tr {
        page-break-inside: avoid;
        page-break-after: auto;
        border: 0;
        font-family: 'Nunito';
    }

    th {
        border: 0;
        font: inherit;
        font-size: 100%;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #1D2F5D;
        font-family: 'Nunito';


    }

    .tableContent img {
        border: 0 !important;
        display: block !important;
        outline: none !important;
    }

    a {
        color: #382F2E;
    }

    a.blend {
        color: #fff;
    }

    p,
    h1,
    h2,
    ul,
    ol,
    li,
    div {
        margin: 10px;
        padding: 10px;
    }

    h1,
    h2 {
        font-weight: normal;
        background: transparent !important;
        border: none !important;
    }

    .header {
        margin-top: 4rem;
        margin-bottom: 2rem;
    }

    @media only screen and (max-width: 480px) {

        table[class="MainContainer"],
        td[class="cell"] {
            width: 100% !important;
            height: auto !important;
        }

        td[class="specbundle"] {
            width: 100% !important;
            float: left !important;
            font-size: 1.5em;
            line-height: 17px !important;
            display: block !important;
            padding-bottom: 15px !important;
        }

        td[class="specbundle2"] {
            width: 80% !important;
            float: left !important;
            font-size: 1.3em;
            line-height: 17px !important;
            display: block !important;
            padding-bottom: 10px !important;
            padding-left: 10% !important;
            padding-right: 10% !important;
        }

        td[class="spechide"] {
            display: none !important;
        }

        img[class="banner"] {
            width: 100% !important;
            height: auto !important;
        }

        td[class="left_pad"] {
            padding-left: 15px !important;
            padding-right: 15px !important;
        }

    }

    @media only screen and (max-width: 540px) {

        table[class="MainContainer"],
        td[class="cell"] {
            width: 100% !important;
            height: auto !important;
        }

        td[class="specbundle"] {
            width: 100% !important;
            float: left !important;
            font-size: 13px !important;
            line-height: 17px !important;
            display: block !important;
            padding-bottom: 15px !important;
        }

        td[class="specbundle2"] {
            width: 80% !important;
            float: left !important;
            font-size: 13px !important;
            line-height: 17px !important;
            display: block !important;
            padding-bottom: 10px !important;
            padding-left: 10% !important;
            padding-right: 10% !important;
        }

        td[class="spechide"] {
            display: none !important;
        }

        img[class="banner"] {
            width: 100% !important;
            height: auto !important;
        }

        td[class="left_pad"] {
            padding-left: 15px !important;
            padding-right: 15px !important;
        }

    }
    img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
        width: auto;
        /* max-width: 100%; */
        clear: both;
        display: block;
        float: none;
    }

    img.logo {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
        float: none;
        display: initial;
        padding: 0 20px;
    }


    img {
        min-width: 100px;
    }

    hr.class-1 {
    border-top: 0.5px solid #ced4da;
}

</style>
</head>


<body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; background-image: url('https://platcorp.farwell-consultants.com/images/certificateAssets/certificate_design.png');
    background-size:100% 100%;
    background-repeat: no-repeat;" offset="0" toppadding="0" leftpadding="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
    <tbody>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                           
                            <td style="text-align:center">

                                <img src="{{asset($logo)}}"style="margin-top:4rem; width: auto; max-height: 120px;"><br><br>
                            
                            </td>
                         
                        </tr>
                        <tr>
                            <tr>
                                <td style="text-align:center;">
                                    <span  style="font-size:28px;">CERTIFICATE OF COMPLETION</span><br><br>
                                    <span style="font-size:20px;">This certificate is proudly presented to </span><br>
                                   
                                </td>
                            </tr>
                        </tr>
                    </tbody>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td style="text-align:left; width:50%">

                                <p style="font-size:24px; height:auto; margin-left:30%;margin-bottom:0;color:{{$company->primary_color}}">Name: <b>{{$name}}</b></p>
                                
                            </td>
                            <td>
                                <p style="font-size:20px; height:auto; margin-bottom:0;">Job Role: <b>{{ucwords(strtolower($role))}}</b></p>
                                
                            </td>

                        </tr>
                    </tbody>
                </table>

                <hr class="class-1" style="margin-left:14%; margin-right:7%; ">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <tr>
                                <td style="text-align:center;">
                                    <span style="font-size:18px;">For completing the courses below</span><br><br>
                                   
                                </td>
                            </tr>
                        </tr>
                    </tbody>
                </table>


                <table width="100%" style="min-height:200px; height:auto;">
                    <tbody>
                        <tr>
                           
                                <?php
                                
                                echo "<td style='text-align:left;width:50%;'>";

                               foreach($module as $index => $record) {
                                if($index < 10){
                                    echo "<span style='font-size:18px;padding:5px 0;margin-left:30%; '>".$record . "</span><br>";
                                }else{
                                    echo "<span style='font-size:18px;padding:5px 0;'>".$record . "</span><br>";
                                }
                               
                                if(($index + 1) % 10 == 0) {
                                    echo "</td>";
                                    echo "<td style='text-align:left;width:50%;'>"; 
                                }
                                
                            }

                            echo "</td> <br><br>";
                                
                           ?>

                        </tr>
                    </tbody>
                </table>

                <hr class="class-1" style="margin-left:14%; margin-right:7%; ">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td style="text-align:left; width:50%">

                                <p style="font-size:20px; height:auto; margin-left:30%;margin-bottom:0;margin-top:1rem">
                                    {{ $date }}</p>
                                    <hr class="class-1" style="margin-left:28%; width:30%;"/>
                                <p style="margin-left:38%; line-height:1em;font-size:20px;">Date</p>
                            </td>
                            <td>

                                <hr class="class-1" style="margin-left:20%; width:30%;margin-top:12%">
                                <p style="margin-left:20%; line-height:1em;font-size:20px;">James Watson <br> HR Director</p>

                            </td>

                        </tr>
                    </tbody>
                </table>

            </td>
        </tr>
    </tbody>
</table>
<p style="line-height:1em;font-size:11px; position:absolute; bottom:60px;right:60px"> Serial Number: {{ $cert_number }} </p>
</body>
</html>
