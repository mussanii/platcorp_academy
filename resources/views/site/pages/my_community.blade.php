@extends('layouts.app')

@section('content')
    @include('site.includes.components.community_parallax', [
        'banner' => 'community_banner',
        'text' => 'My Community',
        'link_1' => 'home',
        'link_3' => 'My Community',
    ])

    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="container-fluid ps-5 pt-3">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                @include('site.includes.components.timeline')

            </div>
        </div>

        <div class="row no-gutters">


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid ps-5 pe-5">

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-9">
                                    <p class="sentence">In this section you will find all colleagues taking similar modules as yourself. 
                                        Follow them to stay up to date on their course milestones and. achievemnts throughout their training journey on this platform.</p>

                                </div>

                                <div class="col-3">

                                    @include('site.includes.components.community_requests', [
                                        'receive' => $receive,
                                    ])

                                </div>
                            </div>


                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p>
                                                        Your list of community users is currently empty.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                            <div class="row">
                                @php
                                    $count = 0;
                                    $today = Carbon\Carbon::today()->format('d-m-y');
                                    $configLms = config()->get('settings.lms.live');
                                @endphp
                                @foreach ($licenses as $license)
                                    @include('site.includes.components.my_community', [
                                        'license' => $license,
                                    ])
                                @endforeach
                            @endif
                        </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    @else
        <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
            @include('site.includes.components.timeline')

        </div>
        <div class="row no-gutters">


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid">

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-12">
                                    <p class="sentence">In this section you will find all colleagues taking similar modules as yourself. 
                                        Follow them to stay up to date on their course milestones and. achievemnts throughout their training journey on this platform</p>

                                </div>

                                <div class="col-12">

                                    @include('site.includes.components.community_requests', [
                                        'receive' => $receive,
                                    ])

                                </div>
                           
                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p>
                                                        Your list of community users is currently empty.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                                @php
                                    $count = 0;
                                    $today = Carbon\Carbon::today()->format('d-m-y');
                                    $configLms = config()->get('settings.lms.live');
                                @endphp
                                @foreach ($licenses as $license)
                                    @include('site.includes.components.my_community', [
                                        'license' => $license,
                                    ])
                                @endforeach
                            @endif

                        </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    @endif

@endsection
