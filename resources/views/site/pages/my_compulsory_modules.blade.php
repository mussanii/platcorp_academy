@extends('layouts.app')

@section('content')
@include('site.includes.components.enrolled_parallax', [
'banner' => 'enrolled_banner',
'text' => 'My Compulsory Modules',
'link_1' => 'home',
'link_3' => 'My Compulsory Modules',
])

@php
if (count(Auth::user()->compulsory) > 0) {
$value = (count(Auth::user()->completed) * 100) / count(Auth::user()->compulsory);
$string = sprintf('%.2f', $value) . '%';
} else {
$value = '';
$string = '';
}

@endphp
<course-progress class=" d-lg-flex float-end" compulsory="{{ count(Auth::user()->compulsory) }}" completed="{{ count(Auth::user()->completed) }}" size="Medium" value="{{ $value }}" string="{{ $string }}" position="inside" color="#ffffff" image="{{ asset('/images/icons/fire.jpg') }}" role="{{ Auth::user()->role }}"></course-progress>

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-5 pt-3">
    <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
        @include('site.includes.components.timeline')

    </div>
</div>
<div class="container-fluid ps-5 pt-3 mb-3">
    <div class="row no-gutters">
        <div class="col-12 mt-3 ms-5 me-5 mb-5">
            <div class="row alignment-class ">
                <div class="col-12">
                    <h5 class="progress-header"> Compulsory modules based on my job role</h5>

                    <ol class="compul">


                        @foreach ($required as $compulsory)
                        <li><a href="{{ route('course.detail', ['key' => $compulsory->courses->link_selected]) }}">{{ $compulsory->courses->name }}</a></li>
                        @endforeach
                    </ol>
                </div>
            </div>

            <div class="col-12 " style="margin-top:20px">
                <h5 class="progress-header"> My completed modules</h5>

                @php
                $configLms = config()->get('settings.lms.live');
                @endphp
                <table class="table-bordered completed-table table-responsive">

                    <thead>
                        <tr class="title ps-2  ">
                            <th>Course Title</th>
                            <th class="text-center">Enrollment Date</th>
                            <th class="text-center">Completion Date</th>
                            <th class="text-center">Score</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($completed as $key => $complete)
                        <tr>
                            <td>
                                <div class="sub-title  ps-1 "> <strong><a class="text-decoration-none" target="_blank" href="{{ $configLms['LMS_BASE'] . '/courses/' . $complete->courses->course_id . '/courseware' }}">{{ $complete->courses->name }}</a></strong>
                                </div>

                            </td>
                            <td>
                                <div class="sub-title ps-2 text-center"><i class='far fa-calendar-alt mx-2'></i>
                                    {{ Carbon\Carbon::parse($complete->enrollment_date)->isoFormat('Do MMMM YYYY') }}
                                </div>


                            </td>
                            <td>
                                <div class="sub-title ps-2 text-center"><i class='far fa-calendar-alt mx-2'></i>
                                    <!-- <i class="icon-calendar" title="clock"></i> -->
                                    {{Carbon\Carbon::parse($complete->completion_date)->isoFormat('Do MMMM YYYY') }}

                                </div>
                            </td>
                            <td>
                                <div class="sub-title ps-2 text-center"><i class="fa fa-check mx-2"></i>

                                    {{ round(($complete->score / $complete->max_score) * 100) }}%
                                </div>


                            </td>



                        </tr>


                        @endforeach
                    </tbody>

                </table>



            </div>

        </div>

    </div>
</div>
@else
<div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
    @include('site.includes.components.timeline')

</div>
<div class="container-fluid mb-3">
    <div class="row no-gutters">
        <div class="col-12 mt-3 me-2 ms-2">
            <div class="row alignment-class ">
                <div class="col-12">
                    <h5 class="progress-header"> Compulsory modules based on my job role</h5>

                    <ol class="compul">


                        @foreach ($required as $compulsory)
                        <li>{{ $compulsory->courses->name }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>

            <div class="col-12 " style="margin-top:20px">


                <h5 class="progress-header"> My completed modules</h5>
                @php
                $configLms = config()->get('settings.lms.live');
                @endphp
                <table class="table-bordered completed-table" cellspacing="5" cellpadding="5">

                    <thead>
                        <th class="complete-stats no-border-right">Course Title</th>
                        <th class="complete-stats">Enrollment Date</th>
                        <th class="complete-stats">Completion Date</th>
                        <th class="complete-stats">Score</th>
                    </thead>


                    @foreach ($completed as $key => $complete)
                    <tr>
                        <td>
                            <div class="complete-stats float-start"><a target="_blank" href="{{ $configLms['LMS_BASE'] . '/courses/' . $complete->courses->course_id . '/courseware' }}">{{ $complete->courses->name }}</a>
                            </div>

                        </td>
                        <td>
                            <p class="complete-stats">{{ Carbon\Carbon::parse($complete->enrollment_date)->isoFormat('Do MMMM YYYY') }}
                            </p>
                        </td>
                        <td>
                            <p class="complete-stats">{{ Carbon\Carbon::parse($complete->completion_date)->isoFormat('Do MMMM YYYY') }}
                            </p>
                        </td>
                        <td>

                            <p class="complete-stats">{{ round(($complete->score / $complete->max_score) * 100) }}%</p>

                        </td>

                    </tr>
                    @endforeach

                </table>
            </div>

        </div>

    </div>
</div>
@endif
@endsection