@extends('layouts.courses')
@php

$configLms = config()->get('settings.lms.live');

$details = [['id' => '1', 'name' => 'Description'], ['id' => '2', 'name' => 'Course Outline']];
@endphp
@section('content')
<div class="courses-page">
    @include('site.includes.components.parallax', [
    'banner' => 'course_banner',
    'text' => $course->name,
    'link_1' => 'home',
    'link_2' => 'courses',
    'link_3' => $course->name,
    ])




    <div class="container-fluid">
        <div class="row column-reverse">
            <div class="col-lg-3 col-md-3 col-sm-12 col-12 mt-4">
                @include('site.includes.components.course-action')


            </div>

            <div class="col-lg-9 col-md-9 col-sm-12 col-12 mt-3">

                <div class="row courses-detail-row">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                        <ul class="nav nav-pills course-tabs" role="tablist">
                            @foreach ($details as $key => $item)
                            @if ($key === array_key_first($details))
                            <li class="nav-item active">
                                <a class="nav-link first-tab" data-toggle="pill" href="#home">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#menu1">Course Outline</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div id="home" class="container-fluid tab-pane active">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="detail-card mt-3">
                                    <article>
                                        <div class="course-card-content">
                                            <div class="row-description">
                                                {!! $course->overview !!}
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="menu1" class="container-fluid tab-pane fade">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="detail-card mt-3">
                                    <article>
                                        <div class="course-card-content">
                                            <div class="row-description">
                                                {!! $course->more_info !!}
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




                <div class="row general-section no-margin-top ml-4">

                    @if ($course->course_video && $course->course_video != '[]')
                    <div class="video-wrapper">
                        <video controls playsinline muted id="bgvid">
                            <source src="{{ asset('videos/platcorp-videos/' . $course->course_video) }}" type="video/mp4">
                        </video>
                    </div>
                    @endif

                    <div>

                    </div>
                </div>









            </div>

        </div>
    </div>

</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('.show-table').removeClass('hidden-xs');

    });


    $(document).ready(function() {

        // Get current page URL
        var url = window.location.href;

        // remove # from URL
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

        // remove parameters from URL
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

        // select file name
        url = url.split('/')[4];

        // Loop all menu items
        $('.navbar-nav .nav-item a').each(function() {

            // select href
            var href = $(this).attr('href');

            link = href.split('/')[3];

            // Check filename
            if (link === 'courses') {

                // Add active class
                $(this).addClass('active');
            }
        });
    });
</script>
@endsection