@extends('layouts.app')

@section('content')
@include('site.includes.components.parallax',[
    'banner'=> 'course_banner',
    'text' => $pageItem->header_title,
    'link_1' => 'home',
    'link_3' => $pageItem->header_title,
])

@include('site.includes.components.course_filter',['jobRoles' =>$jobRoles, 'key'=>$pageItem->key])

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-5 pt-3">
    {!! $pageItem->renderBlocks(true,[], ['jobRole' => $type, 'courseCategories'=> $courseCategories, 'search' => $search]) !!}

</div>
@else
<div class="container-fluid pt-3">
    {!! $pageItem->renderBlocks(true,[], ['jobRole' => $type, 'courseCategories'=> $courseCategories, 'search' => $search]) !!}

</div>

@endif

@endsection


@section('js')

    @if (Auth::user())
        @if (Request::segment(1) === 'courses' && Auth::user()->initial_profile == 0)
            <script>
                $(document).ready(function() {
                    $('#changePass').modal('show');
                });
            </script>
    {{-- @elseif (Request::segment(1) === 'courses' && Auth::user()->checkComplete() == false)
        <script>
            $(document).ready(function() {
                $('#changeDivision').modal('show');
            });
        </script> --}}
    @endif
@endif
    
@endsection
