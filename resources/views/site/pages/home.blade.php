@extends('layouts.app')

@section('content')
    @if (!Auth::check())
        @include('site.includes.components.non_auth_home', ['pageItem' => $pageItem])
    @else
        @include('site.includes.components.auth_home', ['pageItem' => $pageItem, 'company' => $company])
    @endif
@endsection

@section('js')

    <script>
        $('.carousel.first-slider').carousel({
            interval: 10000
        })
    </script>

    @if (Auth::user())
        @if (Auth::user()->initial_profile == 0)
            <script>
                $(document).ready(function() {
                    $('#changePass').modal('show');
                });
            </script>
        @elseif (Auth::user()->checkComplete() == false)
            <script>
                $(document).ready(function() {
                    $('#changeDivision').modal('show');
                });
            </script>
        @endif
    @endif

@endsection
