@extends('layouts.app')

@section('content')
@include('site.includes.components.parallax',[
    'banner'=> 'sessions_banner',
    'text' => $pageItem->header_title,
    'link_1' => 'home',
    'link_3' => $pageItem->header_title,
])



@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-5">
    {!! $pageItem->renderBlocks(true,[], ['jobRole' => $type,  'search' => $search]) !!}

</div>
@else
<div class="container-fluid ">
    {!! $pageItem->renderBlocks(true,[], ['jobRole' => $type,  'search' => $search]) !!}

</div>

@endif



@endsection



