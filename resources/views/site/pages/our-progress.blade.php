@extends('layouts.app_no_js')
@php
use App\Models\Country;

$data = Country::mapData();

@endphp

@section('content')
@include('site.includes.components.parallax',[
    'banner'=> 'our_progress_banner',
    'text'=>$pageItem->header_title
])

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-3 ">
   
    {!! $pageItem->renderBlocks(true,[],['type'=>$type, 'search'=>$search]) !!}

</div>
@else 

   
    {!! $pageItem->renderBlocks(true,[],['type'=>$type, 'search'=>$search]) !!}


@endif

@endsection

@section('js')




   
@endsection