@extends('layouts.app')
@section('css')
    <style>
        .emoji-wysiwyg-editor::before {
            color: grey;
        }

        .emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
            content: attr(data-placeholder);
        }

        .emoji-wysiwyg-editor::before {
            content: 'Whats'' new?';
        }

        .main-message .emoji-wysiwyg-editor {
            /* height:150px; */
            /* border: 1px solid #00000029; */
        }

        .lead.emoji-picker-container {
            /* width: 300px; */
            margin-bottom: 0;
            display: block;

            input {
                width: 100%;
                height: 50px;
            }
        }

        .live {
            color: blue;
        }
    </style>
@endsection
@section('content')
    @include('site.includes.components.community_parallax', [
        'banner' => 'training_feed',
        'text' => 'Training Feed',
        'link_1' => 'home',
        'link_3' => 'Training Feed',
    ])

    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="container-fluid ps-5 pt-3">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                @include('site.includes.components.timeline')

            </div>
        </div>

        <div class="row no-gutters">


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid ps-2 pe-2 more-spacing">

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-12">
                                    <p class="sentence ms-5 ps-5"> Stay updated and inspired by the training milestones achieved by the colleagues you follow on the academy. 
                                        Congratulate them and give them Kudos for their achievements!</p>
                                </div>
                            </div>
                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p style="text-align:center">
                                                        Follow colleagues and engage each other on your training
                                                        progress.
                                                    </p>


                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                            @php
                            $count = 0;
                            $today = Carbon\Carbon::today()->format('d-m-y');
                            $configLms = config()->get('settings.lms.live');
                        @endphp
                            @foreach ($licenses as $license)
                           
                            @include('site.includes.components.training_feed', [
                                'license' => $license,
                            ])
                         
                        @endforeach
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>
    @else
        <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
            @include('site.includes.components.timeline')

        </div>
        <div class="row no-gutters">


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid">

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            <div class="row">
                                <div class="col-12">
                                    <p class="sentence"> Stay updated and inspired by the training milestones achieved by the colleagues you follow on the academy. 
                                        Congratulate them and give them Kudos for their achievements!</p>
                                </div>
                            </div>
                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p style="text-align:center">
                                                        Follow colleagues and engage each other on your training
                                                        progress.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                                
                            @php
                            $count = 0;
                            $today = Carbon\Carbon::today()->format('d-m-y');
                            $configLms = config()->get('settings.lms.live');
                        @endphp
                            @foreach ($licenses as $license)
                           
                            @include('site.includes.components.training_feed', [
                                'license' => $license,
                            ])
                         
                        @endforeach
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>

    @endif



@endsection


@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

 <script>
        $(function() {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '{{ asset('emojis/lib/img/') }}',
                popupButtonClasses: 'fa fa-smile-o',
                position: "bottom"
            });

            window.emojiPicker.discover();
        });
    </script>
<script>
    function showHide(user) {
     
        $("#" + user).toggle();
    };

    function showHideReply(user) {
        $("#" + user ).toggle();
    };
</script>

@endsection