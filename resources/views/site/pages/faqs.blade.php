@extends('layouts.app')

@section('title','FAQ')

@section('css')
<style>
#app{
min-height: 100vh;
}
.accordion .accordion-header .accordion-button:after {
    font-family: 'FontAwesome';  
    content: "\f068";
    float: right; 
    margin-right:15px;
}
.accordion .accordion-header .accordion-button.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\f067"; 
}

@media(max-width:768px){
#app{
min-height: 89vh;
}
.background-page {
    min-height: 31rem;
}
}
</style>

@endsection

@section('content')
<div class="background-page">
    @include('site.includes.components.parallax',[
      'banner' => 'enrolled_banner',
        'text' => 'FAQs',
        'link_1' => 'home',
        'link_3' => 'FAQs',
    ])
   
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class-about">

            
            <div class="col-md-12">
                {!! $pageItem->renderBlocks(false) !!}

            </div> 

        
        </div>

    @else 
    <div class="row ">

            
        <div class="col-md-12">
            {!! $pageItem->renderBlocks(false) !!}

        </div> 

    
    </div>


    @endif


@endsection
