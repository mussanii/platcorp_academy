
<html >
    <head>
      <style type="text/css">
    
    @font-face {
        font-family: 'Lato';
        src: url('{{ storage_path("fonts/Lato-Regular.ttf") }}') format("truetype");
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
         
        /* font-family: 'CenturyGothic';
        src: url('{{ storage_path("fonts/Century_Gothic.ttf") }}') format("truetype");
        font-weight: normal; 
        font-style: normal; 
        font-variant: normal; */
    }
    
      @page {
          size: a4 landscape;
          margin:0;padding:0; // you can set margin and padding 0
        }
      body {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        margin: 0 !important;
        width: 100% !important;
        -webkit-text-size-adjust: 100% !important;
        -ms-text-size-adjust: 100% !important;
        -webkit-font-smoothing: antialiased !important;
        font-family: "Lato";
    
       background-size:100% 100%;
        background-repeat: no-repeat;
      }
    
      table {
            page-break-inside: auto;
            border: 0;
            font-family: 'Lato';
        }
    
        tr {
            page-break-inside: avoid;
            page-break-after: auto;
            border: 0;
            font-family: 'Lato';
        }
    
        th {
            border: 0;
            font: inherit;
            font-size: 100%;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #1D2F5D;
            font-family: 'Lato';
    
    
        }
    
      .tableContent img {
        border: 0 !important;
        display: block !important;
        outline: none !important;
      }
    
      a {
        color: #382F2E;
      }
    
      a.blend {
        color: #fff;
      }
      p, h1, h2, ul, ol, li, div {
        margin: 0;
        padding: 0;
      }
    
      h1, h2 {
        font-weight: normal;
        background: transparent !important;
        border: none !important;
      }
      .header{
        margin-top: 4rem;
        margin-bottom: 2rem;
      }
    
      @media only screen and (max-width: 480px) {
    
        table[class="MainContainer"], td[class="cell"] {
          width: 100% !important;
          height: auto !important;
        }
    
        td[class="specbundle"] {
          width: 100% !important;
          float: left !important;
          font-size: 1.5em;
          line-height: 17px !important;
          display: block !important;
          padding-bottom: 15px !important;
        }
    
        td[class="specbundle2"] {
          width: 80% !important;
          float: left !important;
          font-size: 1.3em;
          line-height: 17px !important;
          display: block !important;
          padding-bottom: 10px !important;
          padding-left: 10% !important;
          padding-right: 10% !important;
        }
    
        td[class="spechide"] {
          display: none !important;
        }
    
    
        td[class="left_pad"] {
          padding-left: 15px !important;
          padding-right: 15px !important;
        }
    
      }
    
      @media only screen and (max-width: 540px) {
    
        table[class="MainContainer"], td[class="cell"] {
          width: 100% !important;
          height: auto !important;
        }
    
        td[class="specbundle"] {
          width: 100% !important;
          float: left !important;
          font-size: 13px !important;
          line-height: 17px !important;
          display: block !important;
          padding-bottom: 15px !important;
        }
    
        td[class="specbundle2"] {
          width: 80% !important;
          float: left !important;
          font-size: 13px !important;
          line-height: 17px !important;
          display: block !important;
          padding-bottom: 10px !important;
          padding-left: 10% !important;
          padding-right: 10% !important;
        }
    
        td[class="spechide"] {
          display: none !important;
        }
    
    
    
        td[class="left_pad"] {
          padding-left: 15px !important;
          padding-right: 15px !important;
        }
    
      }
    
      .contentEditable h2.big, .contentEditable h1.big {
        font-size: 1.4em !important;
      }
    
      .big-bold{
        font-size: 1.2em !important;
        font-weight: 900;
      }
    
      .bigger-bold{
        font-size: 1.3em !important;
        font-weight: 900;
      }
      .contentEditable h2.bigger, .contentEditable h1.bigger {
        font-size: 37px !important;
      }
    
      td, table {
        vertical-align: top;
      }
    
      td.middle {
        vertical-align: middle;
      }
    
      a.link1 {
        font-size: 13px;
        color: #27A1E5;
        line-height: 24px;
        text-decoration: none;
      }
    
      a {
        text-decoration: none;
      }
    
      a.btn-drk-left{
        color: #ffffff;
        border-radius: 25px;
        -moz-border-radius: 25px;
        -webkit-border-radius: 25px;
        background: #000151;
        padding: 10px 20px;
        font-weight: 900;
      }
      .link2 {
        color: #ffffff;
        border-top: 10px solid #27A1E5;
        border-bottom: 10px solid #27A1E5;
        border-left: 18px solid #27A1E5;
        border-right: 18px solid #27A1E5;
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        background: #27A1E5;
      }
    
      .link3 {
        color: #555555;
        border: 1px solid #cccccc;
        padding: 10px 18px;
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        background: #ffffff;
      }
    
      .link4 {
        color: #27A1E5;
        line-height: 24px;
      }
    
    
      p {
        font-size: 23px;
         line-height:1.7em;
      }
    
      .contentEditable li {
    
      }
    
      .appart p {
    
      }
    
      .bgItem {
        background: #ffffff;
      }
    
      .bgBody {
        background: #ffffff;
      }
    
    
      #box {
    background-image: url('Box1.png') }}');
    background-repeat: no-repeat;
    
    }
    
    #box2 {
      background-image: url('Box2.png') }}');
      background-repeat: no-repeat;
      background-position: right top;
      /* position: relative; */
    }
    
    #box3 {
    
      background-color: #fff;
      position: relative;
    }
    
    #box3::before,
    #box3::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      border-color: transparent;
      border-style: solid;
    }
    
    #box3::before {
      border-width: 5em;
      border-left-color: #0f73b8;
      border-bottom-color: #0f73b8;
      opacity:0.5;
    }
    
    #box3::after {
      border-width: 1.35em;
      border-left-color: #0f73b8;
      border-bottom-color: #0f73b8;
    }
    
    #box4 {
      background-color: #fff;
      /* position: relative; */
    }
    
    #box4::before,
    #box4::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      border-color: transparent;
      border-style: solid;
    }
    
    #box4::before {
      border-width: 4em;
      border-left-color: #0f73b8;
      border-bottom-color: #0f73b8;
      opacity:0.5;
    }
    
    #box4::after {
      border-width: 3em;
      border-left-color: #0f73b8;
      border-bottom-color: #0f73b8;
    }
    
    .center {
      display: block;
      margin-left: auto;
      margin-right: auto;
    
    }
    .left {
      display: block;
      float:left;
      margin: 2rem;
    
    }
    
    .right {
      display: block;
      float:right;
      margin-right: 1rem;
    
    }
    
      </style>
    
    </head>
   
<body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; background-image: url('https://academy.platcorpgroup.com/images/certificateAssets/certificate_design.png');
background-size:100% 100%;
background-repeat: no-repeat;" offset="0" toppadding="0" leftpadding="0">
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tbody>
        <tr>
          <td>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                       
                        <td style="text-align:center">

                            <img src="{{asset($logo)}}"style="margin-top:4rem; width: auto; max-height: 120px;"><br><br>
                        </td>
                
                    </tr>

                    <tr>
                       
                      <td style="text-align:center">

                          <img src="https://academy.platcorpgroup.com/images/certificateAssets/stars.png"style="width: auto; max-height: 100px;"><br><br>
                      </td>
              
                  </tr>

                    <tr>
                        <td  style="text-align:center; margin-right:8em !important; margin-left:8em !important; ">
                         <p style="font-size:32px;font-weight:bold;color:#ffffff;background:{{$company->primary_color}}; width:50%; margin-left:25%;padding-bottom:8px">Module Completion Badge</p>
                            <p  style="font-size:32px;font-weight:bold;color:#212529; ">Congratulations!</p>
                            <p style="color:{{$company->primary_color}};margin-bottom:0;font-size:38px;font-weight:900">{{$name}}</p>
                            <p style="text-transform:none; color:#212529; font-size:24px;margin-bottom:0;">Job Role: {{ucwords(strtolower($role))}} </p>
                           <p style="text-transform:none; color:#212529; font-size:24px;margin-bottom:0;"> for completing: <b> {{$course_name}}</b></p>
                            <p style="text-transform:capitalize;color:{{$company->primary_color}}; font-size:24px;font-weight:400;">Date: {{$date}}</p>
                            
                        </td></tr>
                </tbody>
            </table>
           
            <table  width="100%" border="0" cellspacing="0" cellpadding="0">
              
                </table>
                 
    </td>
    </tr>
    </tbody>
    </table>
    <p style="line-height:1em;font-size:11px; position:absolute; bottom:80px;right:90px"> Serial Number: {{ $cert_number }} </p>
    </body>
    </html>
    