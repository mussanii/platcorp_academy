@php
use Carbon\Carbon;
@endphp

@extends('layouts.app')




@section('content')
    <div class="background-page">
        @php
            $text = $pageItem->topic;
        @endphp

        @include('site.includes.components.parallax',[
    'banner'=> 'sessions_banner',
    'text'=>$pageItem->header_title
])
       {{-- @component('site.includes.components.breadcrumb')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('pages', ['key' => 'sessions']) }}">Live Sessions</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active">{{ $pageItem->topic }}</a>
                </li>
            </ol>
        @endcomponent--}}
        <div role="tabpanel " class="session-panel container-fluid mx-5 my-5">
        <div class="col-md-12">
            <div class="row">
        <div class="col-md-12">
        <ul class="nav nav-tabs">
                                    <li role="presentation" class="">
                                        <a href="{{ url()->previous() }}" style="text-decoration:none"
                                            >Sessions Calendar</a>
                                    </li>
                                    
                      
                                    <li role="presentation" class="">
                                        <a href="{{ url()->previous() }}"  style="text-decoration:none"
                                            >Upcoming Sessions</a>
                                    </li>
                         
                                    <li role="presentation"
                                        class="">
                                        <a href="{{ url()->previous() }}"  style="text-decoration:none"
                                            >Previous Sessions</a>
                        </ul>
                        
                    </div>
            </div>
        </div>
        </div>

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {
                    console.log("it has");
                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
        @endif

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        
        <div class="col-md-12">
            <div class="row alignment-class-sessions">
                <div class="col-md-9 col-sm-7 ">
                    @if ($pageItem->start_time >= Carbon::now())
                        @include(
                            'site.includes.components.upcoming_detail',
                            [
                                'pageItem' => $pageItem,
                            ]
                        )
                    @else
                        @include(
                            'site.includes.components.previous_detail',
                            [
                                'pageItem' => $pageItem,
                            ]
                        )
                    @endif
                </div>

            </div>

        </div>
        @else 
        <div class="container-fluid">

         
        <div class="col-sm-12">
            <div class="row ">
                <div class="col-md-9 col-sm-7 ">
                    @if ($pageItem->start_time >= Carbon::now())
                        @include(
                            'site.includes.components.upcoming_detail',
                            [
                                'pageItem' => $pageItem,
                            ]
                        )
                    @else
                        @include(
                            'site.includes.components.previous_detail',
                            [
                                'pageItem' => $pageItem,
                            ]
                        )
                    @endif
                </div>

            </div>

        </div>
        </div>


        @endif
    @endsection
    @section('js')
        <script>
            $(document).ready(function() {
                $('.show-table').removeClass('hidden-xs');

            });


            $(document).ready(function() {

                // Get current page URL
                var url = window.location.href;



                // remove # from URL
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

                // remove parameters from URL
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

                // select file name
                url = url.split('/')[4];




                // Loop all menu items
                $('.navbar-nav .nav-item').each(function() {

                    // select href
                    var href = $(this).find('a').attr('href');

                    link = href.split('/')[3];


                    // Check filename
                    if (link === 'sessions') {

                        // Add active class
                        $(this).addClass('active');
                    }
                });
            });
        </script>



    @endsection
