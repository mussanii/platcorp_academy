@extends('layouts.app')

@section('title','Edit My Profile')

@section('css')
<style>
  label{
    font-size:15px;
  }
</style>
@endsection

@section('content')
@include('site.includes.components.parallax', [
    'banner' => 'enrolled_banner',
    'text' => 'Edit Profile',
    'link_1' => 'home',
    'link_3' => 'Edit Profile',
])
 <div class="container-fluid">
    <div class="clearfix"><br /></div>
    <div class="clearfix"><br /></div>
    <div class="row  profile-row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (session()->has('register_errors'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('register_errors') }}
                </div>
            @endif
            @if (session()->has('register_success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('register_success') }}
                </div>
            @endif
        </div>

        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ppic-change-div">

            <ppic-form
                current="{{ Auth::user()->profile_pic ? asset('uploads/' . Auth::user()->profile_pic) : asset('uploads/images.jpg') }}"
                action="{{ route('profile.ppic.change') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </ppic-form>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 profile-details-form">
            <h1 class="text-purple"><strong>Personal Information</strong></h1>

            <p class="text-purple">Edit your personal information below. Contact the administrator to edit your job
                role, branch, department and country.</p>
            <form method="POST" action="{{ route('profile.edit') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-12">
                    <div class="row">
                        <div class=" col-12 col-md-6">
                            <div class="form-group ">

                                <label>First Name </label>
                                <input id="first_name" type="text"
                                    class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                    name="first_name" value="{{ $user['first_name'] }}" required
                                    placeholder="First Name">

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('class') }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group ">
                                <label>Last Name</label>
                                <input id="last_name" type="text"
                                    class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                    name="last_name" value="{{ $user['last_name'] }}" required
                                    placeholder="Last Name">

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif

                            </div>


                        </div>

                    </div>
                </div>


                <div class="col-12">
                    <div class="row">

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Email</label>
                                <input id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ $user['email'] }}" required placeholder="Email"
                                    readonly>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Employee Number</label>
                                <input id="email" type="email"
                                    class="form-control{{ $errors->has('employee_number') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ $user['employee_number'] }}" required placeholder="employee_number"
                                    readonly>
                                @if ($errors->has('employee_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('employee_number') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

             

                        
                    </div>
                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Gender</label>


                                <select class="form-control downArrow " name="gender_id" id="gender">
                                    <option value="">{{ __('Gender') }}</option>
                                    @foreach ($genders as $key => $value)
                                        @if ($user->gender_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('gender_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('gender_id') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>
                

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Department</label>
                                <select class="form-control downArrow " name="division_id" id="role"
                                    disabled="true">
                                    <option value="">{{ __('Department') }}</option>
                                    @foreach ($departments as $key => $value)
                                        @if ($user->division_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ucwords(strtolower($value->title)) }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('division_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('division_id') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

                    </div>

                </div>

                @if(Auth::user()->company_id !=3)
                <div class="col-12">
                    <div class="row">

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Branch</label>
                                <select class="form-control downArrow " name="branch_id" id="role"
                                    disabled="true">
                                    <option value="">{{ __('Branch') }}</option>
                                    @foreach ($branches as $key => $value)
                                        @if ($user->branch_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ ucwords(strtolower($value->title))  }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Job role </label>
                                <select class="form-control downArrow " name="job_role_id" id="role"
                                    disabled="true">
                                    <option value="">{{ __('Job Role') }}</option>
                                    @foreach ($roles as $key => $value)
                                        @if ($user->job_role_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ucwords(strtolower($value->title)) }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('job_role_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('job_role_id') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-12">
                    <div class="row">
                        
                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Country</label>


                                <select class="form-control downArrow " name="country_id" id="country"
                                    disabled="true">
                                    <option value="">{{ __('Country') }}</option>
                                    @foreach ($countries as $key => $value)
                                        @if ($user->country_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('country_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group ">
                                
                                    <button type="submit" class="btn btn-overall btn_solid_primary float-end mt-4">Submit</button>
                                
                            </div>

                        </div>

                        
                    </div>
                </div>


                @else 
                <div class="col-12">
                    <div class="row">

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Job role </label>
                                <select class="form-control downArrow " name="job_role_id" id="role"
                                    disabled="true">
                                    <option value="">{{ __('Job Role') }}</option>
                                    @foreach ($roles as $key => $value)
                                        @if ($user->job_role_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ucwords(strtolower($value->title)) }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('job_role_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('job_role_id') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group ">

                                <label>Country</label>


                                <select class="form-control downArrow " name="country_id" id="country"
                                    disabled="true">
                                    <option value="">{{ __('Country') }}</option>
                                    @foreach ($countries as $key => $value)
                                        @if ($user->country_id == $value->id)
                                            <option value="{{ $value->id }}" selected>{{ $value->title }}
                                            </option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('country_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-12">
                    <div class="row">
                       
                        <div class="col-12 col-md-6">
                            <div class="form-group ">
                                
                                    <button type="submit" class="btn btn-overall btn_solid_primary float-end mt-4">Submit</button>
                                
                            </div>

                        </div>

                        
                    </div>
                </div>


                @endif

             


               

            </form>
        </div>
    </div>
    <div class="clearfix"><br /></div>
</div>
@endsection
