@extends('layouts.evaluate')

@section('title','Course Evaluation')

@section('content')

@include('site.includes.components.enrolled_parallax', [
    'banner' => 'enrolled_banner',
    'text' => 'Course Evaluation',
    'link_1' => 'home',
    'link_3' => 'Course Evaluation',
])

<div class="container-fluid">
    <div class="evaluate-section ms-3 me-3">
        <div class="row justify-content-center about-row">
    
          <div class="col-md-12">
          <p class="evaluation-text ps-5 pt-4">Thank you for you for participating in this 5 minute evaluation.  Your responses will directly affect how we undertake similar initiatives in future so thoughtful responses are appreciated.</p></div>
            <div class="col-md-12">

               @if($sections) 
    
                {!! $sections->renderBlocks(false,[], ['id' => $id]) !!}
    
              @endif
            </div>
        </div>
    </div>

</div>

@endsection