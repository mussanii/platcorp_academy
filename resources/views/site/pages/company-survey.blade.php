@extends('layouts.evaluate')

@section('title','Survey')

@section('content')

@include('site.includes.components.enrolled_parallax', [
    'banner' => 'enrolled_banner',
    'text' => 'Survey',
    'link_1' => 'home',
    'link_3' => 'Survey',
])

<div class="container-fluid">
    <div class="evaluate-section ms-3 me-3">
        <div class="row justify-content-center about-row">
    
          <div class="col-md-12">
          <p class="evaluation-text ps-5 pt-4">Thank you for you for participating in this 5 minute survey.  Your responses will directly affect how we undertake similar initiatives in future so thoughtful responses are appreciated.</p></div>
            <div class="col-md-12">
             @if(!empty($evaluation))
              
               <h4 class="text-center primary_text">Successful</h4>
            <p class="text-center primary_text">Thank you for submitting the survey.</p>

             @else
               @if($sections) 
    
                {!! $sections->renderBlocks(false) !!}
    
              @endif
              @endif
            </div>
        </div>
    </div>

</div>

@endsection