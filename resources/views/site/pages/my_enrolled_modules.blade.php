@extends('layouts.app')

@section('content')
    @include('site.includes.components.enrolled_parallax', [
        'banner' => 'enrolled_banner',
        'text' => 'My Enrolled Modules',
        'link_1' => 'home',
        'link_3' => 'My Enrolled Modules',
    ])

    @php
        if (count(Auth::user()->compulsory) > 0) {
            $value = (count(Auth::user()->completed) * 100) / count(Auth::user()->compulsory);
            $string = sprintf('%.2f', $value) . '%';
        } else {
            $value = '';
            $string = '';
        }
 
    @endphp

    <course-progress class=" d-lg-flex float-end" compulsory="{{ count(Auth::user()->compulsory) }}"
        completed="{{ count(Auth::user()->completed) }}" size="Medium" value="{{ $value }}"
        string="{{ $string }}" position="inside" color="#ffffff" image="{{ asset('/images/icons/fire.jpg') }}"
        role="{{ Auth::user()->role }}"></course-progress>
    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="container-fluid ps-5 pt-3">
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                @include('site.includes.components.timeline')

            </div>
        </div>

        <div class="row no-gutters">

            @include('site.includes.components.enrolled_filter',['statuses' =>$statuses, 'month' =>$month, 'key'=>'profile.courses' ])

            
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid ps-2 pe-2 more-spacing">

                    @if(Auth::user()->download == 1)
            <div class="row">
                <div class="col-10"></div>
                <div class="col-2">
                    <a href="{{ route('download.certificate')}}" alt="download"
                    class="btn btn-overall btn_solid_secondary mb-2 me-4 " target="_blank"> Download
                    Certificate </a>
                </div>
            </div>
            @endif

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p>
                                                        Your list of courses is currently empty.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                                @php
                                    $count = 0;
                                    $today = Carbon\Carbon::today()->format('d-m-y');
                                    $configLms = config()->get('settings.lms.live');
                                @endphp
                                @foreach ($licenses as $license)

                           
                                    @include('site.includes.components.enrolled_modules', [
                                        'license' => $license,
                                    ])
                                @endforeach
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>
    @else
            <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                @include('site.includes.components.timeline')

            </div>
        <div class="row no-gutters">


            <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                <div class="container-fluid">
                    
                        @if(Auth::user()->download == 1)
                <div class="row">
                    <div class="col-12">
                        <a href="{{ route('download.certificate')}}" alt="download"
                        class="btn btn-overall btn_solid_secondary mb-2 me-4 float-end" target="_blank"> Download
                        Certificate </a>
                    </div>
                </div>
                @endif

                    <!-- Filter section -->
                    <!-- End of filter section -->
                    <!-- Course list -->
                    <div class="tab-content">
                        <div id="home" class="container-fluid tab-pane active">
                            @if (!count($licenses))
                                <div class="course-card detail-card no-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help"></div>
                                            <div class="row justify-content-md-center no-gutters">
                                                <div class="text-center col-12">
                                                    <p>
                                                        Your list of courses is currently empty.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @else
                                @php
                                    $count = 0;
                                    $today = Carbon\Carbon::today()->format('d-m-y');
                                    $configLms = config()->get('settings.lms.live');
                                @endphp
                                @foreach ($licenses as $license)
                                    @include('site.includes.components.enrolled_modules', [
                                        'license' => $license,
                                    ])
                                @endforeach
                            @endif

                        </div>

                    </div>
                </div>

            </div>
        </div>

    @endif

@endsection
