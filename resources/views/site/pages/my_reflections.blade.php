@extends('layouts.app')

@section('content')
@include('site.includes.components.reflection_parallax', [
'banner' => 'enrolled_banner',
'text' => 'My Reflection Notes',
'link_1' => 'home',
'link_2' => 'my Enrolled Modules',
'link_3' => 'My Reflection Notes',
])


@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid ps-5 pt-3">
    <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
        @include('site.includes.components.timeline')

    </div>
</div>


<div class="container-fluid">
    <div class="evaluate-section ms-3 me-3">
        <div class="row justify-content-center about-row">

            <div class="col-md-12">
                <h4 class="primary_text ps-5 pt-4"><b>Thank you for you for participating in the reflection questions. Here are your responses.</b></h4>
            </div>
            <div class="col-md-12 mt-4">
                @foreach ($reflections as $key => $item)
                <div class="ps-5 pb-3">
                    <h5><b>{{$key + 1}}. {{$item->question}}</b></h5>

                    <p class="ps-4 pe-4">{{$item->reflection}}</p>
                </div>
                @endforeach

            </div>
        </div>
    </div>

</div>


@else
<div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
    @include('site.includes.components.timeline')

</div>

<div class="container">
    <div class="evaluate-section ms-3 me-3">
        <div class="row justify-content-center about-row">

            <div class="col-md-12">
                <h4 class="primary_text"><b>Thank you for you for participating in the reflection questions. Here are your responses.</b></h4>
            </div>
            <div class="col-md-12">
                @foreach ($reflections as $key => $item)
                <div class="ps-5 pb-3">
                    <h5><b>{{$key + 1}}. {{$item->question}}</b></h5>

                    <p class="ps-4 pe-4">{{$item->reflection}}</p>
                </div>
                @endforeach

            </div>
        </div>
    </div>

</div>




@endif



@endsection

@section('js')
<script>
    $(document).ready(function() {

        // Get current page URL
        var url = window.location.href;

        // remove # from URL
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

        // remove parameters from URL
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

        // select file name
        url = url.split('/')[4];

        // Loop all menu items
        $('.nav.nav-pills.course-tabs a').each(function() {

            // select href
            var href = $(this).attr('href');


            link = href.split('/')[4];

            // Check filename
            if (link === 'myProfile') {

                // Add active class
                $(this).addClass('active');
            }
        });
    });
</script>
@endsection