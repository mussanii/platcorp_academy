<div class="modal fade popup-modal" id="changeDivision" tabindex="-1" role="dialog" aria-labelledby="edit-header-txt" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-top:15px">

                <h5 id="edit-header-txt" class="text-center"> Please complete your profile by filling in the below
                    information! </h5>
            </div>
            <div class="modal-body">
                <!-- <img src="{{ asset('/images/thumbs.png') }}" alt="" class="popuperror-img"> -->
                <form action="{{ route('update.division', [Auth::user()->id]) }}" method="post" class="" id="profileChange">
                    {{ csrf_field() }}

                    <div class="form-body">

                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Department
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">

                                    <select name="division_id" id="profile-division" class="form-control m-input " required>
                                        <option value="">Select Department</option>
                                        @foreach (App\Models\Division::published()->where('company_id', Auth::user()->company_id)->get() as $role)
                                        <option value="{{ $role->id }}">{{ucwords(strtolower($role->title)) }}</option>
                                        @endforeach

                                    </select>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-12 col-form-label">
                              Employee / Payroll Number
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="employee_number" id="employee_number" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-12 text-center mt-3">
                                    <button type="submit" class="btn btn-overall btn_solid_primary " id="btn-savePass">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>
</div>