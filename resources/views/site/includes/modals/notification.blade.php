
@php
if(session()->get('notification_success')){
    $flashMessage = session()->get('notification_success');
  }else{
      $flashMessage = '';
  }
    
@endphp
<div class="modal fade bd-example-modal-lg" id="notificationModal" role="dialog" aria-labelledby="notificationModal" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content message-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="CourseSuccessLabel">@if($flashMessage)
                    {{ $flashMessage['title'] }}
                    @endif</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                @if($flashMessage)
                {!! $flashMessage['content'] !!}
                @endif
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-overall btn_solid_primary modal-accept notification-dismiss"  data-bs-dismiss="modal" aria-label="Close"  onclick="document.getElementById('notificationModal').style.display='none'">Dismiss</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
