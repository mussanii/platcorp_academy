<div class="modal fade popup-modal" id="changePass" tabindex="-1" role="dialog" aria-labelledby="edit-header-txt" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-top:15px">

                <h5 id="edit-header-txt" class="text-center"> Please complete your profile by filling in the below
                    information! </h5>
            </div>
            <div class="modal-body">
                <!-- <img src="{{ asset('/images/thumbs.png') }}" alt="" class="popuperror-img"> -->
                <form action="{{ route('update.profile', [Auth::user()->id]) }}" method="post" class="" id="profileChange">
                    {{ csrf_field() }}

                    <div class="form-body">
                        @if(Auth::user()->authentication_type == 3)
                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                First Name
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="first_name" id="first_name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Last Name
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="last_name" id="last_name" />
                                </div>
                            </div>
                        </div>

                        @endif

                        @if(Auth::user()->company_id !=3 && Auth::user()->company_id !=7)

                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Branch
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">
                                    <select class="form-control" name="branch_id" id="profile-branch_id" class="form-control m-input " required>
                                        <option value="">Select</option>
                                        @foreach (App\Models\Branch::published()->where('company_id', Auth::user()->company_id)->get() as $branch)
                                        <option value="{{ $branch->id }}">{{ucwords(strtolower($branch->title))}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        @endif

                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Department
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">

                                    <select name="division_id" id="profile-division" class="form-control m-input " required>
                                        <option value="">Select Department</option>
                                        @foreach (App\Models\Division::published()->where('company_id', Auth::user()->company_id)->get() as $role)
                                        <option value="{{ $role->id }}">{{ucwords(strtolower($role->title)) }}</option>
                                        @endforeach

                                    </select>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Job Role
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">

                                    <select name="job_role_id" id="profile-job_role" class="form-control m-input " required>
                                        <option value="">Select Job Role</option>
                                        @foreach (App\Models\JobRole::published()->where('company_id', Auth::user()->company_id)->get() as $role)
                                        <option value="{{ $role->id }}">{{ucwords(strtolower($role->title)) }}</option>
                                        @endforeach

                                    </select>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="col-12 col-form-label">
                              Employee / Payroll Number
                            </label>
                            <div class="col-12">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="employee_number" id="employee_number" />
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->company_id !=7)
                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Country
                            </label>
                            <div class="col-12">

                                <div class="form-group has-feedback">

                                    <select name="country_id" id="profile-country" class="form-control m-input " required>
                                        <option value=""> Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $country)
                                        <option value="{{ $country->id }}">{{ $country->title }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label for="example-text-input" class="col-4 col-form-label">
                                Gender
                            </label>
                            <div class="col-12">

                                <div class="form-group has-feedback">

                                    <select name="gender_id" id="profile-gender" class="form-control m-input " required>
                                        <option value=""> Select Gender</option>
                                        @foreach (App\Models\Gender::all() as $gender)
                                        <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-12 text-center mt-3">
                                    <button type="submit" class="btn btn-overall btn_solid_primary " id="btn-savePass">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>
</div>