@php
use Carbon\Carbon;
@endphp
@if ((new \Jenssegers\Agent\Agent())->isDesktop())

<div class="container-fluid col-md-10 mb-5">
<div class="accordion accordion__item faq-item"  id="accordionExample" >
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
       <h5><strong>This Week</strong></h5>
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse accordion-item__desc collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
      <table class="table table-borderless table-responsive week">
        <thead>
        <tr class="table-title ps-2">
      <th>Sessions</th>
      <th>Start Date </th>
      <th>Status</th>
      <th>Reminder</th>
       </tr>
        </thead>
       
        <tbody>
        @if(count($session_calendar_week) > 0)
        @foreach($session_calendar_week as $key=>$calendar)
        <tr>

        @php
        $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
         @endphp 
         
          
      <th scope="row"  class="title"><a class="text-decoration-none" href="{{ route('session.details', [$calendar->id]) }}">{{$calendar->title}}</a></th>
      <td class="session-calendar-content">{{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }} </td>
   
      <td>
        @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
      
        @endif
      
        @if($calendarEvent->isNotEmpty())
     
     <td><a href="#" class="btn btn-overall btn_border_primary session-calendar"><strong>Added to Calendar</strong></a></td>
     @else
     <td><a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_solid_primary session-calendar"><strong> Add to Calendar</strong></a></td>
     @endif
      </tr>
    @endforeach
    <tr>
          @else
          <tr>
          <td colspan="4"> <p class="text-center no-courses" > There are no sessions for this week</p></td>
          </tr>
        @endif
          <td>
        <div class="d-flex justify-content-center ">
        {!! $session_calendar_week->links() !!}
        </td>
      </tr>
        </tbody>
       
        

    </table>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button accordion__item-button white-bg" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
       <h5><strong> This Month</strong></h5>
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
        
      <table class="table table-borderless table-responsive">
        <thead>
        <tr class="table-title ps-2">
      <th scope="col">Sessions</th>
      <th scope="col">Start Date </th>
      <th scope="col">Status</th>
      <th scope="col">Reminder</th>
       </tr>
        </thead>
      
        <tbody>
        @if(count($session_calendar_monthly) > 0)
        @foreach($session_calendar_monthly as $key=>$calendar)
        <tr>
        @php
         $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
         @endphp
         
          
      <th scope="row"  class="title"><a  class="text-decoration-none" href="{{ route('session.details', [$calendar->id]) }}">{{$calendar->title}}</a></th>
      <td class="session-calendar-content">{{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }}</td>
      
      <td>
      @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
      
        @endif
      @if($calendarEvent->isNotEmpty())

<td><a href="#" class="btn btn-overall btn_border_primary session-calendar"><strong>Added to Calendar</strong></a></td>

@else
<td><a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_border_tertiary session-calendar"><strong> Add to Calendar</strong></a></td>
@endif
    </tr>
    @endforeach

    <tr>
          @else
          <tr>
          <td colspan="4"><p class="text-center no-courses" > There are no sessions for this month</p></td>
          </tr>
        @endif
          <td>
        <div class="d-flex justify-content-center ">
        {!! $session_calendar_monthly->links() !!}
        </td>
      </tr>
        </tbody>
       
    </table>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree" >
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
 <h5><strong> This Quarter</strong></h5>
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
      <table class="table table-borderless table-responsive">
        <thead>
      <tr class="table-title ps-2 ">
      <th scope="col">Sessions</th>
      <th scope="col">Start Date </th>
      <th scope="col">Status</th>
      <th scope="col">Reminder</th>
       </tr>
        </thead>
    
        <tbody>
        @if(count($session_calendar_quarter) > 0)
        @foreach($session_calendar_quarter as $key=>$calendar)
        <tr>
        @php
        $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
         @endphp
          
      <th scope="row" class="title"><a  class="text-decoration-none" href="{{ route('session.details', [$calendar->id]) }}">{{$calendar->title}}</a></th>
      <td class="session-calendar-content"> {{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }} </td>
   
      <td>
      @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a></td>
      
        @endif
     
      @if($calendarEvent->isNotEmpty())
    
    <td><a href="#" data-toggle="tooltip" title="You have already added this session to your calendar!" class="btn btn-overall btn_border_primary session-calendar"><strong>Added to Calendar</strong></a></td>
    @else
    <td><a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_solid_primary session-calendar"><strong>Add to Calendar</strong></a></td>
    @endif
    </tr>
    @endforeach

    <tr>
          @else
          <tr>
          <td colspan="4"> <p class="text-center no-courses" > There are no sessions for this quarter</p></<td>
          </tr>
        @endif
          <td>
        <div class="d-flex justify-content-center ">
        {!! $session_calendar_quarter->links() !!}
        </td>
      </tr>
        </tbody>
    
        

    </table>
      </div>
    </div>
  </div>
</div>

</div>
@else
<div class="container-fluid pt-3 mb-5 col-md-10">

<div class="accordion accordion__item faq-item "  id="accordionExample" >
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
       <h5><strong>This week</strong></h5>
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse accordion-item__desc collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="card-body accordion__item-body">
      @if(count($session_calendar_week) > 0)
        @foreach($session_calendar_week as $key=> $calendar) 

        @php
        $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
        @endphp
      <div class="card">
  <div class="card-body">

  <div class="row">
    
    <h6><strong><a href="{{ route('session.details', [$calendar->id]) }}" target="_blank" class="text-decoration-none" style="color:#000">{{$calendar->title}}</a> | {{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }}</strong></h6>
  </div>
  </div>
  <div class="card-footer">
  <div class="col-12">
    <div class="row">
      <div class="col-6">
      @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
      
        @endif
      </div>
      <div class="col-6">
  @if($calendarEvent->isNotEmpty())
      <a href="#" class="btn btn-overall btn_border_primary ">Added to Calendar</a>
      @else
      <a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_border_primary text-right">Add to Calendar</a>
      @endif
      </div>
        </div>
  </div>
  </div>
        </div>
        @endforeach
        @else
        <dd class="col-sm-9">
        
          <p class="text-center no-courses" > There are no sessions for this week</p>
    @endif
    </dd>

       
        {!! $session_calendar_week->links() !!}
      


      
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button accordion__item-button white-bg" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <h5><strong>This month</strong></h5>
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      @if(count($session_calendar_monthly) > 0)
        @foreach($session_calendar_monthly as $key=> $calendar) 

        @php
        $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
        @endphp
      <div class="card">
  <div class="card-body">

  <div class="row">
    
    <h6><strong><a href="{{ route('session.details', [$calendar->id]) }}" target="_blank" class="text-decoration-none" style="color:#000">{{$calendar->title}}</a> | {{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }}</strong></h6>
  </div>
  </div>
  <div class="card-footer">
  <div class="col-12">
    <div class="row">
      <div class="col-6">
      @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
      
        @endif
      </div>
      <div class="col-6">
  @if($calendarEvent->isNotEmpty())
      <a href="#" class="btn btn-overall btn_border_primary ">Added to Calendar</a>
      @else
      <a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_border_primary text-right">Add to Calendar</a>
      @endif
      </div>
        </div>
  </div>
  </div>
        </div>
        @endforeach

        @else
        <dd class="col-sm-9">
        
          <p class="text-center no-courses" > There are no sessions for this month</p>
    @endif
    </dd>
       
        {!! $session_calendar_monthly->links() !!}
        
        
     
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree" >
      <button class="accordion-button accordion__item-button white-bg collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
 <h5><strong>This quarter</strong></h5>
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse accordion-item__desc collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">

      @if(count($session_calendar_quarter) > 0)
        @foreach($session_calendar_quarter as $key=> $calendar) 

        @php
        $calendarEvent = App\Models\CalendarEvent::where('user_id', Auth::user()->id)
    ->where('session_id', $calendar->id)
    ->get();
        @endphp
      <div class="card">
  <div class="card-body">

  <div class="row">
    
    <h6><strong><a href="{{ route('session.details', [$calendar->id]) }}" target="_blank" class="text-decoration-none" style="color:#000">{{$calendar->title}}</a> | {{ Carbon::parse($calendar->start_time)->isoFormat(' MMMM Do YYYY ') }}</strong></h6>
  </div>
  </div>
  <div class="card-footer">
  <div class="col-12">
    <div class="row">
      <div class="col-6">
      @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
        <a href="{{$calendar->teams_link}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
        @else
        <a href="{{$calendar->join_url}}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Join Session</button></a>
      
        @endif
      </div>
      <div class="col-6">
  @if($calendarEvent->isNotEmpty())
      <a href="#" class="btn btn-overall btn_border_primary ">Added to Calendar</a>
      @else
      <a href="{{ route('session.calendar', $calendar->id) }}" class="btn btn-overall btn_border_primary text-right">Add to Calendar</a>
      @endif
      </div>
        </div>
  </div>
  </div>
        </div>
        @endforeach

        @else
        <dd class="col-sm-9">
        
          <p class="text-center no-courses" > There are no sessions for this month</p>
    @endif
    </dd>
       
        {!! $session_calendar_quarter->links() !!}
        
      
      </div>
    </div>
  </div>
</div>
</div>
@endif


