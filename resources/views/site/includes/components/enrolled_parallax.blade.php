<div class="jarallax enrolled-jarallax">
    <div class="jarallax-img {{ $banner}}">
 
    {{-- <img class="" src="{{ $image }}" alt=""> --}}
     <div class="content">
         <div class="wrap">
             @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                 <h1>{!! $text !!}</h1>
                 {{-- <p>{!! $description !!}</p> --}}
             @else
                 <h1>{!! $text !!}</h1>
                 {{-- <p>{!! $description !!}</p> --}}
             @endif
         
             @component('site.includes.components.breadcrumb')
 <ol class="breadcrumb">
     @if(!empty($link_1))
     <li class="breadcrumb-item">
        
         <a href="{{ route($link_1) }}">{{ucfirst($link_1)}}</a>
     </li>
     @endif
     @if(!empty($link_2))
     <li class="breadcrumb-item">
         <a href="{{ route('pages', ['key' => $link_2]) }}"> {{ucfirst($link_2)}}</a>
     </li>
     @endif
     @if(!empty($link_3))
     <li class="breadcrumb-item active" aria-current="page">
         <a href="{{ url()->current() }}" class="active">{!! $link_3 !!}</a>
     </li>
     @endif
 </ol>
 
         </div>
         <div class="ik-hr-white">
             <span class="hr-inner">
                 <span class="hr-inner-style"></span>
             </span>
         </div>
     </div>
     <!-- <a href="#next-section" title="" class="scroll-down-link " aria-hidden="true" >
       <i class="fas fa-chevron-down"></i>
     </a> -->
    </div>
 </div>
 
 
 
 
 @endcomponent