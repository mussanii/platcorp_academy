@php
    
    $configLms = config()->get('settings.lms.live');
    
@endphp
@if(!empty($course->mandatory) || !empty($course->recommended))
<div class="course-card">
    <article>
        <div class="thumbnail">
            @if ($course['course_image_uri'])
                @php $image = $course['course_image_uri']; @endphp

                {!! responsiveImage([
                    'options' => [
                        'src' => $course['course_image_uri'] ?? '',
                        'w' => $image->width ?? 600,
                        'h' => $image->height ?? 400,
                        'ratio' => $image->ratio ?? 9 / 16,
                        'fit' => 'crop',
                    ],
                    'html' => [
                        'alt' => $image->altText ?? '',
                    ],
                    'sizes' => [
                        ['media' => 'small', 'w' => 730],
                        ['media' => 'medium', 'w' => 426],
                        ['media' => 'large', 'w' => 500],
                        ['media' => 'xlarge', 'w' => 600],
                        ['media' => 'xxlarge', 'w' => 680],
                    ],
                ]) !!}
            @endif
            <div class="overlay">

                @if (!empty($course->mandatory))

                    @if (!empty($course->recommended))
                        <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                            <i class="pro-icon icon-hr-recommended"></i>
                        </div>
                    @else
                        <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                            <i class="pro-icon icon-pro"></i>
                        </div>
                    @endif
                @else
                    @if (!empty($course->recommended))
                        <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                            <i class="pro-icon icon-hr-recommended"></i>
                        </div>
                    @endif
                @endif

            </div>
        </div>

        <div class="course-card-content">
            <p class="title ">
                <span class="title-span"><a
                        href="{{ route('course.detail', ['key' => $course->link_selected]) }}">{{ $course['name'] }}</a></span>
                <span class="title-calendar"> <i class="progress-icon icon-clock" title="clock"></i>
                    45 mins</span>
            </p>

        </div>


        <div class="card-footer ">
            <div class="col-12">
                <div class="row">
                    <div class="col-6">
                        <a href="{{ route('course.detail', ['key' => $course->link_selected]) }}"
                            class="btn btn-overall btn_border_primary">Course Details</a>
                    </div>
                    <div class="col-6">

                        @if (!empty($course->enrollment))
                            @if (!empty($course->completion))
                                @if ($course->completion == 1)
                                    <a class="btn btn-overall btn_solid_primary float-end"
                                        href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                        target="_blank"> Completed </a>
                                @else
                                    <a class="btn btn-overall btn_solid_primary float-end"
                                        href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                        target="_blank"> Retake Course </a>
                                @endif
                            @else
                                <a class="btn btn-overall btn_solid_secondary float-end"
                                    href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}"
                                    target="_blank"> In Progress </a>

                            @endif
                        @else
                            <a class="btn btn-overall btn_solid_tertiary float-end"
                                href="{{ route('course.enroll', $course->course_id) }}/">Enroll </a>
                        @endif

                    </div>
                </div>
            </div>

        </div>

    </article>
</div>
@else 
<div class="course-card other-course-card ">

    @if ($course->getCourseStatus() == 0  || $course->getCourseStatus() == 2)
     <article>
         <div class="overlay">
             <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$course['title']}}">
                 @if ($course->getCourseStatus() == 0 )
               <a class="btn btn-overall btn_solid_tertiary float-end" href="{{route('request.access', $course->id)}}" >Request access </a> 
               @else
               <button class="btn btn-overall btn_solid_secondary float-end" href="#" >Pending approval</button> 
               @endif
             </div>
             <div class="thumbnail">
                 @if ($course['course_image_uri'])
                     <img src="{{ asset($course['course_image_uri']) }}" alt="{{ $course->name }}" />
                 @endif
                 <div class="overlay">
     
                     @if (!empty($course->mandatory))
     
                         @if (!empty($course->recommended))
                             <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                                 <i class="pro-icon icon-hr-recommended"></i>
                             </div>
                         @else
                             <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                                 <i class="pro-icon icon-pro"></i>
                             </div>
                         @endif
     
                     @else 
                     @if (!empty($course->recommended))
                     <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                         <i class="pro-icon icon-hr-recommended"></i>
                     </div>
                
                    @endif
                 @endif
     
                 </div>
             </div>
     
             <div class="course-card-content">
                 <p class="title ">
                     <span class="title-span">{{ $course['name'] }}</span>
                     <span class="title-calendar"> <i class="progress-icon icon-clock" title="clock"></i>
                         45 mins</span>
                 </p>
                
             </div>
     
     
             <div class="card-footer ">
                 <div class="col-12">
                     <div class="row">
                         <div class="col-6">
                            <button class="btn btn-overall btn_border_primary">Course Details</button> 
                         </div>
                         <div class="col-6">
     
                             @if (!empty($course->enrollment))
                                 @if (!empty($course->completion))
                                     @if ($course->completion == 1)
 
                                     <button class="btn btn-overall btn_solid_primary float-end">Completed</button> 
                                     
                                     
                                     @else
 
                                     <button class="btn btn-overall btn_solid_primary float-end">Retake Course</button> 
                                    
                                     
                                     @endif
                                 @else
                                 <button class="btn btn-overall btn_solid_secondary float-end">In Progress</button> 
                                
                                 @endif
                             @else
 
                             <button class="btn btn-overall btn_solid_tertiary float-end">Enroll</button> 
                             {{-- <a class="btn btn-overall btn_solid_tertiary float-end" href="#"
                                 >Enroll </a> --}}
                             @endif
     
                         </div>
                     </div>
                 </div>
     
             </div>
         </div>
         </article>
     
     
     
    @else
     <article>
         <div class="thumbnail">
             @if ($course['course_image_uri'])
                 <img src="{{ asset($course['course_image_uri']) }}" alt="{{ $course->name }}" />
             @endif
             <div class="overlay">
 
                 @if (!empty($course->mandatory))
 
                     @if (!empty($course->recommended))
                         <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                             <i class="pro-icon icon-hr-recommended"></i>
                         </div>
                     @else
                         <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                             <i class="pro-icon icon-pro"></i>
                         </div>
                     @endif
 
                 @else 
                 @if (!empty($course->recommended))
                 <div class="overlay-inside-left" data-toggle="tooltip" data-placement="top" title="">
                     <i class="pro-icon icon-hr-recommended"></i>
                 </div>
            
                @endif
             @endif
 
             </div>
         </div>
 
         <div class="course-card-content">
             <p class="title ">
                 <span class="title-span"><a href="{{ route('course.detail', ['key' => $course->link_selected]) }}">{{ $course['name'] }}</a></span>
                 <span class="title-calendar"> <i class="progress-icon icon-clock" title="clock"></i>
                     45 mins</span>
 
                     
             </p>
            
         </div>
 
 
         <div class="card-footer ">
             <div class="col-12">
                 <div class="row">
                     <div class="col-6">
                         <a href="{{route('course.detail',['key' => $course->link_selected])}}" class="btn btn-overall btn_border_primary">Course Details</a>
                     </div>
                     <div class="col-6">
 
                         @if (!empty($course->enrollment))
                             @if (!empty($course->completion))
                                 @if ($course->completion == 1)
                                 <a class="btn btn-overall btn_solid_primary float-end" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
                                     > Completed </a>
                                 
                                 @else
                                 <a class="btn btn-overall btn_solid_primary float-end" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
                                     > Retake Course </a>
                                 
                                 @endif
                             @else
                             <a class="btn btn-overall btn_solid_secondary float-end" href="{{ $configLms['LMS_BASE'] . '/courses/' . $course->course_id . '/courseware' }}" target="_blank"
                                 > In Progress </a>
                             
                             @endif
                         @else
                         <a class="btn btn-overall btn_solid_tertiary float-end" href="{{ route('course.enroll', $course->course_id) }}/"
                             >Enroll </a>
                         @endif
 
                     </div>
                 </div>
             </div>
 
         </div>
    
     </article>
 
     @endif
        
        
 
 </div>



@endif
