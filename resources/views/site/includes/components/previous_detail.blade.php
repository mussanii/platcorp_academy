@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section">
        <div class="container-fluid ps-5">
        <div class="col-md-12 ">
            <div class="row">
            <div class="col-md-4">
                
            <p><a href="{{ $pageItem->session_video_url }}" target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary">Watch Video</button></a></p>
            <p><img src="{{ asset('images/icons/calendar.svg')}}" alt=""/> <span class="mx-2">{{ Carbon\Carbon::parse($pageItem->start_time)->isoFormat('MMMM Do YYYY H A') }}</span></p>
            <p><img src="{{ asset('images/icons/Clock.svg')}}" alt=""/><span class="mx-2">{{$pageItem->duration}} Minutes</span></p>

          
            </div>

            <div class="col-md-8 border">
             
             <h5 class="mt-2"> About this session</h5>
             <div class="whatYouGetListing gray">

<span id="lessText"> {!! truncateString($pageItem->description,150) !!}</span>
<a href="#">Show More</a>
</div>

<div class="whatYouGetListing orange" style="display: none;">
            <span id="moreText">{!! $pageItem->description !!}</span>
            <a href="#">Show Less</a>
            </div>
            </div>
                </div>
                </div>
            <div class="clearfix">
                <br />
            </div>
        </div>

    </section>
@else
    <section class="general-section">
        <div class="col-md-12 ">

            <div class="row ">
                <div class="col-2  no-padding-right">
                    <b>Topic </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ $pageItem->topic }} </p>
                </div>
            </div>

            <div class="row ">
                <div class="col-2  no-padding-right">
                    <b>Time </b>
                </div>

                <div class="col-8 no-padding-left">
                    <p>{{ Carbon\Carbon::parse($pageItem->start_time)->isoFormat('MMMM Do YYYY H A') }} </p>
                </div>
            </div>

            <div class="row ">
              

                <div class="col-12 col-md-8 no-padding-left">
                    <p>{!! $pageItem->agenda !!}</p>
                </div>
            </div>

            <div class="row ">
                <div class="col-5  no-padding-right">
                    <b>Meeting ID </b>
                </div>

                <div class="col-6 no-padding-left">
                    <p>{{ $pageItem->meeting_id }}</p>
                </div>
            </div>

            <div class="clearfix">
                <br />
            </div>


            {{-- <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    <h4> To Join the Session </h4>
                    <p>Join from a PC, Mac, iPhone or Android device:</p>

                    <a href="{{ $pageItem->join_url }}" class="btn btn-overall btn_register" target="_blank">
                        Join the Meeting
                    </a>

                </div>
            </div> --}}
        </div>

    </section>
@endif

@section('js')
<script>
      $('orange').hide();
    $('.gray a').click(function() {
      $('.gray').hide();
      $('.orange').show();
    });
    $('.orange a').click(function() {
      $('.gray').show();
      $('.orange').hide();
    })
    </script>


@endsection
