@unless(empty($activity))
    @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="card-body d-flex flex-row pt-0 feed-response" id="response-{{ $activity->id }}">
            <div class="col-12">
                <div class="row">
                    <div class="col-10 col-sm-11">
                        <div class="user-response">
                            <div class="commentsContainer">
                                <div class="responseCommentRow">
                                    <span class="name">
                                        {{ $activity->user->name }}
                                        <small class="time float-end">
                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                        </small>
                                    </span>
                                </div>
                                <div class="commentMain">
                                    <p>{{ $activity->comment }}</p>
                                </div>

                                <div class="commentImgInner">
                                    @unless(empty($activity->images))
                                        @foreach (json_decode($activity->images) as $image)
                                            <img src="{{ asset('images/trainingFeed/' . $image) }}"
                                                class="img-fluid main-Commentimg" />
                                        @endforeach
                                    @endunless
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-2 col-sm-1">
                        <div class="commentImg">
                            @if ($activity->user->profile_pic)
                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                    class="img-fluid profile-avatar" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid profile-avatar" />
                            @endif
                        </div>
                    </div>
                    <div class="col-10 col-sm-11 comment-response">
                        <small class="float-left">
                            <div class="comment-click" onclick="showHide('response-reply-{{ $activity->id }}');">
                                <h5 class="innerstats"><i class="progress-icon icon-comment training-icon"
                                        title="clock"></i><span>Comments</span></h5>
                                <p>{{ $license->getSubCommentCount($license->user->id, $license->course->id, $activity->id) }}
                                </p>
                            </div>
                            </form>
                        </small>

                        @foreach ($license->getSubComment($license->user->id, $license->course->id, $activity->id) as $reply)
                            @include('site.includes.components.training_feed_subcomment', [
                                'activity' => $reply,
                                'license' => $license,
                            ])
                        @endforeach

                        <form action="{{ route('profile.myComment') }}" method="POST">
                            @csrf
                            <input type='hidden' name="follow" value="{{ $license->user->id }}" />
                            <input type='hidden' name="course" value="{{ $license->course->id }}" />
                            <input type='hidden' name="unique" value="{{ $activity->id }}" />

                            <div class="reply-reply"id="response-reply-{{ $activity->id }}" style="display:none ">

                                <div class="container-fluid mt-3" id="response-reply-{{ $activity->id }}"
                                    style="display: block;">
                                    <div class="main  d-flex">
                                        <div class="card " style="border: 0px;width:100%">
                                            <div class="row no-gutters">
                                                <div class="col-2 col-md-1">
                                                    @if ($activity->user->profile_pic)
                                                        <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                            class="card-img avatar-small profile-avatar" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="card-img avatar-small profile-avatar" />
                                                    @endif
                                                </div>
                                                <div class="col-8">

                                                    <div class="card-body card-small pb-0">
                                                        <div class="card-text">
                                                            <input type="text" name="id" value="5"
                                                                hidden="" id="replycomment_id">
                                                            <textarea id="replycomment_reply" class="form-control text-input" name="comment" placeholder="Reply"
                                                                data-emojiable="true" data-emoji-input="unicode"></textarea>
                                                        </div>
                                                        <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                                                class="img-fluid inputImg" width="20" /></label>
                                                        <input name="images[]" type="file" id="imageInput" multiple>
                                                        <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                                class="img-fluid inputImg" width="20" /></label>
                                                        <input name="gifs[]" type="file" id="gifInput" multiple>


                                                    </div>
                                                </div>
                                                <div class="col-2 ">
                                                    <div class="submitbuttons">

                                                        <button
                                                            type="submit"class="btn btn-overall btn_solid_secondary">Reply</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </form>


                    </div>
                </div>
            </div>


        </div>
    @else
        <div class="card-body d-flex flex-row pt-0 feed-response" id="response-{{ $activity->id }}">
            <div class="col-12">
                <div class="row">
                    <div class="col-10 ">
                        <div class="user-response">
                            <div class="commentsContainer">
                                <div class="responseCommentRow">
                                    <span class="name">
                                        {{ $activity->user->name }}
                                        <small class="time float-end">
                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                        </small>
                                    </span>
                                </div>
                                <div class="commentMain">
                                    <p>{{ $activity->comment }}</p>
                                </div>

                                <div class="commentImgInner">
                                    @unless(empty($activity->images))
                                        @foreach (json_decode($activity->images) as $image)
                                            <img src="{{ asset('images/trainingFeed/' . $image) }}"
                                                class="img-fluid main-Commentimg" />
                                        @endforeach
                                    @endunless
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-2 ">
                        <div class="commentImg">
                            @if ($activity->user->profile_pic)
                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                    class="img-fluid profile-avatar" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid profile-avatar" />
                            @endif
                        </div>
                    </div>
                    <div class="col-12 comment-response">
                        <small class="float-left">
                            <div class="comment-click" onclick="showHide('response-reply-{{ $activity->id }}');">
                                <h5 class="innerstats"><i class="progress-icon icon-comment training-icon"
                                        title="clock"></i><span>Comments</span></h5>
                                <p>{{ $license->getSubCommentCount($license->user->id, $license->course->id, $activity->id) }}
                                </p>
                            </div>
                            </form>
                        </small>

                        @foreach ($license->getSubComment($license->user->id, $license->course->id, $activity->id) as $reply)
                            @include('site.includes.components.training_feed_subcomment', [
                                'activity' => $reply,
                                'license' => $license,
                            ])
                        @endforeach

                        <form action="{{ route('profile.myComment') }}" method="POST">
                            @csrf
                            <input type='hidden' name="follow" value="{{ $license->user->id }}" />
                            <input type='hidden' name="course" value="{{ $license->course->id }}" />
                            <input type='hidden' name="unique" value="{{ $activity->id }}" />

                            <div class="reply-reply"id="response-reply-{{ $activity->id }}" style="display:none ">

                                <div class="container-fluid mt-3" id="response-reply-{{ $activity->id }}"
                                    style="display: block;">
                                    <div class="main  d-flex">
                                        <div class="card " style="border: 0px;width:100%">
                                            <div class="row no-gutters">
                                                <div class="col-2 col-md-1">
                                                    @if ($activity->user->profile_pic)
                                                        <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                            class="card-img avatar-small profile-avatar" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="card-img avatar-small profile-avatar" />
                                                    @endif
                                                </div>
                                                <div class="col-8">

                                                    <div class="card-body card-small pb-0">
                                                        <div class="card-text">
                                                            <input type="text" name="id" value="5"
                                                                hidden="" id="replycomment_id">
                                                            <textarea id="replycomment_reply" class="form-control text-input" name="comment" placeholder="Reply"
                                                                data-emojiable="true" data-emoji-input="unicode"></textarea>
                                                        </div>
                                                        <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                                                class="img-fluid inputImg" width="20" /></label>
                                                        <input name="images[]" type="file" id="imageInput" multiple>
                                                        <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                                class="img-fluid inputImg" width="20" /></label>
                                                        <input name="gifs[]" type="file" id="gifInput" multiple>


                                                    </div>
                                                </div>
                                                <div class="col-12 ">
                                                    <div class="submitbuttons">

                                                        <button
                                                            type="submit"class="btn btn-overall btn_solid_secondary">Reply</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </form>


                    </div>
                </div>
            </div>


        </div>
    @endif

@endunless
