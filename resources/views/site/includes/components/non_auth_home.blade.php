@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    @if ($pageItem->hasImage('hero_image'))

        @if (!Auth::check())
            <section class="home-banner"
                style="background:url('{{ $pageItem->image('hero_image', 'default') }}');background-position: initial;background-repeat: no-repeat;background-size: cover;">
                <div class="row ">
                    <div class="col-md-12">

                    </div>
                </div>
            </section>
        @endif
    @endif
@elseif ((new \Jenssegers\Agent\Agent())->isTablet())
    @if ($pageItem->hasImage('hero_image'))

        @if (!Auth::check())
            <section class="home-banner"
                style="background:url('{{ $pageItem->image('hero_image', 'default') }}');background-position: initial;background-repeat: no-repeat;background-size: cover;">
                <div class="row ">
                    <div class="col-md-12">

                    </div>
                </div>
            </section>
        @endif
    @endif
@else
    @if ($pageItem->hasImage('mobile_hero_image'))

        @if (!Auth::check())
            <section class="home-banner"
                style="background:url('{{ $pageItem->image('mobile_hero_image', 'default') }}');background-position: initial;background-repeat: no-repeat;background-size: cover;">
                <div class="row ">
                    <div class="col-md-12">
                        <div class='device-banner'>
                            <div class="banner-section-text">
                                <button type="button" class="btn btn-overall btn_solid_home_signin me-4" onclick="location.href='{{ route('user.login') }}'">Sign in </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endif
@endif
