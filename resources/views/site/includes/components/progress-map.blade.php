@php
use App\Models\Country;
use App\Models\User;
use App\Models\Branch;
use App\Models\CourseCompletion;

$countries = Country::published()->get();
$branches = Branch::all();
$participants =User::where('published','=',1)->get();
$female =User::where('gender_id','=',1)->get();
$male =User::where('gender_id','=',2)->get();

$certified = CourseCompletion::getCertified();

@endphp

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section special-margin-top">
        <div class="container-fluid">
            <div class="row alignment-class">
                <div class="col-md-12 ">
                 
                            <div class="row">
                                <div class="col-md-5 col-sm-6">
                                    <div class="community-card-content progress-map mt-2 ml-4 ms-5">
                                        

                                        <h2>Our Training Progress at a Glance</h2>

                                        <p>Catch a glimpse of our training progress across branches and the countries we work in.</p>
                                      </div>
        
    
                                  <div class="community-status ms-5">
                                      <div class="row">

                                         <div class="col-6">
                                            <div id="countries" class="stats">
                                                <div class="community-countries">
                                                    <div class="row">

                                                        <div class="col-5">
                                                            <span class="connect-icon icon-countries"></span>
                                                        </div>
                                                        <div class="col-7">
                                                            Our Footprint <span class="status-numbers pr-3">13</span> affiliates served
                                                        </div>
                                                    </div>
                                                    
                                                        
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div id="countries" class="stats">
                                                <div class="community-countries">
                                                    <div class="row">

                                                        <div class="col-5">
                                                            <span class="connect-icon icon-countries"></span>
                                                        </div>
                                                        <div class="col-7">
                                                            Our Footprint <span class="status-numbers pr-3">{{sprintf("%02d",count($countries))}}</span> Countries covered
                                                        </div>
                                                    </div>
                                                    
                                                        
                                                </div>
                                            </div>
                                        </div>

                                       

                                      </div>

                                      <div class="row mt-3">
                                        <div class="col-6">
                                            <div id="participants" class="stats">
                                                <div class="community-partcipants">
                                                    <div class="row">
                                                        <div class="col--md-5 col-sm-4">
                                                            <span class="connect-icon icon-participants"></span> 
                                                        </div>
                                                        <div class="col-md-7 col-sm-8">
                                                            Staff registered for training <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div id="certified" class="stats">
                                                <div class="community-certified">
                                                    <div class="row">
                                                        <div class="col-5">
                                                            <span class="connect-icon icon-participants-certified"></span> 
                                                        </div>
                                                        <div class="col-7">
                                                            Staff trained / certified  <span class="status-numbers">{{sprintf("%02d",count($certified))}}</span> 
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    
                                      </div>


                                      <div class="row mt-3">
                                        <div class="col-6">
                                            <div id="participants" class="stats">
                                                <div class="community-female">
                                                    <div class="row">
                                                        <div class="col--md-5 col-sm-4">
                                                            <span class="connect-icon icon-participants"></span> 
                                                        </div>
                                                        <div class="col-md-7 col-sm-8">
                                                            Female learners participating <span class="status-numbers">{{sprintf("%02d",count($female))}}</span> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div id="certified" class="stats">
                                                <div class="community-male">
                                                    <div class="row">
                                                        <div class="col-5">
                                                            <span class="connect-icon icon-participants-certified"></span> 
                                                        </div>
                                                        <div class="col-7">
                                                            Male learners participating  <span class="status-numbers">{{sprintf("%02d",count($male))}}</span> 
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                   
    
                                  </div>
                                </div>
                                  <div class="col-md-6 col-sm-6">
                                      <div id="map" >
                         
                                      </div>
              
                                  </div>
        
                           
                    </div>

            </div>

        </div>

    </section>
@else
<div class="col-12">     
    <div class="row ">
        
            <div class="card community-card no-border">
                <article>
                    <div class="row">
                        <div class="col-12">

                            <div class="community-card-content mt-4 ml-2">
                                <h2>Our Training Progress at a Glance</h2>

                                <p>Catch a glimpse of our training progress across branches and the countries we work in.</p>
                              </div>
                           
                          </div>
                          <div class="col-12 d-flex flex-row-reverse">
                              <div id="map" >
                 
                              </div>
      
                          </div>

                          <div class="col-12">
                            <div class="community-status">
                                <div class="row">
                                  <div class="col-6">
                                    <div id="participants" class="stats">
                                        <div class="community-partcipants">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span class="connect-icon icon-participants"></span> 
                                                </div>
                                                <div class="col-12">
                                                    <span class="status-numbers">{{sprintf("%02d",count($participants))}}</span> Staff Registered
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="col-6">
                                    <div id="certified" class="stats">
                                        <div class="community-certified">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span class="connect-icon icon-participants-certified"></span> 
                                                </div>
                                                <div class="col-12">
                                                    <span class="status-numbers">{{sprintf("%02d",count($certified))}}</span> Staff Certified
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-6">
                                    <div id="countries" class="stats">
                                        <div class="community-countries">
                                            <div class="row">

                                                <div class="col-12">
                                                    <span class="connect-icon icon-countries"></span>
                                                </div>
                                                <div class="col-12">
                                                    <span class="status-numbers pr-3">{{sprintf("%02d",count($countries))}}</span> Our Footprint
                                                </div>
                                            </div>
                                            
                                                
                                        </div>
                                    </div>
                                  </div>

                                  <div class="col-6">
                                    <div id="branches" class="stats">
                                        <div class="community-branches">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span class="connect-icon icon-participants-certified"></span> 
                                                </div>
                                                <div class="col-12">
                                                    <span class="status-numbers pr-3">{{sprintf("%02d",count($branches))}}</span> Branches 
                                                </div>
                                            </div>
                                           
                                            
                                        </div>
                                    </div>
                                  </div>

                                </div>
                             

                            </div>

                          </div>

                    </div>
                </article>
            </div>


       
        
        
    </div>
    
</div>
@endif
