@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="card enrolled-card training-card mx-2 more-spacing">
        <div class="card-body">
            <div class="row">
                <div class="col-3 pe-0">
                    <div class="responsive-rectangle">
                        <div class="image-wrapper">
                            <img src="{{ $license->course->course_image_uri }} " alt="">
                        </div>
                    </div>
                </div>

                <div class="col-9 ps-3">
                    <div class="enrolled-card-info ps-2 pt-2">
                        <div class="row">
                            <div class="col-3">
                                <div class="title name">
                                    <span>{!! $license->user->first_name . ' ' . $license->user->last_name !!} </span>

                                </div>
                            </div>
                            <div class="col-4">
                                <span class="title-role"> | {!! $license->user->roles->title !!}</span>
                            </div>

                        </div>

                    </div>
                    <hr>
                    <div class="enrolled-card-info  ">
                        <div class="row">
                            <div class="col-3 stats ps-3 pe-0">
                                <div class=" title"><a target="_blank"
                                        href="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}">{{ $license->course->name }}</a>

                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 stats training-stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="icon-calendar training-icon" title="clock"></i>
                                        <span>Enrollment
                                            Date</span></h5>
                                    <p>{{ Carbon\Carbon::parse($license->enrolled_date)->isoFormat('Do MMMM YYYY') }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 stats training-stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="icon-calendar training-icon"
                                            title="clock"></i><span> Course Completion Date </span></h5>
                                    <p>{{ Carbon\Carbon::parse($license->course->end)->isoFormat('Do MMMM YYYY') }}</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-1 stats training-stats ps-0 pe-0">
                                <div>
                                    @if ($license->status)
                                        <h5 class="innerstats"><i class="progress-icon icon-rocket training-icon"
                                                title="clock"></i></h5>
                                        <p class="training-status">Started</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-1 stats training-stats ps-0 pe-0">
                                <div>

                                    @if ($license->getTrainingStatus($license->user->id, $license->course->id) === 'Completed' &&
                                        $license->getTrainingGrade($license->user->id, $license->course->id) >= env('PassMark'))
                                        <h5 class="innerstats"><i
                                                class="progress-icon icon-finished-trophy training-icon"
                                                title="clock"></i></h5>
                                        <p class="training-completed">Finished</p>
                                    @else
                                        <h5 class="innerstats"><i
                                                class="progress-icon icon-not-finished-trophy training-icon"
                                                title="clock"></i></h5>
                                        <p>Finished</p>
                                    @endif


                                </div>
                            </div>


                        </div>
                    </div>

                    <hr>
                    <div class="enrolled-card-info  ">
                        <div class="row">
                            <div class="col-3 stats ps-3 pe-0">
                                <div class=" title training-stats"> Engagement
                                </div>
                            </div>
                            <div class="col-2 stats training-stats ps-0 pe-0">
                                <div>
                                    <h5 class="innerstats"><i class="icon-user training-icon" title="clock"></i>
                                        <span>Follows</span></h5>
                                    <p class="training-center">{{ $license->getfollowers($license->user->id) }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-2 stats training-stats ps-0 pe-0">
                                <div class="tooltip">

                                    <h5 class="innerstats"><a
                                            href={{ $license->getLikeAction($license->user->id, $license->course->id) }}>
                                            <i class="icon-like training-icon" title="clock"></i><span> Likes </span>
                                        </a></h5>
                                    <p class="training-center">{{ $license->getLikes($license->user->id, $license->course->id) }}
                                    </p>
                                    @if($license->getLikes($license->user->id, $license->course->id) > 0)
                                    <span class="tooltiptext">{!! $license->getLikesUsers($license->user->id, $license->course->id) !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-2 stats training-stats ps-0 pe-0">
                                <div class="comment-click"
                                    onclick="showHide('reply-{{ $license->user->id . '-' . $license->course->id }}');showHideReply('response-{{ $license->user->id . '-' . $license->course->id }}');">
                                    <h5 class="innerstats"><i class="progress-icon icon-comment training-icon"
                                            title="clock"></i><span>Comments</span></h5>
                                    <p class="training-center">{{ $license->getCommentCount($license->user->id, $license->course->id) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card-body pt-0" id="response-{{ $license->user->id . '-' . $license->course->id }}"
                        style="display: none">
                        {{-- <summary>Show Replies</summary> --}}
                        @if ($license->getCommentCount($license->user->id, $license->course->id) > 0)
                            <span class="showReplies pt-0">
                                @foreach ($license->getComment($license->user->id, $license->course->id) as $activity)
                                    @include('site.includes.components.training_feed_replies', [
                                        'activity' => $activity,
                                        'license' => $license,
                                    ])
                                @endforeach
                            </span>
                        @endif
                    </div>
                    <form action="{{ route('profile.myComment') }}" method="POST" enctype="multipart/form-data"
                        style="width: 100%">
                        @csrf
                        <input type='hidden' name="follow" value="{{ $license->user->id }}" />
                        <input type='hidden' name="course" value="{{ $license->course->id }}" />

                        <div class="container-fluid mt-3 mb-3 hide"
                            id="reply-{{ $license->user->id . '-' . $license->course->id }}">
                            <div class="main  d-flex">
                                <div class="card " style="border: 0px;width:100%">
                                    <div class="row no-gutters">
                                        <div class="col-2 col-md-1">
                                            @if ($license->user->profile_pic)
                                                <img src="{{ asset('uploads/' . $license->user->profile_pic) }}"
                                                    class="img-fluid profile-avatar " />
                                            @else
                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                    class="img-fluid  profile-avatar " />
                                            @endif
                                        </div>
                                        <div class="col-8">

                                            <div class="card-body card-small pb-0">
                                                <div class="card-text">
                                                    <input type="text" name="id" value="5" hidden=""
                                                        id="replycomment_id">
                                                    <textarea id="replycomment_reply" class="form-control text-input" name="comment" placeholder="Reply"
                                                        data-emojiable="true" data-emoji-input="unicode"></textarea>
                                                </div>
                                                <label for="imageInput" title="attach images"><img
                                                        src="{{ asset('svgs/images.svg') }}"
                                                        class="img-fluid inputImg" width="20" /></label>
                                                <input name="images[]" type="file" id="imageInput" multiple>
                                                <label for="gifInput" title="gif"><img
                                                        src="{{ asset('svgs/gif.svg') }}" class="img-fluid inputImg"
                                                        width="20" /></label>
                                                <input name="gifs[]" type="file" id="gifInput" multiple>

                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="submitbuttons">

                                                <button type="submit"
                                                    class="btn btn-overall btn_solid_primary">Reply</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
@else
    <div class="card enrolled-card training-card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 ">
                    <div class="responsive-rectangle">
                        <div class="image-wrapper">
                            <img src="{{ $license->course->course_image_uri }} " alt="">
                        </div>
                    </div>


                </div>

                <div class="col-12 ">
                    <div class="enrolled-card-info ps-2 pt-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-6 col-7">
                                        <div class="title name">
                                            <span>{!! $license->user->first_name . ' ' . $license->user->last_name !!} </span>

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-5">
                                        <span class="title-role"> | {!! $license->user->roles->title !!}</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-6 col-12">

                                    <h5 class="innerstats"><a target="_blank"
                                            href="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->course_id . '/courseware' }}">{{ $license->course->name }}</a>
                                    </h5>
                                    <h5 class="innerstats"><i class="icon-calendar"
                                        title="clock"></i><span>Enrollment
                                        Date:</span>
                                    <span>{{ Carbon\Carbon::parse($license->enrolled_date)->isoFormat('Do MMMM YYYY') }}</span>
                                </h5>
                                </div>
                                
                                <div class="col-sm-6 col-12">
                                    <div class="row">
                                        <div class="col-4 stats training-stats ps-3 pe-0">
                                            <div>
                                                @if ($license->status)
                                                    <h5 class="innerstats"><i
                                                            class="progress-icon icon-rocket training-icon"
                                                            title="clock"></i></h5>
                                                    <p class="training-status">Started</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-4 stats training-stats ps-3 pe-0">
                                            <div>

                                                @if ($license->getTrainingStatus($license->user->id, $license->course->id) === 'Completed' &&
                                                    $license->getTrainingGrade($license->user->id, $license->course->id) >= env('PassMark'))
                                                    <h5 class="innerstats"><i
                                                            class="progress-icon icon-finished-trophy training-icon"
                                                            title="clock"></i></h5>
                                                    <p class="training-completed">Finished</p>
                                                @else
                                                    <h5 class="innerstats"><i
                                                            class="progress-icon icon-not-finished-trophy training-icon"
                                                            title="clock"></i></h5>
                                                    <p>Finished</p>
                                                @endif


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mt-3">
                                <div class="row">
                                    <div class="col-4 stats training-stats ps-3 pe-0">
                                        <div>
                                            <h5 class="innerstats"><i class="icon-user training-icon"
                                                    title="clock"></i> <span>Follows</span></h5>
                                            <p>{{ $license->getfollowers($license->user->id) }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-4 stats training-stats ps-0 pe-0">
                                        <div>

                                            <h5 class="innerstats"><a
                                                    href={{ $license->getLikeAction($license->user->id, $license->course->id) }}>
                                                    <i class="icon-like training-icon" title="clock"></i><span> Likes
                                                    </span>
                                                </a></h5>
                                            <p>{{ $license->getLikes($license->user->id, $license->course->id) }}
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-4 stats training-stats ps-0 pe-0">
                                        <div class="comment-click"
                                            onclick="showHide('reply-{{ $license->user->id . '-' . $license->course->id }}');showHideReply('response-{{ $license->user->id . '-' . $license->course->id }}');">
                                            <h5 class="innerstats"><i class="progress-icon icon-comment training-icon"
                                                    title="clock"></i><span>Comments</span></h5>
                                            <p>{{ $license->getCommentCount($license->user->id, $license->course->id) }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="card-body pt-0" id="response-{{ $license->user->id.'-'.$license->course->id }}" style="display: none">
                                {{-- <summary>Show Replies</summary> --}}
                                @if($license->getCommentCount($license->user->id, $license->course->id) > 0)
                                <span class="showReplies pt-0">
                                    @foreach($license->getComment($license->user->id, $license->course->id) as $activity)
        
                                       @include('site.includes.components.training_feed_replies', ['activity' => $activity, 'license' => $license])
        
                                    @endforeach
                                </span>
                                @endif
                            </div>
                        <form action="{{ route('profile.myComment') }}" method="POST" enctype="multipart/form-data" style="width: 100%">
                            @csrf
                            <input type='hidden' name="follow" value="{{$license->user->id}}"/>
                            <input type='hidden' name="course" value="{{$license->course->id}}"/>
                            
                            <div class="container-fluid mt-3 mb-3 hide" id="reply-{{ $license->user->id.'-'.$license->course->id}}" >
                                <div class="main  d-flex">
                                    <div class="card " style="border: 0px;width:100%">
                                        <div class="row no-gutters">
                                            <div class="col-2 col-md-1">
                                                @if ($license->user->profile_pic)
                                                <img src="{{ asset('uploads/' . $license->user->profile_pic) }}"
                                                    class="img-fluid profile-avatar " />
                                                @else
                                                    <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid  profile-avatar " />
                                                @endif
                                            </div>
                                            <div class="col-10">
        
                                                <div class="card-body card-small pb-0">
                                                    <div class="card-text">
                                                        <input type="text" name="id" value="5" hidden=""
                                                            id="replycomment_id">
                                                        <textarea id="replycomment_reply"
                                                            class="form-control text-input" name="comment"
                                                            placeholder="Reply" data-emojiable="true"
                                                            data-emoji-input="unicode"></textarea>
                                                    </div>
                                                    <label for="imageInput" title="attach images"><img
                                                            src="{{ asset('svgs/images.svg') }}"
                                                            class="img-fluid inputImg" width="20" /></label>
                                                    <input name="images[]" type="file" id="imageInput" multiple>
                                                    <label for="gifInput" title="gif"><img
                                                            src="{{ asset('svgs/gif.svg') }}"
                                                            class="img-fluid inputImg" width="20" /></label>
                                                    <input name="gifs[]" type="file" id="gifInput" multiple>
                                                   
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="submitbuttons">
                                                    
                                                    <button type="submit" class="btn btn-overall btn_solid_primary">Reply</button>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        </div>

                    </div>
                    {{-- <hr>
                   <div class="enrolled-card-info  ">
                   
                   </div> --}}
                </div>

            </div>

        </div>
    </div>

@endif
