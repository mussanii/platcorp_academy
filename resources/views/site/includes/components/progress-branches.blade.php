@php
use App\Models\Branch;
use App\Models\JobRole;


if (Auth::user()->company_id == 3 || Auth::user()->company_id == 7){

if(!empty($type)){

$branches = JobRole::whereHas('translations', function ($query) use ($type) {
$query->where('job_role_id', '=', $type);
})
->published()
->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();


}elseif(!empty($search)){
$branches = JobRole::whereHas('translations', function ($query) use ($search) {
$query->where('title', 'LIKE', '%' . $search . '%');
})
->published()
->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();

}else{

$branches = JobRole::published()->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();


}


}else{
if(!empty($type)){

$branches = Branch::whereHas('translations', function ($query) use ($type) {
$query->where('branch_id', '=', $type);
})
->published()
->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();


}elseif(!empty($search)){
$branches = Branch::whereHas('translations', function ($query) use ($search) {
$query->where('title', 'LIKE', '%' . $search . '%');
})
->published()
->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();

}else{

$branches = Branch::published()->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();


}

}


@endphp

@include('site.includes.components.progress-branches-filter')

@if(empty($branches))

<div class="col-lg-12 col-md-12 col-sm-6 col-12">
    <div class="course-card mt-3 sessions-card branch-card">
        <article>

            </p> We could not find a branch matching the search</p>
        </article>
    </div>
</div>

@else

@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="row ms-3">
    @foreach ($branches as $branch)
    <div class="col-md-2 col-sm-3 col-xs-12 ">
        <div class="course-card mt-3 sessions-card branch-card">
            <article>
                <div class="course-card-content session-card-content">
                    <h4 class="text-center mt-3"> {{ucwords(strtolower($branch->title ))  }}</h4>

                    <table width="100%" class="mt-3 pl-3 pr-3">
                        <tr>
                            <td><i class="progress-icon icon-user" title="clock"></i></td>
                            <td>Registered</td>
                            <td>{{ $branch->registered }}</td>
                        </tr>

                        <tr>
                            <td><i class="progress-icon icon-tick" title="clock"></i></td>
                            <td>Completed</td>
                            <td>{{ $branch->completed }}</td>
                        </tr>
                    </table>


                </div>
            </article>
        </div>

    </div>
    @endforeach
</div>
@else
<div class="row ms-1">
    @foreach ($branches as $branch)
    <div class="col-lg-2 col-md-2 col-sm-6 col-12 ">
        <div class="course-card mt-3 sessions-card branch-card">
            <article>
                <div class="course-card-content session-card-content">
                    <h4 class="text-center mt-3"> {{ $branch->title }}</h4>

                    <table width="100%" class="mt-3 pl-3 pr-3">
                        <tr>
                            <td><i class="progress-icon icon-user" title="clock"></i></td>
                            <td>Registered</td>
                            <td>{{ $branch->registered }}</td>
                        </tr>

                        <tr>
                            <td><i class="progress-icon icon-tick" title="clock"></i></td>
                            <td>Completed</td>
                            <td>{{ $branch->completed }}</td>
                        </tr>
                    </table>


                </div>
            </article>
        </div>

    </div>
    @endforeach
</div>

@endif
@endif