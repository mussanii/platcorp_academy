@php
use Carbon\Carbon; 
@endphp
<div class="course-card mt-3 live-sessions-card ">
    <article>
        <div class="thumbnail">
           
        @if ($session->hasImage('session_image'))
        <a href="{{ route('session.details', [$session->id]) }}" style="text-decoration:none"><img src="{{ $session->image('session_image', 'default') }}" alt="{{ $session->topic }}" /></a>
            @endif
           
            <div class="overlay"> 
            </div>

        </div>

        <div class="course-card-content">
             <p class="title">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                        <span class="title-span session-panel-title "><a href="{{ route('session.details', [$session->id]) }}" 
                    class="text-decoration-none">{{ $session->topic }}</a></span>
                        </div>
                        <div class="col-6">
                        @if ($session->start_time >= Carbon::now())
                        @if(Auth::user()->company_id == 3 || Auth::user()->company_id == 7 )
                    <span class="float-end">  <a href="{{$session->teams_link}}"  target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary" style="min-width: 70px;">  Join  </button></a></span>
                    @else
                    <span class="float-end">  <a href="{{$session->join_url}}"  target="_blank"><button type="submit" class="btn btn-overall btn_solid_primary" style="min-width: 70px;">  Join  </button></a></span>
                    @endif
                    @else
                    <span class="float-end">  <button type="submit" class="btn btn-overall btn_solid_primary">Watch Video</button></span>
                    @endif
                        </div>
                    </div>
                </div>
            
                   
            </p>
          
        </div>

        <div class="card-footer">
            <div class="col-12">
                <div class="row">
                    <div class="col-6">
                    <p><span class="session-calendar"> <i class="fa fa-calendar-o mx-2"></i>
                    {{ Carbon::parse($session->start_time)->isoFormat('Do MMMM YYYY') }} </span></p>
                    </div>
                    <div class="col-6">
                    <p><span class="float-end session-calendar"> <i class="fa fa-clock-o mx-2"></i>
                  {{isset($session->duration) ? $session->duration . ' Mins':'NA'}} </span></p>
                  </div>
                </div>
            </div>
        </div>

    </article>
</div>
