@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    
        <div class="row justify-content-center">
            <div class="card course-filter mt-2">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <form >
                                <input type="hidden" name="key" value={{ $key }} />
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-3 col-sm-4 no-padding-right ">
                                        
                                                <select name="category" id="" class="form-control mt-4 mb-4 minimal" required>
                                                    <option value=" ">Filter courses by job role</option>
                                                    @foreach ($jobRoles as $theme)
                                                        <option value="{{ $theme->id }}"> {{ $theme->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>

                                    <div class="col-3 col-sm-4 no-padding-right ">
                                        
                                        <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search for a course"/>
                                        @error('search')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                     </div>

                                    <div class="col-2 col-sm-3">
                                        <button type="submit"
                                            class="btn btn-overall btn_solid_primary mt-4 mb-4">Search</button>
                                        <button type="reset" class="btn btn-overall btn_border_primary mt-4 mb-4"
                                            onClick="window.location.href=window.location.href.split('?')[0]">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
 
@else
  
        <div class="card course-filter mb-2 mt-2">
            <div class="col-12">
                <div class="row">

                    <div class="col-12">
                        <form >
                            @csrf
                            <input type="hidden" name="key" value={{ $key }} />
                            <div class="row">

                                <div class="col-11 ms-3">
                                    <select name="category" id="" class="form-control mt-4 mb-2 minimal" required>
                                        <option value=" ">Filter courses by job role</option>
                                        @foreach ($jobRoles as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-11 ms-3">
                                    <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search for a course"/>
                                    @error('search')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-sm-4 col-xs-12 mb-3">
                                    <button type="reset" class="btn btn-overall btn_border_primary me-5 float-end"
                                        onClick="window.location.href=window.location.href.split('?')[0]">Reset</button>
                                    <button type="submit" class="btn btn-overall btn_solid_primary float-end">Search</button>
                                    
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
 


@endif
