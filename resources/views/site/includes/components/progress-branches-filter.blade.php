@php
use App\Models\Branch;
use App\Models\JobRole;
if (Auth::user()->company_id == 3 || Auth::user()->company_id == 7){

$branches = JobRole::published()->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();
}else{
$branches = Branch::published()->where('company_id',Auth::user()->company_id)
->orderBy('position', 'asc')
->get();

}


@endphp

@if ((new \Jenssegers\Agent\Agent())->isDesktop())

<div class="row justify-content-center">
    <div class="card branch-filter ">
        <div class="col-12">
            <div class="row justify-content-center">
                <div class="col-12">
                    <form action="{{ route('pages', ['key' => 'our-progress']) }}" method="POST">
                        @csrf
                        <input type="hidden" name="key" value='our-progress' />
                        <div class="row justify-content-center">
                            <div class="col-3 col-sm-4 no-padding-right ">

                                <select name="category" id="" class="form-control mt-4 mb-4 minimal" required>
                                    @if (Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
                                    <option value=" ">Filter by Job role</option>

                                    @else
                                    <option value=" ">Filter by branch name</option>
                                    @endif
                                    @foreach ($branches as $theme)
                                    <option value="{{ $theme->id }}">{{ucwords(strtolower($theme->title)) }}
                                    </option>
                                    @endforeach
                                </select>
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-3 col-sm-4 no-padding-right ">
                                @if (Auth::user()->company_id == 3 || Auth::user()->company_id == 7)
                                <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search by job role" />

                                @else
                                <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search by branch name" />
                                @endif
                                @error('search')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="col-2 col-sm-3">
                                <button type="submit" class="btn btn-overall btn_solid_primary mt-4 mb-4">Search</button>
                                <button type="reset" class="btn btn-overall btn_border_primary mt-4 mb-4" onClick="window.location.href=window.location.href">Reset</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>

@else

<div class="card branch-filter mb-2">
    <div class="col-12">
        <div class="row">

            <div class="col-12">
                <form action="{{ route('pages', ['key' => 'our-progress']) }}" method="POST">
                    @csrf

                    <input type="hidden" name="key" value='our-progress' />
                    <div class="row">
                        <div class="col-11 ms-3">
                            <select name="jobrole" id="" class="form-control mt-4 mb-2 minimal" required>
                                <option value=" ">Filter by branch</option>
                                @foreach ($branches as $theme)
                                <option value="{{ $theme->id }}"> {{ucwords(strtolower($theme->title)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-11 ms-3">
                            <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search by branch name" />
                            @error('search')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <button type="reset" class="btn btn-overall btn_border_primary me-5 float-end" onClick="window.location.href=window.location.href">Reset</button>
                            <button type="submit" class="btn btn-overall btn_solid_primary float-end">Search</button>

                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

</div>



@endif