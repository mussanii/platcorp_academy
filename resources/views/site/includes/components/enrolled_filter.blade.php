@if ((new \Jenssegers\Agent\Agent())->isDesktop())

    <div class="row justify-content-center">
        <div class="card course-filter mt-2 ms-5 ps-5">
            <div class="col-12">
                <div class="row justify-content-center ps-5">
                    <div class="col-12">
                        <form action="{{ route($key) }}" method="POST">
                            <input type="hidden" name="key" value={{ $key }} />
                            @csrf
                            <div class="row justify-content-center">

                                <div class="col-md-2 col-sm-2 col-xs-12 no-padding-right ">

                                    <select name="year" id="" class="form-control mt-4 mb-2 minimal">
                                        <option value=" ">Filter by year</option>
                                        @foreach ($years as $key => $y)
                                            <option value="{{ $y }}"> {{ $y }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-12 no-padding-right ">

                                    <select name="month" id="" class="form-control mt-4 mb-2 minimal">
                                        <option value=" ">Filter by month</option>
                                        @foreach ($month as $key => $m)
                                            <option value="{{ $key + 1 }}"> {{ $m }}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-12 no-padding-right ">

                                    <select name="status" id="" class="form-control mt-4 mb-4 minimal">
                                        <option value=" ">Filter by status</option>
                                        @foreach ($statuses as $theme)
                                            <option value="{{ $theme->title }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-12 no-padding-right ">
                                        
                                        <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search for a course"/>
                                        @error('search')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                     </div>

                                <div class="col-2 col-sm-3">
                                    <button type="submit"
                                        class="btn btn-overall btn_solid_primary mt-4 mb-4">Search</button>
                                    <button type="reset" class="btn btn-overall btn_border_primary mt-4 mb-4"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@else
    <div class="card course-filter mb-2 mt-2">
        <div class="col-12">
            <div class="row">

                <div class="col-12">
                    <form action="{{ route($key) }}" method="POST">
                        @csrf
                        <input type="hidden" name="key" value={{ $key }} />
                        <div class="row">
                            <div class="col-11 ms-3">
                                <select name="year" id="" class="form-control mt-4 mb-2 minimal" required>
                                    <option value=" ">Filter by year</option>
                                    @foreach ($years as $key => $y)
                                        <option value="{{ $y}}"> {{ $y }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-11 ms-3">
                                <select name="month" id="" class="form-control mt-4 mb-2 minimal" required>
                                    <option value=" ">Filter by month</option>
                                    @foreach ($month as $key => $m)
                                        <option value="{{ $key + 1 }}"> {{ $m }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-11 ms-3">
                                <select name="status" id="" class="form-control mt-4 mb-2 minimal" required>
                                    <option value=" ">Filter by status</option>
                                    @foreach ($statuses as $theme)
                                        <option value="{{ $theme->title }}"> {{ $theme->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-11 ms-3">
                                <input type="text" name="search" class="form-control mt-4 mb-4" placeholder="Search for a course"/>
                                @error('search')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <button type="reset" class="btn btn-overall btn_border_primary me-3 float-end"
                                    onClick="window.location.href=window.location.href">Reset</button>
                                <button type="submit"
                                    class="btn btn-overall btn_solid_primary float-end">Search</button>

                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>

    </div>



@endif
