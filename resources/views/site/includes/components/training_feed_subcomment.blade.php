<div class="card-body d-flex flex-row pt-0 comment-response" id="response-{{ $reply->id }}">
    <div class="col-2 col-sm-1">
        @if ($reply->user->profile_pic)
            <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}" class="img-fluid profile-avatar" />
        @else
            <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid profile-avatar" />
        @endif
    </div>
    <div class="col-10 col-sm-11">
        <div class="upperRow follow-response">
            <div class="commentsContainer">
                <div class="responseCommentRow">
                    <span class="name">
                        {{ $reply->user->name }}
                        <small class="time float-end">
                            {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                        </small>
                    </span>
                </div>
                <div class="commentMain">
                    <p>{{ $reply->comment }}</p>
                </div>

                <div class="commentImgInner">
                    @unless(empty($reply->images))
                        @foreach (json_decode($reply->images) as $image)
                            <img src="{{ asset('images/trainingFeed/' . $image) }}" class="img-fluid main-Commentimg" />
                        @endforeach
                    @endunless
                </div>
            </div>
        </div>
    </div>

</div>