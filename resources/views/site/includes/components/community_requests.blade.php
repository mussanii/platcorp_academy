<div class="dropdown">
    <button class="btn btn-overall btn_solid_secondary dropdown-toggle following" type="button"
      data-toggle="dropdown">
      + Follower Requests &nbsp;<span> {{ $receive }}</span>

    </button>
    <ul class="dropdown-menu community-accepts">
      <span class="dropdown-menu-arrow"></span>
     @if($receive < 1)
      <li>
        <p><span class="drop-name">No requests found</span></p>
      </li>
     @else
      <li  class="item ps-3" >
        @foreach($details as $det)
         <div class="row">
          <div class="col-7 small-padding">
          <p>{{ $det['name'] }}</p>
          </div>
          <div class="col-5">
            <div class="row">
              <div class="col-5 ps-0 pe-0">
                <form method="POST" action="{{route('profile.myFollowAccept')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                <input type="hidden"  value="{{$det['id']}}" name="id"/>
                <input type="hidden"  value="2" name="follow"/>
                <button class="btn btn-overall btn_solid_primary community_accept" type="submit"> Accept</button>
               </form>
              </div>
              <div class="col-6 ps-0">
              
           <form method="POST" action="{{route('profile.myFollowAccept')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
          <input type="hidden"  value="{{$det['id']}}" name="id"/>
          <input type="hidden"  value="0" name="follow"/>
          <button class="btn btn-overall btn_solid_secondary community_decline" type="submit">Decline</button>
         </form>

              </div>
            </div>
           

          </div>
         </div>
        @endforeach
      </li>
      @endif
    </ul>
  </div>