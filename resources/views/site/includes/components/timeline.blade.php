@php 
use App\Models\Menu;

$sidemenus = Menu::where('menu_type_id',2)->published()->orderBy('position', 'asc')->get();
   @endphp
     
     @if ((new \Jenssegers\Agent\Agent())->isDesktop())
     <ul class="nav nav-pills course-tabs justify-content-center" role="tablist">
       @foreach($sidemenus as $el)
      

       <li class="nav-item">
         @if (\Route::current()->getName() == $el['key'] || (strpos(\Request::url(), $el['key']) !== false))
       <a class="nav-link active first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
       @else
       <a class="nav-link first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
       @endif
     </li>



       @endforeach

  
     </ul>
  @else
  <ul class="nav nav-pills course-tabs enrolled-tabs" role="tablist">
    @foreach($sidemenus as $el)
   

    <li class="nav-item">
      @if (\Route::current()->getName() == $el['key'] || (strpos(\Request::url(), $el['key']) !== false))
    <a class="nav-link active first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
    @else
    <a class="nav-link first-tab" href="{{route($el['key'])}}/">{{$el['title']}}</a>
    @endif
  </li>



    @endforeach


  </ul>

  @endif