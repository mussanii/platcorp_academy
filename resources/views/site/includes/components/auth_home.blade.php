



{!! $company->renderBlocks(false) !!}
@if ((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section company-info mt-3 ms-5 me-5 mb-5">
    <div class="container-fluid">
        <div class="col-12 mt-4 ms-5 me-5">
            <div class="row">
                <div class="col-md-12 col-12">

                    </p>{!! $company->our_mission !!}</p>

                </div>
                <div class="col-md-12 col-12">

                    </p>{!! $company->our_vission !!}</p>

                </div>
            </div>
        </div>
        
    </div>
    <div class="container-fluid mt-2">
        @if (count($company->values) > 0)
        <div class="col-12 ms-5 me-5">
            <div class="hrHeading">
                <h1>
                    Our Values
                </h1>
            </div>
        </div>
        @endif
        <div class="col-12 mt-3 ms-5 me-5">
            <div class="row ">
                @if (count($company->values) == 2)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-6 col-12">
                            {!! $values->value !!}

                        </div>
                    @endforeach
                @elseif(count($company->values) == 3)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-4 col-12">

                            {!! $values->value !!}
                        </div>
                    @endforeach
                @elseif(count($company->values) == 4)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-3 col-12">

                            {!! $values->value !!}
                        </div>
                    @endforeach
                @elseif(count($company->values) == 5)
                    @foreach ($company->values as $key => $values)
                        <div class="col-md-2 col-12">
                            {!! $values->value !!}
                        </div>
                    @endforeach

                @endif

            </div>
        </div>





    </div>
</section>
@else 
<section class="general-section company-info mt-3 mb-5">
    <div class="container-fluid">
        <div class="col-12 mt-4 ">
            <div class="row">
                <div class="col-md-12 col-12">

                    </p>{!! $company->our_mission !!}</p>

                </div>
                <div class="col-md-12 col-12">

                    </p>{!! $company->our_vission !!}</p>

                </div>
            </div>
        </div>
        
    </div>
    <div class="container-fluid mt-2">
        @if (count($company->values) > 0)
        <div class="col-12 ">
            <div class="hrHeading">
                <h1>
                    Our Values
                </h1>
            </div>
        </div>
        @endif
        <div class="col-12 mt-3 ">
            <div class="row ">
                @if (count($company->values) == 2)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-6 col-12">
                            {!! $values->value !!}

                        </div>
                    @endforeach
                @elseif(count($company->values) == 3)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-4 col-12">

                            {!! $values->value !!}
                        </div>
                    @endforeach
                @elseif(count($company->values) == 4)
                    @foreach ($company->values as $key => $values)
                        <div class="col-sm-6 col-md-3 col-12">

                            {!! $values->value !!}
                        </div>
                    @endforeach
                @elseif(count($company->values) == 5)
                    @foreach ($company->values as $key => $values)
                        <div class="col-md-2 col-12">
                            {!! $values->value !!}
                        </div>
                    @endforeach

                @endif

            </div>
        </div>





    </div>
</section>


@endif