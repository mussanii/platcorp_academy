
<div class="col-md-3 col-sm-6 col-xs-12 mt-4">
    <div class="card my_community">
        <div class="card-body">
            <div class="row">
                <div class="col-3">

                    <img src="{{ $license->user->profile_pic ? asset('/uploads/'.$license->user->profile_pic) : asset('/uploads/images.jpg') }} "
                        alt="">
                </div>
                <div class="col-9">
                    <div class="community-details">
                        <div class="title">
                            {{ $license->user->name ? $license->user->name : 'name' }}
                        </div>
                        <div class="sub-title">{{ $license->user->roles ? $license->user->roles->title : '' }} </div>
                        <div class="sub-title">{{ $license->user->branches ? $license->user->branches->title : '' }} |
                            {{ $license->user->countries ? $license->user->countries->title : '' }}</div>
                        <div class="sub-title">{{ $license->user->email ? $license->user->email : ' ' }}</div>

                    </div>
                </div>

               
                    @if ($license->getCommunityStatus($license->user->id) == 0)
                    <div class="col-12 mt-3">
                     <form method="POST" action="{{route('profile.myFollow')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                      <input type="hidden"  value="{{$license->getCommunityAction($license->user->id)}}" name="id"/>
                      <input type="hidden"  value="{{$license->user->id}}" name="follow"/>
                      <button class="btn btn-overall btn_solid_primary w-100" type="submit"><i class="fas fa-user-check"></i> Follow</button>
                     </form>
                    </div>
                    {{-- <a class="btn btn-overall btn_solid_primary w-100" href="{{ $license->getCommunityAction($license->user->id) }}">
                    <i class="fas fa-user-check"></i> Follow</a> --}}

                    @elseif($license->getCommunityStatus($license->user->id) == 1)
                    <div class="col-12 mt-3">
                    <p class="btn btn-overall btn_solid_tertiary w-100" >
                        <i class="fas fa-user-check"></i> Requested</p>
                    </div>

                    @elseif($license->getCommunityStatus($license->user->id) == 2)
                    <div class="col-12 mt-2">
                   
                         <p class="community_following">Status: Following</p>


                        
                            <form method="POST" action="{{route('profile.myFollow')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                              <input type="hidden"  value="{{$license->getCommunityAction($license->user->id)}}" name="id"/>
                              <input type="hidden"  value="{{$license->user->id}}" name="follow"/>
                              <button class="btn btn-overall btn_solid_secondary w-100" type="submit"><i class="fas fa-user-check"></i> Unfollow</button>
                             </form>
                       
                            </div>


                  
                    @endif
                
            </div>


        </div>
    </div>
</div>
