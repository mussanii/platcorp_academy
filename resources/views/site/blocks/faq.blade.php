@if((new \Jenssegers\Agent\Agent())->isDesktop())

			<div class="col-lg-12">
				<div class="row mt-5 mb-5 ms-5 me-5" >
                   

                            @foreach ($block->children as $item)
                            @include('site.blocks.faq_repeater', $item)
                        @endforeach

						{{-- @php dd($block->translatedinput('url_video'));@endphp --}}
						
					
			</div>
		</div>
	
@else 

<section class="general-section" style="margin:0;">

		
			<div class="col-lg-12">
				<div class="row mt-3 mb-5" >
                    <div class="col-12 col-12">
						<div >
							
							@foreach ($block->children as $item)
                            @include('site.blocks.faq_repeater', $item)
                        @endforeach

						</div>
					</div>
					
			</div>
		
	</div>

</section>


@endif