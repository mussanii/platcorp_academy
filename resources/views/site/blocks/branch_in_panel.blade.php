@php
    
    $panels = $block->children;
    
@endphp


@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <section class="general-section">
        <div class="container-fluid mobile-container-fluid">
            <div class="col-12 ">
                <ul class="nav our-progress-tab mt-4 " role="tablist">
                    @foreach ($panels as $key => $item)
                        <li class="nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $item->id }}-tab"
                                data-toggle="tab" href="#parent-{{ $item->id }}" role="tab"
                                aria-controls="parent-{{ $item->id }}" aria-selected="true">
                                {{ $item->translatedinput('panel_title') }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">
                    @foreach ($panels as $key => $item)
                        @if ($key == 0)
                            <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                                id="parent-{{ $item->id }}" role="tabpanel"
                                aria-labelledby="parent-{{ $item->id }}-tab">
                                <div class="row alignment-class mt-3">
                                    @include('site.includes.components.progress-branches')
                                </div>

                            </div>
                        @else
                            <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                                id="parent-{{ $item->id }}" role="tabpanel"
                                aria-labelledby="parent-{{ $item->id }}-tab">
                                <div class="row alignment-class mt-5">
                                    @include('site.includes.components.progress-map')
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>


            </div>

        </div>

    </section>
@else
    <section class="general-section">
        <div class="container-fluid mobile-container-fluid">
            <div class="col-12 ">
                <ul class="nav our-progress-tab mt-4 " role="tablist">
                    @foreach ($panels as $key => $item)
                        <li class="nav-item">
                            <a class="faq-link {{ $key == 0 ? 'active' : '' }}" id="parent-{{ $item->id }}-tab"
                                data-toggle="tab" href="#parent-{{ $item->id }}" role="tab"
                                aria-controls="parent-{{ $item->id }}" aria-selected="true">
                                {{ $item->translatedinput('panel_title') }}
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">
                    @foreach ($panels as $key => $item)
                        @if ($key == 0)
                            <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                                id="parent-{{ $item->id }}" role="tabpanel"
                                aria-labelledby="parent-{{ $item->id }}-tab">
                                <div class="row alignment-class mt-3">
                                    @include('site.includes.components.progress-branches')
                                </div>

                            </div>
                        @else
                            <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}"
                                id="parent-{{ $item->id }}" role="tabpanel"
                                aria-labelledby="parent-{{ $item->id }}-tab">
                                <div class="row alignment-class mt-2 mb-2">
                                    @include('site.includes.components.progress-map')
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>


            </div>

        </div>





    </section>
@endif
