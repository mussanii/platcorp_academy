@php
    
    use App\Models\JobroleCourse;
    use App\Models\JobRole;
    use App\Models\Course;
    
    if ((new \Jenssegers\Agent\Agent())->isDesktop()) {

        if(Auth::user()->company_id == 3 || Auth::user()->company_id == 6){
          
            if (!empty($jobRole)) {
            $role = JobroleCourse::whereHas('courses', function ($query){
                   $query->where('company_id', Auth::user()->company_id);
                
                })->where('job_role_id', $jobRole)
                ->with('courses')
                ->simplePaginate(4);

            $jobr = JobRole::where('id', $jobRole)->first();

        } elseif (!empty($search)) {
            $role = JobroleCourse::whereHas('courses', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%');
              })->whereHas('courses', function ($query){
                $query->where('company_id', Auth::user()->company_id);
                
                })->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);
    
            // $role = Course::where(function ($query) use($search) {
            //     $query->where('name', 'LIKE', '%' . $search . '%');
            //     })->where('status', 1)->published()->paginate(4);
        } else {
            $term = Auth::user()->job_role_id;
            $role = JobroleCourse::whereHas('courses', function ($query){
                $query->where('company_id', Auth::user()->company_id);
                
                })
                ->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);        
            //
        }

        }else{
        
            if (!empty($jobRole)) {
            $role = JobroleCourse::whereHas('courses', function ($query){
                   $query->where('company_id', Auth::user()->company_id)
                         ->orWhereNull('company_id');
                
                })->where('job_role_id', $jobRole)
                ->with('courses')
                ->simplePaginate(4);

            $jobr = JobRole::where('id', $jobRole)->first();

        } elseif (!empty($search)) {
            $role = JobroleCourse::whereHas('courses', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%');
              })->whereHas('courses', function ($query){
                $query->where('company_id', Auth::user()->company_id)
                      ->orWhereNull('company_id');
                
                })->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);
    
            // $role = Course::where(function ($query) use($search) {
            //     $query->where('name', 'LIKE', '%' . $search . '%');
            //     })->where('status', 1)->published()->paginate(4);
        } else {
            $term = Auth::user()->job_role_id;
            $role = JobroleCourse::whereHas('courses', function ($query){
                $query->where('company_id', Auth::user()->company_id)
                      ->orWhereNull('company_id');
                
                })
                ->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->simplePaginate(4);


        
            //
        }

        }


       
    } else {

        if(Auth::user()->company_id == 3 || Auth::user()->company_id == 6){

            if (!empty($jobRole)) {
            $role = JobroleCourse::whereHas('courses', function ($query){
                  $query->where('company_id', Auth::user()->company_id);

                })->where('job_role_id', $jobRole)
                ->with('courses')
                ->paginate(2);
            $jobr = JobRole::where('id', $jobRole)->first();
        } else {
            $role = JobroleCourse::whereHas('courses', function ($query){
                    
                  $query->where('company_id', Auth::user()->company_id);

                })->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->paginate(2);
        }


        }else{

            if (!empty($jobRole)) {
            $role = JobroleCourse::whereHas('courses', function ($query){
                  $query->where('company_id', Auth::user()->company_id)
                        ->orWhereNull('company_id');

                })->where('job_role_id', $jobRole)
                ->with('courses')
                ->paginate(2);
            $jobr = JobRole::where('id', $jobRole)->first();
        } else {
            $role = JobroleCourse::whereHas('courses', function ($query){
                    
                  $query->where('company_id', Auth::user()->company_id)
                        ->orWhereNull('company_id');

                })->where('job_role_id', Auth::user()->job_role_id)
                ->with('courses')
                ->paginate(2);
        }

        }
        
    }
    
@endphp

<section class="general-section">
    <div class="container-fluid mobile-container-fluid">
        <div class="col-12 ">
            <div class="hrHeading">
                <h2 class="line-header ">

                    @if (!empty($jobRole))
                        {{ 'Courses for ' . ucwords(strtolower($jobr->title)) }}
                    @else
                        {{ $block->translatedinput('title') }}
                    @endif

                </h2>
            </div>
        </div>
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row alignment-class mt-3">

            @if (count($role) > 0)
                @foreach ($role as $related)
                    <div class="col-sm-6 col-md-3 col-12">
                        @include('site.includes.components.course-card', [
                            'course' => $related->courses,
                        ])
                    </div>
                @endforeach
            @else
                <p class="text-center no-courses"> There are no courses for your Job role</p>
            @endif
        </div>


        <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}
        </div>

        @else
    
            <div class="row mt-3">
                @if (count($role) > 0)
                    @foreach ($role as $related)
                        <div class="col-sm-6 col-md-3 col-12">
                            @include('site.includes.components.course-card', [
                                'course' => $related->courses,
                            ])
                        </div>
                    @endforeach
                @else
                    <p class="text-center no-courses"> There are no courses for your Job role</p>
                @endif
            </div>

            <div class="row justify-content-center  pagination-spacing">{!! $role->links() !!}</div>



        @endif

    </div>
</section>
