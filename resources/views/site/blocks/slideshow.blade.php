
<section>
    <div class="row ">
        <div class="col-md-12">


            @if ($block->children)
                <div id="carouselExampleIndicators" class="carousel slide first-slider" data-ride="carousel"
                    data-interval="false">

                    <div class="carousel-inner">

                        @foreach ($block->children as $key => $item)
                            <div class="carousel-item item {{ $key == 0 ? ' active' : '' }}">
                               
                                    <img src="{!! $item->image('slide_image', 'default')!!}"
                                        alt="" />
                                   
                                   <div class="carousel-item-overlay">
                                    <div class="carousel-caption">
                                       <p>{!!$item->content['description']['en']!!}</p>

                                       <a href={{$item->content['url']['en']}} class="btn btn-overall btn_solid_secondary">
                                        {{$item->content['link_text']['en']}}</a>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                        data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                        data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            @else
            @endif


        </div>

    </div>
    {{-- <button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
            <i class="fa fa-arrow-down fa-2x"></i>
        </button> --}}
</section>