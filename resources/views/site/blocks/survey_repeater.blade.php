@php
use App\Models\Survery;
@endphp

<fieldset style="padding:10px;">
    <label ><b>{{$item->content['survey_question']['en']}}</b></label><br>
   @if($item->content['option_id'] == 2)
   <div class="star-rating ms-2">
      <span class="fa fa-star-o" data-rating="1"></span>
      <span class="fa fa-star-o" data-rating="2"></span>
      <span class="fa fa-star-o" data-rating="3"></span>
      <span class="fa fa-star-o" data-rating="4"></span>
      <span class="fa fa-star-o" data-rating="5"></span>
      <span class="fa fa-star-o" data-rating="6"></span>
      <span class="fa fa-star-o" data-rating="7"></span>
      <span class="fa fa-star-o" data-rating="8"></span>
      <span class="fa fa-star-o" data-rating="9"></span>
      <span class="fa fa-star-o" data-rating="10"></span>
      <input type="hidden" name="{{$item->content['survey_question']['en']}}" class="rating-value" value="" required>
    </div>

   @elseif($item->content['option_id'] == 1)

    <textarea class="form-control" rows="3" id="comment" name="{{$item->content['survey_question']['en']}}" required></textarea>

    @else

    {!! Survery::getRadio($item->content['option_id'], $item->content['survey_question']['en']) !!}



   @endif

   </fieldset>
