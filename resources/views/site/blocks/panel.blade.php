@php
use App\Models\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\JobRole;
$jobRoles = JobRole::where('company_id', Auth::user()->company_id)->published()->orderBy('position', 'asc')->get();
$panels = $block->children;
@endphp

<style>
    .session-panel-mobile .nav-tabs li{
width:10%;
    }
</style>

<div class="container-fluid ">
    <div role="tabpanel " class="session-panel">
        <div class="col-md-12">
            <div class="row">
                @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
              
                            @foreach ($panels as $key => $item)
                           
                            <li role="presentation" class="tabs-spacing {{ $key == 0 ? 'active' : '' }}">
                                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab" style="text-decoration:none"
                                    onClick="makeActive('home{{ $item->id }}')">{{ $item->translatedinput('panel_title') }}</a>
                            </li>
                            @endforeach
                          
                        </ul>
                        
                    </div>
                @else
                    <div class="row mt-3  session-panel-mobile">
                        <ul class="nav nav-tabs list-group list-group-horizontal" role="tablist">
                            @foreach ($panels as $key => $item)
                            <li role="presentation" class="tabs-spacing {{ $key == 0 ? 'active' : '' }}">
                                <a href="#home{{ $item->id }}" aria-controls="home" role="tab" data-toggle="tab" style="text-decoration:none"
                                    onClick="makeActive('home{{ $item->id }}')">{{ $item->translatedinput('panel_title') }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
          
        </div>
        <div class="tab-content">


            @foreach ($panels as $key => $item)
                @if ($key ==1)
                  
                 
                    @php 
                    $search = request()->query('search');
                    $category = request()->query('category');
                    $user_company = Auth::user()->company_id;
                
                    $sessions = Session::where('start_time', '>=', Carbon::now())
                        ->where('company_id', $user_company)
                        ->where(function($query) use ($search,$category){
                            if($search) {
                                $query->where('topic', 'like', '%' . $search . '%');
                            }
                            if($category) {
                                $query->where('job_role_id', $category);
                            }
                        })->get();
                  
                     @endphp
                @elseif($key == 2)
                    @php
                    $user_company = Auth::user()->company_id;
                    $search = request()->query('search');
                    $category = request()->query('category');
                    $sessions = Session::where('start_time', '<', Carbon::today()->toDateString())
                            ->where('company_id', $user_company)
                            ->when($search, function($query) use ($search) {
                                $query->where('topic', 'like', '%' . $search . '%');
                            })
                            ->when($category, function($query) use ($category) {
                                $query->where('job_role_id', $category);
                            })
                            ->orderBy('created_at', 'desc')
                            ->paginate(12);
                           
                     
                    @endphp
                    @else
                    @php
                    $search = request()->query('search');
                    $category = request()->query('category');
                    $user_company = Auth::user()->company_id;

                    $session_calendar_week = Session::whereBetween('start_time', 
                            [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]
                        )->where(function($query) use($user_company, $search, $category) {
                            $query->where('start_time', '>=', Carbon::today()->toDateString())
                                ->where('company_id', $user_company)
                                ->when($search, function($q) use ($search) {
                                        $q->where('topic', 'like', '%' . $search . '%');
                                })
                                ->when($category, function($q) use ($category) {
                                        $q->where('job_role_id', $category);
                                });
                            })->orderBy('created_at', 'desc')
                            ->paginate(5);

                            $session_calendar_monthly = Session::whereBetween('start_time', [
                                Carbon::now()->startOfMonth(),
                                Carbon::now()->endOfMonth()
                            ])->where(function($query) use($user_company, $search, $category) {
                                $query->where('start_time', '>=', Carbon::today()->toDateString())
                                    ->where('company_id', $user_company)
                                    ->when($search, function($q) use ($search) {
                                            $q->where('topic', 'like', '%' . $search . '%');
                                    })
                                    ->when($category, function($q) use ($category) {
                                            $q->where('job_role_id', $category);
                                    });
                                })->orderBy('created_at', 'desc')
                                ->paginate(5);

                    $session_calendar_quarter = Session::whereBetween('created_at', [Carbon::now(), Carbon::now()->addMonths(3)])
                    ->orWhere(function($query) use($user_company, $search, $category){
                      $query->whereBetween('start_time', [Carbon::now(), Carbon::now()->addMonths(3)]);
                    })
                            ->where (function($query) use($user_company, $search, $category) {
                                $query->where('start_time', '>=', Carbon::today()->toDateString())
                                ->where('company_id', $user_company)
                                ->when($search, function($q) use ($search) {
                                        $q->where('topic', 'like', '%' . $search . '%');
                                })
                                ->when($category, function($q) use ($category) {
                                        $q->where('job_role_id', $category);
                                })
                                ->orderBy('created_at', 'desc');
                            })->paginate(5);
                    @endphp
                    
                @endif
                <div role="tabpanel" class="tab-pane {{ $key == 0 ? 'active' : '' }}" id="home{{ $item->id }}">
                    <div class="row col-sm-12 col-md-12 col-12">
                    @include('site.includes.components.session_filter',['jobRoles' =>$jobRoles])
                    </div>
                    @if($key !=0)
                    <h2 class="title">{{ $item->translatedinput('panel_title') }}</h2>
                    @endif
                    @if($key ===0)
                    <div class="row">
                    @include('site.includes.components.session-calendar',
                    ['session_calendar_monthly'=>$session_calendar_monthly,
                    'session_calendar_quarter'=> $session_calendar_quarter,
                     'session_calendar_week'=>$session_calendar_week])
                    </div>
                   
                   
                    @elseif(($key ==1) && (count($sessions) >= 1))

                    <div class="row">

                            @foreach ($sessions as $session)
                                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                    @include(
                                        'site.includes.components.session-card',
                                        [
                                            'session' => $session,
                                        ]
                                    )
                                </div>
                            @endforeach
                        </div>
                        @elseif(($key == 2) && (count($sessions) >=1))

                        <div class="row">

                        @foreach ($sessions as $session)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                @include(
                                    'site.includes.components.session-card',
                                    [
                                        'session' => $session,
                                    ]
                                )
                            </div>
                        @endforeach
                        </div>
                    
                    @elseif(($key ==1) && (count($sessions) < 1))
                    
                        <p class="mt-4 sub_title">Currently no sessions are scheduled. Keep checking this space for
                            upcoming
                            sessions. For now feel welcome to interact with what we have covered so far on Past
                            Sessions..
                        </p>
                        @else
                        <p class="mt-4 sub_title">Currently no sessions are scheduled. Keep checking this space for
                            upcoming
                            sessions.
                        </p>
                 
                    @endif
                    

                </div>
            @endforeach
        </div>
    </div>


</div>
@section('js')
<script>
    function makeActive(id) {
        let tabContent = document.querySelectorAll('.tab-content .tab-pane');
        let tabNav = document.querySelectorAll('.nav-tabs li');

        tabContent.forEach(function(tab) {
            tab.classList.remove('active');
        });
        tabNav.forEach(function(tab) {
            tab.classList.remove('active');
        });

        document.querySelector(`#${id}`).classList.add('active');
        document.querySelector(`a[href="#${id}"]`).parentNode.classList.add('active');
    }
</script>
@endsection