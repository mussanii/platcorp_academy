@extends('layouts.app')

@section('content')
<section class="login-page">
    <div class="col-12">
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row justify-content-center">
            <div class="col-6 login-image d-none d-md-block">
                <!-- login image background -->
                <div class="mt-5 login-height">
                    <!-- Avartar -->
                <div class="login-center">
                <h1 class="text-white no-bold">Welcome to the</h1>
                <h1 class="text-white">Platcorp Group Academy</h1>
                </div>
                </div>
                <div class="login-center">
                <p class="text-white">Learn anywhere anytime using any device</p>
                </div>
            </div>
            <div class="col-6 col-xs-12">
               <div class="mt-5 page-height">
                    <!-- Avartar -->
                <div class="options-center">
                    <span><i class="signin-icon signin-avatar"></i></span>

                     <h1 class="mt-2">Sign in</h1>
                     {{-- <p class="mt-3"><small>For single sign on, select your company.</small></p> --}}

                    <div class="form-group row justify-content-center">
                        <!-- Google -->
                        <div class="col-sm-9 col-md-8 col-xs-12 center">
{{--                             
                            <form action="" class="auth-form">
                                <select name="authcompany" id="auth-company" class="form-control auth-company" onchange="changeEventHandler(this);">
                                 <option value="">Select Company</option>
                                 @foreach($companies as $company)
                                <option value="{{$company->id}}">{{$company->title}}</option>
                                @endforeach
                                </select>

                            </form> --}}
                                <a href="{{ url('auth/google') }}"  class="btn btn-lg btn-signin-google btn-block mt-4">
                                    <i class="signin-button-icon signin-google-icon"></i> <p>Sign in with Google </p>
                                </a>
                           
                        </div>

                        <!-- Microsoft -->
                        <div class="col-sm-9 col-md-8 col-xs-12 center">
                            <a href="{{ url('/auth/microsoft') }}" class="btn btn-lg btn-signin-google btn-block mt-3">
                                <i class="signin-button-icon signin-microsoft-icon"></i> <p>Sign in with Microsoft </p>
                            </a>
                        </div> 


                         <p class="other-options"> OR </p>

                              <!-- User Code -->
                        <div class="col-sm-9 col-md-8 col-xs-12 center">
                            <a href="{{ url('/login/userCode') }}" class="btn btn-lg btn-signin-usercode btn-block mt-3">
                                 <p>Sign in with Usercode </p>
                            </a>
                        </div>
                    </div>
                </div>
                </div>
                    
               </div>
                
            
            </div>

        </div>

    @else
   
    <div class="row ">
        <div class="col-12 col-xs-12">
           <div class="mt-3 page-height">
                <!-- Avartar -->
            <div class="options-center">
                <span><i class="signin-icon signin-avatar"></i></span>

                 <h1 class="mt-2">Sign in</h1>

                <div class="form-group row justify-content-center">
                    <!-- Google -->
                    <div class="col-11 col-xs-12 center">
                            <a href="{{ url('auth/google') }}"  class="btn btn-lg btn-signin-google btn-block mt-4">
                                <i class="signin-button-icon signin-google-icon"></i> <p>Sign in with Google </p>
                            </a>
                       
                    </div>

                          <!-- Microsoft -->
                    <div class="col-11 col-xs-12 center">
                        <a href="{{ url('/auth/microsoft') }}" class="btn btn-lg btn-signin-google btn-block mt-3">
                            <i class="signin-button-icon signin-microsoft-icon"></i> <p>Sign in with Microsoft </p>
                        </a>
                    </div>


                     <p class="other-options"> OR </p>

                          <!-- User Code -->
                    <div class="col-11 col-xs-12 center">
                        <a href="{{ url('/login/userCode') }}" class="btn btn-lg btn-signin-usercode btn-block mt-3">
                             <p>Sign in with Usercode </p>
                        </a>
                    </div>
                </div>
            </div>
            </div>
                
           </div>
            
        
        </div>

    </div>



    @endif

    </div>
</section>
@endsection

@push('scripts')
<script>
// 
function changeEventHandler(sel){

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  var urlPass = "{{ route('sso.option') }}";

    var company = sel.value;

    $.ajax({
        data: {company: company},
        url: urlPass,
        type: "POST",
        dataType: 'json',
        success: function(data) {
 
            if (data.status == 'error') {
                $("#notificationModal").find(".modal-body").append(data.success);
                $('#notificationModal').modal('show');


            } else {

            }
            // $('#changePass').modal('hide');
        },

    });

    console.log();

}
</script>
@endpush