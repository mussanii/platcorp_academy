@extends('layouts.app')

@section('content')
<section class="login-page">
    <div class="col-12">
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row justify-content-center">
            <div class="col-6 login-image d-none d-md-block">
                <!-- login image background -->
                <div class="mt-5 login-height">
                    <!-- Avartar -->
                <div class="login-center">
                <h1 class="text-white no-bold">Welcome to</h1>
                <h1 class="text-white">Platcorp Academy</h1>
                </div>
                </div>
                <div class="login-center">
                <p class="text-white">Learn anywhere anytime using any device</p>
                </div>
            </div>
            <div class="col-6 col-xs-12">
               <div class="mt-5 page-height">
                    <!-- Avartar -->
                <div class="options-center">
                    <span><i class="signin-icon signin-avatar"></i></span>

                     <h1 class="mt-2">Sign in</h1>

                    <div class="form-group row justify-content-center">
                        <!-- Google -->
                        <div class="col-7 col-xs-12 center">
                                <a href="{{ url('auth/google') }}"  class="btn btn-lg btn-signin-google btn-block mt-4">
                                    <i class="signin-button-icon signin-google-icon"></i> <p>Sign in with Google </p>
                                </a>
                           
                        </div>

                              <!-- Microsoft -->
                        <div class="col-7 col-xs-12 center">
                            <a href="{{ url('/auth/microsoft') }}" class="btn btn-lg btn-signin-google btn-block mt-3">
                                <i class="signin-button-icon signin-microsoft-icon"></i> <p>Sign in with Microsoft </p>
                            </a>
                        </div>


                         <p class="other-options"> OR </p>

                              <!-- User Code -->
                        <div class="col-7 col-xs-12 center">
                            <a href="{{ url('/login/userCode') }}" class="btn btn-lg btn-signin-usercode btn-block mt-3">
                                 <p>Sign in with Usercode </p>
                            </a>
                        </div>
                    </div>
                </div>
                </div>
                    
               </div>
                
            
            </div>

        </div>

    @else
   
    <div class="row ">
        <div class="col-12 col-xs-12">
           <div class="mt-3 page-height">
                <!-- Avartar -->
            <div class="options-center">
                <span><i class="signin-icon signin-avatar"></i></span>

                 <h1 class="mt-2">Sign in</h1>

                <div class="form-group row justify-content-center">
                    <!-- Google -->
                    <div class="col-10 col-xs-12 center">
                            <a href="{{ url('auth/google') }}"  class="btn btn-lg btn-signin-google btn-block mt-4">
                                <i class="signin-button-icon signin-google-icon"></i> <p>Sign in with Google </p>
                            </a>
                       
                    </div>

                          <!-- Microsoft -->
                    <div class="col-10 col-xs-12 center">
                        <a href="{{ url('/auth/microsoft') }}" class="btn btn-lg btn-signin-google btn-block mt-3">
                            <i class="signin-button-icon signin-microsoft-icon"></i> <p>Sign in with Microsoft </p>
                        </a>
                    </div>


                     <p class="other-options"> OR </p>

                          <!-- User Code -->
                    <div class="col-10 col-xs-12 center">
                        <a href="{{ url('/login/userCode') }}" class="btn btn-lg btn-signin-usercode btn-block mt-3">
                             <p>Sign in with Usercode </p>
                        </a>
                    </div>
                </div>
            </div>
            </div>
                
           </div>
            
        
        </div>

    </div>



    @endif

    </div>
</section>
@endsection