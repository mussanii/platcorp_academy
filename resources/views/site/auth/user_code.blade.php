
@extends('layouts.app')


@section('content')
<section class="login-page">
    <div class="col-12">
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="row justify-content-center">
            <div class="col-6 login-image d-none d-md-block">
                <div class="mt-5 login-height">
                    <!-- Avartar -->
                <div class="login-center">
                <h1 class="text-white no-bold">Welcome to the</h1>
                <h1 class="text-white">Platcorp Group Academy</h1>
                </div>
                </div>
                <div class="login-center">
                <p class="text-white">Learn anywhere anytime using any device</p>
                </div>
            </div>
            <div class="col-6 col-xs-12">
               <div class="mt-5 page-height">
                    <!-- Avartar -->
                <div class="options-center">
                    <span><i class="signin-icon signin-avatar"></i></span>

                    <h1 class="mt-2">Sign In</h1>

                    @if(session()->has('login_error'))
                    <div class="form-group row justify-content-center">
                        <div class="col-md-7 col-xs-12">
                            <div class="form-check">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {!! session()->get('login_error') !!}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    @php session()->forget('login_error') @endphp
                    @endif
                    <form method="POST" action="{{ route('user.login.post') }}" id="loginForm" class="form-login">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        {{-- @csrf --}}
                        <div class="form-group row justify-content-center">
                            <div class="col-md-7 col-xs-12">
                                <div class="form-check">
                                    <label>Username</label>
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username"  autocomplete="off" placeholder="Enter Username" autofocus>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row justify-content-center form-password">
                            <div class="col-md-7 col-xs-12">
                                <div class="form-check ">
                                    <label> Password  </label>
                                <div class="input-group mb-3">
                                <input id="login_password" type="password" class="pass form-control @error('login_password') is-invalid @enderror" placeholder="Enter Password"  name="login_password">
                                <div class="input-group-append pass-view">
                                <i class="far fa-eye"></i>
                                <i class="far fa-eye-slash" style="display: none;"></i>
                                </div>
                                @error('login_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="row">
                                <div class="col-5">
                                    <div class="form-check remember-me float-end">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" />
                                        <label class="form-check-label text-small" for="remember">Remember me</label>
                                      </div>
                                </div>
    
                                <div class="col-5">
                                    <button type="submit" class="btn btn-overall btn_solid_primary float-end btn-spacing">Sign in <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            

                            </div>
                           
                          </div>
                      
                        
                        </form>
                </div>
                </div>
                    
               </div>
                
            
            </div>

        </div>

        @else

        <div class="row">
            <div class="col-6 login-image d-none d-md-block">
                <div class="mt-5 login-height">
                    <!-- Avartar -->
                <div class="login-center">
                <h1 class="text-white no-bold">Welcome to</h1>
                <h1 class="text-white">Platcorp Academy</h1>
                </div>
                </div>
                <div class="login-center">
                <p class="text-white">Learn anywhere anytime using any device</p>
                </div>
            </div>
            <div class="col-12 col-xs-12">
               <div class="mt-3 page-height">
                    <!-- Avartar -->
                <div class="options-center">
                    <span><i class="signin-icon signin-avatar"></i></span>

                    <h1 class="mt-2">Sign In</h1>

                    @if(session()->has('login_error'))
                    <div class="form-group row justify-content-center">
                        <div class="col-md-7 col-xs-12">
                            <div class="form-check">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {!! session()->get('login_error') !!}
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            </div>
                        </div>
                    </div>
                    @php session()->forget('login_error') @endphp
                    @endif
                    <form method="POST" action="{{ route('user.login') }}" id="loginForm" class="form-login">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        {{-- @csrf --}}
                        <div class="form-group row justify-content-center">
                            <div class="col-md-7 col-11">
                                <div class="form-check">
                                    <label>Username</label>
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username"  autocomplete="off" placeholder="Enter Username" autofocus>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row justify-content-center form-password">
                            <div class="col-md-7 col-11">
                                <div class="form-check ">
                                    <label> Password  </label>
                                <div class="input-group mb-3">
                                <input id="login_password" type="password" class="pass form-control @error('login_password') is-invalid @enderror" placeholder="Enter Password"  name="login_password">
                                <div class="input-group-append pass-view">
                                <i class="far fa-eye"></i>
                                <i class="far fa-eye-slash" style="display: none;"></i>
                                </div>
                                @error('login_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="row">
                                <div class="col-7">
                                    <div class="form-check remember-me float-end">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" />
                                        <label class="form-check-label text-small" for="remember">Remember me</label>
                                      </div>
                                </div>
    
                                <div class="col-5">
                                    <button type="submit" class="btn btn-overall btn_solid_primary float-end btn-spacing">Sign in <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            

                            </div>
                           
                          </div>
                      
                        
                        </form>
                </div>
                </div>
                    
               </div>
                
            
            </div>

        </div>


        @endif

    </div>
</section>
@endsection


@section('js')
<script>
$(document).on('click', '.pass-view', function(event){
	console.log("clicked");
	var $open = $(this).children('.fa-eye');
	var $close = $(this).children('.fa-eye-slash');
	var $pass = $(this).siblings('.pass');
	if($open.is(':visible')){
		$close.show();
		$open.hide();
		document.getElementById('login_password').setAttribute('type', 'text');
	} else {
		$close.hide();
		$open.show();
        document.getElementById('login_password').setAttribute('type', 'password');

	}

	
});


</script>

@endsection