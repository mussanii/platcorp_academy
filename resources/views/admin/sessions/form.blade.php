@extends('twill::layouts.form')

@section('contentFields')

@formField('wysiwyg', [
    'name'=> 'description',
    'label' => 'Session Agenda',
    'toolbarOptions'=>[
        ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["script" => "super"],
      ["script" => "sub"],
      "blockquote",
      "code-block",
      ['list' => 'ordered'],
      ['list' => 'bullet'],
      ['indent' => '-1'],
      ['indent' => '+1'],
      ["align" => []],
      ["direction" => "rtl"],
      'link',
      'image',
      "clean",
    ],
    'placeholder' => 'Session Agenda',
	'maxlength' => 2000,
])

    @formField('date_picker', [
		'name' => 'start_time',
		'label' => 'Session Date',
])

@formField('input', [
        'name' => 'duration',
        'label' => 'Duration',
        'maxlength' => 100
])

@formField('select', [
    'name' => 'country_id',
    'label' => 'Country',
    'placeholder' => 'Select Country',
    'options' => collect($countryList ?? ''),
    ])

    @formField('select', [
    'name' => 'company_id',
    'label' => 'Company',
    'placeholder' => 'Select Company',
    'options' => collect($companyList ?? ''),
    ])


    @formField('multi_select', [
	'name' => 'job_role_id',
    'id'=>'job_role_id',
	'label' => "Select Role to send message to (if it's everyone select All)",
    'placeholder' => 'Select Job Role',
    'min'=> 1,
    'unpack' => false,
    'searchable'=>true,
    'options' => collect($jobRoleList ?? ''),
])


@formField('medias',[
    'name' => 'session_image',
    'label' => 'Session Cover Image',
])

@formField('files', [
	'name' => 'session_video',
	'label' => 'Session Video',
	'noTranslate' => true,
])

@formField('input', [
        'name' => 'session_video_url',
        'label' => 'Session video Link',
        'maxlength' => 1000
])


@formField('button', [
    'label' => 'Generate Microsoft Meeting Link',
    'type' => 'submit',
    'onClick' => 'generateMeetingLink()',
    'name' => 'submit', 
])




<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    
    function generateMeetingLink() {

        var formData = {
            description: $('input[name=description]').val(),
            start_time: $('input[name=start_time]').val(),
            duration: $('input[name=duration]').val(),
            country_id: $('input[name=country_id]').val(),
            company_id: $('input[name=company_id]').val(),
            job_role_id: $('input[name=job_role_id]').val(),
            
        };
        console.log(formData)

        // Make an AJAX request to the server to generate the meeting link
        $.ajax({
    type: "POST",
    url: "/meeting/link",
    data: {
        _token: '{{ csrf_token() }}',
        formData: formData
    },
    success: function(data) {
        // Display the meeting link to the user
        console.log(data)
        alert("Microsoft Teams meeting link has been created successfully.Click on Update button to save your data");
    },
    error: function(xhr, status, error) {
        
        // Handle the error
        alert("Error: Microsoft teams link has not been created. please try again later. " );
    }
});
    }
</script>

<script>
    
    function generateGoogleMeetingLink() {

        var formData = {
            description: $('input[name=description]').val(),
            start_time: $('input[name=start_time]').val(),
            duration: $('input[name=duration]').val(),
            country_id: $('input[name=country_id]').val(),
            company_id: $('input[name=company_id]').val(),
            job_role_id: $('input[name=job_role_id]').val(),
            
        };
     

        // Make an AJAX request to the server to generate the meeting link
        $.ajax({
    type: "POST",
    url: "/google-meeting/link",
    data: {
        _token: '{{ csrf_token() }}',
        formData: formData
    },
    success: function(data) {
        console.log(data)
        // Display the meeting link to the user
        alert("Google hangouts meeting link has been created successfully.Click on Update button to save your data");
    },
    error: function(xhr, status, error) {
        console.log(error)
        alert("Error: Google hangouts link has not been created. please try again later. " );
    }
});
    }
</script>
@stop






