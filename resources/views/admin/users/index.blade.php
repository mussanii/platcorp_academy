@extends('twill::layouts.main')
@push('extra_css')
    <style>
        .usercode-button{
            float:right; 
            height: 35px;
            line-height: 33px;
            padding: 0 25px;
            background: #1d9f3c;
            color: #fff;
            text-decoration: none;
            margin-top: 1.5%;
            margin-left: 2%;
        }
         .filter .filter__moreInner{
            width:100%;
         }
         .filter .filter__moreInner > div{
            width: 70%;
         }
        .filter .filter__moreInner > div > * {
        margin-right: 20px;
            width: 31%;
            display: flex;
        }
        .filter .filter__moreInner .input{
           width:100%;

        }
        .container {
            width: 100% !important;
        }
        .filter-select {
            height: 32px !important;
            margin-bottom: 2rem;
        }
    </style>
@endpush

@section('appTypeClass', 'body--listing')

@php
    $translate = $translate ?? false;
    $translateTitle = $translateTitle ?? $translate ?? false;
    $reorder = $reorder ?? false;
    $nested = $nested ?? false;
    $bulkEdit = $bulkEdit ?? true;
    $create = $create ?? false;
    $skipCreateModal = $skipCreateModal ?? false;
    $controlLanguagesPublication = $controlLanguagesPublication ?? true;

    $requestFilter = json_decode(request()->get('filter'), true) ? json_decode(request()->get('filter'), true): [];



   
@endphp

@push('extra_css')
    @if(app()->isProduction())
        <link href="{{ twillAsset('main-listing.css') }}" rel="preload" as="style" crossorigin/>
    @endif
    @unless(config('twill.dev_mode', false))
        <link href="{{ twillAsset('main-listing.css') }}" rel="stylesheet" crossorigin/>
    @endunless
@endpush

@push('extra_js_head')
    @if(app()->isProduction())
        <link href="{{ twillAsset('main-listing.js') }}" rel="preload" as="script" crossorigin/>
    @endif
@endpush

@section('content')
    <div class="listing">
        <div class="listing__nav">
            <div class="container" >
                <div class="row">
                   <div class="col-12">

                     <a size="small" class="button button--small button--validate usercode-button" href="{{route('admin.userCodeForm')}}"> Create User Codes </a> 

                    
                   </div>
                </div>
            </div>
            <div class="container" ref="form">
                <a17-filter v-on:submit="filterListing" v-bind:closed="hasBulkIds"
                            initial-search-value="{{ $filters['search'] ?? '' }}" :clear-option="true"
                            v-on:clear="clearFiltersAndReloadDatas">
                    <a17-table-filters slot="navigation"></a17-table-filters>

             

                    @forelse($hiddenFilters as $filter)
                        @if ($loop->first)
                            <div slot="hidden-filters">
                        @endif

                        @if (isset(${$filter.'List'}))
                        @if($filter === 'company_id')
                           @php
                             $list = ${$filter . 'List'};
                             $options =
                                 is_object($list) && method_exists($list, 'map')
                                     ? $list
                                         ->map(function ($label, $value) {
                                             return [
                                                 'value' => $label->id,
                                                 'label' => $label->title,
                                             ];
                                         })
                                         ->values()
                                         ->toArray()
                                     : $list;

                             $selectedIndex = isset($requestFilter[$filter]) ? array_search($requestFilter[$filter], array_column($options, 'value')) : false;
                         @endphp
                            <a17-vselect
                                name="{{ $filter }}"
                                :options="{{ json_encode($options) }}"
                                @if ($selectedIndex !== false)
                                    :selected="{{ json_encode($options[$selectedIndex]) }}"
                                @endif
                                placeholder="All Subsidiaries"
                                ref="filterDropdown[{{ $loop->index }}]"
                            ></a17-vselect>

                            @elseif($filter === 'division_id')
                          <select name='{{$filter}}' class="filter-select" id="{{$filter}}">
                                <option value=""> Select Subsidiary first</option>
                            
                            </select>

                        
                        @else
                            @php
                                $list = ${$filter.'List'};
                                $options = is_object($list) && method_exists($list, 'map') ?
                                    $list->map(function($label, $value) {
                                        return [
                                            'value' => $value,
                                            'label' => $label,
                                        ];
                                    })->values()->toArray() : $list;
                                $selectedIndex = isset($requestFilter[$filter]) ? array_search($requestFilter[$filter], array_column($options, 'value')) : false;
                            @endphp
                               <a17-vselect
                                name="{{ $filter }}"
                                :options="{{ json_encode($options) }}"
                                @if ($selectedIndex !== false)
                                    :selected="{{ json_encode($options[$selectedIndex]) }}"
                                @endif
                                placeholder="All {{ strtolower(\Illuminate\Support\Str::plural($filter)) }}"
                                ref="filterDropdown[{{ $loop->index }}]"
                            ></a17-vselect>
                        @endif
                         

                        @endif

                        @if ($loop->last)
                            </div>
                        @endif
                    @empty
                        @hasSection('hiddenFilters')
                            <div slot="hidden-filters">
                                @yield('hiddenFilters')
                            </div>
                        @endif
                    @endforelse

                    @if($create)
                        <div slot="additional-actions">
                            <a17-button
                                variant="validate"
                                size="small"
                                @if($skipCreateModal) href={{$createUrl ?? ''}} el="a" @endif
                                @if(!$skipCreateModal) v-on:click="create" @endif
                            >
                                {{ twillTrans('twill::lang.listing.add-new-button') }}
                            </a17-button>
                            @foreach($filterLinks as $link)
                                <a17-button el="a" href="{{ $link['url'] ?? '#' }}" download="{{ $link['download'] ?? '' }}" rel="{{ $link['rel'] ?? '' }}" target="{{ $link['target'] ?? '' }}" variant="small secondary">{{ $link['label'] }}</a17-button>
                            @endforeach
                        </div>
                    @elseif(isset($filterLinks) && count($filterLinks))
                        <div slot="additional-actions">
                            @foreach($filterLinks as $link)
                                <a17-button el="a" href="{{ $link['url'] ?? '#' }}" download="{{ $link['download'] ?? '' }}" rel="{{ $link['rel'] ?? '' }}" target="{{ $link['target'] ?? '' }}" variant="small secondary">{{ $link['label'] }}</a17-button>
                            @endforeach
                        </div>
                    @endif

                    @if(isset($additionalTableActions) && count($additionalTableActions))

               
                        <div slot="additional-actions">
                            @foreach($additionalTableActions as $additionalTableAction)
                                <a17-button
                                    variant="{{ $additionalTableAction['variant'] ?? 'primary' }}"
                                    size="{{ $additionalTableAction['size'] ?? 'small' }}"
                                    el="{{ $additionalTableAction['type'] ?? 'button' }}"
                                    href="{{ $additionalTableAction['link'] ?? '#' }}"
                                    target="{{ $additionalTableAction['target'] ?? '_self' }}"
                                >
                                    {{ $additionalTableAction['name'] }}
                                </a17-button>
                            @endforeach
                        </div>
                    @endif
                </a17-filter>
            </div>
            @if($bulkEdit)
                <a17-bulk></a17-bulk>
            @endif
        </div>

        <div class="container" style="margin-bottom: 3%;">
            <div class="row">
               <div class="col-12">
               <button id="exportButton" class="button button--small button--validate" style="float:right; margin-right:20px;"><i class="fa fa-file-excel-o mx-2 my-1" ></i> Export</button>

                  {{--<form action="{{ route('admin.user.export') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{json_encode($tableData)}}" name="tableData">
                    <input type="hidden" value="{{ json_encode($requestFilter) }}" name="requestFilter">
    
                    <button type="submit" class="button button--small button--validate" style="float:right; margin-right:20px;">Export</button>
    
                  </form>--}}
    
                {{-- <a size="small" class="button button--small button--validate" href="{{ route('admin.courseCompletion.export', 1) }}" style="float:right; margin-right:20px;">Export </a> --}}
               </div>
            </div>
        </div>
        
      @if($nested)
    <a17-nested-datatable
        :draggable="{{ $reorder ? 'true' : 'false' }}"
        :max-depth="{{ $nestedDepth ?? '1' }}"
        :bulkeditable="{{ $bulkEdit ? 'true' : 'false' }}"
        empty-message="{{ twillTrans('twill::lang.listing.listing-empty-message') }}"
    >
        <template #default="{ bulkActions, bulkEdit, bulkChecked, bulkCheckAll, bulkEditVisible, listing, deleteItems, restoreItems }">
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Job Role</th>
                        <th>Branch</th>
                        <th>Country</th>
                        <th>Registered on</th>
                        <th>Last Login</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in listing" :key="item.id">
                        <td>@{{ item.name }}</td>
                        <td>@{{ item.email }}</td>
                        <td>@{{ item.jobRole }}</td>
                        <td>@{{ item.branch }}</td>
                        <td>@{{ item.country }}</td>
                        <td>@{{ item.registeredOn }}</td>
                        <td>@{{ item.lastLogin }}</td>
                    </tr>
                </tbody>
            </table>
        </template>
    </a17-nested-datatable>
@else
    <a17-datatable
        class="za-kuexport"
        :draggable="{{ $reorder ? 'true' : 'false' }}"
        :bulkeditable="{{ $bulkEdit ? 'true' : 'false' }}"
        empty-message="{{ twillTrans('twill::lang.listing.listing-empty-message') }}"
    >
        <template #default="{ bulkActions, bulkEdit, bulkChecked, bulkCheckAll, bulkEditVisible, listing, deleteItems, restoreItems }">
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Job Role</th>
                        <th>Branch</th>
                        <th>Country</th>
                        <th>Registered on</th>
                        <th>Last Login</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in listing" :key="item.id">
                        <td>@{{ item.name }}</td>
                        <td>@{{ item.email }}</td>
                        <td>@{{ item.jobRole }}</td>
                        <td>@{{ item.branch }}</td>
                        <td>@{{ item.country }}</td>
                        <td>@{{ item.registeredOn }}</td>
                        <td>@{{ item.lastLogin }}</td>
                    </tr>
                </tbody>
            </table>
        </template>
    </a17-datatable>
@endif


        @if($create)
            <a17-modal-create
                ref="editionModal"
                form-create="{{ $storeUrl }}"
                v-on:reload="reloadDatas"
                @if ($customPublishedLabel ?? false) published-label="{{ $customPublishedLabel }}" @endif
                @if ($customDraftLabel ?? false) draft-label="{{ $customDraftLabel }}" @endif
            >
                <a17-langmanager
                    :control-publication="{{ json_encode($controlLanguagesPublication) }}"
                ></a17-langmanager>
                @partialView(($moduleName ?? null), 'create', ['renderForModal' => true])
            </a17-modal-create>
        @endif

        <a17-dialog ref="warningDeleteRow" modal-title="{{ twillTrans('twill::lang.listing.dialogs.delete.title') }}" confirm-label="{{ twillTrans('twill::lang.listing.dialogs.delete.confirm') }}">
            <p class="modal--tiny-title"><strong>{{ twillTrans('twill::lang.listing.dialogs.delete.move-to-trash') }}</strong></p>
            <p>{{ twillTrans('twill::lang.listing.dialogs.delete.disclaimer') }}</p>
        </a17-dialog>

        <a17-dialog ref="warningDestroyRow" modal-title="{{ twillTrans('twill::lang.listing.dialogs.destroy.title') }}" confirm-label="{{ twillTrans('twill::lang.listing.dialogs.destroy.confirm') }}">
            <p class="modal--tiny-title"><strong>{{ twillTrans('twill::lang.listing.dialogs.destroy.destroy-permanently') }}</strong></p>
            <p>{{ twillTrans('twill::lang.listing.dialogs.destroy.disclaimer') }}</p>
        </a17-dialog>
    </div>
@stop

@section('initialStore')

    window['{{ config('twill.js_namespace') }}'].CMS_URLS = {
        index: @if(isset($indexUrl)) '{{ $indexUrl }}' @else window.location.href.split('?')[0] @endif,
        publish: '{{ $publishUrl }}',
        bulkPublish: '{{ $bulkPublishUrl }}',
        restore: '{{ $restoreUrl }}',
        bulkRestore: '{{ $bulkRestoreUrl }}',
        forceDelete: '{{ $forceDeleteUrl }}',
        bulkForceDelete: '{{ $bulkForceDeleteUrl }}',
        reorder: '{{ $reorderUrl }}',
        create: '{{ $createUrl ?? '' }}',
        feature: '{{ $featureUrl }}',
        bulkFeature: '{{ $bulkFeatureUrl }}',
        bulkDelete: '{{ $bulkDeleteUrl }}'
    }

    window['{{ config('twill.js_namespace') }}'].STORE.form = {
        fields: []
    }

    window['{{ config('twill.js_namespace') }}'].STORE.datatable = {
        data: {!! json_encode($tableData) !!},
        columns: {!! json_encode($tableColumns) !!},
        navigation: {!! json_encode($tableMainFilters) !!},
        filter: { status: '{{ $filters['status'] ?? $defaultFilterSlug ?? 'all' }}' },
        page: '{{ request('page') ?? 1 }}',
        maxPage: '{{ $maxPage ?? 1 }}',
        defaultMaxPage: '{{ $defaultMaxPage ?? 1 }}',
        offset: '{{ request('offset') ?? $offset ?? 60 }}',
        defaultOffset: '{{ $defaultOffset ?? 60 }}',
        sortKey: '{{ $reorder ? (request('sortKey') ?? '') : (request('sortKey') ?? '') }}',
        sortDir: '{{ request('sortDir') ?? 'asc' }}',
        baseUrl: '{{ rtrim(config('app.url'), '/') . '/' }}',
        localStorageKey: '{{ isset($currentUser) ? $currentUser->id : 0 }}__{{ $moduleName ?? Route::currentRouteName() }}'
    }

    @if ($create && ($openCreate ?? false))
        window['{{ config('twill.js_namespace') }}'].openCreate = {!! json_encode($openCreate) !!}
    @endif
@stop

@push('extra_js')
    <script src="{{ twillAsset('main-listing.js') }}" crossorigin></script>

<!-- Include SheetJS library from CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.8/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!-- Add event listener to export button -->
<script>
function exportToExcel() {
  // Get table element
  var table = document.querySelector('.za-kuexport');

  // Clone the table to preserve the original formatting
  var clonedTable = table.cloneNode(true);

  // Remove unnecessary columns
  var colIndexesToRemove = [-1, 0, 1]; // Specify the indexes of columns to remove (zero-based)
  var rows = clonedTable.querySelectorAll('tr');
  rows.forEach(function (row, index) {
    if (index === 1) {
      row.parentNode.removeChild(row);
    } else {
      colIndexesToRemove.forEach(function (colIndex) {
        row.deleteCell(colIndex);
      });
    }
  });

  // Convert table to worksheet
  var ws = XLSX.utils.table_to_sheet(clonedTable);

  // Rename column headers
  ws['A1'].v = 'Name'; // Assign the 'Name' column header to cell A1

  // Create workbook and add worksheet
  var wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

  // Export workbook to Excel file
  XLSX.writeFile(wb, 'CMS_Users_Report.xlsx');
}

// Add event listener to export button
document.getElementById('exportButton').addEventListener('click', exportToExcel);

</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('.vselectOuter').on('click', function() {
            // var companyID = $(this).val();

            var companyID = $("input[name=company_id]").val();
           
            console.log("Company ID: " + companyID);

            if (companyID > 0) {
                $.ajax({
                    url: '/admin/getUserDivision/' + companyID,
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.division) {
                            $('#division_id').empty();
                            $('#division_id').append(
                                '<option value=" " hidden>Select Department</option>');
                            $.each(data.division, function(key, division) {
                                $('select[name="division_id"]').append(
                                    '<option value="' + division.id + '">' +
                                    division
                                    .title + '</option>');
                            });


                        } else {

                            $('#division_id').empty();

                        }
                    }
                });
            } else {
                $('#division_id').empty();
            }
        });

    });
</script>



@endpush
