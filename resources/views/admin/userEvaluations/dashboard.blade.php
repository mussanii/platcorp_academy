@extends('layouts.reports')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">
        <div class="row">
            <form action="{{route('admin.userE.dashboard') }}" method="POST">
                <div class="row justify-content-md-between">
                    <div class="col col-lg-7 col-xl-5 form-group">
                        <div class="input-group">
                             <select class="form-control" v-model="search" name="course_id">
                             <option value=" "> Select Course</option>
                             @foreach ($courses as $course)
                             <option value="{{$course->id}}">{{$course->name }}</option>
                                 
                             @endforeach

                             </select>

                            <span class="input-group-append">
                                <button type="submit" class="btn btn-primary" ><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-auto form-group ">
                        <select class="form-control" v-model="pagination.state.per_page">
                            
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </form>

            <table class="table" id="bank_report" width="100%">
                <thead>
                    <tr>
                     
                    </tr>
                  </thead>
                  @if(is_null($evaluation))
                    <tbody>
                      <td colspan="7" class="text-center">
                        Please select a bank to view report
                      </td>
                    <tbody>
                  @endif
            </table>

           
    </div>

</div>
</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>

<script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


@if(!is_null($evaluation))
<script>

  var dataTable;


  //Set ajax baseURl
  let baseUrl = "{!! route('admin.user-evaluations.json') !!}?";

  @if($evaluation)
    baseUrl += encodeURIComponent('course')
    baseUrl += "="
    baseUrl +=encodeURIComponent({{ $evaluation->id }})
  @endif

  //Query data
  function encodeQueryData(data) {
   const ret = [];
   for (let d in data)
     ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
   return ret.join('&');
}

  
$(document).ready(function(){

	console.log(baseUrl);
 
  dataTable = $('#bank_report').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable coluFirstmn ordering

    "lengthMenu": [
        [100, 200, 300, 400, 500, -1],
        [100, 200, 300, 400, 500, "All"] // change per page values here
      ],
      "pageLength": 100,
      "ajax": {
        url: baseUrl,
        method: 'GET'
      },
      // dom: '<"html5buttons"B>lTfgitp',
      "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
      buttons: [
        { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}},
        {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}},
        {extend: 'excel', title: '{{ config('app.name', 'Innovativkonzept') }} - List of all User Licenses',exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}},
        {extend: 'pdf', title: '{{ config('app.name', 'Innovativkonzept') }} - List of all User Licenses',exportOptions: {columns: [0, 1, 2, 3,4,5,6,7]}},
        {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');
          $(win.document.body).find('table')
          .addClass('compact')
          .css('font-size', 'inherit');
        }
      }
    ],
    columns: [
      {data: 'id',render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }},
      {data: 'username', name: 'username', orderable: true, searchable: true,render:function(data, type, row){
    return "<a href='/admin/user_licenses/bankuser/?username="+ row.username +"'>" + row.username + "</a>"
    }},
      {data: 'status', name: 'status', orderable: true, searchable: true},
      {data: 'completion_date', name: 'completion_date', orderable: true, searchable: true},
    ],
  });

});
</script>
@endif
@endpush
