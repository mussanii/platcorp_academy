@extends('twill::layouts.form')
@php
    $isSuperAdmin = isset(Auth::user()->role) ? Auth::user()->role === 'SUPERADMIN' : false;
   
@endphp


@section('contentFields')

@if($isSuperAdmin)
@formField('input', [
    'name' => 'company_code',
    'label' => 'Company Initials',
    'maxlength' => 100
])

   
    @formField('input', [
        'name' => 'contact_first_name',
        'label' => ' Contact First Name',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'contact_last_name',
        'label' => ' Contact last Name',

        'maxlength' => 100
    ])

@formField('input', [
    'name' => 'contact_email',
    'label' => ' Contact Email Address',
    'maxlength' => 100
])

@formField('medias', [
'name' => 'logo',
'label' => 'Company Logo',
'note' => 'Shown on certificate'
])
@endif

{{-- @formField('wysiwyg', [
'name' => 'learning_statement',
'label' => 'Learning Statement ',
'toolbarOptions' => [ [ 'header' => [1, 2, false] ], 'list-ordered', 'list-unordered', [ 'indent' => '-1'], [ 'indent' => '+1' ], 'bold', 'italic','underline','link' ],
'maxlength' => 2000

]) --}}

@formField('wysiwyg', [
'name' => 'our_mission',
'toolbarOptions' => [ [ 'header' => [1, 2, false] ], 'list-ordered', 'list-unordered', [ 'indent' => '-1'], [ 'indent' => '+1' ], 'bold', 'italic','underline','link' ],
'label' => 'Our Mission',

])

@formField('wysiwyg', [
  'name' => 'our_vission',
   'label' => 'Our Vission',
   'toolbarOptions' => [ [ 'header' => [1, 2, false] ], 'list-ordered', 'list-unordered', [ 'indent' => '-1'], [ 'indent' => '+1' ], 'bold', 'italic','underline','link' ],
 
])

@if($isSuperAdmin)
@formField('input', [
        'name' => 'primary_color',
        'label' => ' Primary Color',
        'maxlength' => 100
])
@formField('input', [
        'name' => 'secondary_color',
        'label' => 'Secondary Color',
        'maxlength' => 100
])
@formField('input', [
        'name' => 'tachiary_color',
        'label' => ' Tertiary Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'menu_color',
        'label' => ' Menu Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'menu_color_active',
        'label' => ' Menu Color Active',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'footer_color',
        'label' => ' Footer Color',
        'maxlength' => 100
])


@formField('input', [
        'name' => 'primary_heading_color',
        'label' => ' Primary Heading Color',
        'maxlength' => 100
])


@formField('input', [
        'name' => 'secondary_heading_color',
        'label' => ' Secondary Heading Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'primary_text_color',
        'label' => ' Primary Text Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'secondary_text_color',
        'label' => ' Secondary Text Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'button_primary_color',
        'label' => ' Button Color Primary',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'button_secondary_color',
        'label' => ' Button Color Secondary',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'button_tachiary_color',
        'label' => ' Button Color Tertiary',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'light_background_color',
        'label' => 'Light Background Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'dark_background_color',
        'label' => ' Dark Background Color',
        'maxlength' => 100
])

@formField('input', [
        'name' => 'additional_color',
        'label' => ' Additional Color',
        'maxlength' => 100
])



@formField('medias', [
'name' => 'courses_banner',
'label' => 'Courses Banner',
'note' => 'Shown on courses page'
])

@formField('medias', [
'name' => 'live_sessions_banner',
'label' => 'Live Sessions Banner',
'note' => 'Shown on Live sessions'
])

@formField('medias', [
'name' => 'our_progress_banner',
'label' => 'Our Progress Banner',
'note' => 'Shown on Our Progress'
])

@formField('medias', [
'name' => 'enrolled_banner',
'label' => 'My Enrolled Module Banner',
'note' => 'Shown on My Enrolled Module'
])

@formField('medias', [
'name' => 'community_banner',
'label' => 'My Community Banner',
'note' => 'Shown on My Community'
])


@formField('medias', [
'name' => 'training_feed',
'label' => 'Training Feed Banner',
'note' => 'Shown on Training Feed'
])

@formField('medias', [
'name' => 'myprogress_banner',
'label' => 'My Progress Banner',
'note' => 'Shown on My Progress'
])

@formField('medias', [
'name' => 'mandatory_svg',
'label' => 'Mandatory svg',

])

@formField('medias', [
'name' => 'hr_recommended_svg',
'label' => 'HR recommened svg',

])

@formField('medias', [
'name' => 'clock_svg',
'label' => 'Clock svg',

])
@formField('medias', [
'name' => 'calendar_svg',
'label' => 'Calendar svg',

])

@formField('medias', [
'name' => 'tick_svg',
'label' => 'Tick svg',

])
@formField('medias', [
'name' => 'user_svg',
'label' => 'User svg',
        
])
@formField('medias', [
'name' => 'notification_bell_svg',
'label' => 'Notification Bell svg',
                
 ])
@formField('medias', [
'name' => 'rocket_svg',
'label' => 'Rocket svg',
                        
])
@formField('medias', [
'name' => 'not_finished_trophy_svg',
'label' => 'Not finished Trophy svg',
                        
])
@formField('medias', [
'name' => 'finished_trophy_svg',
'label' => 'Finished Trophy svg',
                                
])
@formField('medias', [
'name' => 'like_svg',
'label' => 'Like svg',
                                
 ])

@formField('medias', [
'name' => 'comment_svg',
'label' => 'comment svg',
                        
])
@endif

@formField('block_editor', ['blocks' => ['slideshow']])
@stop
@section('fieldsets')

<a17-fieldset title="Our Values" id="ourValues" :open="true">
        @formField('repeater', ['type' => 'our_values'])
       
       </a17-fieldset>   
<a17-fieldset title="Domains" id="domains" :open="true">
 @formField('repeater', ['type' => 'domain'])

</a17-fieldset>   



       
@endsection

