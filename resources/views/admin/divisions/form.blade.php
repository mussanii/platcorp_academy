@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
    'name' => 'company_id',
    'label' => 'Company',
    'placeholder' => 'Select Company',
    'options' => collect($companyList ?? ''),
    ])
@stop
