@extends('twill::layouts.form')
@php
use App\Repositories\DivisionRepository;
$divisions = app(DivisionRepository::class)->where('company_id', $item->company_id)->pluck('title','id');

@endphp
@push('extra_css')
    <style>

        .filter-select {
            height: 43px !important;
            width:100%;
        }

        .filter-select-label{
         margin: 10px 0;
        }

    </style>
@endpush
@section('contentFields')
@formField('select', [
    'name' => 'company_id',
    'label' => 'Company',
    'placeholder' => 'Select Company',
    'options' => collect($companyList ?? ''),
    ])
 @formField('select', [
    'name' => 'division_id',
    'label' => 'Division ',
    'placeholder' => 'Select Division',
    'options' => collect($divisions ?? ''),
    ])
  
@formField('multi_select', [
	'name' => 'courses',
	'label' => 'Select Mandatory Course for Job Role',
	'placeholder' => 'Select Course',
    'min'=> 1,
    'unpack' => false,
    'searchable'=>true,
	'options' => collect($courseList ?? ''),
])

@stop


@push('extra_js')
    <script src="{{ twillAsset('main-listing.js') }}" crossorigin></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script type="text/javascript">
        $(document).ready(function() {

            $('.vselectOuter').on('click', function() {
                // var companyID = $(this).val();

                var companyID = $("input[name=company_id]").val();

                if (companyID > 0) {
                    $.ajax({
                        url: '/admin/getDivision/' + companyID,
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        dataType: "json",
                        success: function(data) {
                            if (data.division) {
                                $('#select_division_id').empty();
                                $('#select_division_id').append(
                                    '<option value=" " hidden>Select Department</option>');
                                $.each(data.division, function(key, division) {
                                    $('select[name="select_division_id"]').append(
                                        '<option value="' + division.id + '">' +
                                        division
                                        .title + '</option>');
                                });


                            } else {

                                $('#select_division_id').empty();

                            }
                        }
                    });
                } else {
                    $('#select_division_id').empty();
                }
            });




            $('#select_division_id').on('change', function() {

                $("input[name=division_id]").val($(this).val());
              
            })

        });
    </script>
@endpush
