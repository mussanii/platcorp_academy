@extends('layouts.admin.reports')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">

        <div class="row">

            <div class="col-12">
                <form action="{{ route('admin.branch.completion') }}" method="POST" class="homeForm">
                    @csrf
                    <div class="row justify-content-center">

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <div class="col-9 no-padding-right ">
                            <div class="row">

                                <div class="col-2 mt-4 mb-4 ">
                                    <label class="filter_by" style="color:#000"> Filter branches by</label>
                                </div>
                                {{-- <div class="col-4">
                                    <select name="country" id="" class="form-control mt-4 mb-4 ">
                                        <option value=''>Select Country</option>
                                        @foreach (App\Models\Country::published()->orderByTranslation('title')->get() as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                <div class="col-5">

                                    <input type="text" placeholder="Free text search ... eg branch name" class="form-control mt-4 mb-4 " name="search">

                                </div>

                            </div>

                        </div>

                        <div class="col-3 ">
                            <button type="submit"
                                class="mt-4 mb-4" style="
                                margin-top: 40px;
                                margin-left: 20px;
                                color: rgb(255, 255, 255);
                                background: #1a8f36;
                                border:1px solid #1a8f36;
                                padding: 10px;
                                text-decoration: none;">Search</button>

                            <button type="reset" class=" mt-4 mb-4" style="
                            margin-top: 40px;
                            margin-left: 20px;
                            color: rgb(255, 255, 255);
                            background: rgb(255, 0, 0);
                            border:1px solid rgb(255, 0, 0);
                            padding: 10px;
                            text-decoration: none;"
                                onClick="window.location.href=window.location.href">Reset</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div class="row">
            <div class="col">
			  <table class="table" style="text-align: center">
                <thead></thead>
                <tr class="tablehead">
                    <th> #</th>
                    <th> Branch</th>
                    <th> Number of Registrations</th>
                    <th> Number of Completions</th>
                </tr>
            </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($branches as $branch)
                    <tr class="tablerow">
                    <td class="tablecell">{{ $i++}}</td>
                    <td class="tablecell">{{ $branch->title }}</td>
                    <td class="tablecell">{{ $branch->registered }}</td>
                    <td class="tablecell">{{ $branch->completed }}</td>
                </tr>
                  @endforeach
                </tbody>
              </table>

              {!! $branches->links() !!}
			</div>

    </div>

</div>
</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>

@endpush
