@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
    'name' => 'company_id',
    'label' => 'Subsidiary',
    'placeholder' => 'Select Subsidiary',
    'options' => collect($companyList ?? ''),
    ])

@formField('input', [
    'name' => 'email',
    'label' => 'Email',
])
@stop
