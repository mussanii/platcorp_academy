@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
    'name' => 'message_type_id',
    'label' => 'Type',
    'placeholder' => 'Select Type',
    'options' => collect($messageTypeList ?? '')
])


@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 2,
    
])
  

        @formField('select', [
        'name' => 'company_id',
        'label' => "Subsidiary",
        'placeholder' => 'Select a subsidiary',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($companyList ?? ''),
        ])


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 1,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList1 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 2,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList2 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 3,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList3 ?? ''),
        ])

@endformConnectedFields

@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 4,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList4 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 5,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList5 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 6,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList6 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 7,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList7 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 8,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList8 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 9,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList9 ?? ''),
        ])

@endformConnectedFields

@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 10,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList10 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 11,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList11 ?? ''),
        ])

@endformConnectedFields


@formConnectedFields([
'fieldName' => 'branch_id',
'fieldValues' => 12,
    
])
@formField('multi_select', [
        'name' => 'job_role_id',
        'label' => "Select Role to send message to (if it's everyone select All)",
        'placeholder' => 'Select Role',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($jobRoleList12 ?? ''),
        ])

@endformConnectedFields



        
    
    
        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])
@endformConnectedFields



@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 3,
    
])
        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])

        @formField('multi_select', [
        'name' => 'target_id',
        'label' => "Select a target to send message to (if it's everyone select All)",
        'placeholder' => 'Select target',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($targetList ?? ''),
        ])

        @formField('select', [
        'name' => 'company_id',
        'label' => "Subsidiary",
        'placeholder' => 'Select a subsidiary',
        'min'=> 1,
        'unpack' => false,
        'options' => collect($companyList ?? ''),
        ])
     
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])
@endformConnectedFields




@formConnectedFields([
    'fieldName' => 'message_type_id',
    'fieldValues' => 1,
    
])



@formField('select', [
    'name' => 'company_id',
    'label' => 'Subsidiary',
    'placeholder' => 'Select a subsidiary',
    'min'=> 1,
    'unpack' => false,
    'options' => $companyList ?? [],
])


@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 1,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList1 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 2,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList2 ?? ''),
        ])


@endformConnectedFields


@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 3,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList3 ?? ''),
        ])


@endformConnectedFields


@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 4,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList4 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 5,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList5 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 6,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList6 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 7,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList7 ?? ''),
        ])


@endformConnectedFields


@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 8,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList8 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 9,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList9 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 10,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList10 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 11,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList11 ?? ''),
        ])


@endformConnectedFields

@formConnectedFields([
    'fieldName' => 'branch_id',
    'fieldValues' => 12,
    
])
@formField('multi_select', [
        'name' => 'email',
        'label' => "Select an email or emails to send message to (if it's everyone select All)",
        'placeholder' => 'Select email',
        'min'=> 1,
        'unpack' => false,
        'searchable'=> true,
        'options' => collect($userList12 ?? ''),
        ])


@endformConnectedFields

        @formField('input', [
        'name' => 'subject',
        'label' => 'Subject',
        'maxlength' => 100
        ])
    
        @formField('wysiwyg', [
        'name' => 'message',
        'label' => 'Message',
        'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6, false]],
        'bold',
        'italic',
        'underline',
        'strike',
        ["script" => "super"],
        ["script" => "sub"],
        "blockquote",
        "code-block",
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        ['indent' => '-1'],
        ['indent' => '+1'],
        ["align" => []],
        ["direction" => "rtl"],
        'link',
        'image',
        "clean",
        ],
        'placeholder' => 'Message',
        'maxlength' => 2000,
    
        ])


@endformConnectedFields








@stop
@push('extra_js')

@endpush
