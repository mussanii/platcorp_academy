@extends('twill::layouts.form')

@section('contentFields')
@formField('select', [
	'name' => 'company_id',
	'label' => 'Related Company',
	'placeholder' => 'Select related Company',
	'options' => collect($companyList ?? ''),
])

@formField('block_editor', [

    'blocks' => ['survey']

    ])
@stop
