@extends('twill::layouts.free')

@push('extra_css')
<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">

@endpush
@section('customPageContent')
<form class="form-horizontal form-create" method="post" action="{{route('admin.upload.store')}}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="card-header">
        <h1> Upload Accounts </h1>
    </div>

    <div class="card-body upload-body">
      <Label class="form-check-label "> Select file to upload:</Label>
      <input type="file" name="file" class="form-control space-top-1">
    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit
        </button>
    </div>

</form>


@stop
