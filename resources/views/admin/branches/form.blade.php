@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
    'name' => 'company_id',
    'label' => 'Company',
    'placeholder' => 'Select Company',
    'options' => collect($companyList ?? ''),
    ])
 
@formField('select', [
    'name' => 'country_id',
    'label' => 'Country',
    'placeholder' => 'Select Country',
    'options' => collect($countryList ?? ''),
    ])

@stop
