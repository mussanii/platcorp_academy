<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.course.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('course_category_id'), 'has-success': fields.course_category_id && fields.course_category_id.valid }">
    <label for="course_category_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.course_category_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <select v-model="form.course_category_id" class="form-control"  placeholder="Select Course Category" label="title" track-by="course_category_id">
            @foreach( $categories as $category)
            <option   style="min-width: 100%;"  name="course_category_id" value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
            </select>

        {{-- <input type="text" v-model="form.course_category_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('course_category_id'), 'form-control-success': fields.course_category_id && fields.course_category_id.valid}" id="course_category_id" name="course_category_id" placeholder="{{ trans('admin.course.columns.course_category_id') }}"> --}}
        <div v-if="errors.has('course_category_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('course_category_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('course_type_id'), 'has-success': fields.course_type_id && fields.course_type_id.valid }">
    <label for="course_type_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.course_type_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.course_type_id" :options="{{ $types->toJson() }}" placeholder="Select Course Type" label="name" track-by="course_type_id" :multiple="false"></multiselect>
        {{-- <input type="text" v-model="form.course_type_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('course_type_id'), 'form-control-success': fields.course_type_id && fields.course_type_id.valid}" id="course_type_id" name="course_type_id" placeholder="{{ trans('admin.course.columns.course_type_id') }}"> --}}
        <div v-if="errors.has('course_type_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('course_type_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('company_id'), 'has-success': fields.company_id && fields.company_id.valid }">
    <label for="company_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.company_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.company_id" :options="{{ $subsidiaries->toJson() }}" placeholder="Select Course Category" label="title" track-by="company_id" :multiple="false"></multiselect>
        {{-- <input type="text" v-model="form.company_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('company_id'), 'form-control-success': fields.company_id && fields.company_id.valid}" id="company_id" name="company_id" placeholder="{{ trans('admin.course.columns.company_id') }}"> --}}
        <div v-if="errors.has('company_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('more_info'), 'has-success': fields.more_info && fields.more_info.valid }">
    <label for="more_info" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.more_info') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <quill-editor v-model="form.more_info" :options="wysiwygConfig" name="more_info" id="more_info" />
            {{-- <textarea class="form-control" v-model="form.more_info" v-validate="''" id="more_info" name="more_info" :options="wysiwygConfig"></textarea> --}}
        </div>
        <div v-if="errors.has('more_info')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('more_info') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('effort'), 'has-success': fields.effort && fields.effort.valid }">
    <label for="effort" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.effort') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.effort" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('effort'), 'form-control-success': fields.effort && fields.effort.valid}" id="effort" name="effort" placeholder="{{ trans('admin.course.columns.effort') }}">
        <div v-if="errors.has('effort')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('effort') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('due_date'), 'has-success': fields.due_date && fields.due_date.valid }">
    <label for="effort" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">
        {{ trans('admin.course.columns.due_date') }}
    </label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <datetime v-model="form.due_date" :config="datetimePickerConfig" class="flatpickr" placeholder="Select date and time" class="form-control" :class="{'form-control-danger': errors.has('due_date'), 'form-control-success': fields.due_date && fields.due_date.valid}"></datetime>
        {{-- <input class="form-check-input" id="due_date" type="checkbox" v-model="form.due_date" v-validate="''" data-vv-name="due_date"  name="due_date_fake_element"> --}}
        <div v-if="errors.has('due_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('due_date') }}</div>
    </div>
</div>

<div class="form-check row mt-3" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="status" type="checkbox" v-model="form.status" v-validate="''" data-vv-name="status" name="status_fake_element">
        <label class="form-check-label" for="status">
            {{ trans('admin.course.columns.status') }}
        </label>
        <input type="hidden" name="status" :value="form.status">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>

<div class="form-check row mt-3" :class="{'has-danger': errors.has('status'), 'has-success': fields.has_reflections && fields.has_reflections.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="has_reflections" type="checkbox" v-model="form.has_reflections" v-validate="''" data-vv-name="has_reflections" name="has_reflections_fake_element">
        <label class="form-check-label" for="has_reflections">
            {{ trans('admin.course.columns.has_reflections') }}
        </label>
        <input type="hidden" name="has_reflections" :value="form.has_reflections">
        <div v-if="errors.has('has_reflections')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('has_reflections') }}</div>
    </div>
</div>


<div class="form-check row mt-3" :class="{'has-danger': errors.has('mobile_available'), 'has-success': fields.mobile_available && fields.mobile_available.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="mobile_available" type="checkbox" v-model="form.mobile_available" v-validate="''" data-vv-name="mobile_available" name="status_fake_element">
        <label class="form-check-label" for="mobile_available">
            {{ trans('admin.course.columns.mobile_available') }}
        </label>
        <input type="hidden" name="mobile_available" :value="form.mobile_available">
        <div v-if="errors.has('mobile_available')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('mobile_available') }}</div>
    </div>
</div>