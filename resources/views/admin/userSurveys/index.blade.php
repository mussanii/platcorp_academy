@extends('layouts.admin.survey')
@push('extra_css')
	<link href="{{ asset('/css/reports.css') }}" rel="stylesheet">
	<style>
		.report_visual {
			margin-bottom: 30px;
		}
        .container {
            width: 100% !important;
        }

	</style>

	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('customPageContent')
<div class="listing">

	<div id="container-fluid">

        <div class="row">

            <div class="col-12">
                <form action="{{ route('admin.userSurvey.responses') }}" method="POST" class="homeForm">
                    @csrf
                    <div class="row justify-content-center">

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <div class="col-9 no-padding-right ">
                            <div class="row">
                                <div class="col-4">
                                    
                                    <select name="survey" id="" class="form-control mt-4 mb-4 ">
                                        <option value=''>Select Survey </option>
                                        @foreach ($surveys as $theme)
                                            <option value="{{ $theme->id }}"> {{ $theme->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-3 ">
                                    <button type="submit"
                                        class="mt-4 mb-4" style="
                                        margin-top: 40px;
                                        margin-left: 20px;
                                        color: rgb(255, 255, 255);
                                        background: #1a8f36;
                                        border:1px solid #1a8f36;
                                        padding: 10px;
                                        text-decoration: none;">Search</button>
        
                                    <button type="reset" class=" mt-4 mb-4" style="
                                    margin-top: 40px;
                                    margin-left: 20px;
                                    color: rgb(255, 255, 255);
                                    background: rgb(255, 0, 0);
                                    border:1px solid rgb(255, 0, 0);
                                    padding: 10px;
                                    text-decoration: none;"
                                        onClick="window.location.href=window.location.href">Reset</button>
                                </div>
                            </div>
                           
                        </div>

                       
                    </div>
                </form>

            </div>

        </div>

        @if(empty($results))

         <h1 class="text-center"> Please select a survey for the results to show</h1>

        @else 
        <div class="row">
            <div class="col">
			  <table class="table" style="text-align: center">
                <thead></thead>
                <tr class="tablehead">
                    <th> #</th>
                    <th> Name </th>
                    <th> Company </th>
                    @if( $questions->question1)
                    <th> {{ $questions->question1 }}</th>
                    @endif
                    @if( $questions->question2)
                    <th> {{ $questions->question2 }}</th>
                    @endif
                    @if( $questions->question3)
                    <th> {{ $questions->question3 }}</th>
                    @endif
                    @if( $questions->question4)
                    <th> {{ $questions->question4 }}</th>
                    @endif
                    @if( $questions->question5)
                    <th> {{ $questions->question5 }}</th>
                    @endif
                    @if( $questions->question6)
                    <th> {{ $questions->question6 }}</th>
                    @endif
                    @if( $questions->question7)
                    <th> {{ $questions->question7 }}</th>
                    @endif
                    @if( $questions->question8)
                    <th> {{ $questions->question8 }}</th>
                    @endif
                    @if( $questions->question9)
                    <th> {{ $questions->question9 }}</th>
                    @endif
                    @if( $questions->question10)
                    <th> {{ $questions->question10 }}</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @foreach ($results as $result)
                <td>{{ $i++ }}</td>
                <td> {{ $result->user_id }}</td>
                <td> {{ $result->company_id }}</td>
                @if($result->answer1)
                 <td>{{ $result->answer1 }}</td>
                @endif
                @if($result->answer2)
                <td>{{ $result->answer2 }}</td>
               @endif
               @if($result->answer3)
               <td>{{ $result->answer3 }}</td>
              @endif
              @if($result->answer4)
              <td>{{ $result->answer4 }}</td>
             @endif
             @if($result->answer5)
             <td>{{ $result->answer5 }}</td>
            @endif
            @if($result->answer6)
            <td>{{ $result->answer6 }}</td>
           @endif
           @if($result->answer7)
           <td>{{ $result->answer7 }}</td>
          @endif
          @if($result->answer8)
          <td>{{ $result->answer8 }}</td>
         @endif
         @if($result->answer9)
         <td>{{ $result->answer9 }}</td>
        @endif
        @if($result->answer10)
        <td>{{ $result->answer10 }}</td>
       @endif
                    
                @endforeach
            </tbody>
               
              </table>

			</div>

        @endif

    </div>

</div>
</div>
@stop
@push('extra_js')
<script src="{{ twillAsset('main-free.js') }}" crossorigin></script>

@endpush
