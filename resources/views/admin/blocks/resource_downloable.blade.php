
@formField('input', [
    'name' => 'file_name',
    'label' => 'File Name',
    'maxlength' => 200,
    'translated' => true,
])
@formField('files', [
		'name' => 'download_resource',
		'label' => 'Downloadable PDF',
		'note' => 'This applies for resources with downloadable resources',
	])

