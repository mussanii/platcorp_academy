@twillRepeaterMax('5')

@formField('wysiwyg', [
'name' => 'value',
'toolbarOptions' => [ [ 'header' => [1, 2, false] ], 'list-ordered', 'list-unordered', [ 'indent' => '-1'], [ 'indent' => '+1' ], 'bold', 'italic','underline','link' ],
'label' => 'Values',
'maxlength' => 2000

])