@twillRepeaterTitle('Surveys')
@formField('input', [
    'name' => 'survey_question',
    'label' => 'Survey Question',
    'maxlength' => 200,
    'translated' => true,
])

@formField('select', [
	'name' => 'option_id',
	'label' => 'Answer Type',
	'placeholder' => 'Select Answer Type',
	'options' => app(\App\Repositories\EvaluationOptionRepository::class)->listAll('title'),
])
