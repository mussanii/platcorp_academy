
@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
])

@formField('input', [
    'name' => 'number',
    'label' => 'Number of Courses to show',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])