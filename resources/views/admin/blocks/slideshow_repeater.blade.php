@twillRepeaterTitle('Slider')
@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 300,
    'translated' => true,
])

@formField('medias', [
    'name' => 'slide_image',  //role
    'label' => 'Slide Show Image',
	'withVideoUrl' => false,
	'max' => 1,
])

@formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    'translated' => true,
])
@formField('input', [
    'name' => 'url',
    'label' => 'URL',
	'translated' => true,
])