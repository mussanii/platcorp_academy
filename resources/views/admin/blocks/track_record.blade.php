@formField('input', [
'name' => 'track_title',
'label' => ' Track Title',
'maxlength' => 250,
'translated' => true,
])
@formField('input', [
    'name' => 'track_description',
    'label' => 'Description',
    'maxlength' => 250,
    'translated' => true,
    ])

@formField('input', [
'name' => 'number',
'label' => ' Title',
'maxlength' => 250,
'translated' => true,
])

@formField('input', [
'name' => 'number_title',
'label' => 'Number Description',
'maxlength' => 250,
'translated' => true,
])

@formField('wysiwyg', [
    'translated' => true,
    'name' => 'description',
    'label' => 'Description',
    'maxlength' => 1000,
    'editSource' => true,
    'note' => 'You can edit the source.',
])