<?php

return [
    'course' => [
        'title' => 'Course',

        'actions' => [
            'index' => 'Course',
            'create' => 'New Course',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'course' => [
        'title' => 'Courses',

        'actions' => [
            'index' => 'Courses',
            'create' => 'New Course',
            'edit' => 'Edit :name',
            'sync' => 'Sync Courses',
        ],

        'columns' => [
            'id' => 'ID',
            'published' => 'Published',
            'course_id' => 'Course',
            'name' => 'Name',
            'short_description' => 'Short description',
            'overview' => 'Overview',
            'more_info' => 'More info',
            'effort' => 'Effort',
            'start' => 'Start',
            'end' => 'End',
            'enrol_start' => 'Enrollment start',
            'enrol_end' => 'Enrollment end',
            'price' => 'Price',
            'course_image_uri' => 'Course image uri',
            'course_video_uri' => 'Course video uri',
            'course_category_id' => 'Course category',
            'status' => 'Status',
            'slug' => 'Slug',
            'order_id' => 'Order',
            'course_video' => 'Course video',
            'position' => 'Position',
            'short_name' => 'Short name',
             'mobile_available' => 'Mobile available',
             'due_date' => 'Course due date',
             'course_type_id' => 'Course Type',
             'company_id' => 'Company',
             'has_reflections' => 'Course has reflection questions',

            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];