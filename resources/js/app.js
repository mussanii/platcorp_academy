require('./bootstrap');


window.Vue = require("vue").default;

Vue.component('course-progress', require('./components/CourseProgress.vue').default);
Vue.component('progress-bar', require('./components/ProgressBar.vue').default);
Vue.component('my-course', require('./components/MyCourse.vue').default);
Vue.component('ppic-form', require('./components/PpicForm.vue').default);
Vue.component('ppic', require('./components/ppic.vue').default);
Vue.component('training-feed', require('./components/TrainingFeed.vue').default);
Vue.component('training-feed-mobile', require('./components/TrainingFeedMobile.vue').default);
Vue.component('sub-comments', require('./components/SubComments.vue').default);
Vue.component('sub-comment', require('./components/SubComment.vue').default);
Vue.component('comment', require('./components/Comments.vue').default);

window.onload = function () {
    const app = new Vue({
      el: '#app'
    })

}