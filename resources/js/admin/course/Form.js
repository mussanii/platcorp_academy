import AppForm from '../app-components/Form/AppForm';

Vue.component('course-form', {
    mixins: [AppForm],
    props:[
        'categories',
        'types',
        'subsidiaries'
    ],
    data: function() {
        return {
            form: {
                name:  '' ,
                short_description:  '' ,
                overview:  '' ,
                more_info:  '' ,
                effort:  '' ,
                start:  '' ,
                end:  '' ,
                enrol_start:  '' ,
                enrol_end:  '' ,
                price:  '' ,
                course_image_uri:  '' ,
                course_video_uri:  '' ,
                job_group_id:  '' ,
                status:  false ,
                course_category_id:  '' ,
                slug:  '' ,
                order_id:  '' ,
                sessions_id:  '' ,
                mobile_available: false,
                due_date:'',
                course_type_id:'',
                company_id:'',
                has_reflections: false,
            },
            mediaCollections: ['course_video']
        }
    }

});