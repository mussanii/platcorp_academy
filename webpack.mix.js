const WebpackShellPluginNext = require('webpack-shell-plugin-next');
let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    plugins: [
        new WebpackShellPluginNext({
            onBuildExit:{
                scripts: ['php artisan compile:themes'],
                blocking: true,
                parallel: false,
                swallowError:true
            }
        })
    ],
})
.js('resources/js/app.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css')
.sourceMaps();


mix
    .js(["resources/js/admin/admin.js"], "public/js")
    .sass("resources/sass/admin/admin.scss", "public/css")
    .vue();

if (mix.inProduction()) {
    mix.version();
}