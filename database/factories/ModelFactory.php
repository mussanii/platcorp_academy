<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Course::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Course::class, static function (Faker\Generator $faker) {
    return [
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'published' => $faker->boolean(),
        'course_id' => $faker->sentence,
        'name' => $faker->firstName,
        'short_description' => $faker->text(),
        'overview' => $faker->text(),
        'more_info' => $faker->text(),
        'effort' => $faker->sentence,
        'start' => $faker->dateTime,
        'end' => $faker->dateTime,
        'enrol_start' => $faker->dateTime,
        'enrol_end' => $faker->dateTime,
        'price' => $faker->randomFloat,
        'course_image_uri' => $faker->sentence,
        'course_video_uri' => $faker->sentence,
        'course_category_id' => $faker->sentence,
        'status' => $faker->boolean(),
        'slug' => $faker->unique()->slug,
        'order_id' => $faker->sentence,
        'course_video' => $faker->sentence,
        'position' => $faker->randomNumber(5),
        'short_name' => $faker->text(),
        
        
    ];
});
