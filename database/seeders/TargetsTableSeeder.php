<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TargetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('targets')->insert([
            ['title' => 'HOD'],
            ['title' => 'COMPANYHR'],
            ['title' => 'GROUPHR'],
            ['title' => 'ADMIN'],
        ]);
        
    }
}
