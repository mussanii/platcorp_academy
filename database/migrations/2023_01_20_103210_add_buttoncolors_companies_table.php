<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddButtoncolorsCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 
        Schema::table('companies', function (Blueprint $table) {
            $table->string('button_primary_color')->nullable();
            $table->string('button_secondary_color')->nullable();
            $table->string('button_tachiary_color')->nullable();
            $table->string('menu_color_active')->nullable();
            $table->string('footer_color')->nullable();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('button_primary_color');
            $table->dropColumn('button_secondary_color');
            $table->dropColumn('button_tachiary_color');
            $table->dropColumn('menu_color_active');
            $table->dropColumn('footer_color');
         
        });
    }
}
