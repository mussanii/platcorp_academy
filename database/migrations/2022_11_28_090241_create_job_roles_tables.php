<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobRolesTables extends Migration
{
    public function up()
    {
        Schema::create('job_roles', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('job_role_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'job_role');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('job_role_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'job_role');
        });

        Schema::create('job_role_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'job_role');
        });
    }

    public function down()
    {
        Schema::dropIfExists('job_role_revisions');
        Schema::dropIfExists('job_role_translations');
        Schema::dropIfExists('job_role_slugs');
        Schema::dropIfExists('job_roles');
    }
}
