<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTables extends Migration
{
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->text('country_id')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('branch_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'branch');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('branch_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'branch');
        });

        
    }

    public function down()
    {
        
        Schema::dropIfExists('branch_translations');
        Schema::dropIfExists('branch_slugs');
        Schema::dropIfExists('branches');
    }
}
