<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsUserLicenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user_licenses', function (Blueprint $table) {
            $table->string('email')->nullable();
            $table->string('company_id')->nullable();
            $table->string('job_role_id')->nullable();
            $table->string('branch')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('user_licenses', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('company_id');
            $table->dropColumn('job_role_id');
            $table->dropColumn('branch');
        });
    }
}
