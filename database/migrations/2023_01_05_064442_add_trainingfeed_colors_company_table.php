<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrainingfeedColorsCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function (Blueprint $table) {
          $table->string('light_background_color')->nullable();
            $table->string('dark_background_color')->nullable();
            $table->string('additional_color')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('training_comments', function (Blueprint $table) {

            $table->dropColumn('light_background_color');
            $table->dropColumn('dark_background_color');
            $table->dropColumn('additional_color');
            });
    }
}
