<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_licenses', function (Blueprint $table) {
            $table->id();
            $table->string('course_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('company_license_id')->nullable();
            $table->datetime('enrolled_at')->nullable();
            $table->datetime('expired_at')->nullable();
            $table->string('position')->nullable();
            $table->softDeletes();
            $table->string('username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_licenses');
    }
}
