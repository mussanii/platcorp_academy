<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsTwillusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        $twillUsersTable = config('twill.users_table', 'twill_users');

        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->string('username')->nullable();
            $table->string('google_id')->nullable();
            $table->string('microsoft_id')->nullable();
            $table->string('is_activated')->nullable();
            $table->date('last_login_at')->nullable();
            $table->date('registered_at')->nullable();
            $table->string('require_new_password', 200)->nullable();
            $table->string('company_id')->nullable();
            $table->string('is_company_admin')->nullable();
            $table->string('is_admin')->nullable();
            $table->string('profile_pic')->nullable();
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {

            $table->dropColumn('username');
            $table->dropColumn('google_id');
            $table->dropColumn('microsoft_id');
            $table->dropColumn('is_activated');
            $table->dropColumn('last_login_at');
            $table->dropColumn('registered_at');
            $table->dropColumn('require_new_password');
            $table->dropColumn('company_id');
            $table->dropColumn('is_company_admin');
            $table->dropColumn('profile_pic');
			
        });
    }
}
