<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageTrainingcommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('training_comments', function (Blueprint $table) {

        $table->json('images')->nullable();
        $table->json('gifs')->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('training_comments', function (Blueprint $table) {

            $table->dropColumn('images');
            $table->dropColumn('gifs');

            });
    }
}
