<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFeildCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function (Blueprint $table) {
            $table->string('primary_text_color')->nullable();
            $table->string('secondary_text_color')->nullable();
            $table->string('primary_heading_color')->nullable();
            $table->string('secondary_heading_color')->nullable();
            $table->string('menu_color')->nullable();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('primary_text_color');
            $table->dropColumn('secondary_text_color');
            $table->dropColumn('primary_heading_color');
            $table->dropColumn('secondary_heading_color');
            $table->dropColumn('menu_color');
         
        });
    }
}
