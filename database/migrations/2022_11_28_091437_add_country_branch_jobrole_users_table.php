<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryBranchJobroleUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $twillUsersTable = config('twill.users_table', 'twill_users');

        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->string('branch_id')->nullable();
            $table->string('job_role_id')->nullable();
            $table->string('country_id')->nullable();
            $table->string('initial_profile')->default(0);
            
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        $twillUsersTable = config('twill.users_table', 'twill_users');
        Schema::table($twillUsersTable, function (Blueprint $table) {
            $table->dropColumn('branch_id');
            $table->dropColumn('job_role_id');
            $table->dropColumn('country_id');
            $table->dropColumn('initial_profile');
			
        });
    }
}
