<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;

class CompileThemes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compile:themes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Compiling customer themes.");
            $customers = Company::all();

            $bar = $this->output->createProgressBar(count($customers));

            foreach($customers as $customer) {
                $customer->compileTheme();

                $bar->advance();
            }

            $bar->finish();
            $bar->clear();
            $this->info("Customer themes compiled successfully.");
    }
}
