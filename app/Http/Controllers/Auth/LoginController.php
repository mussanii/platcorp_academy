<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use App\Models\Company;
use App\Edx\EdxAuthUser;
use App\Edx\AuthUserprofile;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        AuthManager $authManager
      )
      {
        $this->authManager = $authManager;
        $this->middleware('guest')->except('logout','userLogout');
        $this->edxRepository = $this->getEdxRepository();
    }

    public function showLoginOptions(){
       
      $companies = Company::orderByTranslation('title')->get();
        
      return view('site.auth.login', ['companies' => $companies]);
    }

    public function showLoginForm(Request $request){
    
     return redirect()->route('user.login');
   
    }

    public function userCodeLogin(){

        return view('site.auth.user_code');
    }

    public function login(LoginRequest $request){

    return  App::environment(['local', 'staging']) ? $this->handleProviderLocal($request) : $this->handleProviderLive($request);

    }

    public function handleProviderLocal($request){

        $credentials = ['username' => $request->input('username'), 'password' => $request->input('login_password')];


        if(Auth::attempt($credentials)){
   
          $user = User::where('username',$credentials['username'])->first();
   
          if($user->published != 1 && $user->is_activated != 1){
   
               Auth::logout();

      
               return redirect()->route('userCode.login')->with('login_error', 'Kindly confirm that your account is activated');
          }
          if($user->role === "SUPERADMIN" || $user->role === "ADMIN" || $user->role === "GROUPHR" || $user->role === "BRANCHMANAGER" || $user->role === "HOD" || $user->role === "COMPANYHR"){

            $admin = $this->authManager->guard('twill_users')->loginUsingId($user->id);

   
          }
   
          $user->last_login_at = Carbon::now();
          $user->save();
          return redirect()->route('pages', ['key' => 'home']);
   
        }else{
          
          return redirect()->route('userCode.login')->with('login_error', 'The username or password you entered did not match our records. Please confirm you used the correct credentials provided');
        }


    }

    public function handleProviderLive($request){
     $credentials = ['username' => $request->input('username'), 'password' => $request->input('login_password')];

     if(Auth::attempt($credentials)){

       $user = User::where('username',$credentials['username'])->first();

       if($user->published != 1 && $user->is_activated != 1){

            Auth::logout();
     

            Redirect::route('userCode.login')->with('login_error', 'Kindly confirm that your account is activated');
       }
       if($user->role === "SUPERADMIN" || $user->role === "ADMIN" || $user->role === "GROUPHR" || $user->role === "BRANCHMANAGER" || $user->role === "HOD" || $user->role === "COMPANYHR"){

        /** Authenticate twill */

        $admin = $this->authManager->guard('twill_users')->loginUsingId($user->id);

        /** Authenticate Edx */

        $edxStatus =  $this->edxRepository->edxLogin($user, $request->input('login_password'));

        if(!$edxStatus) {
            Auth::logout();
       

            return redirect()->route('userCode.login')->with('login_error', 'The username or password you entered did not match our records. Please confirm you used the correct credentials provided');
        }

       }else{

        $edxStatus =  $this->edxRepository->edxLogin($user, $request->input('login_password'));

        if(!$edxStatus) {
            Auth::logout();

    
            return redirect()->route('userCode.login')->with('login_error', 'The username or password you entered did not match our records. Please confirm you used the correct credentials provided');
        }


       }

       $edxUser= EdxAuthUser::where('username', $user->username)->first();
       $edxCompany = AuthUserprofile::where('user_id', $edxUser->id)->first();

         if(empty($edxCompany->company)){
          $edxCompany->company = Auth::user()->company_id;
            $edxCompany->save();
        }



       $user->last_login_at = Carbon::now();
       $user->save();
       return redirect()->route('pages', ['key' => 'home']);

     }else{
      return redirect()->route('userCode.login')->with('login_error', 'The username or password you entered did not match our records. Please confirm you used the correct credentials provided');

     }
    }


    protected function getEdxRepository()
    {
        return App::make("App\\Repositories\\AuthenticateEdxRepository");
    }

    public function userLogout(Request $request)
    {
  
      if($this->edxLogout()){
        
        auth()->logout();
        $this->authManager->guard('twill_users')->logout();
  
        return redirect()->route('home');
      }
    }
  

    public function logout(Request $request)
  {

    if($this->edxLogout()){
      
      auth()->logout();
      $this->authManager->guard('twill_users')->logout();

      return redirect()->route('home');
    }
  }

  private function edxLogout(){
    //logout
    $configApp = config()->get("settings.app");
    $cookies = ['csrftoken','edxloggedin','edx-user-info','openedx-language-preference','edinstancexid','sessionid','platcorp_academy_session','edx-company-info','was_reloaded'];
    foreach ($cookies as $key => $cookie) {
      if (isset($_COOKIE[$cookie])) {
        setrawcookie($cookie, '', time() - 3600, '/', $configApp['APP_DOMAIN'] );
        unset($_COOKIE[$cookie]);
      }
    }
    return true;
  }

}
