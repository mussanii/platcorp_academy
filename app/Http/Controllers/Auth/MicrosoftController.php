<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Socialite;
use Auth;
use Exception;
use App;
use App\Models\User;
use App\Models\TwillPosition;
use App\Models\Domain;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mail;
use App\Edx\EdxAuthUser;
use App\Edx\AuthUserprofile;
use Ixudra\Curl\Facades\Curl;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\Cookie;



class MicrosoftController extends Controller
{

    use AuthenticatesUsers;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
        $this->repository = $this->getRepository();
        $this->edxRepository = $this->getEdxRepository();
        $this->middleware('guest')->except('logout');


    }


public function redirectToProvider()
{

        return Socialite::driver('microsoft')->redirect();


}

public function handleProviderCallback(Request $request)
{

    return  App::environment(['local', 'staging']) ? $this->handleProviderLocal() : $this->handleProviderLive();
}



protected function handleProviderLocal(){


    try {

        $data = Socialite::with('microsoft')->stateless()->user();

        $accessToken = $data->token;

        Cookie::queue(Cookie::make('access_tokenz', $accessToken, 60));

        // only allow people with @company.com to login

        $domain = explode("@", $data->email)[1];
        $domainCheck = Domain::where('name', $domain)->first();


        if($domainCheck) {
                 /** Verify if a returning user */

                $finduser = User::where('microsoft_id', $data->id)->first();

                if($finduser) {

                    if(empty($finduser->reset_pd))
                    {
    
                     $randomString = $finduser->first_name.'_'.$finduser->last_name.'_'.$finduser->id;
    
                        $position = new TwillPosition();
                        $position->user_id = $finduser->id;
                        $position->position = $randomString;
    
                        $position->save();
    
                        $finduser->reset_pd = 1;
                        $finduser->save();
    
                    }

                    if(empty($finduser->first_name)){

                    

                        $finduser->first_name = $data->user['givenName'];
                        $finduser->last_name = $data->user['surname'];
                          
                        $finduser->save();
                    }

                    Auth::login($finduser, true);

                    if($finduser->role === "SUPERADMIN" || $finduser->role === "ADMIN" || $finduser->role === "GROUPHR" || $finduser->role === "BRANCHMANAGER" || $finduser->role === "HOD" || $finduser->role === "COMPANYHR") {

                        $this->authManager->guard('twill_users')->loginUsingId($finduser->id);

                        $finduser->last_login_at = Carbon::now();
                        $finduser->save();

                        return redirect()->route('pages', ['key' => 'home']);

                    }else{
                        $finduser->last_login_at = Carbon::now();
                        $finduser->save();

                        return redirect()->route('pages', ['key' => 'home']);

                    }
                }else{
                     /** Create a new user */
                    $newUser = $this->repository->createForRegisterMicrosoft($data, $domainCheck->company_id);

                    if(!$newUser) {
                        $user->delete();

                        $course_errors = [
                            'title' => 'Sorry',
                            'content' => 'An error occured while submitting your registration. Please refresh the page and try again.',
                        ];
        
                            Session::flash('course_errors', $course_errors);
                            return redirect()->to('/user/login');

                    }else{
                        
                        $randomString = $newUser->first_name.'_'.$newUser->last_name.'_'.$newUser->id;
    
                        $position = new TwillPosition();
                        $position->user_id = $newUser->id;
                        $position->position = $randomString;
    
                        $position->save();

                        Auth::login($newUser, true);

                        $newUser->last_login_at = Carbon::now();
                        $newUser->save();
                        
                        return redirect()->route('pages', ['key' => 'home']);
                    }

                }
             }else{

                $course_errors = [
                    'title' => 'Sorry',
                    'content' => 'The email domain is not allowed for this platform',
                ];

                    Session::flash('course_errors', $course_errors);
                    return redirect()->to('/user/login');
             }

        } catch (Exception $e) {
            
            return redirect('user/login');

        }

}


protected function handleProviderLive(){
    try {

        $data = Socialite::with('microsoft')->stateless()->user();

          
        // only allow people with @company.com to login

        $domain = explode("@", $data->email)[1];


        $domainCheck = Domain::where('name', $domain)->first();

        if($domainCheck){

              //search for allowed users in the list

                $finduser = User::where('microsoft_id', $data->id)->first();

              

                if($finduser) {

                    $position = TwillPosition::where('user_id', $finduser->id)->first();

                    if(empty($finduser->reset_pd))
                    {
    
                        $randomString =  self::generateRandomString(9);
    
                        $position = new TwillPosition();
                        $position->user_id = $finduser->id;
                        $position->position = $randomString;
    
                       $this->edxRepository->resetEdxPassword($finduser, $randomString);
            
                        $position->save();
    
                        $finduser->reset_pd = 1;
                        $finduser->save();
    
                    }

                    if(empty($finduser->first_name)){

                        $finduser->first_name = $data->user['givenName'];
                        $finduser->last_name = $data->user['surname'];
                          
                        $finduser->save();
                    }

                  
                    Auth::login($finduser, true);


                    if($finduser->role === "SUPERADMIN" || $finduser->role === "ADMIN" || $finduser->role === "GROUPHR" || $finduser->role === "BRANCHMANAGER" || $finduser->role === "HOD" || $finduser->role === "COMPANYHR") {


                        $this->authManager->guard('twill_users')->loginUsingId($finduser->id);

                        $edxStatus = $this->edxRepository->edxLogin($finduser, $position->position);

          
                        // 
                        if(!$edxStatus) {
                            Auth::logout();
                            
                            $course_errors = [
                                'title' => 'Sorry',
                                'content' => 'The username or password you entered did not match our records. Please confirm you have registered.',
                            ];
            
                                Session::flash('course_errors', $course_errors);
                                return redirect()->to('/user/login');

                    

                        }else{

                            $edxUser= EdxAuthUser::where('username', $finduser->username)->first();
                            $edxCompany = AuthUserprofile::where('user_id', $edxUser->id)->first();
        
                            if(empty($edxCompany->company)){
        
                              $edxCompany->company = Auth::user()->company_id;
        
                              $edxCompany->save();
        
                            }
        

                            $finduser->last_login_at = Carbon::now();
                            $finduser->save();

                            return redirect()->route('pages', ['key' => 'home']);

                        }

                    }else{

                        $edxStatus = $this->edxRepository->edxLogin($finduser, $position->position);
                    
                        if(!$edxStatus) {
                            Auth::logout();
                            $course_errors = [
                                'title' => 'Sorry',
                                'content' => 'The username or password you entered did not match our records. Please confirm you have registered.',
                            ];
            
                                Session::flash('course_errors', $course_errors);
                                return redirect()->to('/user/login');

                        }else{

                            $edxUser= EdxAuthUser::where('username', $finduser->username)->first();
                            $edxCompany = AuthUserprofile::where('user_id', $edxUser->id)->first();
        
                            if(empty($edxCompany->company)){
        
                              $edxCompany->company = Auth::user()->company_id;
        
                              $edxCompany->save();
        
                            }

                            $finduser->last_login_at = Carbon::now();
                            $finduser->save();

                            return redirect()->route('pages', ['key' => 'home']);

                        }
                    }


                }else{

                    $newUser =  $this->repository->createForMicrosoft($data, $domainCheck->company_id);


                    if(!$newUser) {
                        $user->delete();
                         
                        $course_errors = [
                            'title' => 'Sorry',
                            'content' => 'An error occured while submitting your registration. Please refresh the page and try again.',
                        ];
        
                            Session::flash('course_errors', $course_errors);
                            return redirect()->to('/user/login');

        
                    }else{

                        $randomString =  self::generateRandomString(9);

                        $position = new TwillPosition();
                        $position->user_id = $newUser->id;
                        $position->position = $randomString;

                        $position->save();


                        $edxResponse =   $this->edxRepository->edxRegister($newUser, $randomString);

                        if ($edxResponse !== true) {

                            $course_errors = [
                                'title' => 'Sorry',
                                'content' => 'An error occured while submitting your registration. Please refresh the page and try again.',
                            ];
            
                                Session::flash('course_errors', $course_errors);
                                return redirect()->to('/user/login');
    

                            
                        }else{


                            $edxuser = EdxAuthUser::where('username', $newUser->username)->first();
                            $edxuser->first_name = $newUser->first_name;
                            $edxuser->last_name = $newUser->last_name;
                            $edxuser->email = $newUser->email;
                            $edxuser->is_active =  1;
                            $edxuser->save();


                            Auth::login($newUser, true);

                            
                            $edxUser= EdxAuthUser::where('username', $newUser->username)->first();
                            $edxCompany = AuthUserprofile::where('user_id', $edxUser->id)->first();

                            if(empty($edxCompany->company)){

                            $edxCompany->company = Auth::user()->company_id;
                            $edxCompany->save();
                            }

                            $edxStatus =  $this->edxRepository->edxLogin($newUser, $randomString);

                            if(!$edxStatus) {
                                Auth::logout();
                                
                                $course_errors = [
                                    'title' => 'Sorry',
                                    'content' => 'An error occured while submitting your registration. Please refresh the page and try again.',
                                ];
                
                                    Session::flash('course_errors', $course_errors);
                                    return redirect()->to('/user/login');
        
    
                            }else{

                                $newUser->last_login_at = Carbon::now();
                                $newUser->reset_pd = 1;
                                $newUser->save();
                               
                                return redirect()->route('pages', ['key' => 'home']);

                            }

                        }

                    }

                }
              
        }
        else{
            $course_errors = [
                'title' => 'Sorry',
                'content' => 'The email domain is not allowed for this platform',
            ];

                Session::flash('course_errors', $course_errors);
                return redirect()->to('/');

        }



    } catch (Exception $e) {
         //dd($e);
        return redirect('user/login');

    }
}


    protected function getRepository()
    {
        return App::make("App\\Repositories\\UserRepository");
    }


    protected function getEdxRepository()
    {
        return App::make("App\\Repositories\\AuthenticateEdxRepository");
    }
  


    protected function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

}

