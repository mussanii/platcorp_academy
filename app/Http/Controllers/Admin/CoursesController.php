<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Course\BulkDestroyCourse;
use App\Http\Requests\Admin\Course\DestroyCourse;
use App\Http\Requests\Admin\Course\IndexCourse;
use App\Http\Requests\Admin\Course\StoreCourse;
use App\Http\Requests\Admin\Course\UpdateCourse;
use App\Models\Course;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

use Auth;

class CoursesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCourse $request
     * @return array|Factory|View
     */
    public function index(IndexCourse $request)
    {
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if (in_array(auth()->guard('twill_users')->user()->roleValue, $CompanyhrRoles)) {

            $company = Auth::user()->company_id;

            //         $data_r = AdminListing::create(Organisation::class)
            // ->processRequestAndGet(
            //     // pass the request with params
            //     $request,

            //     // set columns to query
            //     ['id', 'organisation_type_id', 'name', 'active'],

            //     // set columns to searchIn
            //     ['id', 'name'],

            //     function($query) use ($request){
            //         if ($request->has('author_id')) {
            //             $query->where('author_id', $request->author_id);
            //         }
            //     }
            // );

            // create and AdminListing instance for a specific model and
            $data = AdminListing::create(Course::class)->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id',  'course_id', 'name', 'effort', 'enrol_start', 'enrol_end', 'status', 'order_id', 'course_video', 'position', 'short_name', 'due_date'],

                // set columns to searchIn
                ['id', 'course_id', 'name', 'short_description', 'overview', 'more_info', 'effort', 'course_image_uri', 'course_video_uri', 'slug', 'course_video', 'short_name'],

                function ($query) use ($company) {

                    $query->where('company_id', $company);
                }
            );
        } else {
            // create and AdminListing instance for a specific model and
            $data = AdminListing::create(Course::class)->processRequestAndGet(
                // pass the request with params
                $request,

                // set columns to query
                ['id',  'course_id', 'name', 'effort', 'enrol_start', 'enrol_end', 'status', 'order_id', 'course_video', 'position', 'short_name', 'due_date'],
                // set columns to searchIn
                ['id', 'course_id', 'name', 'short_description', 'overview', 'more_info', 'effort', 'course_image_uri', 'course_video_uri', 'slug', 'course_video', 'short_name']
            );
        }

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }



        return view('admin.course.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {

        $edx_courses = $this->edxgetCourses();





        foreach ($edx_courses as $key => $edx_course) {
            //Process data
            if (Course::where('course_id', $edx_course['id'])->count()) {


                //Update

                // $slug = str_replace(' ', '-', $edx_course['name']);
                // $slug2 = str_replace('-–-', '-', $slug);

                $c = Course::where('course_id', $edx_course['id'])->first();

                if ($c->company_id) {
                    $s = DB::table('company_translations')->where('company_id', $c->company_id)->first();

                    $cs =  str_replace(' ', '-', $s->title);
                    $slug = str_replace(' ', '-', $c->name);
                    $slug2 = $slug . '-' . $cs;
                }else{

                    $slug2 = $c->name;
                }


                // $sanitized['short_name'] = $slug2;


                $video = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $edx_course['course_video_uri']);

                //Update
                DB::table('courses')
                    ->where('course_id', $edx_course['id'])
                    ->update([
                        'course_id' => $edx_course['id'],
                        'name' =>  $edx_course['name'],
                        'short_description' => $edx_course['short_description'],
                        'overview' => $edx_course['overview'],
                        'effort' => $edx_course['effort'],
                        'start' => $edx_course['start'],
                        'end' => $edx_course['end'],
                        'enrol_start' => $edx_course['enrollment_start'],
                        'enrol_end' => $edx_course['enrollment_end'],
                        'course_image_uri' => $edx_course['course_image_uri'],
                        'course_video_uri' => $edx_course['course_video_uri'],
                        'short_name' => ($slug2) ? $slug2 : $c->short_name,
                        'course_category_id' => 0,
                        'order_id' => 0,
                        'course_video' => $video,


                    ]);
            } else {


                $slug = str_replace(' ', '-', $edx_course['name']);
                $slug2 = str_replace('-–-', '-', $slug);

                $c = Course::where('slug', $slug2)->first();

                if ($c) {

                    $slug2 = $slug2 . '-' . rand(10, 100);
                }

                $video = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $edx_course['course_video_uri']);
                //Create
                $course = new Course();
                $course->course_id = $edx_course['id'];
                $course->name =  $edx_course['name'];
                $course->short_description = $edx_course['short_description'];
                $course->overview = $edx_course['overview'];
                $course->effort = $edx_course['effort'];
                $course->start = $edx_course['start'];
                $course->end = $edx_course['end'];
                $course->enrol_start = $edx_course['enrollment_start'];
                $course->enrol_end = $edx_course['enrollment_end'];
                $course->course_image_uri = $edx_course['course_image_uri'];
                $course->course_video_uri = $edx_course['course_video_uri'];
                $course->short_name = $slug2;
                $course->course_video = $video;
                $course->course_category_id = 0;
                $course->order_id = 0;
                $course->save();
            }
        }

        return redirect('admin/courses')->with(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCourse $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCourse $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Course
        $course = Course::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/courses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/courses');
    }

    /**
     * Display the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return void
     */
    public function show(Course $course)
    {
        $this->authorize('admin.course.show', $course);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Course $course)
    {

        $course->load('category');
        return view('admin.course.edit', [
            'course' => $course,
            'categories' => DB::table('course_category_translations')->get(),
            'types' => DB::table('course_types')->select('name', 'id')->get(),
            'subsidiaries' => DB::table('company_translations')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCourse $request
     * @param Course $course
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCourse $request, Course $course)
    {

        
        // Sanitize input






        $sanitized = $request->getSanitized();

        //$sanitized['course_category_id'] = $request->getCourseCategoryId();

        $sanitized['course_type_id'] = $request->getCourseTypeId();

        $sanitized['company_id'] = $request->getCompanyId();



        if ($request->getCompanyId()) {
            $s = DB::table('company_translations')->where('company_id', $request->getCompanyId())->first();

            $cs =  str_replace(' ', '-', $s->title);
            $slug = str_replace(' ', '-', $course->name);
            $slug2 = $slug . '-' . $cs;

            $sanitized['short_name'] = $slug2;
        }



        if ($sanitized['status'] == true) {
            $sanitized['published'] = 1;
        } else {

            $sanitized['published'] = 0;
        }


        // Update changed values Course
        $course->update($sanitized);

        //dd($course);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/courses'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCourse $request
     * @param Course $course
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCourse $request, Course $course)
    {
        $course->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCourse $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCourse $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('courses')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }


    //Function to get courses from edx
    private function edxgetCourses()
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/?page=3&page_size=400000000000000');

            $courses =  json_decode($response->getBody()->getContents())->results;



            foreach ($courses as $key => $value) {
                $course = (array)$value;
                $courses[$key] = (array)$courses[$key];
                $course['overview'] = $this->getOverview($course);
                //Remove unwanted fields
                $course['course_video_uri'] = $course['media']->course_video->uri;
                $course['course_image_uri'] =  $configLms['LMS_BASE'] . $course['media']->course_image->uri;
                unset($course['media']);
                unset($course['course_id']);
                //Format datetime
                $vowels = array("T", "Z");
                $course['start'] =  date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['start'])));
                $course['end'] = date('Y-m-d H:m:i', strtotime(str_replace($vowels, " ", $course['end'])));
                $course['enrollment_start'] = date('Y-m-d H:m:i', strtotime($course['enrollment_start']));
                $course['enrollment_end'] = date('Y-m-d H:m:i', strtotime($course['enrollment_end']));
                //Format time
                $exploded_effort = explode(":", $course['effort']);
                switch (count($exploded_effort)) {
                    case '3':
                        $course['effort'] = Carbon::createFromTime($exploded_effort[0], $exploded_effort[1], $exploded_effort[2])->toTimeString();
                        break;
                    case '2':
                        $course['effort'] = Carbon::createFromTime(0, $exploded_effort[0], $exploded_effort[1])->toTimeString();
                        break;
                }
                $courses[$key] = $course;
            }
            return $courses;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            //dd($response);
        }
    }


    private function getOverview($course)
    {
        $configLms = config()->get("settings.lms.live");
        $client = new \GuzzleHttp\Client();
        try {
            //Get course description
            $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/courses/v1/courses/' . $course['id']);
            $response = json_decode($request->getBody()->getContents());
            return $response->overview;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = $responseJson->getBody()->getContents();
            return Toastr::error("Error enrolling into course");
            return false;
        }
    }


    public function sync()
    {

        $courses = Course::all();

        foreach ($courses as $course) {

            if ($course->company_id) {
                $s = DB::table('company_translations')->where('company_id', $course->company_id)->first();

                $cs =  str_replace(' ', '-', $s->title);
                $slug2 = str_replace(' ', '-', $course->name);
                $slug = $slug2 . '-' . $cs;
            } else {

                $slug = str_replace(' ', '-', $course->name);
            }

            $course->short_name = $slug;

            $course->save();
        }

        return 'Yes';
    }
}
