<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\MenuRepository;
use App\Repositories\MenuTypeRepository;
class MenuController extends BaseModuleController
{
    protected $moduleName = 'menus';
    protected $indexOptions = [
        'reorder' => true,
    ];


    protected function formData($request)
    {

        return [
			'menuList' => app(MenuRepository::class)->listAll('title'),
            'menuTypeList' => app(MenuTypeRepository::class)->listAll('title'),
			
        ];

	}
  
}
