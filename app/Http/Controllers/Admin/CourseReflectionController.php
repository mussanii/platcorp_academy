<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class CourseReflectionController extends BaseModuleController
{
    protected $moduleName = 'courseReflections';

    protected $indexOptions = [
        'permalink' => false,
    ];
}
