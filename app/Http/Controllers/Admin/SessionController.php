<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\NestedModuleController as BaseModuleController;
use App\Models\User;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\JobRoleRepository;
use Illuminate\Support\Facades\Auth;

class SessionController extends BaseModuleController
{
    protected $moduleName = 'sessions';

    protected $indexOptions = [
        'permalink' => false,
    ];

    
    protected function formData($request)
    {
        $company_id = Auth::user()->company_id;
        $userEmails = User::where('company_id',$company_id)->pluck('id','email');
    

        return [
			'countryList' => app(CountryRepository::class)->listAll('title'),
            'companyList' => app(CompanyRepository::class)->listAll('title'),
            'jobRoleList' => app(JobRoleRepository::class)->listAll('title'),
            'emailList'=> $userEmails
        ];

	}
}
