<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Models\Survery;
use App\Models\UserSurvey;
use DB;
use Illuminate\Http\Request;
class UserSurveyController extends BaseModuleController
{
    protected $moduleName = 'userSurveys';
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
        'publish' =>false,
    ];


    public function userSurvey(Request $request){

        $type = 'App\Models\Survery';

        $results= '';
        $questions = '';
        if ($request->isMethod('post')) {
        
           $results = UserSurvey::where('survey_id',$request->input('survey'))->get();
           $questions = UserSurvey::where('survey_id',$request->input('survey'))->first();
         
        }

        $surveys = Survery::published()->get();
        return view('admin.userSurveys.index', compact('surveys','results','questions'));
    }

}
