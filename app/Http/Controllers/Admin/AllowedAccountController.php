<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\CompanyRepository;
use Auth;
use DB;
use App\Imports\LicenseImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Illuminate\Http\Request;

class AllowedAccountController extends BaseModuleController
{
    protected $moduleName = 'allowedAccounts';

    protected $indexOptions = [
        'permalink' => false,
       
    ];

    protected $titleColumnKey = 'email';

    protected $indexColumns = [

       
        'email' => [ // field column
            'title' => 'Email',
            'field' => 'email',
        ],
        'company_id' => [ // field column
            'title' => 'Subsidiary',
            'sort' => true,
            'relationship' => 'companies',
            'field' => 'title'
        ],


    ];

    protected function formData($request)
    {
        $CompanyhrRoles = ['Company HR'];

        if (auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue, $CompanyhrRoles))) {
            $company = DB::table('company_translations')->where('company_id', Auth::user()->company_id)->pluck('title', 'company_id');
        } else {

            $company =  app(CompanyRepository::class)->listAll('title');
        }

        return [
            'companyList' => $company,
        ];
    }


    protected function getIndexItems($scopes = [], $forcePagination = false)
    {


        $CompanyhrRoles = ['Company HR'];

        if (auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue, $CompanyhrRoles))) {
            return $this->transformIndexItems($this->repository->getCompany(
                $this->indexWith,
                $scopes,
                $this->orderScope(),
                $this->request->get('offset') ?? $this->perPage ?? 50,
                $forcePagination
            ));
        } else {
            return $this->transformIndexItems($this->repository->get(
                $this->indexWith,
                $scopes,
                $this->orderScope(),
                $this->request->get('offset') ?? $this->perPage ?? 50,
                $forcePagination
            ));
        }
    }


    public function usersUpload()
    {

        return view('admin.uploadUsers.upload-form');
    }

    public function uploadStore(Request $request)
    {

        Excel::import(new LicenseImport, request()->file('file'));

        $name = Session::get('uploaded');

        if ($name === 'Yes') {

            Session::flash('status', 'Import Successful');

            return redirect()->route('admin.accounts.allowedAccounts.index');
        }
    }
}
