<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Http\Requests\Admin\CourseRequest;
use Mail;
use Carbon\Carbon;
use Auth;
use App\Models\User;
use Illuminate\Support\Arr;
use App\Models\Notification;


class CourseRequestController extends BaseModuleController
{
    protected $moduleName = 'courseRequests';
    protected $indexOptions = [
        'create' =>false,
        'edit' =>false,
        'permalink' => false,
       
    ];


    protected $indexColumns = [

        'title' => [ // field column
            'title' => 'User Name',
            'field' => 'title',
        ],
         'course_id' => [ // relation column
            'title' => 'Course',
            'sort' => true,
            'relationship' => 'courses',
            'field' => 'name'
        ],

        'date_request' => [ // relation column
            'title' => 'Date of Request',
            'field' => 'date_request'
        ],
        'date_approval' => [ // field column
            'title' => 'Date of Approval',
            'field' => 'date_approval',
        ],

        'access_status'=>[
            'title' => 'Status',
            'field' => 'access_status',
        ],
        'published_by' => [ // relation column
            'title' => 'Published By',
            'relationship' => 'userAdmin',
            'field' => 'name'
        ],
    ];

 protected function getIndexData($prependScope = [])
    {
        $scopes = $this->filterScope($prependScope);
        $items = $this->getIndexItems($scopes);

        $data = [
            'tableData' => $this->getIndexTableData($items),
            'tableColumns' => $this->getIndexTableColumns($items),
            'tableMainFilters' => $this->getIndexTableMainFilters($items),
            'filters' => json_decode($this->request->get('filter'), true) ?? [],
            'hiddenFilters' => array_keys(Arr::except($this->filters, array_keys($this->defaultFilters))),
            'filterLinks' => $this->filterLinks ?? [],
            'maxPage' => method_exists($items, 'lastPage') ? $items->lastPage() : 1,
            'defaultMaxPage' => method_exists($items, 'total') ? ceil($items->total() / $this->perPage) : 1,
            'offset' => method_exists($items, 'perPage') ? $items->perPage() : count($items),
            'defaultOffset' => $this->perPage,
        ] + $this->getIndexUrls($this->moduleName, $this->routePrefix);

        $baseUrl = $this->getPermalinkBaseUrl();

        $options = [
            'moduleName' => $this->moduleName,
            'skipCreateModal' => $this->getIndexOption('skipCreateModal'),
            'reorder' => $this->getIndexOption('reorder'),
            'create' => $this->getIndexOption('create'),
            'duplicate' => $this->getIndexOption('duplicate'),
            'translate' => $this->moduleHas('translations'),
            'translateTitle' => $this->titleIsTranslatable(),
            'permalink' => $this->getIndexOption('permalink'),
            'bulkEdit' => $this->getIndexOption('bulkEdit'),
            'titleFormKey' => $this->titleFormKey ?? $this->titleColumnKey,
            'baseUrl' => $baseUrl,
            'permalinkPrefix' => $this->getPermalinkPrefix($baseUrl),
            'additionalTableActions' => $this->additionalTableActions(),
        ];

        return array_replace_recursive($data + $options, $this->indexData($this->request));
    }



    protected function getIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForCompany(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }

/**
     * @param Request $request
     * @return array
     */
    protected function indexData($request)
    {
        return [
            'defaultFilterSlug' => 'published',
            
        ];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $items
     * @param array $scopes
     * @return array
     */
    protected function getIndexTableMainFilters($items, $scopes = [])
    {
        $statusFilters = [];

        $scope = ($this->submodule ? [
            $this->getParentModuleForeignKey() => $this->submoduleParentId,
        ] : []) + $scopes;

       

        if ($this->getIndexOption('publish')) {
            $statusFilters[] = [
                'name' => 'Approved Requests',
                'slug' => 'published',
                'number' => $this->repository->getCountByStatusSlug('published', $scope),
            ];
            $statusFilters[] = [
                'name' => 'Pending Request',
                'slug' => 'draft',
                'number' => $this->repository->getCountByStatusSlug('draft', $scope),
            ];
        }

        if ($this->getIndexOption('restore')) {
            $statusFilters[] = [
                'name' => twillTrans('twill::lang.listing.filter.trash'),
                'slug' => 'trash',
                'number' => $this->repository->getCountByStatusSlug('trash', $scope),
            ];
        }

        return $statusFilters;
    }


    


    public function publish()
    {

       
        try {
            if ($this->repository->updateBasic($this->request->get('id'), [
                'published' => ! $this->request->get('active'),
                 'status' => 1,
                 'published_by'=> Auth::user()->id,
                 'date_approval' => Carbon::now(),

            ])) {
                activity()->performedOn(
                    $this->repository->getById($this->request->get('id'))
                )->log(
                    ($this->request->get('active') ? 'un' : '') . 'published'
                );

                $this->fireEvent();

                if ($this->request->get('active')) {
                    return $this->respondWithSuccess(twillTrans('twill::lang.listing.publish.unpublished', ['modelTitle' => $this->modelTitle]));
                } else {

                    $access = $this->repository->getById($this->request->get('id'));
                    $user = User::where('id',$access->user_id)->first();


                    $message = [
                        'type'=> Notification::REQUEST,
                        'subject'=> 'Course Request Approved',
                        'model_id' => $this->request->get('id'),
                        'message' => 'The Course request for '. $access->courses->name.' has been approved. Click in order to view the course (s).',
                       ];

                       $user->notify(new \App\Notifications\CourseRequestNotification($message));

                    
                    if($user->authentication_type != 3){

                    Mail::send('emails.course_notification',['name' => $user->first_name.' '.$user->last_name, 'course'=> $access->courses->name, 'url'=> route('course.detail', $access->courses->id)],
                    function ($message) use ($user) {
                      $message->from('support@farwell-consultants.com', 'The Platcorp Group Academy Course Access Notification');
                      $message->to($user->email, $user->first_name);
                      $message->subject('The Platcorp Group Academy Course Access Notification');
                    });
                }
                    return $this->respondWithSuccess(twillTrans('twill::lang.listing.publish.published', ['modelTitle' => $this->modelTitle]));
                }
            }
        } catch (\Exception $e) {
            \Log::error($e);
        }

        return $this->respondWithError(twillTrans('twill::lang.listing.publish.error', ['modelTitle' => $this->modelTitle]));
    }




    
}
