<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\EvaluationOptionRepository;
use App\Repositories\CourseRepository;
use Auth;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class EvaluationController extends BaseModuleController
{
    protected $moduleName = 'evaluations';

    protected $indexOptions = [
        'permalink' => false,
    ];

    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'courses_selected' => [ // field column
            'title' => 'Related Course',
            'relationship' => 'courses',
            'field' => 'short_name'
        ],
    ];

    protected function formData($request)
    {

        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles )){
           $course = app(CourseRepository::class)->where('company_id', Auth::user()->company_id)->pluck('short_name','id');
         }else{
            
             $course = app(CourseRepository::class)->listAll('short_name');
         }
        return [
			'optionList' => app(EvaluationOptionRepository::class)->listAll('title'),
            'courseList' => $course,
        ];

	}


    protected function getIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForCompany(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }
}
