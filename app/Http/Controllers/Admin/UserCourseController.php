<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\CourseRepository;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Support\Arr;

class UserCourseController extends BaseModuleController
{
    protected $moduleName = 'userCourses';
    protected $indexOptions = [
        'permalink' => false,
    ];



    protected $indexColumns = [

        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],

        'courses_selected' => [ // field column
            'title' => 'Recommended Courses',
            'field' => 'courses_selected',
        ],

        // 'user_selected' => [ // field column
        //     'title' => 'Recommended Courses',
        //     'field' => 'user_selected',
        // ],
    ];


    protected function formData($request)
    {

        $CompanyhrRoles = ['Company HR'];

        if (auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue, $CompanyhrRoles))) {

            $userList = app(UserRepository::class)->where('published', 1)->where('company_id', Auth::user()->company_id)->where('id', '!=', 1)->pluck('name', 'id');
        } else {

            $userList = app(UserRepository::class)->where('published', 1)->where('id', '!=', 1)->pluck('name', 'id');
        }

        return [
            'userList' => $userList,
            'courseList' => app(CourseRepository::class)->where('status', 1)->pluck('name', 'id'),

        ];
    }


    protected function getIndexData($prependScope = [])
    {
        $scopes = $this->filterScope($prependScope);
        $items = $this->getIndexItems($scopes);



        $data = [
            'tableData' => $this->getIndexTableData($items),
            'tableColumns' => $this->getIndexTableColumns($items),
            'tableMainFilters' => $this->getIndexTableMainFilters($items),
            'filters' => json_decode($this->request->get('filter'), true) ?? [],
            'hiddenFilters' => array_keys(Arr::except($this->filters, array_keys($this->defaultFilters))),
            'filterLinks' => $this->filterLinks ?? [],
            'maxPage' => method_exists($items, 'lastPage') ? $items->lastPage() : 1,
            'defaultMaxPage' => method_exists($items, 'total') ? ceil($items->total() / $this->perPage) : 1,
            'offset' => method_exists($items, 'perPage') ? $items->perPage() : count($items),
            'defaultOffset' => $this->perPage,
        ] + $this->getIndexUrls($this->moduleName, $this->routePrefix);

        $baseUrl = $this->getPermalinkBaseUrl();

        $options = [
            'moduleName' => $this->moduleName,
            'skipCreateModal' => $this->getIndexOption('skipCreateModal'),
            'reorder' => $this->getIndexOption('reorder'),
            'create' => $this->getIndexOption('create'),
            'duplicate' => $this->getIndexOption('duplicate'),
            'translate' => $this->moduleHas('translations'),
            'translateTitle' => $this->titleIsTranslatable(),
            'permalink' => $this->getIndexOption('permalink'),
            'bulkEdit' => $this->getIndexOption('bulkEdit'),
            'titleFormKey' => $this->titleFormKey ?? $this->titleColumnKey,
            'baseUrl' => $baseUrl,
            'permalinkPrefix' => $this->getPermalinkPrefix($baseUrl),
            'additionalTableActions' => $this->additionalTableActions(),
        ];

        return array_replace_recursive($data + $options, $this->indexData($this->request));
    }



    protected function getIndexItems($scopes = [], $forcePagination = false)
    {
        return $this->transformIndexItems($this->repository->getForCompany(
            $this->indexWith,
            $scopes,
            $this->orderScope(),
            $this->request->get('offset') ?? $this->perPage ?? 50,
            $forcePagination
        ));
    }




}
