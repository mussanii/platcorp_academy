<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Models\JobRole;
use App\Models\User;
use App\Repositories\BranchRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\MessageTypeRepository;
use App\Repositories\UserRepository;
use App\Repositories\JobRoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends BaseModuleController
{
    protected $moduleName = 'messages';
    protected $indexOptions = [
        'permalink' => false,
    ];
    



    protected function formData($request)
    {
        $userList1 =User::where('company_id',1)->pluck('email','id');
        $userList2 =User::where('company_id',2)->pluck('email','id');
        $userList3 =User::where('company_id',3)->pluck('email','id');
        $userList4 =User::where('company_id',4)->pluck('email','id');
        $userList5 =User::where('company_id',5)->pluck('email','id');
        $userList6 =User::where('company_id',6)->pluck('email','id');
        $userList7 =User::where('company_id',7)->pluck('email','id');
        $userList8 =User::where('company_id',8)->pluck('email','id');
        $userList9 =User::where('company_id',9)->pluck('email','id');
        $userList10=User::where('company_id',10)->pluck('email','id');
        $userList11=User::where('company_id',11)->pluck('email','id');
        $userList12=User::where('company_id',12)->pluck('email','id');
        $targetList = DB::table('targets')->pluck('title','id');

        $jobrole1 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 1)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole2 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 2)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole3 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 3)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole4 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 4)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole5 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 5)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole6 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 6)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole7 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 7)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole8 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 8)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole9 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 9)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole10 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 10)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');


        $jobrole11 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 11)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');

        $jobrole12 = JobRole::leftJoin('job_role_translations', 'job_roles.id', '=', 'job_role_translations.job_role_id')
        ->where('job_roles.company_id', 12)
        ->orderBy('job_role_translations.title')
        ->pluck('job_role_translations.title', 'job_roles.id');
  
        return [
			'companyList' => app(CompanyRepository::class)->listAll('title'),
            'messageTypeList' => app(MessageTypeRepository::class)->listAll('title'),
            'jobRoleList1' => $jobrole1,
            'jobRoleList2' => $jobrole2,
            'jobRoleList3' => $jobrole3,
            'jobRoleList4' => $jobrole4,
            'jobRoleList5' => $jobrole5,
            'jobRoleList6' => $jobrole6,
            'jobRoleList7' => $jobrole7,
            'jobRoleList8' => $jobrole8,
            'jobRoleList9' => $jobrole9,
            'jobRoleList10' => $jobrole10,
            'jobRoleList11' => $jobrole11,
            'jobRoleList12' => $jobrole12,
            'targetList' =>$targetList,
            'userList1' =>$userList1,
            'userList2' =>$userList2,
            'userList3' =>$userList3,
            'userList4' =>$userList4,
            'userList5' =>$userList5,
            'userList6' =>$userList6,
            'userList7' =>$userList7,
            'userList8' =>$userList8,
            'userList9' =>$userList9,
            'userList10' =>$userList10,
            'userList11' =>$userList11,
            'userList12' =>$userList12,
            'branchList' => app(CompanyRepository::class)->listAll('title'),
        ];

	}

   
}
