<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\CompanyRepository;
use App\Models\Company;

class DivisionController extends BaseModuleController
{
    protected $moduleName = 'divisions';

    protected $indexOptions = [
        'permalink' => false,
        'skipCreateModal' => true,
    ];


    protected $filters = [
        'company_id'=> 'company_id',
    ];
    
    protected $indexColumns = [
            
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'company_id' => [ // relation column
            'title' => 'Company',
            'sort' => true,
            'relationship' => 'company',
            'field' => 'title'
        ],
    ];

    protected function formData($request)
{

    return [
        'companyList' => app(CompanyRepository::class)->listAll('title'),
        
    ];

}

protected function indexData($request)
{
    return [
        'company_idList' =>  Company::all(),
       
    ];
}
    
}
