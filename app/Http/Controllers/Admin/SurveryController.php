<?php
namespace App\Http\Controllers\Admin;
use App\Repositories\CompanyRepository;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use Auth;
use DB;
class SurveryController extends BaseModuleController
{
    protected $moduleName = 'surverys';
    protected $indexOptions = [
        'permalink' => false,
    ];

    protected function formData($request)
    {
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles )){
           $course = DB::table('company_translations')->where('id', Auth::user()->company_id)->pluck('title','id');
         }else{
            
             $course = app(CompanyRepository::class)->listAll('title');
         }

        return [
            'companyList' => $course,
        ];

	}
}
