<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
class OurValueController extends BaseModuleController
{
    protected $moduleName = 'ourValues';
    protected $indexOptions = [
        'permalink' => false,
    ];
}
