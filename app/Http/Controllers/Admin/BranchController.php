<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\CountryRepository;
use App\Repositories\CompanyRepository;
class BranchController extends ModuleController
{
    protected $moduleName = 'branches';
    

    protected $indexOptions = [
        'reorder' => true,
];


protected $indexWith = ['country'];


    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        'company_id' => [ // relation column
            'title' => 'Company',
            'sort' => true,
            'relationship' => 'company',
            'field' => 'title'
        ],
        'country_id' => [ // relation column
            'title' => 'Country',
            'sort' => true,
            'relationship' => 'country',
            'field' => 'title'
        ],
        'created_at' => [
            'title' => 'Uploaded On',
            'field' => 'created_at',
            'present' => true, 
        ],
    ];


protected function formData($request)
    {

        return [
			'countryList' => app(CountryRepository::class)->listAll('title'),
            'companyList' => app(CompanyRepository::class)->listAll('title'),
        ];

	}



    public function getIndexTableMainFilters($items, $scopes = [])
    {
        $statusFilters = [];
    
        array_push($statusFilters, [
            'name' => twillTrans('twill::lang.listing.filter.published'),
            'slug' => 'published',
            'number' => $this->repository->getCountByStatusSlug('published'),
        ], [
            'name' => twillTrans('twill::lang.listing.filter.draft'),
            'slug' => 'draft',
            'number' => $this->repository->getCountByStatusSlug('draft'),
        ],
        
        );
    
        if ($this->getIndexOption('restore')) {
            array_push($statusFilters, [
                'name' => twillTrans('twill::lang.listing.filter.trash'),
                'slug' => 'trash',
                'number' => $this->repository->getCountByStatusSlug('trash'),
            ]);
        }
    
        return $statusFilters;
    }
}
