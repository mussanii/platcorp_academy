<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
class MenuTypeController extends BaseModuleController
{
    protected $moduleName = 'menuTypes';
    protected $indexOptions = [
        'reorder' => true,
    ];
   
}
