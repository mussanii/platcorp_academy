<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Models\Enums\UserRole;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use PragmaRX\Google2FAQRCode\Google2FA;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\BranchRepository;
use App\Repositories\JobRoleRepository;
use App;
use App\Edx\EdxAuthUser;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\User;
use App\Models\Company;
use App\Models\Division;
use App\Http\Requests\Admin\UserCodeRequest;
use App\Exports\UsersExport;
use Carbon\Carbon;
use Redirect;
use App\Models\TwillPosition;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersListExport;


class UserController extends BaseModuleController
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var AuthFactory
     */
    protected $authFactory;

    /**
     * @var string
     */
    protected $namespace = 'A17\Twill';

    /**
     * @var string
     */
    protected $moduleName = 'users';

    /**
     * @var string[]
     */
    protected $indexWith = ['medias'];

    /**
     * @var array
     */
    protected $defaultOrders = ['name' => 'asc'];

    /**
     * @var array
     */
    protected $defaultFilters = [
        'search' => 'search',
    ];

    /**
     * @var array
     */
    protected $filters = [
        'role' => 'role',
        'company_id' => 'company_id',
        'division_id'=>'division_id',
    ];

    /**
     * @var string
     */
    protected $titleColumnKey = 'name_value';


    protected $perPage = 100;


    /**
     * @var array
     */
    protected $indexColumns = [
        'name_value' => [
            'title' => 'Name',
            'field' => 'name_value',
        ],
        'email' => [
            'title' => 'Email',
            'field' => 'email',
            'sort' => true,
        ],

        'employee_number' => [
            'title' => 'Employee Number',
            'field' => 'employee_number',
            'sort' => true,
        ],

        'department_value' => [
            'title' => 'Department',
            'field' => 'department_value',
            'sort' => true,
            'sortKey' => 'department',
        ],
        'job_role_value' => [
            'title' => 'Job Role',
            'field' => 'job_role_value',
            'sort' => true,
            'sortKey' => 'job_role',
        ],

        'branch_value' => [
            'title' => 'Branch',
            'field' => 'branch_value',
            'sort' => true,
            'sortKey' => 'branch',
        ],
        'country_value' => [
            'title' => 'Country',
            'field' => 'country_value',
            'sort' => true,
            'sortKey' => 'country',
        ],
        'created_at' => [
            'title' => 'Registered on',
            'field' => 'created_at',
            'sort' => true,

        ],
        'last_login_at' => [
            'title' => 'Last Login',
            'field' => 'last_login_at',
            'sort' => true,

        ],
    ];

    /**
     * @var array
     */
    protected $indexOptions = [
        'permalink' => false,
    ];

    /**
     * @var array
     */
    protected $fieldsPermissions = [
        'role' => 'manage-users',
    ];

    public function __construct(Application $app, Request $request, AuthFactory $authFactory, Config $config)
    {
        parent::__construct($app, $request);

        $this->authFactory = $authFactory;
        $this->config = $config;

        $this->edxRepository = $this->getEdxRepository();

        $this->removeMiddleware('can:edit');
        $this->removeMiddleware('can:delete');
        $this->removeMiddleware('can:publish');
        $this->middleware('can:manage-users', ['only' => ['index']]);
        $this->middleware('can:edit-user,user', ['only' => ['store', 'edit', 'update', 'destroy', 'bulkDelete', 'restore', 'bulkRestore']]);
        $this->middleware('can:publish-user', ['only' => ['publish']]);

        if ($this->config->get('twill.enabled.users-image')) {
            $this->indexColumns = [
                'image' => [
                    'title' => 'Image',
                    'thumb' => true,
                    'variant' => [
                        'role' => 'profile',
                        'crop' => 'default',
                    ],
                ],
            ] + $this->indexColumns;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function indexData($request)
    {
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ) )){
            $company = Company::published()->where('id', Auth::user()->company_id)->get();
         }else{
            
             $company = Company::published()->get();
         }

        return [
            'defaultFilterSlug' => 'published',
            'create' => $this->getIndexOption('create') && $this->authFactory->guard('twill_users')->user()->can('manage-users'),
            'roleList' => Collection::make(UserRole::toArray()),
            'company_idList' =>  $company,
            'division_idList' =>  Division::all(),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
        ];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     */
    protected function formData($request)
    {
        $user = $this->authFactory->guard('twill_users')->user();
        $with2faSettings = $this->config->get('twill.enabled.users-2fa') && $user->id == $request->route('user');

        if ($with2faSettings) {
            $user->generate2faSecretKey();

            $qrCode = $user->get2faQrCode();
        }

        return [
            'roleList' => Collection::make(UserRole::toArray()),
            'countryList' => app(CountryRepository::class)->listAll('title'),
            'jobRoleList' => app(JobRoleRepository::class)->listAll('title'),
            'branchList' => app(BranchRepository::class)->listAll('title'),
            'single_primary_nav' => [
                'users' => [
                    'title' => twillTrans('twill::lang.user-management.users'),
                    'module' => true,
                ],
            ],
            'customPublishedLabel' => twillTrans('twill::lang.user-management.enabled'),
            'customDraftLabel' => twillTrans('twill::lang.user-management.disabled'),
            'with2faSettings' => $with2faSettings,
            'qrCode' => $qrCode ?? null,
        ];
    }

    /**
     * @return array
     */
    protected function getRequestFilters()
    {
        return json_decode($this->request->get('filter'), true) ?? ['status' => 'published'];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $items
     * @param array $scopes
     * @return array
     */
    public function getIndexTableMainFilters($items, $scopes = [])
    {
        $statusFilters = [];

        array_push($statusFilters, [
            'name' => twillTrans('twill::lang.user-management.active'),
            'slug' => 'published',
            'number' => $this->repository->getCountByStatusSlug('published'),
        ], [
            'name' => twillTrans('twill::lang.user-management.disabled'),
            'slug' => 'draft',
            'number' => $this->repository->getCountByStatusSlug('draft'),
        ]);

        if ($this->getIndexOption('restore')) {
            array_push($statusFilters, [
                'name' => twillTrans('twill::lang.user-management.trash'),
                'slug' => 'trash',
                'number' => $this->repository->getCountByStatusSlug('trash'),
            ]);
        }

        return $statusFilters;
    }

    /**
     * @param string $option
     * @return bool
     */
    protected function getIndexOption($option)
    {
        if (in_array($option, ['publish', 'delete', 'restore'])) {
            return $this->authFactory->guard('twill_users')->user()->can('manage-users');
        }

        return parent::getIndexOption($option);
    }

    /**
     * @param \A17\Twill\Models\Model $item
     * @return array
     */
    protected function indexItemData($item)
    {

        $user = $this->authFactory->guard('twill_users')->user();
        $canEdit = $user->can('manage-users') || $user->id === $item->id;
        return [
            'edit' => $canEdit ? $this->getModuleRoute($item->id, 'edit') : null,
        ];
    }

    public function getSubmitOptions(Model $item): ?array
    {
        // Use options from form template
        return null;
    }


    /**
     * @param array $scopes
     * @param bool $forcePagination
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getIndexItems($scopes = [], $forcePagination = false)
    {

       
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){
            return $this->transformIndexItems($this->repository->getCompany(
                $this->indexWith,
                $scopes,
                $this->orderScope(),
                $this->request->get('offset') ?? $this->perPage ?? 50,
                $forcePagination
            ));

        }else{
            return $this->transformIndexItems($this->repository->get(
                $this->indexWith,
                $scopes,
                $this->orderScope(),
                $this->request->get('offset') ?? $this->perPage ?? 50,
                $forcePagination
            ));


        }

       
    }




    public function userCodeForm()
    {

        $companyList =  app(CompanyRepository::class)->listAll('name');

        return view('admin.users.user_code', compact($companyList));
    }


    public function userCodeStore(UserCodeRequest $request)
    {

        return  App::environment(['local', 'staging']) ? $this->userCodeLocal($request) : $this->userCodeLive($request);
    }


    public function userCodeLocal(Request $request)
    {
        $data = [];
        $users = [];
        $number = $request->get('numbers');
        $team = Company::where('id', '=', $request->get('company'))->first();
        $initials = $team->company_code;


        if ($initials) {
            $check = DB::table('twill_users')->where('company_initials', '=', $initials)->latest('id')->first();

            if (empty($check->user_number)) {
                for ($i = 1; $i <= $number; $i++) {
                    $randomString =  self::generateRandomString(9);

                    $pass = Hash::make($randomString);

                    $topic[] = ['Name' => $team->title . '-' . Carbon::now()];
                    $data[] = [
                        'username' => $initials . sprintf("%'05d", $i),
                        'Password' => $randomString

                    ];
                    $users[] = User::create([
                        'name' => $initials . '' . sprintf("%'05d", $i),
                        'first_name' => $initials,
                        'last_name' => sprintf("%'05d", $i),
                        'username' => $initials . sprintf("%'05d", $i),
                        'role' => UserRole::Learner,
                        'email' => $team->id . '@' . sprintf("%'05d", $i) . '.com',
                        'company_initials' => $initials,
                        'company_id' => $team->id,
                        'user_number' => sprintf("%'05d", $i),
                        'password' => $pass,
                        'email_verified_at' => Carbon::now(),
                        'is_activated' => 1,
                        'published' => 1,
                        'registered_at' => Carbon::now(),
                        'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    ]);
                }

                foreach ($data as $user) {

                    $u = User::where('username', $user['username'])->first();

                    $position = new TwillPosition();
                    $position->user_id = $u->id;
                    $position->position = $user['Password'];

                    $position->save();
                }

                return (new UsersExport($data, $topic))->download($team->title . '- UsersExport - ' . Carbon::now() . '.csv', \Maatwebsite\Excel\Excel::CSV);
            } else {
                $count = $check->user_number + 1;
                $numbers = $count + $number;
                for ($i = $count; $i <= $numbers - 1; $i++) {
                    $randomString =  self::generateRandomString(9);

                    $pass = Hash::make($randomString);


                    $topic[] = ['Name' => $team->title . '-' . Carbon::now()];
                    $data[] = [
                        'username' => $initials . sprintf("%'05d", $i),
                        'Password' => $randomString
                    ];


                    $user[] = User::create([
                        'name' => $initials . '' . sprintf("%'05d", $i),
                        'first_name' => $initials,
                        'last_name' => sprintf("%'05d", $i),
                        'username' => $initials . sprintf("%'05d", $i),
                        'role' => UserRole::Learner,
                        'email' => $team->id . '@' . sprintf("%'05d", $i) . '.com',
                        'company_initials' => $initials,
                        'company_id' => $team->id,
                        'user_number' => sprintf("%'05d", $i),
                        'password' => $pass,
                        'email_verified_at' => Carbon::now(),
                        'is_activated' => 1,
                        'published' => 1,
                        'registered_at' => Carbon::now(),
                        'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    ]);
                }

                foreach ($data as $user) {


                    $u = User::where('username', $user['username'])->first();

                    $position = new TwillPosition();
                    $position->user_id = $u->id;
                    $position->position = $user['Password'];

                    $position->save();
                }
                return (new UsersExport($data, $topic))->download($team->title . '- UsersExport - ' . Carbon::now() . '.csv', \Maatwebsite\Excel\Excel::CSV);
            }
        } else {

            $errors = "Please update the Company Initials.";

            return Redirect::back()->withErrors($errors);
        }
    }


    public function userCodeLive(Request $request)
    {
        $data = [];
        $users = [];
        $number = $request->get('numbers');
        $team = Company::where('id', '=', $request->get('company'))->first();
        $initials = $team->company_code;

        if ($initials) {

            $check = DB::table('twill_users')->where('company_initials', '=', $initials)->latest('id')->first();

            if (empty($check->user_number)) {
                for ($i = 1; $i <= $number; $i++) {

                    $randomString =  self::generateRandomString(9);

                    $request->session()->put('register_password', $randomString);

                    $pass = Hash::make($randomString);

                    $topic[] = ['Name' => $team->name . '-' . Carbon::now()];
                    $data[] = [
                        'username' => $initials . sprintf("%'05d", $i),
                        'Password' => $randomString

                    ];



                    $users[] = User::create([
                        'name' => $initials . '' . sprintf("%'05d", $i),
                        'first_name' => $initials,
                        'last_name' => sprintf("%'05d", $i),
                        'username' => $initials . sprintf("%'05d", $i),
                        'role' => UserRole::Learner,
                        'email' => $team->id . '@' . sprintf("%'05d", $i) . '.com',
                        'company_initials' => $initials,
                        'company_id' => $team->id,
                        'user_number' => sprintf("%'05d", $i),
                        'password' => $pass,
                        'email_verified_at' => Carbon::now(),
                        'is_activated' => 1,
                        'published' => 1,
                        'registered_at' => Carbon::now(),
                        'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    ]);
                }
              
                foreach ($users as  $v){
                    $edxuser = EdxAuthUser::where('username',$v->username)->first();
                    if ($edxuser === null) {
                      return "No such user";
                    }else{
                      $edxuser->is_active = 1;
                      $edxuser->save();
                    }
                  }

                foreach ($data as $user) {


                    $u = User::where('username', $user['username'])->first();

                    $position = new TwillPosition();
                    $position->user_id = $u->id;
                    $position->position = $user['Password'];

                    $position->save();
                }

                return (new UsersExport($data, $topic))->download($team->title . '- UsersExport - ' . Carbon::now() . '.csv', \Maatwebsite\Excel\Excel::CSV);
            } else {
                $count = $check->user_number + 1;
                $numbers = $count + $number;
                for ($i = $count; $i <= $numbers - 1; $i++) {

                    $randomString =  self::generateRandomString(9);
                    
                    $request->session()->put('register_password', $randomString);
                    //$request->request->add(['register_password' => $randomString]);
                    $pass = Hash::make($randomString);

                    
                    $topic[] = ['Name' => $team->name . '-' . Carbon::now()];
                    $data[] = ['username' => $initials . sprintf("%'05d", $i), 'Password' => $randomString];



                    $users[] = User::create([
                        'name' => $initials . '' . sprintf("%'05d", $i),
                        'first_name' => $initials,
                        'last_name' => sprintf("%'05d", $i),
                        'username' => $initials . sprintf("%'05d", $i),
                        'role' => UserRole::Learner,
                        'email' => $team->id . '@' . sprintf("%'05d", $i) . '.com',
                        'company_initials' => $initials,
                        'company_id' => $team->id,
                        'user_number' => sprintf("%'05d", $i),
                        'password' => $pass,
                        'email_verified_at' => Carbon::now(),
                        'is_activated' => 1,
                        'published' => 1,
                        'registered_at' => Carbon::now(),
                        'authentication_type' => USER::AUTHENTICATION_USERCODE,
                    ]);
                }

                foreach ($users as  $v){
                    $edxuser = EdxAuthUser::where('username',$v->username)->first();
                    if ($edxuser === null) {
                      return "No such user";
                    }else{
                      $edxuser->is_active = 1;
                      $edxuser->save();
                    }
                  }

                foreach ($data as $user) {
                    $u = User::where('username', $user['username'])->first();

                    $position = new TwillPosition();
                    $position->user_id = $u->id;
                    $position->position = $user['Password'];

                    $position->save();
                }


                return (new UsersExport($data, $topic))->download($team->title . '- UsersExport - ' . Carbon::now() . '.csv', \Maatwebsite\Excel\Excel::CSV);
            }
        } else {

            $errors = "Please update the Company Initials.";

            return Redirect::back()->withErrors($errors);
        }
    }



    protected function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }


    protected function getEdxRepository()
    {
        return App::make("App\\Repositories\\AuthenticateEdxRepository");
    }


    public function export(Request $request)
    {
        $filter = json_decode(request()->get('requestFilter'));
        
       if(isset($filter) && $filter->status ==='trash')
       {
        $data = DB::table('twill_users')->whereNotNull('deleted_at')->get();

        $topic[] = ['Name' => 'Deleted Learners Report '.config('app.name').'_'.Carbon::now()];

    
        return Excel::download(new UsersListExport($data,$topic), 'Deleted Learners Report ' .config('app.name').'_'.Carbon::now().'.xlsx');

        
       }else{
        $data = json_decode($request->input('tableData'));
   
        $topic[] = ['Name' => 'Onboarded Learners Report '.config('app.name').'_'.Carbon::now()];

        return Excel::download(new UsersListExport($data,$topic), 'Onboarded Learners Report ' .config('app.name').'_'.Carbon::now().'.xlsx');

       }
       
    }


    
public function userDivisionInfo($id)
{

    $division = Division::where('company_id', $id)->orderBy('title')->get();

    return response()->json(['division' => $division]);
}

}
