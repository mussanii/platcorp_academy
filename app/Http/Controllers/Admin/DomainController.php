<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class DomainController extends BaseModuleController
{
    protected $moduleName = 'domains';

    protected $indexOptions = [
        'permalink' => false,
    ];
}
