<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class EvaluationOptionController extends BaseModuleController
{
    protected $moduleName = 'evaluationOptions';

    protected $indexOptions = [
        'permalink' => false,
    ];
}
