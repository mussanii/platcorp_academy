<?php
namespace App\Http\Controllers\Admin;
use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;
use App\Repositories\MenuTypeRepository;


class PageController extends BaseModuleController

{
    protected $moduleName = 'pages';
    protected $indexOptions = [
        'reorder' => true,
    ];


    protected function formData($request)
    {

        return [
			'menuTypeList' => app(MenuTypeRepository::class)->listAll('title'),
        ];

	}
    
}
