<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\CourseRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DivisionRepository;
use App\Models\Company;
use App\Models\Division;
use App\Models\JobRole;

class JobRoleController extends ModuleController
{
    protected $moduleName = 'jobRoles';

    protected $indexOptions = [
        'reorder' => true,
];

protected $filters = [
    'company_id'=> 'company_id',
    'division_id'=> 'division_id',
];

protected $indexColumns = [
        
    'title' => [ // field column
        'title' => 'Title',
        'field' => 'title',
    ],
    'company_id' => [ // relation column
        'title' => 'Company',
        'sort' => true,
        'relationship' => 'company',
        'field' => 'title'
    ],

    'division_id' => [ // relation column
        'title' => 'Department',
        'sort' => true,
        'relationship' => 'division',
        'field' => 'title'
    ],
    'courses_selected' => [ // field column
        'title' => 'Compulsory Courses',
        'field' => 'courses_selected',
    ],
];


protected function formData($request)
{

    return [
        'courseList' => app(CourseRepository::class)->where('status',1)->pluck('short_name','id'),
        'companyList' => app(CompanyRepository::class)->listAll('title'),
        'divisionList' => app(DivisionRepository::class)->listAll('title'),
        
    ];

}

protected function indexData($request)
{
    return [
        'company_idList' =>  Company::all(),
        'division_idList' =>  Division::all(),
       
    ];
}



public function divisionInfo($id)
    {

        $division = Division::where('company_id', $id)->orderBy('title')->get();

        return response()->json(['division' => $division]);
    }

    
}
