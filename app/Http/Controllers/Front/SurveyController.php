<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Survery;
use App\Models\UserSurvey;
use App\Models\Company;
use Artisan;
use Auth;
use Session;

class SurveyController extends Controller
{
    //


    public function survey(){

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {
            
            Artisan::call('view:clear');
            Artisan::call('optimize:clear');

        }

        $sections = Survery::published()->where('company_id',Auth::user()->company_id)->latest()->first();

        if($sections){

          $evaluation = UserSurvey::where('user_id','=',Auth::user()->id)->where('company_id', $sections->company_id)->first();

          return view('site.pages.company-survey',['sections'=>$sections, 'evaluation'=>$evaluation]);

        }else{
         
          $redirectMessage = [
            'title' => 'Error',
            'content' => 'There is no active survey at this time.',
        ];
            Session::flash('course_error', $redirectMessage);
            return redirect()->back();


        }


      }


      public function surveyStore(Request $request){



      foreach ($_POST as $key => $value) {
         $keys[]=$key;
         $values [] = $value;
        }


        $userEvaluation = new UserSurvey;
        $userEvaluation->username = Auth::user()->username;
        $userEvaluation->user_id = Auth::user()->id;
        $userEvaluation->company_id = $request->input('company_id');
        $userEvaluation->survey_id = $request->input('survey_id');
        if(!empty($keys[3])){
        $userEvaluation->question1 = $keys[3];
        $userEvaluation->answer1 = $values[3];
        }
        if(!empty($keys[4])){
        $userEvaluation->question2 = $keys[4];
        $userEvaluation->answer2 = $values[4];
        }
        if(!empty($keys[5])){
        $userEvaluation->question3 = $keys[5];
        $userEvaluation->answer3 = $values[5];
        }
        if(!empty($keys[6])){
        $userEvaluation->question4 = $keys[6];
        $userEvaluation->answer4 = $values[6];
        }
        if(!empty($keys[7])){
        $userEvaluation->question5 = $keys[7];
        $userEvaluation->answer5 = $values[7];
        }
        if(!empty($keys[8])){
        $userEvaluation->question6 = $keys[8];
        $userEvaluation->answer6 = $values[8];
        }
        if(!empty($keys[9])){
          $userEvaluation->question7 = $keys[9];
          $userEvaluation->answer7 = $values[9];
        }
        if(!empty($keys[10])){
          $userEvaluation->question8 = $keys[10];
          $userEvaluation->answer8 = $values[10];
        }
        if(!empty($keys[11])){
          $userEvaluation->question9 = $keys[11];
          $userEvaluation->answer9 = $values[11];
        }
        if(!empty($keys[12])){
          $userEvaluation->question10 = $keys[12];
          $userEvaluation->answer10 = $values[12];
        }
        $userEvaluation->save();

        $redirectMessage = [
            'title' => 'Successful',
            'content' => 'Thank you for submitting the survey.',
        ];
        Session::flash('course_success', $redirectMessage);
        return redirect()->back();

      }
}
