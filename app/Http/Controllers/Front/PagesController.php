<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use Validator;
use Redirect;
use Auth;
use App\Models\JobRole;
use App\Models\CourseCategory;
use App\Models\Session;
use App\Models\Country;
use App\Models\CourseRequest;
use App\Models\Company;
use App\Models\User;
use Carbon\Carbon;
use Artisan;
use Session as Msessions;
use Mail;

class PagesController extends Controller
{
    //
    protected $pageKey;

    public function __construct()
    {
      $this->homeKey = PageRepository::PAGE_HOMEPAGE;
  
      $this->middleware('auth', ['except' => ['index', 'noneAuthPages']]);
      
    }

    public function index(){

      $itemPage = app(PageRepository::class)->getPage($this->homeKey);

       if(!Auth::check()){
         
        return view('site.pages.home', [
          'pageItem' => $itemPage,
        ]);

       }else{
      
        $id = Auth::user()->company_id;

        $company = Company::where('id', $id)->first();
        return view('site.pages.home', [
          'pageItem' => $itemPage,
           'company' => $company,
        ]);


       }


    }


    public function pages(Request $request, $key)
  {

    if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

      Artisan::call('view:clear');
      Artisan::call('optimize:clear');
  
  }

    $data = $request->all();
    if ($request->isMethod('post')) {
         
        if ($request->input('key') === 'courses') {

          $type = $request->input('category');
          $search = $request->input('search');
        }

        if ($request->input('key') === 'our-progress') {
          $type = $request->input('category');
          $search = $request->input('search');

        }
        
      }else {
        
        $search = '';
        $type = '';
      }

     

    $itemPage = app(PageRepository::class)->getPage($key);
    $jobRoles = JobRole::published()->orderBy('position', 'asc')->get();
    $courseCategories = CourseCategory::with(['courses'=>function($query){
                          //$query->where('company_id',Auth::user()->company_id);
                       }])->published()->orderBy('position', 'asc')->get();

    if ($key === 'home') {
      return redirect()->route('home');
    } else {
      return view('site.pages.' . $key, [
        'pageItem' => $itemPage,
        'jobRoles' => $jobRoles,
        'search' => $search,
        'type' => $type,
        'courseCategories' => $courseCategories,
      ]);
    }
  }


  public function noneAuthPages(Request $request, $key)
  {
    if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

      Artisan::call('view:clear');
      Artisan::call('optimize:clear');
  
   }

    $itemPage = app(PageRepository::class)->getPage($key);
   

    if ($key === 'home') {
      return redirect()->route('home');
    } else {
      return view('site.pages.' . $key, [
        'pageItem' => $itemPage,
      ]);
    }
  }




  public function sessionDetail(Request $request, $id)
  {

    if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

      Artisan::call('view:clear');
      Artisan::call('optimize:clear');
  
  }
 
    $itemPage = Session::where('id', '=', $id)->first();
    $upcoming = Session::where('start_time', '>=', Carbon::now())->first();
    $previous = Session::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->orderBy('position', 'desc')->get();


    // dd(count($articles));
    return view(
      'site.pages.session_details',
      [
        'pageItem' => $itemPage,
        'upcoming' => $upcoming,
        'previous' => $previous,
      ]
    );
  }

  public function progressCountries()
  {
    $data = [];

    $countries = Country::published()->get();
  
    foreach ($countries as $country) {
      array_push($data, strtolower($country['code']));
    }
    return $data;
  }


  public function requestCourseAccess(Request $request, $id){

    $access = CourseRequest::where('user_id', Auth::user()->id)->where('course_id',$id)->first();

    if(!$access){

      $naccess = new CourseRequest();
      $naccess->title = Auth::user()->first_name.' '.Auth::user()->last_name;
      $naccess->user_id = Auth::user()->id;
      $naccess->course_id = $id;
      $naccess->status = CourseRequest::ACCESS_REQUESTED;
      $naccess->date_request = Carbon::now();
      $naccess->company_id = Auth::user()->company_id;

      $naccess->save();

     $users = User::where('company_id','=', Auth::user()->company_id)->where('role','=','COMPANYHR')->where('authentication_type','!=',3)->get();

     foreach($users as $user){
        Mail::send('emails.course_request',['name' => $user->first_name.' '.$user->last_name, 'url'=> route('login')],
        function ($message) use ($user) {
          $message->from('support@farwell-consultants.com', 'The Platcorp Group Academy Course Request');
          $message->to($user->email, $user->first_name);
          $message->subject('The Platcorp Group Academy Course Request');
        });

     }

      $course_success = [
        'title' => 'Request Sent!',
        'content' => 'Please wait for approval from the platform administrator.',
    ];
    
    Msessions::flash('course_success', $course_success);
      return redirect()->route('pages',['key'=>'courses']);

    }else{

      $course_errors = [
        'title' => 'Thank you for requesting',
        'content' => 'looks like this request has already been sent. Please wait for approval',
    ];
  
    Msessions::flash('course_errors', $course_errors);
      return redirect()->route('pages',['key'=>'courses']);


    }


  }

}
