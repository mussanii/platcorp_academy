<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\JobroleCourse;
use App\Models\CourseCompletion;
use App\Models\UserLicense;
use App\Models\Course;
use App\Models\User;
use App\Models\Evaluation;
use App\Models\UserEvaluation;
use App\Models\MyCommunity;
use App\Models\Notification;
use App\Models\SerialNumber;
use App\Models\Company;
use App\Models\TrainingComment;
use App\Models\TrainingLike;
use App\Models\LearningStatus;
use App\Models\CourseReflection;
use Session;
use Redirect;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use Artisan;
use DB;

class UserLicenseController extends Controller
{
    //

    public function profile(Request $request)
    {
        if (!isset($_COOKIE['edinstancexid'])) {

            Auth::logout();
            return false;
        }

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        }

        $passMark = env('PassMark');
        $courses = Course::orderBy('name')->get();
        $statuses = LearningStatus::all();
        $month = [];

        $years = range(now()->year, now()->year);
        // $years= range(now()->year, now()->year + 5);

        for ($m = 1; $m <= 12; $m++) {
            $month[] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
        }


        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();

        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }

        //$licensed = UserLicense::where('user_id', Auth::user()->id)->latest()->get();
        $user = Auth::user()->id;

        $licensed = UserLicense::leftJoin('course_completions', function ($join) use ($user) {
            $join->on('user_licenses.course_id', '=', 'course_completions.course_id')
                ->where('course_completions.user_id', $user);
        })
            ->select('user_licenses.*', 'course_completions.completion_date')
            ->where(function ($query) use ($user) {
                $query->where('user_licenses.user_id', $user);
            })->orderBy('course_completions.completion_date', 'desc')
            ->get();


        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
            } else {
                unset($licensed[$key]);
            }
        }


        $result = array_diff($licenseid, $coursesid);


        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            }

            if (!empty($request->input('year'))) {

                if (Carbon::parse($license->created_at)->format('Y') != $request->input('year')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('month'))) {

                if (Carbon::parse($license->created_at)->format('m') != $request->input('month')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('status'))) {

                if ($license->status != $request->input('status')) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($request->input('search'))) {
                if (strpos($license->course->name, $request->input('search')) === FALSE) {

                    unset($licenses[$key]);
                }
            }
        }



        return view('site.pages.my_enrolled_modules', ['licenses' => $licenses, 'courses' => $courses, 'passMark' => $passMark, 'statuses' => $statuses, 'month' => $month, 'years' => $years]);
    }


    public function community(Request $request)
    {

        if (!isset($_COOKIE['edinstancexid'])) {

            Auth::logout();
            return false;
        }

        $details = [];
        $i = 0;
        if ($request->query('search', false)) {

            $users = User::where(function ($query) {
                $query->where('first_name', 'LIKE', '%' . $_GET['search'] . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $_GET['search'] . '%');
            })->get();

            $results = [];
            foreach ($users as $user) {
                $results[] = $user->id;
            }
            //dd($results);
            $licensed = UserLicense::all()->groupBy('user_id');

            $licenses = [];
            foreach ($licensed as $key => $license) {
                if (isset($license[0]->user->id)) {
                    if (in_array($license[0]->user_id, $results)) {
                        $licenses[] = $license[0];
                    }
                } else {
                    unset($licensed[$key]);
                }
            }
        } else {

            $users = UserLicense::where('user_id', Auth::user()->id)->get();

            $courses = [];

            foreach ($users as $us) {
                $courses[] = $us->user_id;
            }




            $licensed = UserLicense::where('company_license_id', Auth::user()->company_id)->get()->groupBy('user_id');

            $licenses = [];
            foreach ($licensed as $key => $license) {
                if (isset($license[0]->user->id)) {
                    if (!in_array($license[0]->user_id, $courses)) {
                        $licenses[] = $license[0];
                    }
                } else {
                    unset($licensed[$key]);
                }
            }
        }

        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        foreach ($req as $request) {
            $user = User::where('id', $request->user_id)->first();

            if (isset($user->id)) {
                $details[$i]['id'] = $request->id;
                $details[$i]['user_id'] = $user->id;
                $details[$i]['name'] = ucwords($user->first_name . ' ' . $user->last_name);
                $i++;
            }
        }
        $receive = count($req);



        return view('site.pages.my_community', ['licenses' => $licenses, 'receive' => $receive, 'details' => $details]);
    }

    public function myFollow(Request $request)
    {
        $follow = $request->input('follow');
        $id = $request->input('id');

        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if ($status && $id == 0) {
            $follows = MyCommunity::where(['id' => $status->id])->first();
            $follows->user_id = Auth::user()->id;
            $follows->follow_id = $follow;
            $follows->status = 1;
            $follows->update();
        } elseif ($status && $id == 1) {
            $follo = MyCommunity::where(['id' => $status->id])->first();
            $follo->user_id = Auth::user()->id;
            $follo->follow_id = $follow;
            $follo->status = 2;
            $follo->update();
        } elseif ($status && $id == 2) {
            $foll = MyCommunity::where(['id' => $status->id])->first();
            $foll->user_id = Auth::user()->id;
            $foll->follow_id = $follow;
            $foll->status = 0;
            $foll->update();
        } elseif ($status && $id == 3) {
            $fol = MyCommunity::where(['id' => $status->id])->first();
            $fol->user_id = Auth::user()->id;
            $fol->follow_id = $follow;
            $fol->status = 3;
            $fol->update();
        } elseif ($status && $id == 4) {
            $fo = MyCommunity::where(['id' => $status->id])->first();
            $fo->user_id = Auth::user()->id;
            $fo->follow_id = $follow;
            $fo->status = 0;
            $fo->update();
        } else {
            $f = new MyCommunity();
            $f->user_id = Auth::user()->id;
            $f->follow_id = $follow;
            $f->status = 1;
            $f->save();


            $users = User::where('id', $follow)->first();


            $message = [
                'type' => Notification::FOLLOW,
                'subject' => 'Follow Request',
                'model_id' => $f->id,
                'message' => 'New friend request from ' . Auth::user()->name . ' Click here in order to view the request',
            ];

            $users->notify(new \App\Notifications\FollowRequestNotification($message));
        }

        // if ($id == 0) {
        //     $redirectMessage = [
        //         'title' => 'Success',
        //         'content' => 'Thank you for the follow request.',
        //     ];
        // } else {
        //     $redirectMessage = [
        //         'title' => 'Success',
        //         'content' => 'Thank you for responding follow request.',
        //     ];
        // }
        // Session::flash('notification_success', $redirectMessage);

        return Redirect::back();
    }


    public function myFollowAccept(Request $request)
    {

        $status = $request->input('follow');
        $id = $request->input('id');

        $following = MyCommunity::where('id', $id)->where('status', 1)->first();

        if ($following) {
            $following->status = $status;
            $following->update();
            $notification =  Notification::where('notifiable_id', $following->follow_id)->first();

            if ($status == 2) {
                $redirectMessage = [
                    'title' => 'Success',
                    'content' => 'Thank you for accepting follow request.',
                ];

                Session::flash('course_success', $redirectMessage);
            }
        } else {
            $redirectMessage = [
                'title' => 'Information',
                'content' => 'The invite was alread accepted.',
            ];

            Session::flash('course_success', $redirectMessage);
        }




        return redirect()->route('profile.myCommunity');
    }

    public function trainingFeed()
    {
        if (!isset($_COOKIE['edinstancexid'])) {

            Auth::logout();
            return false;
        }

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        }
        $following = MyCommunity::where('user_id', Auth::user()->id)->where('status', 2)->get();

        $folo = [];

        foreach ($following as $f) {
            $folo[] = $f->follow_id;
        }

        $licensed = UserLicense::all();

        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->user->id)) {
                if (in_array($license->user->id, $folo)) {
                    $licenses[] = $license;
                }
                if ($license->user->id == Auth::user()->id) {
                    unset($licenses[$key]);
                }
                if (!empty($_GET['year'])) {
                    if ($license->created_at->year != $_GET['year']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['month'])) {
                    if ($license->created_at->month != $_GET['month']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['status'])) {
                    if ($license->status != $_GET['status']) {
                        unset($licenses[$key]);
                    }
                }
            } else {
                unset($licensed[$key]);
            }
        }


        return view('site.pages.training-feed', ['licenses' => $licenses]);
    }

    public function myComment(Request $request)
    {


        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = TrainingComment::saveFiles($images);
        $gifsToSave = TrainingComment::saveFiles($gifs);




        if ($request->input('unique')) {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->where('parent_id', $request->input('unique'))->first();
        } else {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->first();
        }

        if ($comment) {
            $comments = TrainingComment::where('id', $comment->id)->first();
            $comments->user_id = Auth::user()->id;
            $comments->comment_id = $request->input('follow');
            $comments->course_id = $request->input('course');
            $comments->parent_id = $request->input('unique');
            $comments->comment = $request->input('comment');
            $comments->images =  json_encode($imagesToSave);
            $comments->gifs = json_encode($gifsToSave);
            $comments->update();
        } else {
            $commen = new TrainingComment();
            $commen->user_id = Auth::user()->id;
            $commen->comment_id = $request->input('follow');
            $commen->course_id = $request->input('course');
            $commen->parent_id = $request->input('unique');
            $commen->comment = $request->input('comment');
            $commen->images =  json_encode($imagesToSave);
            $commen->gifs = json_encode($gifsToSave);
            $commen->save();
        }

        $site_success = [
            'title' => '',
            'content' => 'Thank you for keeping the conversation going!',
        ];

        Session::flash('course_success', $site_success);

        return Redirect::route('profile.trainingFeed');
    }

    public function myLike($follow, $id)
    {
        $likes = TrainingLike::where('user_id', Auth::user()->id)->where('like_id', $follow)->where('course_id', $id)->first();

        if ($likes) {
            $redirectMessage = [
                'title' => 'Thank you for like',
                'content' => 'You can only like a feed once.',
            ];

            Session::flash('redirectMessage', $redirectMessage);
            return Redirect::route('profile.trainingFeed');
        } else {
            $like = new TrainingLike();
            $like->user_id = Auth::user()->id;
            $like->like_id = $follow;
            $like->course_id = $id;
            $like->save();
        }

        return Redirect::route('profile.trainingFeed');
    }


    public function mycomments($follow, $course)
    {
        $details = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->whereNull('parent_id')->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $details[$i]['id'] = $comment->id;
            $details[$i]['user_id'] = $user->id;
            $details[$i]['course_id'] = $comment->course_id;
            $details[$i]['name'] = ucwords($user->first_name . ' ' . $user->last_name);
            $details[$i]['position'] = $user->roles->title;
            $details[$i]['course_name'] = $course->name;
            $details[$i]['comment'] = $comment->comment;
            $details[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $details[$i]['pic'] = $user->profile_pic;
            $i++;
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }

    public function subCommentCount($follow, $course, $id)
    {
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();
        $count = count($comments);
        return response()->json(['count' => $count], Response::HTTP_OK);
    }


    public function subComment($follow, $course, $id)
    {
        $detail = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $detail[$i]['id'] = $comment->id;
            $detail[$i]['user_id'] = $user->id;
            $detail[$i]['course_id'] = $comment->course_id;
            $detail[$i]['name'] = ucwords($user->first_name . ' ' . $user->last_name);
            $detail[$i]['position'] = $user->roles->title;
            $detail[$i]['course_name'] = $course->name;
            $detail[$i]['comment'] = $comment->comment;
            $detail[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $detail[$i]['pics'] = $user->profile_pic;
            $i++;
        }

        return response()->json(['subdetails' => $detail], Response::HTTP_OK);
    }



    public function myProgress()
    {
        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        }

        $passMark = env('PassMark');
        $required = JobroleCourse::where('job_role_id', '=', Auth::user()->job_role_id)->get();
        $completed = CourseCompletion::where('user_id', '=', Auth::user()->id)->get();


        return view('site.pages.my_compulsory_modules', ['completed' => $completed, 'passMark' => $passMark, 'required' => $required]);
    }



    public function evaluate($id)
    {

        if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

            Artisan::call('view:clear');
            Artisan::call('optimize:clear');
        }

        $sections = Evaluation::where('course_id', $id)->first();
        $evaluation = UserEvaluation::where('user_id', '=', Auth::user()->id)->first();

        return view('site.pages.course-evaluation', ['sections' => $sections, 'evaluation' => $evaluation, 'id' => $id]);
    }


    public function Evaluationstore(Request $request)
    {



        foreach ($_POST as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }


        $userEvaluation = new UserEvaluation;
        $userEvaluation->username = Auth::user()->username;
        $userEvaluation->user_id = Auth::user()->id;
        $userEvaluation->course_id = $request->input('course_id');
        $userEvaluation->question1 = $keys[2];
        $userEvaluation->answer1 = $values[2];
        $userEvaluation->question2 = $keys[3];
        $userEvaluation->answer2 = $values[3];
        $userEvaluation->question3 = $keys[4];
        $userEvaluation->answer3 = $values[4];
        $userEvaluation->question4 = $keys[5];
        $userEvaluation->answer4 = $values[5];
        if (!empty($keys[6])) {
            $userEvaluation->question5 = $keys[6];
            $userEvaluation->answer5 = $values[6];
        }
        if (!empty($keys[7])) {
            $userEvaluation->question6 = $keys[7];
            $userEvaluation->answer6 = $values[7];
        }
        if (!empty($keys[8])) {
            $userEvaluation->question7 = $keys[8];
            $userEvaluation->answer7 = $values[8];
        }
        if (!empty($keys[9])) {
            $userEvaluation->question8 = $keys[9];
            $userEvaluation->answer8 = $values[9];
        }
        if (!empty($keys[10])) {
            $userEvaluation->question9 = $keys[10];
            $userEvaluation->answer9 = $values[10];
        }
        if (!empty($keys[11])) {
            $userEvaluation->question10 = $keys[11];
            $userEvaluation->answer10 = $values[11];
        }
        $userEvaluation->save();

        $redirectMessage = [
            'title' => 'Thank you for evaluating the course',
            'content' => 'You can now download your badge.',
        ];
        Session::flash('course_success', $redirectMessage);
        return redirect()->route('profile.courses');
    }

    public function badge($courseid)
    {

        $user = User::where('id', Auth::user()->id)->first();
        $c = Course::where('id', $courseid)->first();
        $company = Company::where('id', Auth::user()->company_id)->first();

        $name = ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
        $date = Carbon::today()->isoFormat('Do MMMM YYYY');

        $number = SerialNumber::where('user_id', '=', Auth::user()->id)->first();

        if ($number) {
            $cert_number = $number->serial_number;
        } else {

            $serial = new SerialNumber();
            $serial->username = Auth::user()->username;
            $serial->user_id = Auth::user()->id;
            $serial->serial_number = self::generateRandomString(16);

            $serial->save();

            $cert_number = $serial->serial_number;
        }


        $data = [
            'name' => $name,
            'date' => $date,
            'course_name' => $c->name,
            'role' => $user->roles->title,
            'cert_number' => $cert_number,
            'logo' => $company->image('logo'),
            'company' => $company,
        ];

        //dd($company->image('logo'));

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('site.pages.certificate', $data)->setPaper('a4', 'landscape');

        //return $pdf->stream();
        return $pdf->download('Badge_' . $name . '.pdf');
        //return  $pdf->stream();
    }




    public function certificate()
    {
        $module = array();
        $user = User::where('id', Auth::user()->id)->first();
        $company = Company::where('id', Auth::user()->company_id)->first();
        $compulsory = JobroleCourse::where('job_role_id', '=', Auth::user()->job_role_id)->get();
        $completed = CourseCompletion::where('user_id', '=', Auth::user()->id)->get();
        $name = ucfirst($user->name);

        foreach ($compulsory as $comp) {
            $compulsories[] = $comp->course_id;
        }

        foreach ($completed as $compl) {
            $complete[] = $compl->course_id;
        }

        foreach ($completed as $key => $l) {
            if (!in_array($l->course_id, $compulsories)) {
                unset($completed[$key]);
            }
        }

        foreach ($completed as $key => $value) {

            $cname = str_replace('-', ' ', $value->courses->name);

            $module[] = $cname;
        }


        $date = Carbon::today()->isoFormat('Do MMMM YYYY');

        $number = SerialNumber::where('user_id', '=', Auth::user()->id)->first();

        if ($number) {
            $cert_number = $number->serial_number;
        } else {

            $serial = new SerialNumber();
            $serial->username = Auth::user()->username;
            $serial->user_id = Auth::user()->id;
            $serial->serial_number = self::generateRandomString(16);

            $serial->save();

            $cert_number = $serial->serial_number;
        }


        $data = [
            'name' => $name,
            'date' => $date,
            'module' => $module,
            'role' => $user->roles->title,
            'cert_number' => $cert_number,
            'logo' => $company->image('logo'),
            'company' => $company,
        ];

        //dd($company->image('logo'));

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('site.pages.mandatory_certificate', $data)->setPaper('a4', 'landscape');

        //return $pdf->stream();
        return $pdf->download('Certificate_' . $name . '.pdf');
        //return  $pdf->stream();
    }



    public function myReflections($id)
    {

        $reflections = CourseReflection::where('username', Auth::user()->username)->where('course_id', $id)->get();

        return view('site.pages.my_reflections', compact('reflections'));
    }


    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }



    public function sync()
    {

        if (Auth::user()->role === "SUPERADMIN") {
            $l = array();
            $userlinceses = UserLicense::all();

            foreach ($userlinceses as $userL) {

                $user = CourseCompletion::where('course_id', $userL->course_id)->where('user_id', $userL->user_id)->first();
                if (!empty($user)) {

                    $user->user_license_id =  $userL->id;
                    $user->save();
                }
            }


            return "done";
        } else {

            return "No Access";
        }
    }





    public function syncL()
    {

        if (Auth::user()->role === "SUPERADMIN") {
            $l = array();
            $userlinceses = UserLicense::all();

            foreach ($userlinceses as $userL) {

                $user = User::where('id', $userL->user_id)->first();

                if (!empty($user)) {

                    $userL->email =  $user->email;
                    $userL->company_id =  $user->company_id;
                    $userL->job_role_id =  $user->job_role_id;
                    $userL->branch =  $user->branch_id;
                    $userL->save();
                }
            }


            return "done";
        } else {

            return "No Access";
        }
    }
}
