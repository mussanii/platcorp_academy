<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use App\Models\Session;
use Carbon\Carbon;
use Google_Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class SessionsController extends Controller
{

  protected $client;

  public function __construct()
  {
    $client= new Google_Client();
$client->setAuthConfig('google-calendar/client_secret.json');

$client->addScope([Google_Service_Calendar::CALENDAR]);
$guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER=>false)));
$client->setHttpClient($guzzleClient);
$this->client = $client;
    
  }

  public function addToCalendar($id) {
    session_start();
    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {

        $this->client->setAccessToken($_SESSION['access_token']);
        $accessToken = $this->client->getAccessToken();

        if ($this->client->isAccessTokenExpired()) {
            if ($this->client->getRefreshToken()) {
                $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
                $accessToken = $this->client->getAccessToken();
                $_SESSION['access_token'] = $accessToken;
            } else {
                return redirect()->route('oauthCallback');
            }
        }

        $this->client->setAccessToken($accessToken);
        $service = new Google_Service_Calendar($this->client);

        $calendarId = 'primary';
        $join_url = null;

        $session = Session::find($id);
        if(Auth::user()->company_id == 7 || Auth::user()->company_id == 3)
        {
          $join_url = $session->teams_link;
        }else{
          $join_url=$session->join_url;

        }
        

        $start_time = Carbon::parse($session->start_time);
        $end_time = $start_time->addMinutes($session->duration);
        

        $event = new Google_Service_Calendar_Event([

            'summary' => $session->topic,
            'location' => $join_url,
            'start' => [
                'dateTime' => $start_time,
                'timeZone' => 'Africa/Nairobi',
            ],
            'end' => [
                'dateTime' => $end_time,
                'timeZone' => 'Africa/Nairobi',
            ],
            'reminders' => [
                'useDefault' => true,
            ],
            'conferenceData' => [
              'createRequest' => [
                  'requestId' => uniqid()
              ]
          ],
        ]);
        $results = $service->events->insert($calendarId, $event,[
          'conferenceDataVersion' => 1]);
          $hangoutLink = $results->getConferenceData()->getEntryPoints()[0]->getUri();
          $calendarId = $results->getId();
          $calendarEvent = new CalendarEvent;
          $calendarEvent->user_id = Auth::user()->id;
          $calendarEvent->session_id = $id;
          $calendarEvent->calendar_id = $calendarId;
          $calendarEvent->meeting_link = $hangoutLink;
          $calendarEvent->save();

        if (!$results) {
            return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
 }
 $event_success = [
  'title' => 'Success!',
  'content' => 'Event added successfully',
];
\Session::flash('course_success', $event_success);
return Redirect::back();

    

} else {
  return redirect()->route('oauthCallback');
  }
 
     
  }


  public function oauth()
  {
  
    
    session_start();
    $url = action([SessionsController::class,'oauth']);
 
 
 $this->client->setRedirectUri($url); 
    if(!isset($_GET['code'])){
   
      $auth_url = $this->client->createAuthUrl();
      
      $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
      
 

       return redirect($filtered_url);
    }else{
      $this->client->authenticate($_GET['code']);
      $_SESSION['access_token'] = $this->client->getAccessToken();
      $token_success = [
        'title' => 'Success',
        'content' => 'Google calendar has been added to the system.',
    ];

        \Session::flash('course_success', $token_success);
      return redirect('/sessions');
    }

  }
 

  
}
