<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Beta\Microsoft\Graph\Model\Event;
use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Drive;
use Google_Service_Calendar_Event;
use Illuminate\Http\Request;
use Socialite;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\DateTimeTimeZone;
use Microsoft\Graph\Model\ItemBody;

class MeetingController extends Controller
{
    

    

    public function generateMicrosoftLink(Request $request)
    {
        $start_time = $request->formData['start_time'];
        $duration = $request->formData['duration'];
        $description = $request->formData['description'];
        $topic = 'Live Session';
        $client = new Client();

        
    
        $response = $client->post('https://login.microsoftonline.com/987a006e-3266-486c-93e2-96e40fa9341d/oauth2/v2.0/token', [
            'form_params' => [
                'client_id' => env('MICROSOFT_CLIENT_ID'),
                'client_secret' => env('MICROSOFT_CLIENT_SECRET'),
                'grant_type' => 'client_credentials',
                'scope' => 'https://graph.microsoft.com/.default',
            ],
        ]);
    

        
       
        $accessToken = json_decode($response->getBody(), true)['access_token'];
 
    
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
      

        $userPrincipalName = Auth::user()->email;

        
    
        $start_time_parsed = Carbon::parse($start_time)->toIso8601String();
        $start_time = new DateTimeTimeZone();
        $start_time->setDateTime($start_time_parsed);
        $start_time->setTimeZone('EAT');
        $end_time = new DateTimeTimeZone();
        $end_time->setDateTime(Carbon::parse($start_time->getDateTime())->addMinutes($duration)->toIso8601String());
        $end_time->setTimeZone('EAT');
    
        $event = new Event();
        $event->setSubject($topic);
        $event->setBody(new ItemBody([
            'contentType' => 'HTML',
            'content' => strip_tags($description)
        ]));
        $event->setStart($start_time);
        $event->setEnd($end_time);
         $event->setIsOnlineMeeting(true);
        $event->setOnlineMeetingProvider("teamsForBusiness");
        $event->setAllowNewTimeProposals(true);
        $event->setAllowNewTimeProposals(false);
    
        try {
            $calendar = $graph->createRequest('POST', '/users/' .$userPrincipalName . '/calendar/events')
    ->attachBody($event)
    ->execute();


     $results = $calendar->getBody();

    

    $link = $results['onlineMeeting']['joinUrl'];
    session_start();
    $_SESSION['teams_link'] = $link;



   return $link;
   
        } catch (\Exception $e) {
            return $e->getMessage();
    }
 }


 public function generateGoogleLink(Request $request)
{
    // Get the start time, duration, and description of the live session from the request data
    $start_time = $request->formData['start_time'];
    $duration = $request->formData['duration'];
    $description = $request->formData['description'];

    // Authenticate with the Google Calendar API
    $client = new Google_Client();
    $client->setApplicationName('Your App Name');
    $client->setAuthConfig(base_path('config/google_credentials.json'));
    $client->addScope(\Google_Service_Calendar::CALENDAR);
    $client->addScope(\Google_Service_Drive::DRIVE);
    $client->setAccessType('offline');

   
    // Get the access token for the API
    $accessToken = $client->fetchAccessTokenWithAssertion()['access_token'];
    $client->setAccessToken($accessToken);

    // Create a new instance of the Google Calendar API client and set the access token
    $calendar = new \Google_Service_Calendar($client);
 

   // Get the authenticated user's email address
$user = $calendar->calendarList->get('test');
$user_email = $user->id;

// Format the start and end times of the event as RFC3339 strings and set the time zone to 'EAT' (East Africa Time)
$start_time = date('c', strtotime($start_time));
$end_time = date('c', strtotime($start_time . '+ ' . $duration . ' minutes'));

// Create a new event object and set the start and end times, summary, and description
$event = new Google_Service_Calendar_Event(array(
    'summary' => 'Live Session',
    'description' => $description,
    'start' => array(
        'dateTime' => $start_time,
        'timeZone' => 'EAT',
    ),
    'end' => array(
        'dateTime' => $end_time,
        'timeZone' => 'EAT',
    ),
    'reminders' => array(
        'useDefault' => true,
    ),
    'conferenceData' => array(
        'createRequest' => array(
            'conferenceSolutionKey' => array(
                'type' => 'hangoutsMeet',
            ),
            'requestId' => uniqid(),
        ),
    ),
));

// Insert the event into the user's calendar
$event = $calendar->events->insert($user_email, $event, array(
    'conferenceDataVersion' => 1,
));

// Get the URL for the Google Meet conference and return it
$link = $event->getHangoutLink();

return $link;

}


 
}
