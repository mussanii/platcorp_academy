<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use Carbon\Carbon;


class NotificationController extends Controller
{
    //

    public function singleNotification($id){

        $notification = Notification::where('id', $id)->first();
   
         if($notification->data['type'] == 1){
   
           $notification->read_at = Carbon::now();
           $notification->save();
   
           return redirect()->route('pages',['key'=>'groups']);
   
         }else if($notification->data['type'] == 2){
   
           $notification->read_at = Carbon::now();
           $notification->save();
   
           return redirect()->route('session.details',[$notification->data['model_id']]);
   
         }else if($notification->data['type'] == 4){
   
            $notification->read_at = Carbon::now();
            $notification->save();
    
            return redirect()->route('profile.myCommunity');
    
          }
         
         else if($notification->data['type'] == 5){
   
           $notification->read_at = Carbon::now();
           $notification->save();
   
           return redirect()->route('profile.myProgress');
   
         }

         else if($notification->data['type'] == 6){
   
          $notification->read_at = Carbon::now();
          $notification->save();
  
          return redirect()->route('profile.survey');
  
        }
        else if($notification->data['type'] == 7){
   
          $notification->read_at = Carbon::now();
          $notification->save();
  
          return redirect()->route('pages',['key'=>'courses']);
  
        }
   
   
   
   
       }
}
