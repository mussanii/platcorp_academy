<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobRole;
use App\Models\Branch;
use App\Models\User;
use App\Models\Gender;
use App\Models\Division;
use Redirect;
use Validator;
use App;
use Session;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Auth\AuthManager;
use App\Models\Country;
use App\Models\TwillPosition;
use Mail;
use Artisan;

class UserController extends Controller
{
  //
  public function __construct(AuthManager $authManager)
  {
    $this->authManager = $authManager;
    $this->edxRepository = $this->getEdxRepository();
  }


  public function showRegistrationForm()
  {

    abort(403, 'You are not allowed on this page');
    // return view('site.auth.register');
  }


  public function completeProfile(Request $request, $user = null)
  {

    $validator = Validator::make($request->except('_token'), [
      'job_role_id' => 'required',
      'gender_id' => 'required',

    ]);
    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 401);
    } else {


      $user = User::where('id', '=', $user)->first();

      $user->country_id = (!empty($request->get('country_id'))) ? $request->get('country_id') : 1;
      $user->branch_id = $request->get('branch_id');
      $user->job_role_id = $request->get('job_role_id');
      // $user->division_id = $request->get('division_id');
      $user->gender_id = $request->get('gender_id');
      $user->first_name = $request->get('first_name');
      $user->last_name = $request->get('last_name');
      $user->initial_profile = 1;
      $user->save();

      $course_success = [
        'title' => 'Thank you !',
        'content' => 'Your profile has been updated',
      ];

      Session::flash('notification_success', $course_success);

      return Redirect::back();
    }
    return redirect()->back()->with('modal_error', '<h3 style="text-align:center">invalid request</h3>');
  }


  public function completeDivision(Request $request, $user = null)
  {

    $validator = Validator::make($request->except('_token'), [
      'division_id' => 'required',


    ]);
    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 401);
    } else {


      $user = User::where('id', '=', $user)->first();

      $user->division_id = $request->get('division_id');
      $user->employee_number = $request->get('employee_number');
      $user->save();

      $course_success = [
        'title' => 'Thank you !',
        'content' => 'Your profile has been updated',
      ];

      Session::flash('notification_success', $course_success);

      return Redirect::back();
    }
    return redirect()->back()->with('modal_error', '<h3 style="text-align:center">invalid request</h3>');
  }



  public function getBranch($id)
  {

    $branch = Branch::where('country_id', $id)->orderByTranslation('title')->get();
    return response()->json($branch);
  }

  public function profile(Request $request)
  {

    if (env('APP_DEBUG') === 'false' || env('APP_ENV') === 'production') {

      Artisan::call('view:clear');
      Artisan::call('optimize:clear');
    }

    //Get user
    $user = Auth::user();

    if ($request->isMethod('post')) {

      //Validate
      $request->validate([
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],

      ]);

      //Get updates
      $updates = $request->all();


      //Save changes
      $user = User::find($user->id);
      if ($user->update($updates)) {
        return Redirect::back()->with('register_success', 'Your profile has been successfully updated.');
      } else {
        //Show session message

        return Redirect::back()->with('register_error', 'There was a problem updating your profile.Please refresh the page and try again.');
      }
    }

    $roles = JobRole::all();
    $branches = Branch::all();
    $countries = Country::all();
    $genders = Gender::all();
    $departments = Division::all();

    if (strpos(Auth::user()->email, 'noemail')) {
      $user->email = '';
    }

    return view('site.pages.edit-profile', ['user' => $user, 'roles' => $roles, 'branches' => $branches, 'countries' => $countries, 'genders' => $genders, 'departments' => $departments]);
  }


  public function picture(Request $request)
  {
    if (!Auth::check()) {
      return response('Unauthorized', 403);
    }

    $user = Auth::user();

    //Save to user
    $user->profile_pic = $this->Fileupload($request, 'ppic', 'uploads');
    if ($user->save()) {

      return Redirect::back()->with('register_success', 'Your profile photo has been successfully updated.');
    } else {
      return Redirect::back()->with('register_error', 'Profile photo updating failed. Please refresh the page and try again.');
    }
  }


  public function Fileupload($request, $file = 'file', $folder = null)
  {
    $upload = null;
    //create folder if none
    if (!File::exists($folder)) {
      File::makeDirectory($folder);
    }
    //chec fo file
    if (!is_null($request->file($file))) {
      $extension = $request->file($file)->getClientOriginalExtension();
      $fileName = rand(11111, 99999) . uniqid() . '.' . $extension;
      $upload = $request->file($file)->move($folder, $fileName);
      if ($upload) {
        $upload = $fileName;
      } else {
        $upload = null;
      }
    }
    return $upload;
  }


  protected function getEdxRepository()
  {
    return App::make("App\\Repositories\\AuthenticateEdxRepository");
  }


  public function requestAuth(Request $request)
  {


    $validator = Validator::make($request->except('_token'), [
      'email' => 'required',

    ]);

    if ($validator->fails()) {
      return response()->json(['message' => $validator->errors()], 401);
    }


    $user = User::where('email', $request->input('email'))->first();


    if ($user) {

      $position = TwillPosition::where('user_id', $user->id)->first();

      Mail::send(
        'emails.reset',
        ['name' => $user->first_name . ' ' . $user->last_name, 'username' => $user->email, 'password' => $position->position],
        function ($message) use ($user) {
          $message->from('support@farwell-consultants.com', 'The Platcorp Group Academy Mobile APP Credentials');
          $message->to($user->email, $user->first_name);
          $message->subject('The Platcorp Group Academy Mobile APP');
        }
      );


      $message = "Your account credentials have been sent to your email address";
    } else {

      $message = "The email address provided is not attached to any user account";
    }

    return response()->json(['value' => $message, 'success' => true], 201);
  }



  public function requestCompany($username)
  {

    if (!$username) {
      return response()->json(['message' => 'The email address provided is not attached to any user account'], 401);
    } else {

      $user = User::where('username', $username)->first();

      if ($user) {

        $message = $user->company_id;
      } else {

        $message = "The email address provided is not attached to any user account";
      }

      return response()->json(['value' => $message, 'success' => true], 201);
    }
  }
}
