<?php

namespace App\Http\Requests\Admin\Course;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCourse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'more_info' => ['nullable', 'string'],
            'effort' => ['nullable', 'string'],
            'course_category_id' => ['nullable'],
            'status' => ['sometimes', 'boolean'],
            'course_video' => ['nullable'],
           
            'due_date' => ['nullable', 'string'],
            'course_type_id' => ['nullable'],
            'company_id' => ['nullable'],
            'short_name' =>['nullable'],
            'has_reflections' => ['sometimes', 'boolean'],
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getCourseCategoryId(){
        if ($this->has('course_category_id')){
           
            return $this->get('course_category_id')['course_category_id'];
        }
        return null;
    }


    public function getCourseTypeId(){
        if ($this->has('course_type_id')){
          
            if(!empty($this->get('course_type_id')) && is_array($this->get('company_id'))){
            return $this->get('course_type_id')['id'];
            }else{
                return null;
            }
        }
        return null;
    }


    public function getCompanyId(){
      
        if ($this->has('company_id')){

          if(!empty($this->get('company_id')) && is_array($this->get('company_id'))){
            return $this->get('company_id')['id'];
          }else{
            return null;
          }
           
        }
        return null;
    }
}
