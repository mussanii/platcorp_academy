<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;

class DynamicMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $superRole = ['SUPERADMIN'];
		$adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];
        $hodRoles = ['HOD'];
        $branchRoles = ['Branch Manager'];

        //  dd(auth()->guard('twill_users')->user()->roleValue);


		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ))){
			config(['twill-navigation' => adminMenu()]);
		}

		if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){
			config(['twill-navigation' => hrMenu()]);
		}
        
        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){
			config(['twill-navigation' => companyHRMenu()]);
		}

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$hodRoles ))){
			config(['twill-navigation' => hodMenu()]);
		}

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$branchRoles ))){
			config(['twill-navigation' => branchManagerMenu()]);
		}
        return $next($request);
    }
}
