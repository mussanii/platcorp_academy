<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class IndividualNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $individualMessage;
    public function __construct($individualMessage)
    {
       
        //
        $this->individualMessage = $individualMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)

        ->greeting($this->individualMessage['subject'])
        ->line($this->individualMessage['message'])
        ->action('Login', url('https://academy.platcorpgroup.com/user/login'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'type' => $this->individualMessage['type'],
            'title' => $this->individualMessage['subject'],
            'model_id' => $this->individualMessage['model_id'],
            'data' => $this->individualMessage['message'],

        ];
    }
}
