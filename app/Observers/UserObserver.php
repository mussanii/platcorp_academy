<?php

namespace App\Observers;

use App\Models\User;


class UserObserver
{
    
 
       /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {

    

        $configLms = config()->get("settings.lms.live");
      
        //Validate user
        if ($user === null) {
            return;
        }
        //Package data to be sent
        if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                 $email = $user->email;
        }
       
        $data = [
            'email' => $user->email,
            'name' => $user->first_name . ' ' . $user->last_name,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'honor_code' => 'true',
            'password' => request()->session()->get('register_password'),
            'country'=> 'KE',
            'terms_of_service'=> 'true',
           
        ];

   

        $headers = array(
        'Content-Type'=>'application/x-www-form-urlencoded',
        'cache-control' => 'no-cache',
        'Referer'=>env('APP_URL').'/register',
        );

        $client = new \GuzzleHttp\Client();

        try {

            $response = $client->request(
                'POST', $configLms['LMS_REGISTRATION_URL'], [
                'form_params' => $data,
                'headers'=>$headers,
                ]
            );

            return true;

        }catch (\GuzzleHttp\Exception\ClientException $e) {

            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            
            $errors = [];
            foreach ($response as $key => $error) {
                //Return error
                $errors[] = $error;
            }
            //echo "CATCH 1";

            return $errors[0];


        }catch ( \Exception $e){

           
            return $e->getMessage();

        }




    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }



}
