<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AuthenicateEdxUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");

        $email = $event->email;
        $password = $event->pass;
        //Get CSRF Token

        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $response = $client->request('GET', $configLms['LMS_LOGIN_URL']);
        $csrfToken = null;
        foreach ($response->getHeader('Set-Cookie') as $key => $cookie) {
            if (strpos($cookie, 'csrftoken') === false) {
                continue;
            }
            $csrfToken = explode('=', explode(';', $cookie)[0])[1];
            break;
        }

        if (!$csrfToken) {
            //Error, reactivate reset
            return false;

        }

        $data = [
            'email' => $email,
            'password' => $password,
        ];
        $headers = [
            'Content-Type' => ' application/x-www-form-urlencoded ; charset=UTF-8',
            'Accept' => ' text/html,application/json',
            'X-CSRFToken' => $csrfToken,
            'Cookie' => ' csrftoken=' . $csrfToken,
            'Origin' => $configLms['LMS_BASE'],
            'Referer' => $configLms['LMS_BASE'] . '/login',
            'X-Requested-With' => ' XMLHttpRequest',
        ];
        $client = new \GuzzleHttp\Client(['verify' => $configApp['VERIFY_SSL']]);
        $cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray(
            [
            'csrftoken' => $csrfToken
            ], env('LMS_DOMAIN')
        );
        $response = $client->request(
            'POST', $configLms['LMS_LOGIN_URL'], [
            'form_params' => $data,
            'headers' => $headers,
            'cookies' => $cookieJar
            ]
        );
        //set cookies

        if (!$response->hasHeader('Set-Cookie')) {
            return false;
        }


        $loggedInCookies = $response->getHeader('Set-Cookie');

        $setCookies = [];
        foreach ($loggedInCookies as $userCookie) {
            //format cookies
            $cookieDetails = (explode(';', $userCookie));
            $ourCookie = [];
            foreach ($cookieDetails as $cookieDetail) {
                $key = strtolower(trim(explode('=', $cookieDetail)[0]));
                $value = isset(explode('=', $cookieDetail)[1]) ? trim(explode('=', $cookieDetail)[1]) : 1;
                if (in_array(
                    strtolower($key),
                    ['__cfduid', 'csrftoken', 'edxloggedin', 'sessionid', 'openedx-language-preference', 'edx-user-info']
                )
                ) {
                    $ourCookie['name'] = $key;
                    $ourCookie['value'] = $value;
                } else {
                    $ourCookie[$key] = $value;
                }
            }
            if ($ourCookie['name'] != 'edx-user-info' && !isset($ourCookie['domain'])) {
                if ($ourCookie['name'] == 'csrftoken') {
                    $ourCookie['domain'] = '';
                } else {
                    $ourCookie['domain'] = '.' . $configApp['APP_DOMAIN'];
                }
                //Set the cookie
                $setCookies[] = ['name' => $ourCookie['name'], 'value' => $ourCookie['value'], 'domain' => $ourCookie['domain']];
            }
        }


        $data = [
            'grant_type' => 'password',
            'client_id' => $configLms['EDX_KEY'],
            'client_secret' => $configLms['EDX_SECRET'],
            'username' => auth()->user()->username,
            'password' => $password,
            //'token_type'=>'jwt',
        ];
        $tokenUrl = $configLms['LMS_BASE']. '/oauth2/access_token/';
        //Get authorization token
        $accessResponse = Curl::to($tokenUrl)
            ->withData($data)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->post();
           

        if ($accessResponse->status !== 200) {
            return false;
        }
        //Set access token
        $accessToken = json_decode($accessResponse->content, true);

        $setCookies[] = ['name' => 'edinstancexid', 'value' => $accessToken['access_token'], 'expiry' => $accessToken['expires_in']];
        $setCookies[] = ['name' => 'edx-company-info', 'value' => $event->company_id, 'expiry' => $accessToken['expires_in']];
        foreach ($setCookies as $cookie) {
            $cookie['expiry'] = 0;
            if (!isset($cookie['domain'])) {
                $cookie['domain'] = '.' . $configApp['APP_DOMAIN'];
            }
            setrawcookie($cookie['name'], $cookie['value'], $cookie['expiry'], '/', $cookie['domain']);
        }
       
        return true;

    }
}
