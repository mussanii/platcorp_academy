<?php
if (!function_exists('adminMenu')) {
	function adminMenu()
	{
		return [

			'companies' => [
				'title' => 'Companies',
				'module' => true,

			],

			'content' => [
				'title' => 'Content',
				'route' => 'admin.content.pages.index',
				'primary_navigation' => [
					'pages' => [
						'title' => 'Pages',
						'module' => true,
					],
					'menus' => [
						'title' => 'Menus',
						'route' => 'admin.content.menus.menus.index',
						'secondary_navigation' => [
							'menus' => [
								'title' => 'Menus',
								'module' => true
							],
							'menuTypes' => [
								'title' => 'Menu Types',
								'module' => true,

							],
						]

					],

				]
			],



			'courses' => [
				'title' => 'Courses',
				'route' => 'admin.courses.index',

			],

			'courseRequests' => [
				'title' => 'Course Access Requests',
				'module' => true,
			],

			'userCourses' => [
				'title' => 'Recommend Course',
				'module' => true,
			],

			'reports' => [
				'title' => 'Reports',
				'route' => 'admin.graph.dashboard',
				'primary_navigation' => [
					'graph' => [
						'title' => 'Dashboard',
						'route' => 'admin.graph.dashboard',
					],
					'subsidiary' => [
						'title' => 'Subsidiary Completion',
						'route' => 'admin.subsidiary.completion',
					],

					'branch' => [
						'title' => 'Branch Completion',
						'route' => 'admin.branch.completion',
					],
					'courseCompletions' => [
						'title' => 'Course Completions',
						'module' => true,

					],

					'userLicenses' => [
						'title' => 'Course Enrollments',
						'module' => true
					],
					'courseReflections' => [
						'title' => 'Course Reflection',
						'module' => true,

					],
				]
			],

			'evaluation' => [
				'title' => 'Course Evaluation',
				'route' => 'admin.evaluation.userEvaluations.index',
				'primary_navigation' => [
					'userEvaluations' => [
						'title' => 'User Evaluations',
						'module' => true,
					],

					'evaluations' => [
						'title' => 'Evaluation Questions',
						'module' => true
					],
					'evaluationOptions' => [
						'title' => 'Evaluation Options',
						'module' => true
					],

				]
			],



			'survey' => [
				'title' => 'Surveys',
				'route' => 'admin.survey.surverys.index',
				'primary_navigation' => [
					'surverys' => [
						'title' => 'Create Survey',
						'module' => true
					],

					'userSurveys' => [
						'title' => 'User Responses',
						'route' => 'admin.userSurvey.responses',
					],

				]
			],


			'countries' => [
				'title' => 'Countries',
				'module' => true
			],

			'branches' => [
				'title' => 'Branches',
				'module' => true
			],
			'sessions' => [
				'title' => 'Live Sessions',
				'module' => true,

			],
			'divisions' => [
				'title' => 'Departments',
				'module' => true,
			],

			'jobRoles' => [
				'title' => 'Job Roles',
				'module' => true
			],

			'messages' => [
				'title' => 'Messages',
				'route' => 'admin.messages.messages.index',
				'primary_navigation' => [

					'messages' => [
						'title' => 'Messages',
						'module' => true,
					],

					'messageTypes' => [
						'title' => 'MessageTypes',
						'module' => true,
					],
				]
			],

			'accounts' => [
				'title' => 'Allowed Accounts',
				'route' => 'admin.accounts.allowedAccounts.index',
				'primary_navigation' => [
					'allowedAccounts' => [
						'title' => 'Allowed Accounts',
						'module' => true,
					],
				],
			],

		];
	}

	if (!function_exists('hrMenu')) {
		function hrMenu()
		{
			return [


				'reports' => [
					'title' => 'Reports',
					'route' => 'admin.graph.dashboard',
					'primary_navigation' => [
						'graph' => [
							'title' => 'Dashboard',
							'route' => 'admin.graph.dashboard',
						],
						'subsidiary' => [
							'title' => 'Subsidiary Completion',
							'route' => 'admin.subsidiary.completion',
						],

						'branch' => [
							'title' => 'Branch Completion',
							'route' => 'admin.branch.completion',
						],
						'courseCompletions' => [
							'title' => 'Course Completions',
							'module' => true,

						],
						'userLicenses' => [
							'title' => 'Course Enrollments',
							'module' => true
						],
						'courseReflections' => [
							'title' => 'Course Reflection',
							'module' => true,

						],
					]
				],

				'evaluation' => [
					'title' => 'Course Evaluation',
					'route' => 'admin.evaluation.userEvaluations.index',
					'primary_navigation' => [
						'userEvaluations' => [
							'title' => 'User Evaluations',
							'module' => true,
						],

						'evaluations' => [
							'title' => 'Evaluation Questions',
							'module' => true
						],
						'evaluationOptions' => [
							'title' => 'Evaluation Options',
							'module' => true
						],

					]
				],

				'survey' => [
					'title' => 'Surveys',
					'route' => 'admin.survey.userSurveys.index',
					'primary_navigation' => [
						'surverys' => [
							'title' => 'Create Survey',
							'module' => true
						],

						'userSurveys' => [
							'title' => 'User Responses',
							'module' => true,
						],

					]
				],
				'sessions' => [
					'title' => 'Live Sessions',
					'module' => true,

				],
				'courses' => [
					'title' => 'Courses',
					'route' => 'admin.courses.index',

				],
				'courseRequests' => [
					'title' => 'Course Access Requests',
					'module' => true,
				],
				'userCourses' => [
					'title' => 'Recommend Course',
					'module' => true,
				],
				'jobRoles' => [
					'title' => 'JobRoles',
					'module' => true
				],

				'branches' => [
					'title' => 'Branches',
					'module' => true
				],


				'accounts' => [
					'title' => 'Allowed Accounts',
					'route' => 'admin.accounts.allowedAccounts.index',
					'primary_navigation' => [
						'allowedAccounts' => [
							'title' => 'Allowed Accounts',
							'module' => true,
						],
					],
				],

			];
		}
	}



	if (!function_exists('companyHRMenu')) {
		function companyHRMenu()
		{
			return [

				'companyReports' => [
					'title' => 'Reports',
					'route' => 'admin.companygraph.dashboard',
					'primary_navigation' => [
						'graph' => [
							'title' => 'Dashboard',
							'route' => 'admin.companygraph.dashboard',
						],
						'reports' => [
							'title' => 'Company Reports',
							'route' => 'admin.companyReport.completion',

						],

					]
				],


				'courses' => [
					'title' => 'Courses',
					'route' => 'admin.courses.index',

				],

				'evaluation' => [
					'title' => 'Evaluation',
					'route' => 'admin.evaluation.userEvaluations.index',
					'primary_navigation' => [
						'userEvaluations' => [
							'title' => 'User Evaluations',
							'module' => true,
						],
						'evaluations' => [
							'title' => 'Evaluation Questions',
							'module' => true
						],
						'evaluationOptions' => [
							'title' => 'Evaluation Options',
							'module' => true
						],

					]
				],


				'survey' => [
					'title' => 'Surveys',
					'route' => 'admin.survey.userSurveys.index',
					'primary_navigation' => [
						'surverys' => [
							'title' => 'Create Survey',
							'module' => true
						],

						'userSurveys' => [
							'title' => 'User Responses',
							'module' => true,
						],


					]
				],

				'sessions' => [
					'title' => 'Live Sessions',
					'module' => true,

				],

				'userCourses' => [
					'title' => 'Recommend Course',
					'module' => true,
				],
				'courseRequests' => [
					'title' => 'Course Access Requests',
					'module' => true,
				],


				'accounts' => [
					'title' => 'Allowed Accounts',
					'route' => 'admin.accounts.allowedAccounts.index',
					'primary_navigation' => [
						'allowedAccounts' => [
							'title' => 'Allowed Accounts',
							'module' => true,
						],
					],
				],

			];
		}
	}


	if (!function_exists('branchManagerMenu')) {
		function branchManagerMenu()
		{
			return [
				'reports' => [
					'title' => 'Branch Reports',
					'route' => 'admin.branchReport.completion',

				],
			];
		}
	}



	if (!function_exists('hodMenu')) {
		function hodMenu()
		{
			return [
				'reports' => [
					'title' => 'Department Reports',
					'route' => 'admin.departmentReport.completion',

				],


			];
		}
	}
}
