<?php

namespace App\Jobs;

use App\Mail\EmailForQueuing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendSessionsEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $users;
    protected $name;

    protected $title;
    protected $url;
    protected $decline_url;
    public function __construct($name,$users,$title,$url,$decline_url)
    {
        $this->users = $users;
      $this->name = $name;
        $this->title = $title;
        $this->url = $url;
        $this->decline_url = $decline_url;

        // dd($this->users);

        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            $creator = $this->name;
            $title = $this->title;
            $name = $user->name;
            $url = $this->url;
            $decline_url = $this->decline_url;
    
            $data = [
                'creator' => $creator,
                'title' => $title,
                'name' => $name,
                'url' => $url,
                'decline_url' => $decline_url ?? '',
            ];
    
            $email = new EmailForQueuing($data);
            Mail::to($user->email, $name)->send($email);
        }
    }
}
