<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\AllowedAccount;
use App;
use Maatwebsite\Excel\Facades\Excel;



class LicenseImport implements ToCollection, WithHeadingRow
{

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        //dd($rows);
        foreach ($rows as $row) {

            $allowed = AllowedAccount::where('email', $row['email_address'])->first();

            if (empty($allowed)) {
                $allow = new AllowedAccount();
                $allow->title = $row['name'];
                $allow->email = $row['email_address'];
                $allow->company_id = $row['company'];
                $allow->published = 1;
                $allow->save();
            }
        }

        session()->put('uploaded', 'Yes');
    }
}
