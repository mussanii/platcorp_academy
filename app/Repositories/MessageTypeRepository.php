<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\MessageType;

class MessageTypeRepository extends ModuleRepository
{
    use HandleRevisions;

    public function __construct(MessageType $model)
    {
        $this->model = $model;
    }
}
