<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Survery;
use App\Models\User;
use Illuminate\Support\Arr;
use App\Models\Notification;
use DB;

class SurveryRepository extends ModuleRepository
{
    use HandleBlocks;

    public function __construct(Survery $model)
    {
        $this->model = $model;
    }



    public function update($id, $fields)
    {
        
        DB::transaction(function () use ($id, $fields) {
            
            $object = $this->model->findOrFail($id);

            $this->beforeSave($object, $fields);

            $fields = $this->prepareFieldsBeforeSave($object, $fields);

            $object->fill(Arr::except($fields, $this->getReservedFields()));

            $object->save();

            $message = [
                'type'=> Notification::SURVEY,
                'subject'=> 'New Survey Alert',
                'model_id' => $object->id,
                'message' => 'New Survey has been created for your company . Click in order to view the survey.',
               ];
 
              
               $users = User::where('company_id', $object->company_id)->get();
 
               foreach($users as $user){
 
                  $user->notify(new \App\Notifications\SurveyNotification($message));
 
               }
            $this->afterSave($object, $fields);
        }, 3);
    }
}
