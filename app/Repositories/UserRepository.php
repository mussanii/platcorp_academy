<?php

namespace App\Repositories;

use A17\Twill\Repositories\UserRepository as TwillUserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use A17\Twill\Models\Group;
use App;
use TaylorNetwork\UsernameGenerator\Generator;
use App\Models\Domain;
use App\Models\TwillUsersAuth;
use A17\Twill\Models\Enums\UserRole;
use App\Edx\EdxAuthUser;
use Carbon\Carbon;

class UserRepository extends TwillUserRepository
{

  public function __construct(User $model)
  {
    $this->model = $model;
  }



    /**
     * @param array $with
     * @param array $scopes
     * @param array $orders
     * @param int $perPage
     * @param bool $forcePagination
     * @return \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get($with = [], $scopes = [], $orders = [], $perPage = 20, $forcePagination = false)
    {
        $query = $this->model->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }



        /**
     * @param array $with
     * @param array $scopes
     * @param array $orders
     * @param int $perPage
     * @param bool $forcePagination
     * @return \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getCompany($with = [], $scopes = [], $orders = [], $perPage = 20, $forcePagination = false)
    {
        $query = $this->model->where('company_id', Auth::user()->company_id)->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }


  /**
   * @param string[] $fields
   * @return \A17\Twill\Models\Model
   */
  public function createForRegisterGoogle($fields, $company)
  {

    return DB::transaction(function () use ($fields, $company) {


      $original_fields = $fields;

      $fields = $this->prepareFieldsBeforeCreate($fields);

      $generator = new Generator(['separator' => '_']);
      $username = $generator->generate($fields['given_name'] . ' ' . $fields['family_name']);



      $object = $this->model->create([
        'name' => $fields['given_name'] . ' ' . $fields['family_name'],
        'email' => $fields['email'],
        'role' =>  UserRole::Learner,
        'username' => $username,
        'password' => '',
        'company_id' => $company,
        'branch_id' => '',
        'job_role_id' => '',
        'first_name' => $fields['given_name'],
        'last_name' => $fields['family_name'],
        'is_activated' => 1,
        'published' => 1,
        'registered_at' => Carbon::now(),
        'google_id' => $fields['id'],
        'authentication_type' => USER::AUTHENTICATION_GOOGLE,

      ]);
      $object->save();


      return $object;
    }, 3);
  }


  public function createForGoogle($fields, $company)
  {

    return DB::transaction(function () use ($fields, $company) {


      $original_fields = $fields;

      $fields = $this->prepareFieldsBeforeCreate($fields);

      $generator = new Generator(['separator' => '_']);
      $username = $generator->generate($fields['given_name'] . ' ' . $fields['family_name']);


      $usern = EdxAuthUser::where('username', $username)->first();


      if (!empty($usern)) {
        $username = $username . '_' . rand(0, 100);
      } else {
        $username = $username;
      }
     
      $object = $this->model->create([
        'name' => $fields['given_name'] . ' ' . $fields['family_name'],
        'email' => $fields['email'],
        'role' =>  UserRole::Learner,
        'username' => $username,
        'password' => '',
        'company_id' => $company,
        'branch_id' => '',
        'job_role_id' => '',
        'first_name' => $fields['given_name'],
        'last_name' => $fields['family_name'],
        'is_activated' => 1,
        'published' => 1,
        'registered_at' => Carbon::now(),
        'google_id' => $fields['id'],
        'authentication_type' => USER::AUTHENTICATION_GOOGLE,

      ]);


      $object->save();

      return $object;
    }, 3);
  }



  public function createForRegisterMicrosoft($fields, $company)
  {

    return DB::transaction(function () use ($fields, $company) {
      $original_fields = $fields;

      $fields = $this->prepareFieldsBeforeCreate($fields);

      $generator = new Generator(['separator' => '_']);
      $username = $generator->generate($fields->user['givenName'] . ' ' . $fields->user['surname']);

      $object = $this->model->create([
        'name' => $fields->user['givenName'] . ' ' . $fields->user['surname'],
        'email' => $fields->email,
        'role' =>  UserRole::Learner,
        'username' => $username,
        'password' => '',
        'company_id' => $company,
        'branch_id' => '',
        'job_role_id' => '',
        'first_name' => $fields->user['givenName'],
        'last_name' => $fields->user['surname'],
        'is_activated' => 1,
        'published' => 1,
        'registered_at' => Carbon::now(),
        'microsoft_id' => $fields['id'],
        'authentication_type' => USER::AUTHENTICATION_MICROSOFT,
      ]);

      $object->save();

      return $object;
    }, 3);
  }


  public function createForMicrosoft($fields, $company)
  {

    return DB::transaction(function () use ($fields, $company) {


      $original_fields = $fields;

      $fields = $this->prepareFieldsBeforeCreate($fields);



      $generator = new Generator(['separator' => '_']);
      $username = $generator->generate($fields->user['givenName'] . ' ' . $fields->user['surname']);


      $usern = EdxAuthUser::where('username', $username)->first();

      if (!empty($usern)) {
        $username = $username . '_' . rand(0, 7);
      } else {
        $username = $username;
      }

      $object = $this->model->create([
        'name' => $fields->user['givenName'] . ' ' . $fields->user['surname'],
        'email' => $fields->email,
        'role' =>  UserRole::Learner,
        'username' => $username,
        'password' => '',
        'company_id' => $company,
        'branch_id' => '',
        'job_role_id' => '',
        'first_name' => $fields->user['givenName'],
        'last_name' => $fields->user['surname'],
        'is_activated' => 1,
        'published' => 1,
        'registered_at' => Carbon::now(),
        'microsoft_id' => $fields['id'],
        'authentication_type' => USER::AUTHENTICATION_MICROSOFT,
      ]);
      $object->save();

     

      return $object;
    }, 3);
  }





  public function createForValidation($fields)
  {
    $verify = TwillUsersAuth::where('user_id', $fields['user'])->where('token', $fields['user'])->first();

    if (!empty($verify)) {
      $this->model->where('id', $verify->user_id)->update([
        'is_activated' => 1,
        'registered_at' => Carbon\Carbon::now(),
      ]);
    }
  }

  protected function changeCompany($email)
  {
    //Get domain
    $raw_domain = explode("@", $email)[1];
    //Get company with domain
    $domain = Domain::where('name', $raw_domain)->first();
    if ($domain) {
      $company_id = $domain->company_id;
    } else {
      $company_id = null;
    }

    return $company_id;
  }
}
