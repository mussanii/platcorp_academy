<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Division;

class DivisionRepository extends ModuleRepository
{
    

    public function __construct(Division $model)
    {
        $this->model = $model;
    }
}
