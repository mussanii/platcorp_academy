<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\Behaviors\HandleRepeaters;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Company;
use Artisan;

class CompanyRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions, HandleRepeaters;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function afterSave($object, $fields)
    {
     
    $this->updateRepeater($object, $fields, 'domains', 'Domain','domain');
      $this->updateRepeater($object, $fields, 'values', 'OurValue','our_values');
      parent::afterSave($object, $fields);

     
       
    }

    public function getFormFields($object){
        $fields = parent::getFormFields($object);

         $fields = $this->getFormFieldsForRepeater($object, $fields, 'domains','Domain','domain');
         $fields = $this->getFormFieldsForRepeater($object, $fields, 'values','OurValue','our_values');
        return $fields;
    }

    public function prepareFieldsBeforeCreate($fields){
        $fields['layout'] ='regular';
        return parent::prepareFieldsBeforeCreate($fields);
    }
}
