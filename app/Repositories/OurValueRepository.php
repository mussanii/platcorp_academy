<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\OurValue;

class OurValueRepository extends ModuleRepository
{
    

    public function __construct(OurValue $model)
    {
        $this->model = $model;
    }
}
