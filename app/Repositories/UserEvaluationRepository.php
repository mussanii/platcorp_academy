<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\UserEvaluation;

class UserEvaluationRepository extends ModuleRepository
{
    

    public function __construct(UserEvaluation $model)
    {
        $this->model = $model;
    }
}
