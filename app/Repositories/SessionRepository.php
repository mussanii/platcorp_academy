<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\Behaviors\HandleNesting;
use A17\Twill\Repositories\ModuleRepository;
use App\Jobs\SendSessionsEmail;
use App\Models\Session;
use App\Models\User;
use App\Traits\ZoomMeetingTrait;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Socialite;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\ItemBody;
use Microsoft\Graph\Model\Event;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Microsoft\Graph\Model\DateTimeTimeZone;


class SessionRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions, HandleNesting;

    
    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    public $client;
    public $jwt;
    public $headers;
   
   
    public function __construct(Session $model)
    {
        $this->model = $model;
        $this->client = new Client();
        $this->jwt = $this->generateZoomToken();
        $this->headers = [
            'Authorization' => 'Bearer '.$this->jwt,
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ];
      
    }

    public function generateZoomToken()
    {
        $configZoom = config()->get("settings.zoom");

        $key = $configZoom['ZOOM_API_KEY'];
        $secret = $configZoom['ZOOM_API_SECRET'];
        $payload = [
            'iss' => $key,
            'exp' => strtotime('+1 minute'),
        ];
        return \Firebase\JWT\JWT::encode($payload, $secret, 'HS256');

    }
     public function update($id, $fields)
     {
       
    
        if($fields['start_time'] < Carbon::now()){
            DB::transaction(function() use ($id, $fields){
                $object = $this->model->findOrFail($id);


                $start = $this->toZoomTimeFormat($fields['start_time']);
                $object->update([
                    'meeting_id' => null,
                    'host_email' => null,
                    'topic' => $fields['title']['en'],
                    'status'=> null,
                    'start_time' => $start,
                    'agenda' => $fields['description'],
                    'description' => $fields['description'],
                    'join_url' => null,
                    'teams_link'=>null,
                    'meeting_password' => null,
                    'country_id'=>$fields['country_id'],
                    'company_id'=>$fields['company_id'],
                    'branch_id'=>NULL,
                    'job_role_id'=>(!empty($fields['job_role_id']))? $fields['job_role_id']:null,
                    'session_video_url' =>(! empty($fields['session_video_url'])) ? $fields['session_video_url'] : null ,
                    'active' => 0,
                    'published' => 1,
                    'duration' => $fields['duration']
    
                ]);
                $this->afterSave($object, $fields);
    
            }, 3);

        }else{

            DB::transaction(function() use($id,$fields){             
                $object = $this->model->findOrFail($id);
                $create = $this->zoomCreate($fields);
             
                 session_start();
                $teams_link = (!empty($_SESSION['teams_link']))? $_SESSION['teams_link']:null;
             
               
            
                $start = $this->toZoomTimeFormat($fields['start_time']);
                $this->beforeSave($object, $fields);
                $fields = $this->prepareFieldsBeforeSave($object, $fields);


                $object->update([
                    'meeting_id' => $create['data']['id'],
                    'host_email' => $create['data']['host_email'],
                    'topic' => $create['data']['topic'],
                    'status'=> $create['data']['status'],
                    'start_time' => $start,
                    'agenda' => $create['data']['agenda'],
                    'description' => $create['data']['agenda'],
                    'join_url' => $create['data']['join_url'],
                    'teams_link'=>(! empty($teams_link)) ? $teams_link: null,
                    'meeting_password' => $create['data']['password'],
                    'country_id'=>$fields['country_id'],
                    'company_id'=>$fields['company_id'],
                    'branch_id'=>NULL,
                    'job_role_id'=>(!empty($fields['job_role_id']))? $fields['job_role_id']:null,
                    'session_video_url' =>(! empty($fields['session_video_url'])) ? $fields['session_video_url'] : null ,
                    'active' => 0,
                    'published' => 1,
                    'duration' => $fields['duration'],
    
                ]);


                   $email = [
                    'subject'=> 'New Live Session',
                     'message' => 'A New Live Session has been created ('. $object->title.'). Click in order to view the details.',
                   ];

                   

                  

                   if (isset($fields['job_role_id']) && is_array($fields['job_role_id'])) {
                    $users = User::where('id', '!=', 1)
                        ->where('company_id', $fields['company_id'])
                        ->where('country_id', $fields['country_id'])
                        ->whereIn('job_role_id', $fields['job_role_id'])
                        ->get();
                } else {
                    $users = User::where('id', '!=', 1)
                        ->where('country_id', $fields['country_id'])
                        ->where('company_id', $fields['company_id'])
                        ->get();
                }
 
                   $name = Auth::user()->name;
                   $title = $object->title;
                   $url = route('login');
                   $decline_url = $teams_link;

                    SendSessionsEmail::dispatch($name, $users, $title, $url, $decline_url);
                $this->afterSave($object, $fields);
                

              }, 3);
        }
        
     }

     public function updateBasic($id, $values, $scopes = [])
     {
        return DB::transaction( function() use ($id, $values, $scopes){
            if(is_null($id)){
                $query = $this->model->query();
                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }
                $query->update($values);

                $query->get()->each(function($object) use ($values){
                    $this->afterUpdateBasic($object,$values);
                });
                return true;

            }

            if(is_array($id)){
                $query = $this->model->whereIn('id',$id);
                $query->update($values);
                $query->get()->each(function ($object) use($values){
                    $this->afterUpdateBasic($object,$values);

                });
                return true;
            }

            if(!is_null($object = $this->model->find($id)) ){
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                return true;
            }
            return false;

        }, 3);

     }


     
     

     public function zoomCreate($data)
     {
      
        $path ='users/me/meetings';
        $url = $this->retrieveZoomUrl();
        $body = [
            'headers' => $this->headers,
            'body'    => json_encode([
                'topic'      => $data['title']['en'],
                'type'       => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration'   => $data['duration'],
                'agenda'     => (! empty($data['description'])) ? $data['description'] : null,
                'timezone'     => 'Africa/Nairobi',
                'settings'   => [
                    'host_video'        => true ,
                    'participant_video' => false,
                    'waiting_room'      => true,

                    "join_before_host" => true,

                ],
            ]),
        ];
      
        $response = $this->client->post($url.$path,$body);

        return [
            'success'=> $response->getStatusCode() === 201,
            'data' => json_decode($response->getBody(), true),
        ];
     }

     public function zoomUpdate($id, $data)
    {
       

        
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        
      
        $body = [
            'headers' => $this->headers,
            'body'    => json_encode([
                'topic'      => $data['title'],
                'type'       => self::MEETING_TYPE_SCHEDULE,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration'   => $data['duration'],
                'agenda'     => (! empty($data['description'])) ? $data['description'] : null,
                'timezone'     => 'Africa/Nairobi',
                'settings'   => [
                    'host_video'        => ($data['host_video'] == "1") ? true : false,
                    'participant_video' => ($data['participant_video'] == "1") ? true : false,
                    'waiting_room'      => true,
                     'join_before_host' => true,
                ],
            ]),
        ];
        $response =  $this->client->patch($url.$path, $body);

       

        return [
            'success' => $response->getStatusCode() === 204,
            'data'    => json_decode($response->getBody(), true),
        ];
    }

    public function retrieveZoomUrl()
    {
        $configZoom = config()->get("settings.zoom");
        return $configZoom['ZOOM_API_URL'];
    }

    public function toZoomTimeFormat(string $dateTime)
    {

    
    try {
        $date = new \DateTime($dateTime);
        return $date->format('Y-m-d\TH:i:s');
    } catch (\Throwable $th) {
        Log::error('ZoomJWT->toZoomTimeFormat : '.$th->getMessage());
        return '';
    }
}

private function getAccessToken($microsoft_id)
{
    $client = new Client();
    $response = $client->post('https://login.microsoftonline.com/987a006e-3266-486c-93e2-96e40fa9341d/oauth2/v2.0/token', [
        'form_params' => [
            'client_id' => env('MICROSOFT_CLIENT_ID'),
            'client_secret' => env('MICROSOFT_CLIENT_SECRET'),
            'grant_type' => 'client_credentials',
            'scope' => 'https://graph.microsoft.com/.default',
        ],
    ]);
    return json_decode($response->getBody(), true)['access_token'];
}

}
