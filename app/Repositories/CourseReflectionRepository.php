<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseReflection;

class CourseReflectionRepository extends ModuleRepository
{
    

    public function __construct(CourseReflection $model)
    {
        $this->model = $model;
    }
}
