<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\AllowedAccount;
use DB;
use Auth;

class AllowedAccountRepository extends ModuleRepository
{


    public function __construct(AllowedAccount $model)
    {
        $this->model = $model;
    }

    public function update($id, $fields)
    {
        DB::transaction(function () use ($id, $fields) {

            $object = $this->model->findOrFail($id);

            $object->update([
                'email' => $fields['email'],
                'company_id' => $fields['company_id'],
                'published' => 1,
            ]);
            // $object->save();

            $this->afterSave($object, $fields);
        }, 3);
    }


    public function getCompany($with = [], $scopes = [], $orders = [], $perPage = 20, $forcePagination = false)
    {
        $query = $this->model->where('company_id', Auth::user()->company_id)->with($with);

        $query = $this->filter($query, $scopes);
        $query = $this->order($query, $orders);

        if (!$forcePagination && $this->model instanceof Sortable) {
            return $query->ordered()->get();
        }

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }
}
