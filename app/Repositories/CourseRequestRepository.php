<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\CourseRequest;
use Auth;
use Carbon\Carbon;

class CourseRequestRepository extends ModuleRepository
{
    

    public function __construct(CourseRequest $model)
    {
        $this->model = $model;
    }



        /**
     * @param string $slug
     * @param array $scope
     * @return int
     */
    public function getCountByStatusSlug($slug, $scope = [])
    {
        $this->countScope = $scope;

        switch ($slug) {
            
            case 'published':
                return $this->getCountForPublished();
            case 'draft':
                return $this->getCountForDraft();
            case 'trash':
                return $this->getCountForTrash();
        }

        foreach ($this->traitsMethods(__FUNCTION__) as $method) {
            if (($count = $this->$method($slug)) !== false) {
                return $count;
            }
        }

        return 0;
    }

    /**
     * @return int
     */
    public function getCountForAll()
    {

        $superRole = ['SUPERADMIN'];
        $adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){

        $query = $this->model->newQuery();

        }

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){

            $query = $this->model->where('company_id', Auth::user()->company_id)->newQuery();

        }
        return $this->filter($query, $this->countScope)->count();
    }

    /**
     * @return int
     */
    public function getCountForPublished()
    {
        $superRole = ['SUPERADMIN'];
        $adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){

        $query = $this->model->newQuery();

        }

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){

            $query = $this->model->where('company_id', Auth::user()->company_id)->newQuery();

        }
        return $this->filter($query, $this->countScope)->published()->count();
    }

    /**
     * @return int
     */
    public function getCountForDraft()
    {
        $superRole = ['SUPERADMIN'];
        $adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){

        $query = $this->model->newQuery();

        }

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){

            $query = $this->model->where('company_id', Auth::user()->company_id)->newQuery();

        }
        return $this->filter($query, $this->countScope)->draft()->count();
    }

    /**
     * @return int
     */
    public function getCountForTrash()
    {
        $superRole = ['SUPERADMIN'];
        $adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){

        $query = $this->model->newQuery();

        }

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){

            $query = $this->model->where('company_id', Auth::user()->company_id)->newQuery();

        }
        return $this->filter($query, $this->countScope)->onlyTrashed()->count();
    }

   
      /** Get function for
     * Course Request Report with parameter*/

     public function getForCompany($with = [], $scopes = [], $orders = [], $perPage = 1000, $forcePagination = false)
     {
        $superRole = ['SUPERADMIN'];
        $adminRoles = ['Admin'];
        $GrouphrRoles = ['Group HR'];
        $CompanyhrRoles = ['Company HR'];

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$superRole ) || in_array(auth()->guard('twill_users')->user()->roleValue,$adminRoles ) || in_array(auth()->guard('twill_users')->user()->roleValue,$GrouphrRoles ))){

         $query = $this->model->with($with);
        }

        if(auth()->guard('twill_users')->user() && (in_array(auth()->guard('twill_users')->user()->roleValue,$CompanyhrRoles ))){
            $query = $this->model->where('company_id', Auth::user()->company_id)->with($with);
        }
 
         $query = $this->filter($query, $scopes);
         $query = $this->order($query, $orders);
 
         if (!$forcePagination && $this->model instanceof Sortable) {
             return $query->ordered()->get();
         }
 
         if ($perPage == -1) {
             return $query->get();
         }
 
         return $query->paginate($perPage);
     }
    


}
