<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class UserEvaluation extends Model implements Sortable
{
    use HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];

    public function users(){
        return $this->belongsTo(User::class,'user_id','id');
      }
  
      public function courses(){
        return $this->belongsTo(Course::class,'course_id','id');
      }
  
  
      public function evaluations(){
        return $this->belongsTo(Evaluation::class,'question1','id');
      }
  
  
      public function getQuestion($question){
  
      $questions = Evaluation::where('id','=', $question)->first();
      if (empty($questions)){
        return '';
      }else{
        return $questions->title;
      }
      }
  
  
      protected $presenterAdmin = 'App\Presenters\Front\UserEvaluationPresenter';
  
  
      public static function getQuestionOneAttribute(){
        
        $questions = Evaluation::where('id','=', $this->question1)->first();
  
        return $questions->title;
  
      }
    
}
