<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Edx\StudentCourseCompletion;
use App\Edx\StudentCourseware;
use App\Models\CourseCompletion;
use App\Models\JobroleCourse;
use Carbon\Carbon;
use App;
use Auth;
use App\Models\MyCommunity;

class UserLicense extends Model
{
  use HasPosition;


  protected $dates = ['enrolled_at', 'created_at'];

  public function user()
  {
    return $this->belongsTo('App\Models\User', 'user_id', 'id');
  }


  public function users()
  {
    return $this->belongsTo('App\Models\User', 'learner_id', 'id');
  }
  /**
   * Get the company that this user belongs to .
   */
  public function course()
  {
    return $this->belongsTo('App\Models\Course', 'course_id', 'id');
  }


  public function courses()
  {
    return $this->belongsTo('App\Models\Course', 'course_id', 'id');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::parse($value)->format('d-m-Y H:i:s');
  }



  public function getResourceUrlAttribute()
  {
    return url('/admin/user-licenses/' . $this->getKey());
  }

  public function getEnrollments($course)
  {
    $enrollments = self::where('course_id', $course)->count();

    return $enrollments;
  }

  public function branches()
  {
    return $this->belongsTo(Branch::class, 'branch', 'id');
  }

  public function roles()
  {
    return $this->belongsTo(JobRole::class, 'job_role_id', 'id');
  }

  public function countries()
  {
    return $this->belongsTo(Country::class, 'country_id', 'id');
  }

  public function companies()
  {
    return $this->belongsTo(Company::class, 'company_id', 'id');
  }


  public function getLearnerEmailAttribute($value)
  {


    return $value;
  }

 

  public function getLearnerEnrollAttribute($value)
  {
    return Carbon::parse($value)->format('d-m-Y H:i:s');
  }


  public function getCompletionStatusAttribute()
  {

    $completed = $this->checkCompletionsStatus($this->course_id, $this->user_id);

    return $completed['status'];
  }


  private function checkCompletionsStatus($course_id, $user_id)
  {

    $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', $user_id)->first();

    if ($completion) {

      if ($completion->score >= 0.80) {

        $completionStatus = 1;
        $status = "Completed";
      } else {

        $completionStatus = 2;
        $status = 'Completed but failed';
      }
    } else {
      $completionStatus = false;
      $status = 'In Progress';
    }

    return [
      'status' => $status,
      'completionStatus' => $completionStatus
    ];
  }



  public function getcompletionDate($course, $user)
  {

    $quiz = CourseCompletion::where('user_id', $user)->where('course_id', $course)->first();

    if ($quiz) {

      return $quiz->completion_date;
    } else {

      return '';
    }
  }



  public function getStatusAttribute()
  {


    return  App::environment(['local', 'staging']) ? $this->statusLocal() : $this->statusLive();
  }


  public function statusLocal()
  {

    if ($this->enrolled_at) {
      $user = User::where('email', Auth::user()->email)->first();

      $quiz = CourseCompletion::where('user_id', $user->id)->where('course_id', $this->course->id)->first();
      if ($quiz) {
        $score = (float)$quiz->score;
        $pass = (float)0.80;
        $high_low = (float)0.79;


        if ($score >= $pass) {
          return 'Completed';
        } elseif ($score < $high_low) {
          return 'Completed but failed';
        }
      } else {
        return 'In Progress';
      }
    }
  }


  public function statusLive()
  {

    $email = '';
    if ($this->enrolled_at) {
      $result = $this->getCourseProgress($this->course->course_id, $email);
      return $result['status'];
    } else {
      return 'Pending';
    }
  }


  public function getGradeAttribute()
  {

    return  App::environment(['local', 'staging']) ? $this->gradeLocal() : $this->gradeLive();
  }

  public function gradeLocal()
  {

    $user = User::where('email', Auth::user()->email)->first();

    $quiz = CourseCompletion::where('user_id', $user->id)->where('course_id', $this->course->id)->first();

    if ($quiz) {

      return $quiz->score;
    } else {
      return 0;
    }
  }


  public function gradeLive()
  {

    $email = '';
    if ($this->enrolled_at) {
      $result = $this->getCourseProgress($this->course->course_id, $email);

      $user = User::where('email', Auth::user()->email)->first();
      $license = UserLicense::where('user_id', $user->id)->where('course_id', $this->course->id)->first();

      $quiz = CourseCompletion::where('user_id', $user->id)->where('course_id', $this->course->id)->first();

      if ($result['grade'] > 0) {
        if ($quiz) {
          $quiz->user_id = $user->id;
          $quiz->course_id = $this->course->id;
          $quiz->username = $user->name;
          $quiz->email = $user->email;
          $quiz->branch = $user->branch_id;
          $quiz->score = $result['grade'];
          $quiz->max_score = 1;
          $quiz->completion_date = $result['completion'];
          $quiz->enrollment_date = $result['enrollment'];
          $quiz->job_role_id = $user->job_role_id;
          $quiz->country_id = $user->country_id;
          $quiz->company_id = $user->company_id;
          $quiz->user_license_id = $license->id;
          $quiz->save();
        } else {
          $quiz = new CourseCompletion();
          $quiz->user_id = $user->id;
          $quiz->course_id = $this->course->id;
          $quiz->username = $user->name;
          $quiz->email = $user->email;
          $quiz->branch = $user->branch_id;
          $quiz->score = $result['grade'];
          $quiz->max_score = 1;
          $quiz->completion_date = $result['completion'];
          $quiz->enrollment_date = $result['enrollment'];
          $quiz->job_role_id = $user->job_role_id;
          $quiz->country_id = $user->country_id;
          $quiz->company_id = $user->company_id;
          $quiz->user_license_id = $license->id;
          $quiz->save();
        }

        return $quiz->score;
      } else {
        return 0;
      }
    }
  }


  public function getTrainingStatus($user, $course)
  {
    if ($this->enrolled_at) {

      $quiz = CourseCompletion::where('user_id', $user)->where('course_id', $course)->first();
      if ($quiz) {
        $score = (float)$quiz->score;
        $pass = (float)0.80;
        $high_low = (float)0.79;


        if ($score >= $pass) {
          return 'Completed';
        } elseif ($score < $high_low) {
          return 'Completed but failed';
        }
      } else {
        return 'In Progress';
      }
    }
  }

  public function getTrainingGrade($user, $course)
  {

    $quiz = CourseCompletion::where('user_id', $user)->where('course_id', $course)->first();

    if ($quiz) {

      return $quiz->score;
    } else {
      return 0;
    }
  }


  public function getActionAttribute()
  {


    return  App::environment(['local', 'staging']) ? $this->actionLocal() : $this->actionLive();
  }

  public function actionLocal()
  {
    $configLms = config()->get("settings.lms.live");
    if ($this->enrolled_at) {
      $user = User::where('email', Auth::user()->email)->first();

      $quiz = CourseCompletion::where('user_id', $user->id)->where('course_id', $this->course->id)->first();
      if ($quiz) {
        if ($quiz->score >= 0.80) {

          return route('course.badge', $this->course->id);
        }
      }

      return $configLms['LMS_BASE'] . '/courses/' . $this->course->course_id . '/courseware';
    } else {
      return route('course.enroll', $this->course->id);
    }
  }



  public function actionLive()
  {

    $configLms = config()->get("settings.lms.live");
    $email = '';
    if ($this->enrolled_at) {
      $result = $this->getCourseProgress($this->course->course_id, $email);
      if ($result['grade'] >= 0.80 && $result['status'] == "Completed") {

        return route('course.badge', $this->course->id);
      }
      return $configLms['LMS_BASE'] . '/courses/' . $this->course->course_id . '/courseware';
    } else {
      return route('course.enroll', $this->course->id);
    }
  }




  private function getCourseProgress($courseId, $email)
  {
    if (!empty($email)) {
      $user = EdxAuthUser::where('email', $email)->firstOrFail();
    } else {
      $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
    }
    $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
    $completion = StudentCourseCompletion::where(['student_id' => $user->id, 'module_type' => 'problem', 'course_id' => $courseId])->orderBy('id', 'DESC')->first();
    if ($enrollment) {

      $gradeApiObject = $enrollment->getGenCert();
      //var_dump($gradeApiObject);
      if ($gradeApiObject[0]->percent >= 0.80) {
        $gradeApiObject[0]->letter_grade = 'Completed';
        $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
      } elseif ($gradeApiObject[0]->percent <= 0.79 && $gradeApiObject[0]->percent >= 0.10) {

        $gradeApiObject[0]->letter_grade = 'Completed but Failed';
        $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
      } else {
        $gradeApiObject[0]->letter_grade = 'In Progress';
        $completion = '';
      }

      return [
        'status' => $gradeApiObject[0]->letter_grade,
        'grade' => (float)$gradeApiObject[0]->percent,
        'completion' => $completion,
        'enrollment' => $enrollment->created,
      ];
    } else {
      return ['status' => 'Pending', 'grade' => '0'];
    }
  }

  private function getCourseProgresses($courseId, $email)
  {
    $grades = array();
    $max = array();
    if (!empty($email)) {
      $user = EdxAuthUser::where('email', $email)->firstOrFail();
    } else {
      $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
    }

    $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
    $completion = StudentCourseCompletion::where(['student_id' => $user->id, 'module_type' => 'problem', 'course_id' => $courseId])->orderBy('id', 'DESC')->first();
    if ($enrollment) {

      $gradeApiObject = $enrollment->getGenCerts($user->id, $courseId);

      if ($gradeApiObject[0]->percent >= 0.80) {
        $gradeApiObject[0]->letter_grade = 'Completed';
        $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
      } elseif ($gradeApiObject[0]->percent <= 0.79 && $gradeApiObject[0]->percent >= 0.10) {

        $gradeApiObject[0]->letter_grade = 'Completed but Failed';
        $completion = date('Y-m-d H:i:s', strtotime($completion->modified));
      } else {

        $gradeApiObject[0]->letter_grade = 'In Progress';
        $completion = '';
      }
      return [
        'status' => $gradeApiObject[0]->letter_grade,
        'grade' => (float)$gradeApiObject[0]->percent,
        'completion' => $completion,
        'enrollment' => $enrollment->created,
      ];
    } else {
      return ['status' => 'Pending', 'grade' => '0'];
    }
  }





  public function getEvaluationStatus($courseid, $userid)
  {

    $evaluation  = UserEvaluation::where('user_id', '=', $userid)->where('course_id', '=', $courseid)->first();

    if (empty($evaluation)) {
    } else {

      return 1;
    }
  }



  public function getCommunityStatus($follow)
  {

    $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

    if (empty($status['status']) || $status['status'] == 0) {

      $status['status'] = 0;
    }

    return $status['status'];
  }


  public function getCommunityAction($follow)
  {
    $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

    if (empty($status['status']) || $status['status'] == 0) {
      $id = 0;
      return $id;
    } elseif ($status['status'] == 1 && $status['follow_id'] == Auth::user()->id) {
      $id = 1;
      return $id;
    } elseif ($status['status'] == 2) {
      $id = 2;
      return $id;
    }
  }


  public function getCommunityBlockAction($follow)
  {
    $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();
    if (!empty($status)) {
      if ($status['status'] == 2) {
        $id = 3;
        return route('profile.myFollow', [$follow, $id]);
      } elseif ($status['status'] == 3) {
        $id = 4;
        return route('profile.myFollow', [$follow, $id]);
      }
    }
  }


  public function getLikes($like, $courseid)
  {
    $likes = TrainingLike::where('like_id', $like)->where('course_id', $courseid)->get();

    if (empty($likes)) {
      return 0;
    }
    return count($likes);
  }

  public function getLikeAction($like, $courseid)
  {

    return route('profile.myLike', [$like, $courseid]);
  }

  public function getLikesUsers($like, $courseid)
  {
    $users = '';
    $likes = TrainingLike::where('like_id', $like)->where('course_id', $courseid)->get();
    if (!empty($likes)) {

      $users .= '<ul style="list-style:none;padding-left:0">';

      foreach ($likes as $like) {

        $users .= '<li>' . $like->user->first_name . ' ' . $like->user->last_name . '</li>';
      }
      $users .= '<ul>';

      return $users;
    }
    return '';
  }


  public function getFollowers($follow)
  {

    $followers = Mycommunity::where('follow_id', $follow)->get();

    if (empty($followers)) {

      return 0;
    }

    return count($followers);
  }


  public function getCommentCount($follow, $courseid)
  {

    $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $courseid)->whereNull('parent_id')->get();

    if (empty($comments)) {
      return 0;
    }
    return count($comments);
  }

  public function getComment($follow, $courseid)
  {

    $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $courseid)->whereNull('parent_id')->get();

    if (empty($comments)) {
      return 0;
    }

    return $comments;
  }

  public function getCommentAction()
  {
    return route('profile.myComment');
  }


  public function getSubCommentCount($follow, $courseid, $feed)
  {

    $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $courseid)->where('parent_id', $feed)->get();

    if (empty($comments)) {
      return 0;
    }
    return count($comments);
  }

  public function getSubComment($follow, $courseid, $feed)
  {

    $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $courseid)->where('parent_id', $feed)->get();

    if (empty($comments)) {
      return 0;
    }

    return $comments;
  }
}
