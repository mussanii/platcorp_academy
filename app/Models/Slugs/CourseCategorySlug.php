<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CourseCategorySlug extends Model
{
    protected $table = "course_category_slugs";
}
