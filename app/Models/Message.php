<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Message extends Model implements Sortable
{
    use HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'subject',
        'message',
        'email',
        'read_at',
        'company_id',
        'job_role_id',
        'message_type_id',
        'position',
        'target_id'
    ];
    protected $casts = [
        'target_id' => 'array',
        'email'=>'array',
        
    ];
    
}
