<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Division extends Model implements Sortable
{
    use HasPosition;

    protected $fillable = [
        'published',
        'title',
        'company_id',
        'position',
    ];
    

    public function company()
    {
        return $this->belongsTo(Company::class,'company_id','id');
    }
 
    public function scopePublished($query)
    {
        return $query->wherePublished(true)->orderBy('title');
    }

    public function scopeDraft($query)
    {
        return $query->wherePublished(false)->orderBy('title');
    }

    public function scopeOnlyTrashed($query)
    {
        return $query->whereNotNull('deleted_at')->orderBy('title');
    }
}
