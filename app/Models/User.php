<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use A17\Twill\Models\Enums\UserRole;
use Carbon\Carbon;
use Auth;
use App\Models\CourseCompletion;
use App\Models\JobroleCourse;

use A17\Twill\Models\User as TwillUser;

class User extends TwillUser
{

    const AUTHENTICATION_GOOGLE = 1;
    const AUTHENTICATION_MICROSOFT = 2;
    const AUTHENTICATION_USERCODE = 3;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'username',
        'is_activated',
        'last_login_at',
        'registered_at',
        'require_new_password',
        'company_id',
        'is_company_admin',
        'is_admin',
        'profile_pic',
        'first_name',
        'last_name',
        'branch_id',
        'job_role_id',
        'google_id',
        'microsoft_id',
        'published',
        'country_id',
        'company_initials',
        'user_number',
        'authentication_type',
        'gender_id',
        'completion_status',
        'employee_number',
        'division_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsTo(JobRole::class, 'job_role_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }


    public function branches()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function countries()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function getNameValueAttribute()
    {
        if (!empty($this->first_name)) {
            return $this->first_name . ' ' . $this->last_name;
        } else {
            return $this->name;
        }
    }

    public function getRoleValueAttribute()
    {
        if (!empty($this->role)) {
            if ($this->role == 'SUPERADMIN') {
                return "SUPERADMIN";
            }

            return UserRole::{$this->role}()->getValue();
        }

        return null;
    }

    public function getJobRoleValueAttribute()
    {
        if (empty($this->roles)) {
            return '';
        } else {
            return $this->roles->title;
        }
    }

    public function getDepartmentValueAttribute()
    {
        if (empty($this->division)) {
            return '';
        } else {
            return $this->division->title;
        }
    }

    public function getCountryValueAttribute()
    {
        if (empty($this->countries)) {
            return '';
        } else {
            return $this->countries->title;
        }
    }

    public function getBranchValueAttribute()
    {
        if (empty($this->branches)) {
            return '';
        } else {
            return $this->branches->title;
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }


    public function getCompulsoryAttribute()
    {

        $result = $this->getGraphProgress();
        return $result['compulsory'];
    }


    public function getCompletedAttribute()
    {

        $result = $this->getGraphProgress();
        return $result['completed'];
    }


    public function getDownloadAttribute()
    {

        $configApp = config()->get("settings.app");
        $result = $this->getGraphProgress();

        if ($result['download'] == 1 && ($result['total']) >= $configApp['PASSMARK']) {

            $this->completion_status = 1;
            $this->save();

            return 1;
        } else {

            return 0;
        }
    }


    private function getGraphProgress()
    {
        $compulsories = array();
        $complete = array();
        $scores = array();
        $max_score = array();

        $compulsory = JobroleCourse::where('job_role_id', '=', Auth::user()->job_role_id)->get();
        $completed = CourseCompletion::where('user_id', '=', Auth::user()->id)->get();

        foreach ($compulsory as $comp) {
            $compulsories[] = $comp->course_id;
        }

        foreach ($completed as $compl) {
            $complete[] = $compl->course_id;
        }

        foreach ($completed as $key => $l) {
            if (!in_array($l->course_id, $compulsories)) {
                unset($completed[$key]);
            } else {
                $scores[] = $l->score;
                $max_score[] = $l->max_score;
            }
        }

        $Found = array_diff($compulsories, $complete);
        if (count($Found) > 0) {
            $download = 0;
        } else {
            $download = 1;
        }


        $pass = 70;

        if (array_sum($max_score) == 0) {
            $total = 0;
        } else {
            $total = (array_sum($scores) / array_sum($max_score));
        }

        return [
            'compulsory' => $compulsory,
            'completed' => $completed,
            'pass' => $pass,
            'total' => $total,
            'download' => $download,


        ];
    }


    public function checkComplete()
    {
        $inComplete = false;
        $inComplete = $inComplete ?: is_null($this->division_id);
        $inComplete = $inComplete ?: is_null($this->employee_number);
        return !$inComplete;
    }
}
