<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Country;

class CountryTranslation extends Model
{
    protected $baseModuleModel = Country::class;
}
