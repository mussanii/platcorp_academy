<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Company;

class CompanyTranslation extends Model
{
    protected $baseModuleModel = Company::class;
}
