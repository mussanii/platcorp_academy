<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class AllowedAccount extends Model implements Sortable
{
    use HasPosition;

    protected $fillable = [
        'published',
        'title',
        'email',
        'company_id',
        'position',
    ];


    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
