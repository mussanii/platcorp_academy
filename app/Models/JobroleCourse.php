<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobroleCourse extends Model
{
    use HasFactory;

    protected $fillable = ['job_role_id', 'course_id'];


    public function courses(){
      return $this->belongsTo(Course::class,'course_id','id');
    }

    public function userCourses(){
      return $this->belongsTo(UserCourses::class,'job_role_id','job_role_id');
    }
}
