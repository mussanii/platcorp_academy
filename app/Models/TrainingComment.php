<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingComment extends Model
{
    use HasFactory;


    public function user()
    {
      return $this->belongsTo(User::class, 'user_id', 'id');
    }
    /**
     * Get the company that this user belongs to .
     */
    public function course()
    {
      return $this->belongsTo(Course::class, 'course_id', 'id');
    }
    

    public function follow()
    {
      return $this->belongsTo(User::class, 'comment_id', 'id');
    }

    public static function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('images/trainingFeed', $fileName);
            return $fileName;
        });
    }
}
