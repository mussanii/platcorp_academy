<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Branch extends Model implements Sortable
{
    use HasTranslation, HasSlug, HasMedias, HasFiles, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'country_id',
        'company_id'
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

    protected $presenterAdmin = 'App\Presenters\Front\BranchPresenter';

   
    public function scopePublished($query)
    {
        return $query->wherePublished(true)->orderByTranslation('title');
    }

    public function scopeDraft($query)
    {
        return $query->wherePublished(false)->orderByTranslation('title');
    }

    public function scopeOnlyTrashed($query)
    {
        return $query->whereNotNull('deleted_at')->orderByTranslation('title');
    }

    
   public function users(){

    return $this->hasMany(User::class);

   }

   public function country()
   {
       return $this->belongsTo(Country::class,'country_id','id');
   }

   public function company()
   {
       return $this->belongsTo(Company::class,'company_id','id');
   }


   public function  getRegisteredAttribute(){
    $users = array();

    foreach($this->users as $user){
       
        $users[] = $user->id;
    }

    return count($users);
   
   }



   public function  getCompletedAttribute(){
    
    $compulsories = array();
    $complete = array();
    $total = array();

    $users = $this->users;
    

    foreach( $users as $key => $user){
        $compulsory = JobroleCourse::where('job_role_id','=',$user->job_role_id)->get();
        $completed = CourseCompletion::where('user_id','=',$user->id)->get();
       
    
        if(count($completed) > 0){
            foreach($compulsory as $comp){
                $compulsories[] = $comp->course_id;
               }
            foreach($completed as $compl){
                 $complete[] = $compl->course_id;
        
            }

             if(count($compulsories) != count($complete)){
                unset($users[$key]);

             }else{

                $total[] += 1;
             }
                
        }else{

            unset($users[$key]);
        }
  
       
    }

 
    
    return count($total);
   
   }

}
