<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    const GROUP=1;
    const ACTIVITY=2;
    const MESSAGE=3;
    const FOLLOW=4;
    const NEWCOURSE=5;
    const SURVEY=6;
    const REQUEST=7;

    protected $table = 'notifications';

    protected $fillable = ['read_at'];

    protected $casts = [
    'data' => 'array',
   ];
}
