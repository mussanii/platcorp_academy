<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Carbon\Carbon;

class CourseRequest extends Model implements Sortable
{
    use HasPosition;

    const ACCESS_APPROVED = 1;
    const ACCESS_REQUESTED = 2;
 
    protected $fillable = [
        'published',
        'title',
        'user_id',
        'course_id',
        'status',
        'date_request',
        'date_approval',
        'published_by',
        'position',
    ];
    

    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


    public function userAdmin()
    {
        return $this->belongsTo(User::class, 'published_by', 'id');
    }



    public function getDateRequestAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    public function getDateApprovalAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

   
    public function getAccessStatusAttribute(){

      if($this->status == 2){

        return "Pending";
      }else{

        return "Approved";
      }


      }

      public function scopePublished($query)
      {
          return $query->wherePublished(true);
      }
  
      public function scopeDraft($query)
      {
          return $query->wherePublished(false);
      }
  
      public function scopeOnlyTrashed($query)
      {
          return $query->whereNotNull('deleted_at');
      }
}
