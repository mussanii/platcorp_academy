<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Carbon\CarbonInterval;
use Auth;
use App\Models\JobroleCourse;
use App\Models\UserLicense;
use App\Models\CourseCompletion;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Course extends Model {
    //
    

    protected $fillable = [
        'course_id',
        'name',
        'short_description',
        'overview',
        'more_info',
        'effort',
        'start',
        'end',
        'enrol_start',
        'enrol_end',
        'price',
        'course_image_uri',
        'course_video_uri',
        'course_category_id',
        'status',
        'slug',
        'order_id',
        'course_video',
        'published',
        'due_date',
        'course_type_id',
        'company_id',
        'short_name',
        'has_reflections',
       
    
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

   
    //Tell laravel more dates
    protected $dates = ['start', 'end', 'enrol_start', 'enrol_end','created_at','updated_at'];

    
    protected $appends = ['resource_url'];


    public function evaluations()
    {
        return $this->hasMany(Evaluation::class, 'id', 'course_id');
    }


   

    public function Category()
    {
        return $this->hasOne(CourseCategory::class,  'id' , 'course_category_id');
    }


    public function getResourceUrlAttribute()
    {
        return url('/admin/courses/'.$this->getKey());
    }

    public function registerMediaCollections() {
        $this->addMediaCollection('course_video');
    }
  

    public function getEffortSelectedAttribute()
    {
    $time = explode(":", $this->effort);
    if (sizeof($time) == 3) {
        $duration = CarbonInterval::hours($time[1]);
        $duration = $duration->minutes($time[2]);
        return $duration;

    }
  }


  public function getLinkSelectedAttribute()
    {
   
        return $this->short_name;
  }

  public function getEnrollmentAttribute(){
    

      /** use this when live */

    // if($this->course_id === 'course-v1:TowerSacco+TS101+2022_Q1'){
        
    //    $enrolled = Auth::check() ? $this->checkEnrollmentStatus($this->course_id) : false;

    //   }else{

       /** Local instance */
      $enrolled = Auth::check() ? $this->checkLicenseStatus($this->id) : false;
    //   }
    
    
    return $enrolled;

   
  }


  public function getCompletionAttribute(){
    
    $completed = Auth::check() ? $this->checkCompletionStatus($this->id) : false;

    return $completed['completionStatus'];


  }

  public function getRequiredAttribute(){
    // dd(Auth::user());
   $role = JobroleCourse::where('course_id', $this->id)->where('job_role_id', Auth::user()->job_role_id)->first();
    
   if(!empty($role)){

    return true;

   }else{

    return false;
   }
   
 }


  

  public function getMandatoryAttribute(){
    // dd(Auth::user());
   $role = JobroleCourse::where('course_id', $this->id)->where('job_role_id', Auth::user()->job_role_id)->first();
    
   if(!empty($role)){

    return true;

   }else{

    return false;
   }
   
 }

 public function getRecommendedAttribute(){
    // dd(Auth::user());
   $role = UserCourses::where('course_id', $this->id)->where('user_id', Auth::user()->id)->first();
    
   if(!empty($role)){

    return true;

   }else{

    return false;
   }
   
 }



 public static function getNotMandatory(){
    $compulsories = array();
    
    $role = JobroleCourse::where('job_role_id', Auth::user()->job_role_id)->get();

            if(Auth::user()->company_id == 3 || Auth::user()->company_id == 6){

            $all = Course::published()->where(function ($query){
                $query->where('course_type_id', 1)
                    ->orWhere('course_type_id', 2);
            })->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id);
            
            })->where('status','=',1)->orderBy('created_at','desc')->simplePaginate(12);
        
        }else{
            $all = Course::published()->where(function ($query){
                $query->where('course_type_id', 1)
                    ->orWhere('course_type_id', 2);
            })->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id)
                    ->orWhereNull('company_id');
            
            })->where('status','=',1)->orderBy('created_at','desc')->simplePaginate(12);
        


        }
      
    foreach($role as $comp){
        $compulsories[] = $comp->course_id;
    }
    foreach($all as $key => $l){
        if(in_array($l->id,$compulsories)){
                unset($all[$key]);
            }
    }
    
   
    return $all;
  }


  public static function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
      
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


 private function checkLicenseStatus($course_id){

    $enrollment = UserLicense::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();
    if ($enrollment) {
        $enrollmentStatus = true;
    } else {
        $enrollmentStatus = false;
    }

    return $enrollmentStatus;

}



private function checkCompletionStatus($course_id){

    $completion = CourseCompletion::where('course_id', $course_id)->where('user_id', Auth::user()->id)->first();

    if($completion){
     
        if($completion->score >= 0.80){

            $completionStatus = 1;
            $status = "Completed";

        }else{

            $completionStatus = 2;
            $status = 'Completed but failed';
        }
    }else{
        $completionStatus = false;
        $status = 'In Progress';
    }
    
    return [
    'status' => $status,
    'completionStatus'=>$completionStatus
     ];

}



public function getCourseStatus(){

    $access = CourseRequest::where('course_id', $this->id)->where('user_id', Auth::user()->id)->first();

    if($access){
        return $access->status;

    }else{

        return 0;
    }
    
    

}

 private function checkEnrollmentStatus($course_id){
    $configLms = config()->get("settings.lms.live");
    //dd(isset($_COOKIE['edinstancexid']));
    if (!isset($_COOKIE['edinstancexid'])) {
        Auth::logout();
        return false;
    }
    $client = new \GuzzleHttp\Client(
        [
            'verify' => env('VERIFY_SSL', true),
            'headers' => [
                'Authorization' => 'Bearer ' . $_COOKIE['edinstancexid']
            ]
        ]
    );
    $request = $client->request('GET', $configLms['LMS_BASE'] . '/api/enrollment/v1/enrollment/' . Auth::user()->username . ',' . $course_id);

    $response = json_decode($request->getBody()->getContents());
    //dd($response);
    if ($response && $response->is_active == true) {
        $enrollmentStatus = true;
    } else {
        $enrollmentStatus = false;
    }
    //dd($enrollmentStatus);
    return $enrollmentStatus;

}


}
