<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class MenuTypeRevision extends Revision
{
    protected $table = "menu_type_revisions";
}
