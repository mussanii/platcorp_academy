<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CompanyRevision extends Revision
{
    protected $table = "company_revisions";
}
