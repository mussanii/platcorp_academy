<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CourseCategoryRevision extends Revision
{
    protected $table = "course_category_revisions";
}
