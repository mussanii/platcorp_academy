<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class MessageRevision extends Revision
{
    protected $table = "message_revisions";
}
