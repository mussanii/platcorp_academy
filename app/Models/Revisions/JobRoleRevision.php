<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class JobRoleRevision extends Revision
{
    protected $table = "job_role_revisions";
}
