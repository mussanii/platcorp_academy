<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use Leafo\ScssPhp\Compiler;
use Illuminate\Support\Facades\Storage;

class Company extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;



    protected $fillable = [
        'published',
        'title',
        'description',
        'contact_first_name',
        'contact_last_name',
        'contact_email',
        'primary_color',
        'secondary_color',
        'tachiary_color',
        'position',
        'company_code',
        'primary_text_color',
        'secondary_text_color',
        'primary_heading_color',
        'secondary_heading_color',
        'menu_color',
        'light_background_color',
        'dark_background_color',
        'additional_color',
        'button_primary_color',
        'button_secondary_color',
        'button_tachiary_color',
        'menu_color_active',
        'footer_color',
        'learning_statement',
        'our_mission',
        'our_vission',

    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];

    public $filesParams = ['slide_show'];

    protected $with = ['translations', 'medias','files'];
    
    public $mediasParams = [
        'logo' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],




        'courses_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'live_sessions_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'our_progress_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'enrolled_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'community_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'training_feed' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'myprogress_banner' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],

        'mandatory_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'hr_recommended_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'clock_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'calendar_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'tick_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'user_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],

        'notification_bell_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],

        'rocket_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'not_finished_trophy_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'finished_trophy_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'like_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        'comment_svg' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
    
        ],
        
    ];


    public function domains(){
        return $this->hasMany(Domain::class)->orderBy('position');
    }


    public function values(){
        return $this->hasMany(OurValue::class)->orderBy('position');
    }




    // ...

    public function compileTheme()
    {
        $scss = new Compiler();
        $scss->addImportPath(realpath(app()->path() . '/../resources/sass'));
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');

        $logo = $this->image("logo", "flexible");

        $courses_banner = $this->image("courses_banner", "flexible");
        
        $live_sessions_banner= $this->image("live_sessions_banner", "flexible");

        $training_calendar_banner= $this->image("training_calendar_banner", "flexible");
        
        $our_progress_banner= $this->image("our_progress_banner", "flexible");
        
        $enrolled_banner= $this->image("enrolled_banner", "flexible");
        
        $community_banner= $this->image("community_banner", "flexible");
        
        $training_feed= $this->image("training_feed", "flexible");
        
        $myprogress_banner= $this->image("myprogress_banner", "flexible");

        $mandatory_svg= $this->image("mandatory_svg", "default");

        $hr_recommended_svg= $this->image("hr_recommended_svg", "default");

        $clock_svg= $this->image("clock_svg", "default");

        $calendar_svg= $this->image("calendar_svg", "default");

        $tick_svg= $this->image("tick_svg", "default");

        $user_svg= $this->image("user_svg", "default");

        $notification_bell_svg = $this->image("notification_bell_svg", "default");

        $rocket_svg = $this->image("rocket_svg", "default");

        $not_finished_trophy_svg = $this->image("not_finished_trophy_svg", "default");

        $finished_trophy_svg = $this->image("finished_trophy_svg", "default");

        $like_svg = $this->image("like_svg", "default");

        $comment_svg = $this->image("comment_svg", "default");
        

        $output = $scss->compile('
        @import "variables";
        
        $primary_color: '.$this->primary_color.';
        $secondary_color: '.$this->secondary_color.';
        $tertiary_color: '.$this->tachiary_color.';
        $primary_text_color:'.$this->primary_text_color.';
        $secondary_text_color: '.$this->secondary_text_color.';
        $primary_heading_color:'.$this->primary_heading_color.';
        $secondary_heading_color:'.$this->secondary_heading_color.';
        $menu_color:'.$this->menu_color.';
        $light_background_color:'.$this->light_background_color.';
        $dark_background_color:'.$this->dark_background_color.';
        $additional_color:'.$this->additional_color.';
        $button_primary_color:'.$this->button_primary_color.';
        $button_secondary_color:'.$this->button_secondary_color.';
        $button_tachiary_color:'.$this->button_tachiary_color.';
        $menu_color_active:'.$this->menu_color_active.';
        $footer_color:'.$this->footer_color.';

        $primary_logo: "'.$logo.'";

        $courses_banner: "'.$courses_banner.'";
        $live_sessions_banner: "'.$live_sessions_banner.'";
        $training_calendar_banner: "'.$training_calendar_banner.'";
        $our_progress_banner: "'.$our_progress_banner.'";
        $enrolled_banner: "'.$enrolled_banner.'";

        $community_banner: "'.$community_banner.'";
        $training_feed: "'.$training_feed.'";
        $myprogress_banner: "'.$myprogress_banner.'";
        $mandatory_svg: "'.$mandatory_svg.'";
        $hr_recommended_svg: "'.$hr_recommended_svg.'";
        $clock_svg: "'.$clock_svg.'";
        $calendar_svg: "'.$calendar_svg.'";
        $tick_svg: "'.$tick_svg.'";
        $user_svg: "'.$user_svg.'";
        $rocket_svg : "'.$rocket_svg.'";
        $not_finished_trophy_svg : "'.$not_finished_trophy_svg.'";
        $finished_trophy_svg : "'.$finished_trophy_svg.'";
        $like_svg : "'.$like_svg.'";
        $comment_svg : "'.$comment_svg.'";
        $notification_bell_svg : "'.$notification_bell_svg.'";

      
        @import "components/company_specific";
       
        ');

        Storage::disk('public')->put("css/$this->id/app.css", $output);
    }


    public function users(){

        return $this->hasMany(User::class);
    
       }
    

    public function  getRegisteredAttribute(){
        $users = array();
    
        foreach($this->users as $user){
           
            $users[] = $user->id;
        }
    
        return count($users);
       
       }
    
    
    
       public function  getCompletedAttribute(){

        $users = User::where('company_id',$this->id)->where('completion_status',1)->get();
        
        return count($users);
       
       }


       public function  getPercentageAttribute(){

          if($this->registered > 0){
           
            if($this->completed > 0){

                $pre = ($this->completed / $this->registered) * 100;

                return $pre.'%';
                
            }else{

                return 0;
            }
          

          }else{
           
            return 0;

          }
       }



   
}
