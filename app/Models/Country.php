<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use PragmaRX\Countries\Package\Countries;

class Country extends Model implements Sortable
{
    use HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'code',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];


    public static function mapData()
    {
        $pc = array();

        $countries = Country::published()->get();
       

        foreach ($countries as $c) {

            $pc[] = array(strtolower($c['code']) => '');
        }
        
        

        foreach ($pc as $key => $value) {
            foreach ($value as $ke => $val) {

                foreach ($countries as $keys => $country) {
                    if ($ke == strtolower($country['code'])) {

                        $users = User::where('country_id', '=', $country->id)->where('published', '=', 1)->get();

                        $countryFlag = Countries::where('postal', $country['code'])->first();

                        if (!empty($countryFlag->flag)) {
                            $country_flag = $countryFlag->flag['svg_path'];
                        } else {
                            $country_flag = "";
                        }

                        $flag = substr($country_flag, strpos($country_flag, "flags") + 6);
                        $name = $countryFlag->name['common'];

                        $certified_users = self::getCertified($country->id);

                      


                        $pc[$key] = ['code' => $ke, 'common_name' => $name, 'users' => count($users), 'country_flag' => $flag, 'certified' => $certified_users];
                    }
                }
            }
        }
        return $pc;
    }



    public static function getCertified($country)
    {
        $compulsories = array();
        $complete = array();
        $certified = array();

        $Certs = User::where('country_id', '=', $country)->where('published', '=', 1)->get();
        foreach ($Certs as $k => $cert) {
            $compulsory = JobroleCourse::where('job_role_id', '=', $cert->job_role_id)->get();
            $completed = CourseCompletion::where('user_id', '=', $cert->id)->where('country_id', '=', $country)->get();

            if (count($completed) > 0) {
                foreach ($compulsory as $comp) {
                    $compulsories[] = $comp->course_id;
                }
                foreach ($completed as $compl) {
                    $complete[] = $compl->course_id;
                }

                if (count($compulsories) != count($complete)) {
                    unset($Certs[$k]);
                } else {

                    $certified[] += 1;
                }
            } else {

                unset($Certs[$k]);
            }
        }

        return count($certified);
    }
}
