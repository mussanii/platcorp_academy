<?php

namespace App\Presenters\Front;


class ClientPresenter extends BasePresenter
{
	public function image()
    {
        return $this->imageMedia('cover');
	}

	public function description()
    {
        return $this->entity->description;
	}

	public function url()
    {
        return "#";
    }
}
