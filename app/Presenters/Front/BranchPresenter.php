<?php

namespace App\Presenters\Front;


class BranchPresenter extends BasePresenter
{
	
    public function created_at()
    {
        return $this->entity->created_at != null ? $this->entity->created_at->formatLocalized("%B %e, %Y %I:%M:%S %p") : "";
	}
}
