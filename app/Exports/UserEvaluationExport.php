<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;


class UserEvaluationExport implements FromView, WithHeadings, WithEvents
{

    use Exportable;
    protected $data;
    protected $topic;
    
 
    public function __construct($data,$topic){
 
     $this->data = $data;
     $this->topic = $topic;
     $this->headings = ['User Name','Course','Question One','Question Two','Question Three','Question Four','Question Five','Question Six','Question Seven','Question Eight','Question Nine','Question Ten'];
     }
 
     public function view(): View
     {
        
         return view('admin.exports.user_evaluation', [
             'data' => $this->data,
             'topic' => $this->topic, 
             'headings' => $this->headings
             
         ]);
     }
      public function headings(): array
      {
        return [
            ['First row', 'Name'],
            ['Second row', '#','User Name','Course','Question One','Question Two','Question Three','Question Four','Question Five','Question Six','Question Seven','Question Eight','Question Nine','Question Ten'],

        ];
      }

      public function registerEvents(): array
      {
     return [
         AfterSheet::class    => function(AfterSheet $event) {
             // ... HERE YOU CAN DO ANY FORMATTING
              $cellRange = 'A2:E2';
             $event->sheet->getDelegate()->mergeCells($cellRange);
 
             $event->sheet->getColumnDimension('A')->setWidth(13);
             $event->sheet->getColumnDimension('B')->setWidth(13);
         },
     ];
   }
}
