
<?php

return [
    'app' => [
        'APP_DOMAIN' => env('APP_DOMAIN'),
        'VERIFY_SSL' => true,
        'PASSMARK' => 0.6,
    ],

    'lms' => [
        'status' => 'test',
        'live' => [
            'EDX_KEY' => 'fcb1e8c022cb73459b7f',
            'EDX_SECRET' => '7a12e70d500d6352d687eeb94ef5f5c09a5ca7c8',
            'EDX_CONNECT' => true,
            'COOKIE_DOMAIN' => 'courses.platcorpgroup.com',
            'LMS_DOMAIN' => 'https://courses.platcorpgroup.com',
            'LMS_BASE' => 'https://courses.platcorpgroup.com',
            'CMS_BASE' => 'https://studio.platcorpgroup.com',
            'LMS_REGISTRATION_URL' => 'https://courses.platcorpgroup.com/user_api/v1/account/registration/',
            'LMS_LOGIN_URL' => 'https://courses.platcorpgroup.com/user_api/v1/account/login_session/',
            'LMS_RESET_PASSWORD_PAGE' => 'https://courses.platcorpgroup.com/user_api/v1/account/password_reset/',
            'LMS_RESET_PASSWORD_API_URL' => 'https://courses.platcorpgroup.com/user_api/v1/account/password_reset/',
            'edxWebServiceApiToken' => 'c90685ce8bc75a8e03ad35deb28a5fade4a4cc87',
            'default_token' => '133ad7f0ecd269b63637a522dbb529c50475a493'
        ],
        
    ],

    'fix_page_css' => [
        'case-study-index'
    ],
    'zoom' =>[
       'ZOOM_API_URL'=>"https://api.zoom.us/v2/",
       'ZOOM_API_KEY'=>"4sphFe7MQgC6GFAT4b_jlA",
       'ZOOM_API_SECRET'=>"98TJ4l8jIynRBs89nFkQo9uhwz6CSKE7rNDl",  

    ],



];
