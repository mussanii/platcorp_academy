<?php

use App\Http\Controllers\Admin\SessionController;
use App\Repositories\SessionRepository;
use Illuminate\Support\Facades\Route;

// Register Twill routes here eg.
// Route::module('posts');
if (config('twill.enabled.users-management')) {

    Route::module('users', ['except' => ['sort', 'feature']]);
    Route::module('roles', ['except' => ['sort', 'feature']]);
    Route::get('/usercode', 'UserController@userCodeForm')->name('userCodeForm');
    Route::post('/usercode', 'UserController@userCodeStore')->name('userCodeStore');
}


Route::group(['prefix' => 'content'], function () {
    Route::module('pages');
    Route::group(['prefix' => 'menus'], function () {
        Route::module('menus');
        Route::module('menuTypes');
    });
});

Route::module('companies');
Route::module('jobRoles');
Route::module('branches');
Route::module('countries');
Route::module('sessions');
Route::module('userCourses');
Route::module('courseCategories');
Route::module('divisions');

Route::group(['prefix' => 'evaluation'], function () {
    Route::module('userEvaluations');
    Route::module('evaluationOptions');
    Route::module('evaluations');
});

Route::group(['prefix' => 'reports'], function () {
    Route::group(['prefix' => 'graph'], function () {
        Route::name('graph.dashboard')->get('dashboard', 'CourseCompletionController@dashboard');
        Route::name('graph.eresults')->get('eresults', 'CourseCompletionController@eresults');
        Route::name('graph.cresults')->get('cresults', 'CourseCompletionController@cresults');


        Route::name('graph.yearlyEResults')->get('yearlyEResults', 'CourseCompletionController@yearlyEResults');
        Route::name('graph.yearlyCRresults')->get('yearlyCRresults', 'CourseCompletionController@yearlyCRresults');
    });
    Route::group(['prefix' => 'branch'], function () {
        Route::name('branch.completion')->any('branchCompletion', 'CourseCompletionController@branchCompletion');
    });
    Route::group(['prefix' => 'subsidiary'], function () {
        Route::name('subsidiary.completion')->any('subsidiaryCompletion', 'CourseCompletionController@subsidiaryCompletion');
    });

    Route::module('courseCompletions');
    Route::module('courseReflections');
    Route::module('userLicenses');
});

Route::group(['prefix' => 'companyReports'], function () {
    Route::group(['prefix' => 'graph'], function () {
        Route::name('companygraph.dashboard')->get('companyDashboard', 'CourseCompletionController@companyDashboard');
        Route::name('companygraph.eresults')->get('companyEresults', 'CourseCompletionController@companyEresults');
        Route::name('companygraph.cresults')->get('companyCresults', 'CourseCompletionController@companyCresults');


        Route::name('companygraph.companyyearlyEResults')->get('companyyearlyEResults', 'CourseCompletionController@companyyearlyEResults');
        Route::name('companygraph.companyyearlyCRresults')->get('companyyearlyCRresults', 'CourseCompletionController@companyyearlyCRresults');
    });

    Route::name('companyReport.completion')->any('companyReportCompletion', 'CourseCompletionController@companyReportCompletion');
});

Route::group(['prefix' => 'departmentReports'], function () {

    Route::group(['prefix' => 'department'], function () {
        Route::name('departmentReport.completion')->any('departmentReportCompletion', 'CourseCompletionController@departmentReportCompletion');
    });
});

Route::name('branchReport.completion')->any('branchReportCompletion', 'CourseCompletionController@branchReportCompletion');


Route::group(['prefix' => 'messages'], function () {
    Route::module('messages');
    Route::module('messageTypes');
});


Route::group(['prefix' => 'survey'], function () {
    Route::module('userSurveys');
    Route::module('surverys');

    Route::any('userSurvey', 'UserSurveyController@userSurvey')->name('userSurvey.responses');
});

Route::module('courseRequests');

Route::group(['prefix' => 'accounts'], function () {
    Route::module('allowedAccounts');
    Route::get('/upload', 'AllowedAccountController@usersUpload')->name('licenseUpload');
    Route::post('/upload', 'AllowedAccountController@uploadStore')->name('upload.store');
});

Route::get('/courses', 'CoursesController@index')->name('courses.index');
Route::get('/courses/create', 'CoursesController@create')->name('courses.create');
Route::post('/courses', 'CoursesController@store')->name('courses.store');
Route::get('/courses/{course}/edit', 'CoursesController@edit')->name('courses.edit');
Route::post('/courses/bulk-destroy', 'CoursesController@bulkDestroy')->name('courses.bulk-destroy');
Route::post('/courses/{course}', 'CoursesController@update')->name('courses.update');
Route::post('/courses/{course}/delete', 'CoursesController@destroy')->name('courses.destroy');
Route::get('/courses/sync/table', 'CoursesController@sync')->name('courses.sync');

Route::post('userEvaluation/export', 'UserEvaluationController@export')->name('userEvaluation.export');
Route::post('courseCompletion/export', 'CourseCompletionController@export')->name('courseCompletion.export');

Route::get('userLicenses/export', 'UserLicenseController@export')->name('userLicenses.export');

Route::post('users/export', 'UserController@export')->name('user.export');

Route::get('/oauth', [SessionController::class, 'oauth'])->name('oauthCallbackk');

Route::get('/getInfo/{id}', 'CourseCompletionController@companyInfo')->name('company.info');
Route::get('/getDivision/{id}', 'JobRoleController@divisionInfo')->name('division.info');
Route::get('/getUserDivision/{id}', 'UserController@userDivisionInfo')->name('userDivision.info');