<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\PagesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\MicrosoftController;
use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\Front\UserController;
use App\Http\Controllers\Front\CourseController;
use App\Http\Controllers\Front\MeetingController;
use App\Http\Controllers\Front\SessionsController;
use App\Http\Controllers\Front\UserLicenseController;
use App\Http\Controllers\Front\NotificationController;
use App\Http\Controllers\Front\SurveyController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'index'])->name('home');
Route::any('/page/{key}', [PagesController::class, 'noneAuthPages'])->name('noneAuthPages');



Route::get('/user/login', [LoginController::class, 'showLoginOptions'])->name('user.login');
Route::post('/user/login', [LoginController::class, 'login'])->name('user.login.post');
Route::get('/login/userCode', [LoginController::class, 'userCodeLogin'])->name('userCode.login');

Route::any('/sso/option', [LoginController::class, 'ssoOption'])->name('sso.option');

Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);


Route::get('auth/microsoft', [MicrosoftController::class, 'redirectToProvider'])->name('auth.micorsoft');
Route::get('user/callback', [MicrosoftController::class, 'handleProviderCallback'])->name('user.callback');

Auth::routes(['verify' => true]);
Route::any('/{key}', [PagesController::class, 'pages'])->name('pages');


Route::get('getBranch/{id}', [UserController::class, 'getBranch'])->name('get.branch');
Route::any('/update/profile/{id}', [UserController::class, 'completeProfile'])->name('update.profile');
Route::any('/update/division/{id}', [UserController::class, 'completeDivision'])->name('update.division');
Route::post('/meeting/link', [MeetingController::class,'generateMicrosoftLink'])->name('generate.link');
Route::post('/google-meeting/link', [MeetingController::class,'generateGoogleLink'])->name('generate.links');

Route::group(['prefix' => 'profile', 'middleware' => ['auth', 'verified']], function () {

    Route::any('/request/access/{id}', [PagesController::class, 'requestCourseAccess'])->name('request.access');

    /** Sessions */
    Route::get('/session-details/{id}', [PagesController::class, 'sessionDetail'])->name('session.details');
    Route::get('/session-calendar/{id}', [SessionsController::class, 'addToCalendar'])->name('session.calendar');
    Route::get('/oauth', [SessionsController::class, 'oauth'])->name('oauthCallback');

    /** Course details */
    Route::get('/course/detail/{key}', [CourseController::class, 'detail'])->name('course.detail');
    Route::get('/courses/enroll/{id}', [CourseController::class, 'enroll'])->name('course.enroll');


    /** our progress */
    Route::get('/our-progress/countries', [PagesController::class, 'progressCountries'])->name('progress.countries');

    /** Edit Profile */
    Route::any('/edit', [UserController::class, 'profile'])->name('profile.edit');
    Route::any('/ppic/change', [UserController::class, 'picture'])->name('profile.ppic.change');

    /** My Enrolled Modules */
    Route::any('/myProfile', [UserLicenseController::class, 'profile'])->name('profile.courses');
    Route::any('/myReflections/{id}', [UserLicenseController::class, 'myReflections'])->name('profile.reflections');

    /** My Community  */
    Route::any('/myCommunity', [UserLicenseController::class, 'community'])->name('profile.myCommunity');
    Route::post('/myCommunity/follow', [UserLicenseController::class, 'myFollow'])->name('profile.myFollow');
    Route::any('/myCommunity/accept', [UserLicenseController::class, 'myFollowAccept'])->name('profile.myFollowAccept');

    /** Training Feed */
    Route::any('/trainingFeed', [UserLicenseController::class, 'trainingFeed'])->name('profile.trainingFeed');
    Route::any('/like/{follow}/{id}/', [UserLicenseController::class, 'myLike'])->name('profile.myLike');
    Route::post('/comment', [UserLicenseController::class, 'myComment'])->name('profile.myComment');
    Route::get('/comments/{follow}/{course}/', [UserLicenseController::class, 'myComments'])->name('profile.myComments');
    Route::get('/subCommentCount/{follow}/{course}/{id}/', [UserLicenseController::class, 'subCommentCount'])->name('profile.subCommentCount');
    Route::get('/subComment/{follow}/{course}/{id}/', [UserLicenseController::class, 'subComment'])->name('profile.subComment');

    /** My Course Progress */
    Route::any('/myProgress', [UserLicenseController::class, 'myProgress'])->name('profile.myProgress');

    /** Course Evaluation */
    Route::post('/course/evaluate/store', [UserLicenseController::class, 'Evaluationstore'])->name('course.evaluate.store');
    Route::any('/course/evaluate/{courseid}', [UserLicenseController::class, 'evaluate'])->name('course.evaluate');


    Route::post('/user/survey/store', [SurveyController::class, 'surveyStore'])->name('profile.survey.store');
    Route::any('/user/survey/', [SurveyController::class, 'survey'])->name('profile.survey');

    /** Certificate */
    Route::any('/course/badge/{id}', [UserLicenseController::class, 'badge'])->name('course.badge');

    Route::any('/download/certificate', [UserLicenseController::class, 'certificate'])->name('download.certificate');
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::get('/single/notification/{id}', [NotificationController::class, 'singleNotification'])->name('single.notification');

    Route::get('/user/logout', [LoginController::class, 'userLogout'])->name('user.logout');


    Route::get('/sync/table', [UserLicenseController::class, 'sync'])->name('profile.sync');
    Route::get('/sync/license', [UserLicenseController::class, 'syncL'])->name('profile.syncL');
});
